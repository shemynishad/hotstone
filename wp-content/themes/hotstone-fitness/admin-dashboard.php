<?php /* Template name: Admin Dashboard */
if (is_user_logged_in()) {
  if (current_user_can('administrator') || current_user_can('front_office_user')) {
    $current_user = wp_get_current_user();
  } elseif (current_user_can('customer')) {
    wp_redirect(get_home_url('', '/customer-dashboard/'));
  } elseif (current_user_can('trainer')) {
    wp_redirect(get_home_url('', '/trainer-dashboard/'));
  } else {
    wp_redirect(get_home_url());
  }
} else {
  wp_redirect(get_home_url());
}
get_header(); ?>

<div id="content">
  <!-- section -->
  <div class="section dashboard">
    <div class="container">
      <div class="row align-items-center flex-row-reverse mb-30 mb-md-5">
        <div class="col-md-6 mb-4 mb-md-0">
          <ul class="breadcrumb justify-content-end mb-0">
            <li><a href="#">Dashboard</a></li>
            <li>Home</li>
          </ul>
        </div>
        <div class="col-md-6">
          <p class="welcome-title">Hello, Welcome <?= $current_user->display_name; ?>.</p>
        </div>
      </div>
      <?php $result = count_users();
      ?>
      <div class="row row2">
        <div class="col-lg-4 col-md-6 mb-30 mb-lg-0">
          <div class="box1">
            <div class="d-flex stat">
              <i class="icon"><img src="<?= get_template_directory_uri(); ?>/assets/images/icon1.svg" alt="icon"></i>
              <div class="text">
                <p>Total Users</p>
                <p class="value"><?= sprintf("%02d", $result['avail_roles']['front_office_user']); ?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 mb-30 mb-lg-0">
          <div class="box1">
            <div class="d-flex stat">
              <i class="icon"><img src="<?= get_template_directory_uri(); ?>/assets/images/icon2.svg" alt="icon"></i>
              <div class="text">
                <p>Total Customers</p>
                <p class="value"><?= sprintf("%02d", $result['avail_roles']['customer']); ?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 mb-30 mb-lg-0">
          <div class="box1">
            <div class="d-flex stat">
              <i class="icon"><img src="<?= get_template_directory_uri(); ?>/assets/images/icon3.svg" alt="icon"></i>
              <div class="text">
                <p>Total Trainers</p>
                <p class="value"><?= sprintf("%02d", $result['avail_roles']['trainer']); ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /section -->
</div>



<?php get_footer(); ?>