<?php /* Template name: Login */
if ( is_user_logged_in() ) {
        wp_redirect(get_home_url('','/admin-dashboard/'));     
}
get_header();?>


<div id="content">
        <!-- section -->
        <div class="section">
          <div class="container py-xl-5">
            <div class="col-xxl-6 col-md-8 mx-auto px-xxl-4">
              <div class="box1">
                <h3 class="text-uppercase text-color1 mb-3 mb-md-4">Sign In</h3>
                <p class="mb-4 mb-md-5">Enter your email and password to sign in</p>
                <div class="form1 mb-30 mb-md-5">
                  <?= form_login();?>
                </div>
                <!--<p>Don’t have an account? <a href="#" class="link">Sign up</a></p> -->
              </div>
            </div>
          </div>
        </div>
        <!-- /section -->
      </div>

<?php get_footer();?>
<script>
     jQuery(window).on('load', function(){ 
<?php if(isset($_REQUEST['login'])){?>
<?php if($_REQUEST['login']=="failed"){?>
    jQuery("#loginerror").css("display", "block");
    jQuery("#loginerror").html("<?php _e('Invalid username or password','sidf');?>");
    <?php } ?>
    <?php if($_REQUEST['login']=="disabled"){?>
    jQuery("#loginerror").css("display", "block");
    jQuery("#loginerror").html("You account have been disabled");
    <?php } ?>
    <?php } ?>
        jQuery('#login-form').validate({
        onfocusout: function(element) {$(element).valid()},
        onkeyup: false,
        rules:
        {
            log:
            {

                required: true



            },
            pwd:
            {

                required: true

            }
        }

    });
});
    
$(document).on('click', '.show-hide-pass', function() {
    if ($(this).prev().attr('type') == 'password') {
        $(this).prev().attr('type', 'text');
        $(this).html('<img src="<?=get_template_directory_uri();?>/assets/images/show-password-eye.svg" alt="icon" class="show-password">');
    } else {
        $(this).prev().attr('type', 'password');
        $(this).html('<img src="<?=get_template_directory_uri();?>/assets/images/hide-password-eye.svg" alt="icon" class="hide-password">');
        
    }
});
</script>

