<?php
if ( is_user_logged_in() ) {
    if (current_user_can('administrator') || current_user_can('front_office_user') || current_user_can('trainer')) {
        $current_user = wp_get_current_user();
    } else {
        wp_redirect(get_home_url());     
    }
}else{
wp_redirect(get_home_url()); 
} 
if(current_user_can('administrator') || current_user_can('front_office_user')):
get_header();?>

<div id="content">
        <!-- section -->
        <div class="section dashboard">
            <div id="loader"></div>
          <div class="container">
            <div class="row align-items-center flex-row-reverse mb-30 mb-md-5">
              <div class="col-md-6 mb-4 mb-md-0">
                <ul class="breadcrumb justify-content-end mb-0">
                  <li><a href="#">Dashboard</a></li>
                  <li><a href="#">Class</a></li>
                  <li>Schedule</li>
                </ul>
              </div>
              <div class="col-md-6">
                <p class="welcome-title">Hello, Welcome <?= $current_user->display_name;?>.</p>
              </div>
            </div>
            <div class="text-center text-lg-end mb-4">
              <a class="btn button1 btn-sm" href="<?= home_url('admin-class');?>">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.999 23.003" class="me-2">
                  <path id="Path_35" data-name="Path 35" d="M16.515,21.525a.709.709,0,0,0,.135.013.721.721,0,0,0,.709-.585,3.918,3.918,0,0,0,.073-.74,4.337,4.337,0,0,0-2.63-3.889,3.158,3.158,0,0,0,1.087-2.38,3.251,3.251,0,0,0-6.5,0,3.16,3.16,0,0,0,1.105,2.394,4.331,4.331,0,0,0-2.6,3.874,4.027,4.027,0,0,0,.072.737.723.723,0,1,0,1.422-.261,2.621,2.621,0,0,1-.049-.476A3.143,3.143,0,0,1,12.664,17.3a3.142,3.142,0,0,1,3.324,2.917,2.528,2.528,0,0,1-.048.471.717.717,0,0,0,.57.841Zm-3.876-9.338a1.757,1.757,0,1,1-1.8,1.756,1.784,1.784,0,0,1,1.8-1.756Z" transform="translate(-4.334 -6.458)" fill="#fff"/>
                  <path id="Path_36" data-name="Path 36" d="M29.611,12.2a.72.72,0,0,0,.722-.718h0V9.328a2.883,2.883,0,0,0-2.889-2.87H7.222a2.882,2.882,0,0,0-2.888,2.87V22.96a2.882,2.882,0,0,0,2.888,2.87h5.563a.73.73,0,0,1,.511.211l3.016,3a1.449,1.449,0,0,0,2.041,0l3.017-3a.73.73,0,0,1,.511-.211h5.562a2.882,2.882,0,0,0,2.89-2.87V15.067a.722.722,0,1,0-1.445,0V22.96a1.442,1.442,0,0,1-1.444,1.44H21.882a2.16,2.16,0,0,0-1.532.628l-3.017,3-3.017-3a2.159,2.159,0,0,0-1.532-.63H7.222a1.442,1.442,0,0,1-1.444-1.435V9.328A1.442,1.442,0,0,1,7.222,7.893H27.444a1.442,1.442,0,0,1,1.444,1.435V11.48a.72.72,0,0,0,.722.718Z" transform="translate(-4.334 -6.458)" fill="#fff"/>
                  <path id="Path_37" data-name="Path 37" d="M22.388,19.372a.719.719,0,0,0,.722-.716h0V16.5h2.167a.718.718,0,1,0,0-1.435H23.111v-2.15a.723.723,0,0,0-1.444,0v2.152H19.5a.718.718,0,1,0,0,1.435h2.167v2.152a.719.719,0,0,0,.722.717Z" transform="translate(-4.334 -6.458)" fill="#fff"/>
                </svg>
                Back         
              </a>
            </div>
                  <h3>Class Details</h3>
            <div class="table1 mb-4">
              <table class="table mb-0">
                <thead>
                  <tr>
                    <td>ID</td>
                    <td>Date</td>     
                    <td>Time(Start-End)</td>
                    <td>Duration</td>
                    <td>Trainer</td>
                    <td>Workouts</td>
                    <td>Class</td>
                    <td>Type</td>
                    <td>Bookings</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?= get_field('id');?></td>
                    <?php $date = get_field('date');?>
                    <td><?= date("d F Y", strtotime($date));?></td>
                    <td><?= get_field('start_time').'-'.get_field('end_time');?></td>
                    <?php $to_time = strtotime("2008-12-13 ".get_field('end_time').":00");
                        $from_time = strtotime("2008-12-13 ".get_field('start_time').":00");
                        $hours = round(abs($to_time - $from_time) / 3600,2);?>
                    <td><?= $hours.' hours';?></td>
                    <?php $trainer_id = get_field('trainer');
                    $author_obj = get_user_by('id', $trainer_id);?>
                    <td><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></td>
                    <?php $workout_titles = array();
                    $workouts = explode(",",get_field('workouts'));
                    foreach($workouts as $workout):
                        $workout_titles[] = get_the_title($workout);
                    endforeach;?>
                    <td><?= implode(",",$workout_titles);?></td>
                    <td><?= get_field('class_title');?></td>
                    <td><?= get_field('name',get_field('membership'));?></td>
                    <?php 
                    $class_array = array(
                           'key'     => 'class',
                           'value'   => $post->ID,
                           'compare' => '='
                           );
     
                  $args = array(
                    	'post_type' => 'booking',
                    	'numberposts' => -1,
                    	'meta_query' => array(
                            $class_array
                        )
                        
                    );
                    $latest_class = get_posts( $args );
                    ?>
                    <td><?= count($latest_class);?>/<?= get_field('no_of_slots');?></td>
                    
                  </tr>
                </tbody>
              </table>
            </div>
            <h3>Booking Details</h3>
            <div class="more-results">
                  <?php 
                  $args = array(
                    	'post_type' => 'booking',
                    	'posts_per_page' => -1,
                    	'meta_query' => array(
                            $class_array
                        )
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
            <div class="table1 mb-4">
              <table class="table mb-0">
                <thead>
                  <tr>
                    <td>ID</td>
                    <td>Customer</td>       
                    <td>Booking Date</td>      
                    <td>Class</td>
                    <td>Date</td>
                    <td>Trainer</td>
                    <td>Type</td>
                    <td>Created By</td>
                             
                  </tr>
                </thead>
                <tbody>
                <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
                  <tr>
                    <td><?= get_field('id');?></td>
                    <?php $customer_id = get_field('customer');
                    $author_obj = get_user_by('id', $customer_id);?>
                    <td><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></td>
                    <?php $booking_date = get_field('booking_date');?>
                    <td><?= date("d F Y", strtotime($booking_date));?></td>
                    <td><?= get_field('id',get_field('class'));?></td>
                    <?php $created_date = get_field('created_date');?>
                    <td><?= date("d F Y", strtotime($created_date));?></td>
                    <?php $trainer_id = get_field('trainer',get_field('class'));
                    $author_obj = get_user_by('id', $trainer_id);?>
                    <td><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></td>
                    <td><?= get_field('name',get_field('membership',get_field('class')));?></td>
                    <td><?= get_field('created_by');?></td>
                   
                  </tr>
                  <?php endwhile;?>
                </tbody>
              </table>
            </div>
            <?php
            $pages = paginate_links( array(
                'base' => '%_%',
                'format' => '?page=%#%',
                'current' => max( 1, get_query_var('paged')  ),
                'total' => $query->max_num_pages,
                'end_size' => 1,
                'type'  => 'array',
                'mid_size' => 2,
                'next_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                'prev_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ) );
            ?>            
            <?php if( is_array( $pages ) ) :?>
            <ul class="pagination justify-content-center justify-content-lg-end">
            <?php foreach ( $pages as $page ):?>
                        <li>
                            <?php echo $page;?>
                        </li>
                        <?php endforeach;?>
                    </ul>
            <?php endif;?>
            <?php else:?>
                <div class="alert alert-info" role="alert"><?php _e('No bookings found yet.');?> </div>
            <?php endif;?>
          </div>
            
            
            
        </div>
        <!-- /section -->
      </div>

<?php else:?>

<?php get_header('trainer');?>

<div id="content">
        <!-- section -->
        <div class="section dashboard">
          <div class="container">
            <div class="row align-items-center flex-row-reverse mb-30 mb-md-5">
              <div class="col-md-6 mb-4 mb-md-0 d-none d-md-block">
                <ul class="breadcrumb alt justify-content-end mb-0">
                  <li><a href="#">Home</a></li>
                  <li><a href="<?= home_url('/trainer-class/');?>">Classes</a></li>
                  <li>Schedule</li>
                </ul>
              </div>
              <div class="col-md-6">
                <div class="text-center text-md-start">
                  <h2 class="dashboard-title">Bookings</h2>
                </div>
              </div>
            </div>
            <div class="mb-30">
              <div class="table1 alt mb-0">
                <table class="table mb-0">
                  <thead>
                    <tr>
                      <td colspan="8">Class Details</td>                  
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <span class="title">ID</span>
                        <span class="text"><?= get_field('id');?></span>
                      </td>
                       <?php $date = get_field('date');?>
                      <td>
                        <span class="title">Booking date:</span>
                        <span class="text"><?= date("d F Y", strtotime($date));?></span>
                      </td>
                      <td>
                        <span class="title">Time (Start - End):</span>
                        <span class="text"><?= get_field('start_time').'-'.get_field('end_time');?></span>
                      </td>
                      <td>
                        <span class="title">Hours:</span>
                        <?php $to_time = strtotime("2008-12-13 ".get_field('end_time').":00");
                        $from_time = strtotime("2008-12-13 ".get_field('start_time').":00");
                        $hours = round(abs($to_time - $from_time) / 3600,2);?>
                        <span class="text"><?= $hours.' hours';?></span>
                      </td>
                      <td>
                        <span class="title">Workouts:</span>
                        <?php $workout_titles = array();
                    $workouts = explode(",",get_field('workouts'));
                    foreach($workouts as $workout):
                        $workout_titles[] = get_the_title($workout);
                    endforeach;?>
                   <span class="text"><?= implode(",",$workout_titles);?></span>
                      </td>
                      <td>
                        <span class="title">Class:</span>
                        <span class="text"><?= get_field('class_title');?></span>
                      </td>
                       <td>
                        <span class="title">Type:</span>
                        <span class="text"><?= get_field('name',get_field('membership'));?></span>
                      </td>
                      <td>
                        <span class="title">Bookings:</span>
                        <?php 
                    $class_array = array(
                           'key'     => 'class',
                           'value'   => $post->ID,
                           'compare' => '='
                           );
     
                  $args = array(
                    	'post_type' => 'booking',
                    	'numberposts' => -1,
                    	'meta_query' => array(
                            $class_array
                        )
                        
                    );
                    $latest_class = get_posts( $args );
                    ?>
                    <span class="text"><?= count($latest_class);?>/<?= get_field('no_of_slots');?></span>
                    
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="table1-options d-none">
                <div class="button-group justify-content-end">
                  <button class="btn button5 rounded lead p-md-3 p-2"><i class="fa fa-play me-2"></i> Start class</button>
                  <button class="btn button5 rounded lead p-md-3 p-2"><i class="fa fa-stop me-2"></i> Stop class</button>
                </div>
              </div>
            </div>
             <?php 
                  $args = array(
                    	'post_type' => 'booking',
                    	'posts_per_page' => -1,
                    	'meta_query' => array(
                            $class_array
                        )
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
                  <div class="mb-30">
              <label class="check1 lead text-color-dark">
                <input type="checkbox" class="select-all">
                <span class="inner">
                  Select all
                </span>
              </label>
            </div>
            <form class="attendance-form" method="post" action="#">
            <ul class="list1 four-cols mb-30">
                 <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
              <li>
                <div class="card1 check-item">
                  <div class="inner-wrapper">
                      <?php if(!get_field('attendance_taken')):?>
                    <label class="check1 check-box">
                      <input type="checkbox" class="booking_id" name="booking_ids[]" value="<?= $query->post->ID;?>">
                      <span class="inner"></span>
                    </label>
                    <?php endif;?>
                    <ul class="details-list mb-3 mb-md-0">
                      <li>
                        <div class="d-flex">
                          <span class="title">ID:</span>
                          <span class="text"><?= get_field('id');?></span>
                        </div>
                      </li>
                      <li>
                        <div class="d-flex">
                          <span class="title">Customer:</span>
                          <?php $customer_id = get_field('customer');
                    $author_obj = get_user_by('id', $customer_id);?>
                    <span class="text"><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></span>
                          
                        </div>
                      </li>
                      <li>
                        <div class="d-flex">
                          <span class="title">Booking date:</span>
                          <?php $booking_date = get_field('booking_date');?>
                     <span class="text"><?= date("d F Y", strtotime($booking_date));?></span>
                          
                        </div>
                      </li>
                      <li>
                        <div class="d-flex">
                          <span class="title">Class:</span>
                          <span class="text"><?= get_field('id',get_field('class'));?></span>
                        </div>
                      </li>
                      <li>
                        <div class="d-flex">
                          <span class="title">Type:</span>
                          <span class="text"><?= get_field('name',get_field('membership',get_field('class')));?></span>
                        </div>
                      </li>
                      <li>
                        <div class="d-flex">
                          <span class="title">Created by:</span>
                          <span class="text"><?= get_field('created_by');?></span>
                        </div>
                      </li>
                    </ul>
                    <div class="row align-items-center options">
                      <div class="col-auto">
                        <h4 class="fw-semibold mb-0 text-capitalize">Attendance</h4>
                      </div>
                      <div class="col">
                        <label class="toggle1 alt">
                          <input type="checkbox" disabled <?php if(get_field('attendance_taken')):?> checked <?php endif;?>>
                          <span class="slider"></span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <?php endwhile;?>
            </ul>
            <div class="text-end">
              <button class="btn button1 rounded lead fw-medium d-inline-flex align-items-center" type="submit">
                <i class="fa fa-calendar me-2 fs-2"></i>
                Take attendance
              </button>
            </div>
            </form>
            <?php else:?>
                <div class="alert alert-info" role="alert"><?php _e('No bookings found yet.');?> </div>
            <?php endif;?>
          </div>
        </div>
        <!-- /section -->
      </div>

<?php endif;?>


<?php get_footer();?>
<script>
    $('.select-all').change(function() {
        if($(this).is(":checked")) {
            $('.booking_id').prop('checked',true);
        } else {
            $('.booking_id').prop('checked',false);
        }
        
    });
    
    $('.booking_id').change(function() {
        var valid = true;
        $(".booking_id").each(function(){
            if($(this).is(":checked")) {
                
            } else {
                valid = false;
            }
        });
        $('.select-all').prop('checked',valid);
    });
    
   jQuery(".attendance-form").submit(function(e) {
    e.preventDefault();
      
        jQuery('#loader').show();
      
        var fd= new FormData(jQuery('.attendance-form')[0]);
        // process the form
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>?action=trainer_class_attendance_action',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            location.reload();
             
          });
        
        return false;
    });
    
</script>
