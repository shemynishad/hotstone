<?php /* Template name: Trainer Class */
if ( is_user_logged_in() ) {
    if (current_user_can('trainer')) {
        $current_user = wp_get_current_user();
    }elseif (current_user_can('administrator')) {
        wp_redirect(get_home_url('','/admin-dashboard/')); 
    }elseif (current_user_can('customer')) {
        wp_redirect(get_home_url('','/customer-dashboard/')); 
    }else {
        wp_redirect(get_home_url());     
    }
}else{
wp_redirect(get_home_url()); 
} 
get_header('trainer');?>


<div id="content">
        <!-- section -->
        <div class="section dashboard">
          <div class="container">
            <div class="row align-items-center flex-row-reverse mb-30 mb-md-5">
              <div class="col-md-6 mb-4 mb-md-0 d-none d-md-block">
                <ul class="breadcrumb alt justify-content-end mb-0">
                  <li><a href="<?php home_url('/trainer-dashboard'); ?>">Home</a></li>
                  <li><a href="<?php home_url('/trainer-class'); ?>">Classes</a></li>
                  <li>Schedule</li>
                </ul>
              </div>
              <div class="col-md-6">
                <div class="text-center text-md-start">
                  <h2 class="dashboard-title d-none d-md-block">Schedule</h2>
                  <h2 class="dashboard-title mb-4 mb-md-0 d-md-none">My Classes / Schedule</h2>
                  <figure class="d-md-none">
                    <img src="<?= get_template_directory_uri();?>/assets/images/img1.jpg" alt="img" class="w-100 rounded1 shadow2">
                  </figure>
                </div>
              </div>
            </div>
            <div class="box1 px-md-4 mb-30 bg-color3 border border-color1 shadow1">
              <div class="form1 alt">
                <form action="#" class="filter-form">
                  <div class="row align-items-end">
                    <div class="col">
                      <div class="row">
                        <div class="col-xxl-auto col-12 mb-4 mb-xxl-0 align-self-center d-none d-md-block">
                          <img src="<?= get_template_directory_uri();?>/assets/images/filter-gray.svg" alt="icon">
                        </div>
                        <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                          <div class="date-picker end-date-picker further-date">
                            <input type="text" class="form-control" name="start_date" placeholder="Select from date">
                          </div>
                        </div>
                        <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                          <div class="date-picker end-date-picker further-date">
                            <input type="text" class="form-control" name="end_date" placeholder="Select to date">
                          </div>
                        </div>
                        <div class="col-lg col-md-6 mb-md-4 mb-0 mb-xxl-0">
                            <div class="selectbox">
                          <select class="form-control" name="membership">
                            <option value="">Type</option>
                            <?php 
                                  $args = array(
                                    	'post_type' => 'membership_types',
                                    	'posts_per_page' => -1
                                    );
                                    $query = new WP_Query( $args );
                                  if($query->have_posts()):
                                      while($query->have_posts()): $query->the_post();
                                  ?>
                                <option value="<?= $query->post->ID;?>"><?= get_field('name');?></option>
                                
                                 <?php endwhile; endif;?>
                            
                          </select>
                        </div>
                        </div>
                        <div class="col-xxl-auto col-12 d-none d-md-block">
                          <button class="btn button1 rounded px-3 py-2 h-100" type="submit"><i class="fa fa-search fs-4"></i></button>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto d-md-none">
                      <button class="btn button1 rounded px-3 py-2 h-100" type="submit"><i class="fa fa-search fs-4"></i></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="row mb-4 align-items-center">
              <div class="col-6 text-md-end">
                <button class="btn button5 px-4 py-1 d-md-inline-flex align-items-center d-none" data-bs-toggle="modal" data-bs-target="#add-customer">
                  <span class="fs-2 me-2">&plus;</span> 
                  Add class
                </button>
                <button class="link d-inline-flex align-items-center d-md-none" data-bs-toggle="modal" data-bs-target="#add-customer">
                  <span class="fs-2 me-2">&plus;</span> 
                  Add class
                </button>
              </div>
              <div class="col-6 order-md-first">
                <div class="button-group justify-content-end justify-content-md-start">
                  <button class="btn button4 d-inline-flex align-items-center text-normal p-md-3 p-2 d-md-none">
                    <i class="btn-icon me-md-2">
                      <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M14.005 0H0.596563C0.282732 0 0.0283813 0.25435 0.0283813 0.568182C0.0283813 2.15044 0.706649 3.661 1.88929 4.71225L4.37764 6.92394C4.8091 7.30746 5.05657 7.85856 5.05657 8.43595V13.9766C5.05657 14.4294 5.56261 14.7008 5.93981 14.4492L9.29197 12.2146C9.44999 12.1092 9.54499 11.9318 9.54499 11.7418V8.43595C9.54499 7.85856 9.79246 7.30746 10.2239 6.92394L12.7122 4.71225C13.8948 3.661 14.5731 2.15044 14.5731 0.568182C14.5731 0.25435 14.3187 0 14.005 0ZM11.9572 3.86286L9.46897 6.07466C8.79514 6.6737 8.40862 7.53429 8.40862 8.43584V11.4378L6.19282 12.9149V8.43595C6.19282 7.53429 5.8063 6.6737 5.13248 6.07466L2.64424 3.86297C1.84679 3.15396 1.33775 2.18251 1.20148 1.13625H13.4C13.2637 2.18251 12.7548 3.15396 11.9572 3.86286Z" fill="#808184"/>
                      </svg>                        
                    </i> 
                  </button>
                  <!-- <button class="btn button4 d-inline-flex align-items-center text-normal p-md-3 p-2">
                    <i class="btn-icon me-md-2">
                      <svg viewBox="0 0 31 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.9169 17.913C10.9169 17.509 10.6029 17.1818 10.2155 17.1818H7.76771C7.38071 17.1818 7.06665 17.509 7.06665 17.913V20.4671C7.06665 20.8716 7.38071 21.199 7.76771 21.199H10.2155C10.6029 21.199 10.9169 20.8716 10.9169 20.4671V17.913Z" fill="#1C1D21"/>
                        <path d="M17.0335 17.9129C17.0335 17.5089 16.7194 17.1816 16.3327 17.1816H13.8847C13.4977 17.1816 13.1836 17.5089 13.1836 17.9129V20.467C13.1836 20.8715 13.4977 21.1989 13.8847 21.1989H16.3327C16.7194 21.1989 17.0335 20.8715 17.0335 20.467V17.9129Z" fill="#1C1D21"/>
                        <path d="M23.151 17.9129C23.151 17.5089 22.837 17.1816 22.45 17.1816H20.0022C19.6148 17.1816 19.3008 17.5089 19.3008 17.9129V20.467C19.3008 20.8715 19.6148 21.1989 20.0022 21.1989H22.45C22.837 21.1989 23.151 20.8715 23.151 20.467V17.9129Z" fill="#1C1D21"/>
                        <path d="M10.9167 24.2981C10.9167 23.8934 10.6027 23.5664 10.2152 23.5664H7.76747C7.38047 23.5664 7.06641 23.8934 7.06641 24.2981V26.8517C7.06641 27.2559 7.38047 27.5833 7.76747 27.5833H10.2152C10.6027 27.5833 10.9167 27.2558 10.9167 26.8517V24.2981Z" fill="#1C1D21"/>
                        <path d="M17.0335 24.2981C17.0335 23.8934 16.7194 23.5664 16.3327 23.5664H13.8847C13.4977 23.5664 13.1836 23.8934 13.1836 24.2981V26.8517C13.1836 27.2559 13.4977 27.5833 13.8847 27.5833H16.3327C16.7194 27.5833 17.0335 27.2558 17.0335 26.8517V24.2981Z" fill="#1C1D21"/>
                        <path d="M23.151 24.2981C23.151 23.8934 22.837 23.5664 22.4503 23.5664H20.0022C19.6148 23.5664 19.3008 23.8934 19.3008 24.2981V26.8517C19.3008 27.2559 19.6148 27.5833 20.0022 27.5833H22.4503C22.837 27.5833 23.151 27.2558 23.151 26.8517V24.2981Z" fill="#1C1D21"/>
                        <path d="M27.5285 3.71127V7.61238C27.5285 9.37564 26.1578 10.7966 24.4684 10.7966H22.538C20.8485 10.7966 19.4596 9.37564 19.4596 7.61238V3.69727H10.7587V7.61238C10.7587 9.37564 9.36985 10.7966 7.68057 10.7966H5.74978C4.06044 10.7966 2.68978 9.37564 2.68978 7.61238V3.71127C1.21328 3.75771 0 5.03269 0 6.59981V29.4622C0 31.0587 1.24005 32.37 2.76999 32.37H27.4483C28.9759 32.37 30.2182 31.056 30.2182 29.4622V6.59981C30.2182 5.03269 29.005 3.75771 27.5285 3.71127ZM26.6319 28.0385C26.6319 28.7284 26.0958 29.2882 25.4344 29.2882H4.73111C4.06968 29.2882 3.5336 28.7284 3.5336 28.0385V16.2292C3.5336 15.539 4.06961 14.9792 4.73111 14.9792H25.4343C26.0958 14.9792 26.6318 15.539 26.6318 16.2292L26.6319 28.0385Z" fill="#1C1D21"/>
                        <path d="M5.74481 8.70584H7.65427C8.23385 8.70584 8.70377 8.21617 8.70377 7.61135V1.21598C8.70377 0.611094 8.23385 0.121094 7.65427 0.121094H5.74481C5.16518 0.121094 4.69531 0.611094 4.69531 1.21598V7.61135C4.69531 8.21617 5.16518 8.70584 5.74481 8.70584Z" fill="#1C1D21"/>
                        <path d="M22.5143 8.70584H24.4237C25.0029 8.70584 25.4729 8.21617 25.4729 7.61135V1.21598C25.4729 0.611094 25.003 0.121094 24.4237 0.121094H22.5143C21.9348 0.121094 21.4648 0.611094 21.4648 1.21598V7.61135C21.4648 8.21617 21.9348 8.70584 22.5143 8.70584Z" fill="#1C1D21"/>
                      </svg>
                    </i> 
                    <span class="d-none d-md-inline-block">Calendar view</span>
                  </button>
                  <button class="btn button4 d-inline-flex align-items-center text-normal p-md-3 p-2 active">
                    <i class="btn-icon me-md-2">
                      <svg viewBox="0 0 25 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect width="11.4244" height="12.2707" rx="1" fill="#1C1D21"/>
                        <rect y="14.0234" width="11.4244" height="12.2707" rx="1" fill="#1C1D21"/>
                        <rect x="13.0586" width="11.4244" height="12.2707" rx="1" fill="#1C1D21"/>
                        <rect x="13.0586" y="14.0234" width="11.4244" height="12.2707" rx="1" fill="#1C1D21"/>
                      </svg>                      
                    </i> 
                    <span class="d-none d-md-inline-block">Card view</span>
                  </button> -->
                </div>
              </div>
            </div>
            <div class="more-results">
        <?php  $today = date('Ymd');
                  $args = array(
                    	'post_type' => 'class',
                    	'posts_per_page' => -1,
                    	'meta_query' => array(
                             array(
                                'key'     => 'date',
                                'compare' => '>=',
                                'value'   => $today,
                            ),
                    		array(
                    			'key'     => 'trainer',
                    			'value'   => $current_user->ID,
                    			'compare' => '=',
                    		)
                        ),
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
            <ul class="list1 four-cols">
                <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
              <li>
                <div class="card1">
                  <div class="inner-wrapper">
                    <ul class="details-list mb-0">
                      <li>
                            <div class="d-flex">
                              <span class="title">ID:</span>
                              <span class="text"><?= get_field('id');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Date:</span>
                              <?php $date = get_field('date');?>
                              <span class="text"><?= date("d F Y", strtotime($date));?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Time:</span>
                              <span class="text"><?= get_field('start_time');?> - <?= get_field('end_time');?></span>
                            </div>
                          </li>
                      <li>
                            <div class="d-flex">
                              <span class="title">Workouts:</span>
                              <?php $workout_titles = array();
                    $workouts = explode(",",get_field('workouts'));
                    foreach($workouts as $workout):
                         $workout_titles[] = get_field('name',$workout);
                    endforeach;?>
                              <span class="text"><?= implode(",",$workout_titles);?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Class:</span>
                              <span class="text"><?= get_field('class_title');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Type:</span>
                              <span class="text"><?= get_field('name',get_field('membership'));?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Bookings:</span>
                              <?php 
                    $class_array = array(
                           'key'     => 'class',
                           'value'   => $query->post->ID,
                           'compare' => '='
                           );
     
                  $args = array(
                    	'post_type' => 'booking',
                    	'numberposts' => -1,
                    	'meta_query' => array(
                            $class_array
                        )
                        
                    );
                    $latest_class = get_posts( $args );
                    ?>
                              <span class="text"><?= count($latest_class);?>/<?= get_field('no_of_slots');?></span>
                            </div>
                          </li>
                    </ul>
                    <div class="row align-items-center options">
                        <?php 
                        $formatted_date =  date("m/d/Y", strtotime($date));
                         $data = array(   
                            'class_title' => get_field('class_title'),
                            'class_title_ar' => get_field('class_title_in_arabic'),
                            'date' => $formatted_date,
                            'start_time' => get_field('start_time'),
                            'end_time' => get_field('end_time'),
                            'type' => get_field('membership'),
                            'no_of_slots' => get_field('no_of_slots'),
                            'trainer' => get_field('trainer'),
                            'workouts' =>get_field('workouts')
                        );?>
                      <div class="col-auto">
                        <button type="button" class="edit-user" data-index="<?= $query->post->ID;?>" data-details='<?php echo json_encode($data);?>'><i class="circle-icon1"><img src="<?= get_template_directory_uri();?>/assets/images/edit.svg" alt="icon"></i></button>
                      </div>
                      <div class="col-auto">
                        <button type="button" class="delete-user" data-index="<?= $query->post->ID;?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri();?>/assets/images/delete.svg" alt="icon"></i></button>
                      </div>
                      <div class="col-auto">
                        <a href="<?php the_permalink();?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri();?>/assets/images/eye-icon.svg" alt="icon"></i></a>
                      </div>
                      <?php $disable = get_field('is_disable');?>
                      <div class="col text-md-end">
                        <label class="toggle1 alt">
                          <input type="checkbox" class="disable-user" <?php if(!$disable){ echo 'checked';}?> value="<?= $query->post->ID;?>">
                          <span class="slider"></span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <?php endwhile;?>
            </ul>
            <?php else:?>
                <div class="alert alert-info" role="alert">
				  No upcoming schedules found matching the search criteria. Please try again.
				</div>
            <?php endif;?>
            </div>
          </div>
        </div>
        <!-- /section -->
      </div>


        <div class="modal2 modal fade" id="add-customer" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered small">
            <div class="modal-content">
              <header class="modal-header">
                <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                <h3>Add class</h3>
              </header>
              <div class="modal-body">
                <div class="form1">
                  <form action="#" class="add-user-form">
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label class="form-label">Class Title (EN)</label>                        
                        <input type="text" class="form-control" name="class_title" placeholder="Enter Class Title">                        
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">Class Title (AR)</label>                        
                        <input type="text" class="form-control" name="class_title_ar" placeholder="Enter Class title">                        
                      </div>
                      <div class="col-md-6 form-group date-field">
                        <label class="form-label">Select Date</label>
                        <div class="date-picker end-date-picker further-date">
                          <input type="text" class="form-control date_class" name="date[]" placeholder="Select Date">
                        </div>
                        <button type="button" class="option add-new link1 d-flex align-items-center ms-auto mt-1">
                          <span class="fs-1 me-2">&plus;</span>
                          Add more date & time
                        </button>
                      </div>
                       <div class="col-md-3 form-group start-time-field">
                        <label class="form-label">Start Time</label>
                        <div class="selectbox23">
                          <select class="form-control start_time" name="start_time[]">
                            <option value="">Select Time</option>
                            <option value="00:00">00:00</option>
                            <option value="00:30">00:30</option>
                            <option value="01:00">01:00</option>
                            <option value="01:30">01:30</option>
                            <option value="02:00">02:00</option>
                            <option value="02:30">02:30</option>
                            <option value="03:00">03:00</option>
                            <option value="03:30">03:30</option>
                            <option value="04:00">04:00</option>
                            <option value="04:30">04:30</option>
                            <option value="05:00">05:00</option>
                            <option value="05:30">05:30</option>
                            <option value="06:00">06:00</option>
                            <option value="06:30">06:30</option>
                            <option value="07:00">07:00</option>
                            <option value="07:30">07:30</option>
                            <option value="08:00">08:00</option>
                            <option value="08:30">08:30</option>
                            <option value="09:00">09:00</option>
                            <option value="09:30">09:30</option>
                            <option value="10:00">10:00</option>
                            <option value="10:30">10:30</option>
                            <option value="11:00">11:00</option>
                            <option value="11:30">11:30</option>
                            <option value="12:00">12:00</option>
                            <option value="12:30">12:30</option>
                            <option value="13:00">13:00</option>
                            <option value="13:30">13:30</option>
                            <option value="14:00">14:00</option>
                            <option value="14:30">14:30</option>
                            <option value="15:00">15:00</option>
                            <option value="15:30">15:30</option>
                            <option value="16:00">16:00</option>
                            <option value="16:30">16:30</option>
                            <option value="17:00">17:00</option>
                            <option value="17:30">17:30</option>
                            <option value="18:00">18:00</option>
                            <option value="18:30">18:30</option>
                            <option value="19:00">19:00</option>
                            <option value="19:30">19:30</option>
                            <option value="20:00">20:00</option>
                            <option value="20:30">20:30</option>
                            <option value="21:00">21:00</option>
                            <option value="21:30">21:30</option>
                            <option value="22:00">22:00</option>
                            <option value="22:30">22:30</option>
                            <option value="23:00">23:00</option>
                            <option value="23:30">23:30</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3 form-group end-time-field">
                        <label class="form-label">End Time</label>
                        <div class="selectbox">
                          <select class="form-control end_time" name="end_time[]">
                            <option value="">Select Time</option>
                            <option value="00:00">00:00</option>
                            <option value="00:30">00:30</option>
                            <option value="01:00">01:00</option>
                            <option value="01:30">01:30</option>
                            <option value="02:00">02:00</option>
                            <option value="02:30">02:30</option>
                            <option value="03:00">03:00</option>
                            <option value="03:30">03:30</option>
                            <option value="04:00">04:00</option>
                            <option value="04:30">04:30</option>
                            <option value="05:00">05:00</option>
                            <option value="05:30">05:30</option>
                            <option value="06:00">06:00</option>
                            <option value="06:30">06:30</option>
                            <option value="07:00">07:00</option>
                            <option value="07:30">07:30</option>
                            <option value="08:00">08:00</option>
                            <option value="08:30">08:30</option>
                            <option value="09:00">09:00</option>
                            <option value="09:30">09:30</option>
                            <option value="10:00">10:00</option>
                            <option value="10:30">10:30</option>
                            <option value="11:00">11:00</option>
                            <option value="11:30">11:30</option>
                            <option value="12:00">12:00</option>
                            <option value="12:30">12:30</option>
                            <option value="13:00">13:00</option>
                            <option value="13:30">13:30</option>
                            <option value="14:00">14:00</option>
                            <option value="14:30">14:30</option>
                            <option value="15:00">15:00</option>
                            <option value="15:30">15:30</option>
                            <option value="16:00">16:00</option>
                            <option value="16:30">16:30</option>
                            <option value="17:00">17:00</option>
                            <option value="17:30">17:30</option>
                            <option value="18:00">18:00</option>
                            <option value="18:30">18:30</option>
                            <option value="19:00">19:00</option>
                            <option value="19:30">19:30</option>
                            <option value="20:00">20:00</option>
                            <option value="20:30">20:30</option>
                            <option value="21:00">21:00</option>
                            <option value="21:30">21:30</option>
                            <option value="22:00">22:00</option>
                            <option value="22:30">22:30</option>
                            <option value="23:00">23:00</option>
                            <option value="23:30">23:30</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6 form-group mb-0">
                        <label class="form-label">Select Type</label>
                         <div class="selectbox">
                            <select class="form-control" name="membership">
                                <!-- <option value="">Select Membership Type</option> -->
                                <?php 
                                  $args = array(
                                    	'post_type' => 'membership_types',
                                    	'posts_per_page' => -1
                                    );
                                    $query = new WP_Query( $args );
                                  if($query->have_posts()):
                                      while($query->have_posts()): $query->the_post();
                                  ?>
                                <option value="<?= $query->post->ID;?>"><?= get_field('name');?></option>
                                
                                 <?php endwhile; endif;?>
                            </select>
                        </div>
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">No of Slots</label>                        
                        <input type="text" class="form-control" name="no_of_slots" placeholder="Enter No of Slots">                        
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Select Workouts</label>
                        <div class="custom-selectbox">
                          <div class="form-control p-0 h-auto">
                            <select class="chosen-select" multiple name="workout[]" data-placeholder="Type workouts name">
                              <!-- <option value="">Select Workouts</option> -->
                               <?php 
                            $args = array(
                              'numberposts' => -1,
                              'post_type'   => 'workout'
                            );
                            
                            $latest_books = get_posts( $args );
                            foreach($latest_books as $member):
                            ?>
                            <option value="<?= $member->ID;?>"><?= $member->post_title;?></option>
                            <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="button-group d-none d-md-flex">
                      <input type="hidden" class="form-control" name="trainer" value="<?= $current_user->ID;?>">                        
                      <button class="btn button6-outline rounded" type="button" data-bs-dismiss="modal">Cancel</button>
                      <button class="btn button1 rounded submit-btn" type="submit">Add</button>
                    </div>
                  </form>
                </div>
              </div>
              <footer class="modal-footer d-md-none justify-content-center">
                <div class="button-group">
                  <button class="btn button6 rounded">Cancel</button>
                  <button class="btn button1 rounded">Add</button>
                </div>
              </footer>
            </div>
          </div>
        </div>
        
        
         <div class="modal2 modal fade" id="edit-customer" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered small">
            <div class="modal-content">
              <header class="modal-header">
                <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                <h3>Edit class</h3>
              </header>
              <div class="modal-body">
                <div class="form1">
                  <form action="#" class="edit-user-form">
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label class="form-label">Class Title (EN)</label>                        
                        <input type="text" class="form-control edit_class_title" name="edit_class_title" placeholder="Enter Class Title">                        
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">Class Title (AR)</label>                        
                        <input type="text" class="form-control edit_class_title_ar" name="edit_class_title_ar" placeholder="Enter Class title">                        
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Select Date</label>
                        <div class="date-picker end-date-picker further-date">
                          <input type="text" class="form-control edit_date" name="edit_date" placeholder="Select Date">
                        </div>
                      </div>
                       <div class="col-md-3 form-group">
                        <label class="form-label">Start Time</label>
                        <div class="selectbox">
                          <select class="form-control edit_start_time" name="edit_start_time" id="edit_start">
                            <option value="">Select Time</option>
                            <option value="00:00">00:00</option>
                            <option value="00:30">00:30</option>
                            <option value="01:00">01:00</option>
                            <option value="01:30">01:30</option>
                            <option value="02:00">02:00</option>
                            <option value="02:30">02:30</option>
                            <option value="03:00">03:00</option>
                            <option value="03:30">03:30</option>
                            <option value="04:00">04:00</option>
                            <option value="04:30">04:30</option>
                            <option value="05:00">05:00</option>
                            <option value="05:30">05:30</option>
                            <option value="06:00">06:00</option>
                            <option value="06:30">06:30</option>
                            <option value="07:00">07:00</option>
                            <option value="07:30">07:30</option>
                            <option value="08:00">08:00</option>
                            <option value="08:30">08:30</option>
                            <option value="09:00">09:00</option>
                            <option value="09:30">09:30</option>
                            <option value="10:00">10:00</option>
                            <option value="10:30">10:30</option>
                            <option value="11:00">11:00</option>
                            <option value="11:30">11:30</option>
                            <option value="12:00">12:00</option>
                            <option value="12:30">12:30</option>
                            <option value="13:00">13:00</option>
                            <option value="13:30">13:30</option>
                            <option value="14:00">14:00</option>
                            <option value="14:30">14:30</option>
                            <option value="15:00">15:00</option>
                            <option value="15:30">15:30</option>
                            <option value="16:00">16:00</option>
                            <option value="16:30">16:30</option>
                            <option value="17:00">17:00</option>
                            <option value="17:30">17:30</option>
                            <option value="18:00">18:00</option>
                            <option value="18:30">18:30</option>
                            <option value="19:00">19:00</option>
                            <option value="19:30">19:30</option>
                            <option value="20:00">20:00</option>
                            <option value="20:30">20:30</option>
                            <option value="21:00">21:00</option>
                            <option value="21:30">21:30</option>
                            <option value="22:00">22:00</option>
                            <option value="22:30">22:30</option>
                            <option value="23:00">23:00</option>
                            <option value="23:30">23:30</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3 form-group">
                        <label class="form-label">End Time</label>
                        <div class="selectbox">
                          <select class="form-control edit_end_time" name="edit_end_time">
                            <option value="">Select Time</option>
                            <option value="00:00">00:00</option>
                            <option value="00:30">00:30</option>
                            <option value="01:00">01:00</option>
                            <option value="01:30">01:30</option>
                            <option value="02:00">02:00</option>
                            <option value="02:30">02:30</option>
                            <option value="03:00">03:00</option>
                            <option value="03:30">03:30</option>
                            <option value="04:00">04:00</option>
                            <option value="04:30">04:30</option>
                            <option value="05:00">05:00</option>
                            <option value="05:30">05:30</option>
                            <option value="06:00">06:00</option>
                            <option value="06:30">06:30</option>
                            <option value="07:00">07:00</option>
                            <option value="07:30">07:30</option>
                            <option value="08:00">08:00</option>
                            <option value="08:30">08:30</option>
                            <option value="09:00">09:00</option>
                            <option value="09:30">09:30</option>
                            <option value="10:00">10:00</option>
                            <option value="10:30">10:30</option>
                            <option value="11:00">11:00</option>
                            <option value="11:30">11:30</option>
                            <option value="12:00">12:00</option>
                            <option value="12:30">12:30</option>
                            <option value="13:00">13:00</option>
                            <option value="13:30">13:30</option>
                            <option value="14:00">14:00</option>
                            <option value="14:30">14:30</option>
                            <option value="15:00">15:00</option>
                            <option value="15:30">15:30</option>
                            <option value="16:00">16:00</option>
                            <option value="16:30">16:30</option>
                            <option value="17:00">17:00</option>
                            <option value="17:30">17:30</option>
                            <option value="18:00">18:00</option>
                            <option value="18:30">18:30</option>
                            <option value="19:00">19:00</option>
                            <option value="19:30">19:30</option>
                            <option value="20:00">20:00</option>
                            <option value="20:30">20:30</option>
                            <option value="21:00">21:00</option>
                            <option value="21:30">21:30</option>
                            <option value="22:00">22:00</option>
                            <option value="22:30">22:30</option>
                            <option value="23:00">23:00</option>
                            <option value="23:30">23:30</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6 form-group mb-0">
                        <label class="form-label">Select Type</label>
                        <div class="selectbox">
                            <select class="form-control edit_membership" disabled="disabled" name="edit_membership">
                                <!-- <option value="">Select Membership Type</option> -->
                                <?php 
                                  $args = array(
                                    	'post_type' => 'membership_types',
                                    	'posts_per_page' => -1
                                    );
                                    $query = new WP_Query( $args );
                                  if($query->have_posts()):
                                      while($query->have_posts()): $query->the_post();
                                  ?>
                                <option value="<?= $query->post->ID;?>"><?= get_field('name');?></option>
                                
                                 <?php endwhile; endif;?>
                            </select>
                        </div>
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">No of Slots</label>                        
                        <input type="text" class="form-control edit_no_of_slots" name="edit_no_of_slots" placeholder="Enter No of Slots">                        
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Select Workouts</label>
                        <div class="custom-selectbox">
                          <div class="form-control p-0 h-auto">
                            <select class="chosen-select edit_workout" multiple name="edit_workout[]" data-placeholder="Type workouts name">
                              <!-- <option value="">Select Workouts</option> -->
                               <?php 
                            $args = array(
                              'numberposts' => -1,
                              'post_type'   => 'workout'
                            );
                            
                            $latest_books = get_posts( $args );
                            foreach($latest_books as $member):
                            ?>
                            <option value="<?= $member->ID;?>"><?= $member->post_title;?></option>
                            <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="button-group d-none d-md-flex">
                      <input type="hidden" class="form-control" name="edit_trainer" value="<?= $current_user->ID;?>">    
                      <input type="hidden" name="user_id" class="edit_user_id" value="">
                      <button class="btn button6-outline rounded" type="button" data-bs-dismiss="modal">Cancel</button>
                      <button class="btn button1 rounded submit-btn" type="submit">Update</button>
                    </div>
                  </form>
                </div>
              </div>
              <footer class="modal-footer d-md-none justify-content-center">
                <div class="button-group">
                  <button class="btn button6 rounded" type="button" data-bs-dismiss="modal">Cancel</button>
                  <button class="btn button1 rounded submit-btn" type="submit">Update</button>
                </div>
              </footer>
            </div>
          </div>
        </div>

   <div class="modal2 modal fade" id="disable-enable" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered small">
            <div class="modal-content">
              <header class="modal-header">
                <h3></h3>
              </header>
              <div class="modal-body">
                <div class="box1 px-4 shadow-none bg-color3">
                  <div class="content-body"></div>
                </div>
              </div>
              <footer class="modal-footer justify-content-center">
                <div class="button-group">
                  <button class="btn button6 rounded disable-enable-close" type="button">No</button>
                  <button class="btn button1 rounded submit-booking disable-enable-submit" type="button">Yes</button>
                  <input type="hidden" class="disable-user-id" value="">
                  <input type="hidden" class="disable-enable-id" value="">
                </div>
              </footer>
            </div>
          </div>
        </div>

 <div class="modal2 modal fade" id="disable-enable-success" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered small">
            <div class="modal-content">
              <header class="modal-header">
                <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                <h3></h3>
              </header>
              <div class="modal-body">
                <div class="box1 px-4 shadow-none bg-color3">
                  <div class="content-body"></div>
                </div>
              </div>
              <footer class="modal-footer justify-content-center">
                <div class="button-group">
                  <button class="btn button6 rounded" data-bs-dismiss="modal" type="button">OK</button>
                </div>
              </footer>
            </div>
          </div>
        </div>
              
      <div class="modal2 modal fade" id="delete-confirm" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered small">
            <div class="modal-content">
              <header class="modal-header">
                <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                <h3>Delete Class</h3>
              </header>
              <div class="modal-body">
                <div class="box1 px-4 shadow-none bg-color3">
                  <h3>Are you sure want to delete this Class ?</h3>
                </div>
              </div>
              <footer class="modal-footer justify-content-center">
                <div class="button-group">
                  <button data-bs-dismiss="modal" class="btn button6 rounded">No</button>
                  <button class="btn button1 rounded delete-submit" type="button">Yes</button>
                  <input type="hidden" class="delete-user-id">
                </div>
              </footer>
            </div>
          </div>
        </div>
        
        <div class="modal2 modal fade" id="delete-success" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered small">
            <div class="modal-content">
              <header class="modal-header">
                <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                <h3>Delete Class</h3>
              </header>
              <div class="modal-body">
                <div class="box1 px-4 shadow-none bg-color3">
                  <h3>Class has been successfully deleted</h3>
                </div>
              </div>
              <footer class="modal-footer justify-content-center">
                <div class="button-group">
                  <button data-bs-dismiss="modal" class="btn button6 rounded">Ok</button>
                </div>
              </footer>
            </div>
          </div>
        </div>
        
        <div class="modal2 modal fade" id="edit-user-success" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered small">
            <div class="modal-content">
              <header class="modal-header">
                <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                <h3>Edit Class</h3>
              </header>
              <div class="modal-body">
                <div class="box1 px-4 shadow-none bg-color3">
                  <h3>Class has been successfully updated</h3>
                </div>
              </div>
              <footer class="modal-footer justify-content-center">
                <div class="button-group">
                  <button data-bs-dismiss="modal" class="btn button6 rounded">Ok</button>
                </div>
              </footer>
            </div>
          </div>
        </div>

        <?php get_footer('trainer');?>
<script>
      $.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param * 1000000)
}, 'File size must be less than {0} MB');
       var abc = jQuery('.add-user-form').validate({
            // ignore: ".ignore",
            // onfocusout: function(element) {$(element).valid()},
            // onkeyup: false,
            ignore: [],
            rules:
            {
                trainer:
                {

                    required: true,  
                },
                class_title:
                {

                    required: true, 
                },
                'date[]':
                {
                    required: true, 
                },
                'start_time[]':
                {
                    required: true, 
                },
                'end_time[]':
                {
                    required: true,
                },
                membership:
                {
                    required: true,
                },
                no_of_slots:
                {
                    required: true,
                },
                'workout[]':
                {
                    required: true,
                }
            },

        });

        jQuery(".add-user-form").submit(function(e){
            e.preventDefault();

            $('input.date_class').each(function() {
              if($(this).val() == ''){
                $(this).addClass('error');
              }else{
                $(this).removeClass('error');
              }
            }); 
            $('select.start_time').each(function() {
              if($(this).val() == ''){
                $(this).addClass('error');
              }else{
                $(this).removeClass('error');
              }
            });
            $('select.end_time').each(function() {
              if($(this).val() == ''){
                $(this).addClass('error');
              }else{
                $(this).removeClass('error');
              }
            });
            
            var start_time = $('select[name="start_time[]"]').map(function() {
              if(this.value){
                  return this.value;
                }
            }).get();

            var end_time = $('select[name="end_time[]"]').map(function() {
              if(this.value){
                  return this.value;
                }
            }).get();

            var isValid = true;
            if(start_time.length > 0){
              $.each(start_time, function(index, start) {
                var end = end_time[index];
                if (end <= start) {
                  isValid = false;
                  return false;
                }
              });

              if (!isValid) {
                $('#time_error').remove();
                var errorDiv = $('<div id="time_error" style="color:red">').text('Invalid time range');
                $('.start-time-field').append(errorDiv);
                return false;
              }else{
                $('#time_error').remove();
              }
            }
            if(abc.form()  && abc.form())
            {
                
                 jQuery('#loader').show();
                 jQuery('.submit-btn').attr('disabled','disabled');
                
                    var fd= new FormData(jQuery('.add-user-form')[0]);
                    // process the form
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=admin_class_action',
                        data: fd,
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                           location.reload();                      


                    });
            }

            return false;
        });

        jQuery(".filter-form").submit(function(e) {
    e.preventDefault();
      
        jQuery('#loader').show();
      
        var fd= new FormData(jQuery('.filter-form')[0]);
        // process the form
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>?action=trainer_filter_class_action&page=1',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            var obj = JSON.parse(result);     
            jQuery('.more-results').html(obj.html); 
             jQuery('#loader').hide();
             
          });
        
        return false;
    });

    $( document ).on( "click", "a.page-numbers", function() {
        jQuery('#loader').show();
        var href = $(this).attr('href');
        var this1 = $(this);
        var fd= new FormData(jQuery('.filter-form')[0]);
        // process the form
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>'+href+'&action=admin_filter_class_action',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            var obj = JSON.parse(result);     
            jQuery('.more-results').html(obj.html); 
             jQuery('#loader').hide();          
          });
        
        return false;
    });

    $(document).on('change', '.disable-user', function() {
        var disable = 1;
            $('#disable-enable h3').html('Disable Class');
            $('#disable-enable .content-body').html('<h3>Are you sure to disable this Class ?</h3>');
            $('#disable-enable-success h3').html('Disable Class');
            $('#disable-enable-success .content-body').html('<h3>Class is successfully disabled</h3>');
        if(this.checked) {
        disable = 0;
        $('#disable-enable h3').html('Enable Class');
        $('#disable-enable .content-body').html('<h3>Are you sure to enable this Class ?</h3>');
        $('#disable-enable-success h3').html('Enable Class');
        $('#disable-enable-success .content-body').html('<h3>Class is successfully enabled</h3>');
            
        }
        var user_id = this.value;
        $('.disable-user-id').val(user_id);
        $('.disable-enable-id').val(disable);
        $('#disable-enable').modal('show');
         
    });

    $( document ).on( "click", ".disable-enable-close", function() {
        var user_id = $('.disable-user-id').val();
        var disable = $('.disable-enable-id').val();
        if(disable==1){
          $("input[type=checkbox][value="+user_id+"]").prop("checked",true);
        }else{
          $("input[type=checkbox][value="+user_id+"]").prop("checked",false);
        }
        $('#disable-enable').modal('hide');
    });

    $( document ).on( "click", ".disable-enable-submit", function() {
        var user_id = $('.disable-user-id').val();
        var disable = $('.disable-enable-id').val();
        $('#disable-enable').modal('hide');
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>?action=admin_enable_disable_member_action&postid='+user_id+'&disable='+disable,
          data: '',
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            $('#disable-enable-success').modal('show'); 
             
          });
    });

    $( document ).on( "click", ".delete-user", function() {
        var user_id = $(this).data('index');
        $('.delete-user-id').val(user_id);
        $('#delete-confirm').modal('show'); 
    });

    $( document ).on( "click", ".delete-submit", function() {
        var user_id = $('.delete-user-id').val();
        $('#delete-confirm').modal('hide');
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>?action=delete_member_action&id='+user_id,
          data: '',
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            $('#delete-success').modal('show'); 
             
          });
    });

    jQuery('#delete-success').on('hidden.bs.modal', function () {
      location.reload();
    });

    $( document ).on( "change", ".date_class", function() {
      var dateValue = $(this).val();
      if(dateValue){
        $(this).removeClass('error');
        $(this).next().remove();
      }
    });

    $( document ).on( "click", ".edit-user", function() {
        var user_id = $(this).data('index');
        var details = $(this).data('details');
        $('.edit_user_id').val(user_id);
        $('.edit_class_title').val(details.class_title);
        $('.edit_class_title_ar').val(details.class_title_ar);
        $('.edit_date').val(details.date);
        $('.edit_start_time').val(details.start_time);
        $('.edit_end_time').val(details.end_time);
       // $('input[name=edit_type][value="'+details.type+'"]').prop("checked",true);
       $('.edit_membership').val(details.type);
        $('.edit_no_of_slots').val(details.no_of_slots);
        var str_array = details.workouts.split(',');
        $(".edit_workout").val(str_array).trigger("chosen:updated");
        $('#edit-customer').modal('show');
        
    });

    var abcd = jQuery('.edit-user-form').validate({
            // ignore: ".ignore",
            // onfocusout: function(element) {$(element).valid()},
            // onkeyup: false,
            ignore: [],
            rules:
            {
                edit_trainer:
                {

                    required: true,  
                },
                edit_class_title:
                {

                    required: true, 
                },
                edit_date:
                {
                    required: true, 
                },
                edit_start_time:
                {
                    required: true, 
                },
                edit_end_time:
                {
                    required: true,
                    greaterThan: "#edit_start"
                },
                edit_type:
                {
                    required: true,
                },
                edit_no_of_slots:
                {
                    required: true,
                },
                'edit_workout[]':
                {
                    required: true,
                }
                
            },
            messages: {               
              edit_end_time: {
                      greaterThan : "Invalid time range"
                  },
                    
            },
            errorPlacement: function(error, element) {
                 if (element.attr("name") == "edit_end_time" ) {
                      error.insertAfter("#edit_start");
                    }
                 },

        });

        $.validator.addMethod("greaterThan", function(value, element, params) {

          if (value > $(params).val()) {
              return value
          } 
        },'Must be greater than {0}.');

        jQuery(".edit-user-form").submit(function(e){
            e.preventDefault();
            if(abcd.form()  && abcd.form())
            {
              $('#edit-customer').modal('hide');
                 jQuery('#loader').show();
                 jQuery('.submit-btn').attr('disabled','disabled');
                
                    var fd= new FormData(jQuery('.edit-user-form')[0]);
                    // process the form
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=admin_edit_class_action',
                        data: fd,
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                        jQuery('#loader').hide();
                          $('#edit-user-success').modal('show');
                      


                    });
            }

            return false;
        });
        jQuery('#edit-user-success').on('hidden.bs.modal', function () {
      location.reload();
    });

    

        
        jQuery( document ).on( "click", ".add-new", function() {
            $(this).attr('style', 'display: none !important');
            var body = '<div class="date-picker end-date-picker further-date mt-3"><input type="text" class="form-control date_class" name="date[]" placeholder="Select Date"></div><button type="button" class="option add-new link1 d-flex align-items-center ms-auto mt-1"><span class="fs-1 me-2">&plus;</span>Add more date & time</button>';
            var start_time_section = '<div class="selectbox2 start_class" style="margin-top:15px"><select class="form-control start_time" name="start_time[]">';
            start_time_section += '<option value="">Select Time</option><option value="00:00">00:00</option><option value="00:30">00:30</option><option value="01:00">01:00</option><option value="01:30">01:30</option><option value="02:00">02:00</option><option value="02:30">02:30</option><option value="03:00">03:00</option><option value="03:30">03:30</option><option value="04:00">04:00</option>';
            start_time_section += '<option value="04:30">04:30</option><option value="05:00">05:00</option><option value="05:30">05:30</option><option value="06:00">06:00</option><option value="06:30">06:30</option><option value="07:00">07:00</option><option value="07:30">07:30</option><option value="08:00">08:00</option>';
            start_time_section += '<option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option>';
            start_time_section += '<option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option>';
            start_time_section += '<option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option><option value="18:30">18:30</option><option value="19:00">19:00</option><option value="19:30">19:30</option><option value="20:00">20:00</option><option value="20:30">20:30</option><option value="21:00">21:00</option><option value="21:30">21:30</option>';
            start_time_section += '<option value="22:00">22:00</option><option value="22:30">22:30</option><option value="23:00">23:00</option><option value="23:30">23:30</option>';
            start_time_section += '</select></div>';

            var end_time_section = '<div class="selectbox end_class" style="margin-top:15px"><select class="form-control end_time" name="end_time[]">';
            end_time_section += '<option value="">Select Time</option><option value="00:00">00:00</option><option value="00:30">00:30</option><option value="01:00">01:00</option><option value="01:30">01:30</option><option value="02:00">02:00</option><option value="02:30">02:30</option><option value="03:00">03:00</option><option value="03:30">03:30</option><option value="04:00">04:00</option>';
            end_time_section += '<option value="04:30">04:30</option><option value="05:00">05:00</option><option value="05:30">05:30</option><option value="06:00">06:00</option><option value="06:30">06:30</option><option value="07:00">07:00</option><option value="07:30">07:30</option><option value="08:00">08:00</option>';
            end_time_section += '<option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option>';
            end_time_section += '<option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option>';
            end_time_section += '<option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option><option value="18:30">18:30</option><option value="19:00">19:00</option><option value="19:30">19:30</option><option value="20:00">20:00</option><option value="20:30">20:30</option><option value="21:00">21:00</option><option value="21:30">21:30</option>';
            end_time_section += '<option value="22:00">22:00</option><option value="22:30">22:30</option><option value="23:00">23:00</option><option value="23:30">23:30</option>';
            end_time_section += '</select></div>';

            jQuery('.date-field').append(body);
            jQuery('.start-time-field').append(start_time_section);
            jQuery('.end-time-field').append(end_time_section);
            
            $(".further-date").each(function () {
        		$(this).find(".form-control").datepicker({
        			changeMonth: true,
        			changeYear: true,
        			showOn: "both",
        			buttonImage: "../wp-content/themes/hotstone-fitness/assets/images/calendar-icon.svg",
        			buttonImageOnly: true,
        			buttonText: "Select date",
        			minDate: "+1d",
        			yearRange: "c:2030"
        		});
        	});
            
        });

        
        
</script>