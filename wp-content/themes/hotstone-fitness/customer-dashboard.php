<?php /* Template name: Customer Dashboard */
if ( is_user_logged_in() ) {
    if (current_user_can('customer')) {
        $current_user = wp_get_current_user();
    }elseif (current_user_can('administrator')) {
        wp_redirect(get_home_url('','/admin-dashboard/'));   
    }elseif (current_user_can('trainer')) {
        wp_redirect(get_home_url('','/trainer-dashboard/'));   
    }else {
        wp_redirect(get_home_url());     
    }
}else{
wp_redirect(get_home_url()); 
} 
get_header('customer');?>


<div id="content">
        <!-- section -->
        <div class="section dashboard">
          <div class="container">
            <div class="d-md-none mb-30">
              <h2 class="dashboard-title text-center mb-4">User Dashboard Home</h2>
              <figure>
                <img src="<?= get_template_directory_uri();?>/assets/images/img1.jpg" alt="img" class="w-100 rounded1 shadow2">
              </figure>
            </div>
            <div class="row align-items-center flex-row-reverse">
              <div class="col-md-6 mb-4 mb-md-0 d-none d-md-block">
                <ul class="breadcrumb alt justify-content-end mb-0">
                  <li>Home</li>
                </ul>
              </div>
              <div class="col-md-6">
                <h2 class="dashboard-title mb-2 mb-md-4">Active subscriptions</h2>
              </div>
            </div>
            <div class="mb-30">
                <?php 
                $active_subcription = 0;
                $end_date_formatted = date("Ymd");
                  $args = array(
                    	'post_type' => 'subscription',
                    	'posts_per_page' => -1,
                    	// 'meta_query' => array(
                    	// 	array(
                    	// 		'key'     => 'customer',
                    	// 		'value'   => $current_user->ID,
                    	// 		'compare' => '=',
                    	// 	),
                    	// 	array(
                    	// 		'key'     => 'is_disable',
                    	// 		'value'   => 1,
                    	// 		'compare' => '!=',
                    	// 	),
                    	// 	array(
                      //           'key'     => 'end_date',
                      //           'compare' => '>=',
                      //           'value'   => $end_date_formatted,
                      //       ),
                      //        array(
                      //           'key'     => 'remaining_sessions',
                      //           'compare' => '>=',
                      //           'value'   => 0,
                      //          )
                    	// ),
                      'meta_query' => array(
                        array(
                    			'key'     => 'customer',
                    			'value'   => $current_user->ID,
                    			'compare' => '=',
                    		),
                    		array(
                    			'key'     => 'is_disable',
                    			'value'   => 1,
                    			'compare' => '!=',
                    		),
                        'relation' => 'AND',
                        array(
                          'relation' => 'OR',
                          array(
                            'key'     => 'end_date',
                            'compare' => '>=',
                            'value'   => $end_date_formatted,
                        ),
                           array(
                              'key'     => 'remaining_sessions',
                              'compare' => '>=',
                              'value'   => 0,
                             )
                        ),
                      ),
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
              <div class="horizontal-scroll">
                   
                <ul class="list1">
                <?php while($query->have_posts()): $query->the_post();
                       $active_subcription = get_field('membership_type');
                    ?>
                  <li>
                    <div class="card1">
                      <div class="inner-wrapper">
                        <ul class="details-list mb-0">
                          <li>
                            <div class="d-flex">
                              <span class="title">ID:</span>
                              <span class="text"><?= get_field('id');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Membership:</span>
                              <?php $membership = get_field('membership_type');?>
                              <span class="text"><?= get_field('name',$membership);?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Registration date:</span>
                              <?php $start_date = get_field('start_date');?>
                              <span class="text"><?= date("d F Y", strtotime($start_date));?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Expiry date:</span>
                              <?php $end_date = get_field('end_date');?>
                              <span class="text"><?= date("d F Y", strtotime($end_date));?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Rem. Sessions:</span>
                              <span class="text"><?= get_field('remaining_sessions');?></span>
                            </div>
                          </li>
                        </ul>
                        <div class="row align-items-center">
                        <div class="col-auto">
                            <button type="button" class="view-subscription" data-index="<?= $query->post->ID;?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri();?>/assets/images/eye-icon.svg" alt="icon"></i></button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <?php endwhile;?>
                </ul>
              </div>
              <div class="text-md-end">
                <a href="<?= home_url('/customer-subcriptions/');?>" class="link1">View all active subscriptions</a>
              </div>
              <?php else:?>
              <p>No active subscription found</p>
              <?php endif;?>
            </div>
            <div class="mb-30">
              <h2 class="dashboard-title mb-2 mb-md-4">Upcoming bookings</h2>
              <?php 
              $class_id_array = array();
                  $today = date('Ymd');
                  $args = array(
                    	'post_type' => 'booking',
                    	'posts_per_page' => -1,
                    	'meta_query' => array(
                             array(
                                'key'     => 'booking_date',
                                'compare' => '>=',
                                'value'   => $today,
                            ),
                            array(
                    			'key'     => 'customer',
                    			'value'   => $current_user->ID,
                    			'compare' => '=',
                    		),
                    		array(
                    			'key'     => 'is_disable',
                    			'value'   => 1,
                    			'compare' => '!=',
                    		)
                        ),
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
              <div class="horizontal-scroll">
                <ul class="list1">
                    <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
                  <li>
                    <div class="card1">
                      <div class="inner-wrapper">
                        <ul class="details-list mb-0">
                          <li>
                            <div class="d-flex">
                              <span class="title">ID:</span>
                              <span class="text"><?= get_field('id');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Booking date:</span>
                              <?php $booking_date = get_field('booking_date');?>
                              <span class="text"><?= date("d F Y", strtotime($booking_date));?></span>
                            </div>
                          </li>
                          <?php $class = get_field('class');
                          $class_id_array[] = $class;
                          ?>
                          <?php $workout_titles = array();
                    $workouts = explode(",",get_field('workouts',$class));
                    foreach($workouts as $workout):
                        $workout_titles[] = get_field('name',$workout);
                    endforeach;?>
                          <li>
                            <div class="d-flex">
                              <span class="title">Workout:</span>
                              <span class="text"><?= implode(",",$workout_titles);?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Date:</span>
                              <?php $created_date = get_field('created_date');?>
                              <span class="text"><?= date("d F Y", strtotime($created_date));?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Trainer:</span>
                              <?php $trainer_id = get_field('trainer',$class);
                    $author_obj = get_user_by('id', $trainer_id);?>
                              <span class="text"><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Type:</span>
                              <span class="text"><?= get_field('name',get_field('membership',$class));?></span>
                            </div>
                          </li>
                        </ul>
                        <div class="row align-items-center">
                        <div class="col-auto">
                            <button type="button" data-index="<?= $query->post->ID;?>" class="btn button5 px-md-4 py-md-3 p-2 btn-lg text-normal cancel-booking">Cancel Booking</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                 <?php endwhile;?>
                </ul>
              </div>
              <div class="text-md-end">
                <a href="<?= home_url('/customer-booking/');?>" class="link1">View all upcoming bookings</a>
              </div>
                <?php else:?>
              <p>No upcoming bookings found</p>
              <?php endif;?>
            </div>
            <div class="mb-30">
              <h2 class="dashboard-title mb-2 mb-md-4">Upcoming Classes</h2>
               <?php 
               $today = date('Ymd');
                  $args = array(
                    	'post_type' => 'class',
                    	'posts_per_page' => -1,
                    	'post__not_in' => $class_id_array,
                    	'meta_query' => array(
                             array(
                                'key'     => 'date',
                                'compare' => '>=',
                                'value'   => $today,
                            ),
                    		array(
                    			'key'     => 'is_disable',
                    			'value'   => 1,
                    			'compare' => '!=',
                    		),
                    		array(
                    			'key'     => 'membership',
                    			'value'   => $active_subcription,
                    			'compare' => '=',
                    		)
                        ),
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
              <div class="horizontal-scroll">
                <ul class="list1">
                    <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
                  <li>
                    <div class="card1">
                      <div class="inner-wrapper">
                        <ul class="details-list mb-0">
                          <li>
                            <div class="d-flex">
                              <span class="title">ID:</span>
                              <span class="text"><?= get_field('id');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Date:</span>
                              <?php $date = get_field('date');?>
                              <span class="text"><?= date("d F Y", strtotime($date));?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Time:</span>
                              <span class="text"><?= get_field('start_time');?> - <?= get_field('end_time');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Workouts:</span>
                              <?php $workout_titles = array();
                    $workouts = explode(",",get_field('workouts'));
                    foreach($workouts as $workout):
                        $workout_titles[] = '<a href="#" class="workout-show" data-name="'.get_field('name', $workout).'" data-id="'.get_field('id', $workout).'" data-description="'.get_field('description', $workout).'" data-video="'.get_field('video_url', $workout).'">'.get_field('name',$workout).'</a>';
                    endforeach;
                    
                    ?>
                              <span class="text"><?= implode(",",$workout_titles);?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Trainer</span>
                              <?php $trainer_id = get_field('trainer');
                    $author_obj = get_user_by('id', $trainer_id);?>
                              <span class="text"><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Class:</span>
                              <span class="text"><?= get_field('class_title');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Type:</span>
                              <span class="text"><?= get_field('name',get_field('membership'));?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Bookings:</span>
                              <?php 
                    $class_array = array(
                           'key'     => 'class',
                           'value'   => $query->post->ID,
                           'compare' => '='
                           );
     
                  $args = array(
                    	'post_type' => 'booking',
                    	'numberposts' => -1,
                    	'meta_query' => array(
                            $class_array
                        )
                        
                    );
                    $latest_class = get_posts( $args );
                    $customer_check = array(
                      'key'     => 'customer',
                      'value'   => $current_user->ID,
                      'compare' => '=',
                     );
                    $args1 = array(
                    	'post_type' => 'booking',
                    	'numberposts' => -1,
                    	'meta_query' => array(
                            $class_array,
                            $customer_check
                        )
                        
                    );
                    $booking_alredy_done = get_posts( $args1 );
                    $schedule_id = isset($booking_alredy_done[0]->ID) ? $booking_alredy_done[0]->ID : '';
                    ?>
                              <span class="text"><?= count($latest_class);?>/<?= get_field('no_of_slots');?></span>
                            </div>
                          </li>
                        </ul>
                        <div class="row align-items-center">
                        <div class="col-auto">
                            <button type="button" class="view-class" data-index="<?= $query->post->ID;?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri();?>/assets/images/eye-icon.svg" alt="icon"></i></button>
                          </div>
                          <?php if(!$schedule_id){?>
                          <div class="col-auto">
                            <button type="button" data-index="<?= $query->post->ID;?>" class="btn button5 px-md-4 py-md-3 p-2 btn-lg text-normal book-class">Join</button>
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                  </li>
                 <?php endwhile;?>
                </ul>
              </div>
              <div class="text-md-end">
                <a href="<?= home_url('/customer-schedule/');?>" class="link1">View all upcoming classes</a>
              </div>
            <?php else:?>
              <p>No upcoming class found</p>
              <?php endif;?>
            </div>
            <div class="mb-30">
              <h2 class="dashboard-title mb-2 mb-md-4">Upcoming measurements</h2>
              <?php 
                  $args = array(
                    	'post_type' => 'measurement',
                    	'posts_per_page' => -1,
                    	'meta_query' => array(
                    		array(
                    			'key'     => 'customer',
                    			'value'   => $current_user->ID,
                    			'compare' => '=',
                    		),
                    		array(
                    			'key'     => 'is_disable',
                    			'value'   => 1,
                    			'compare' => '!=',
                    		),
                    	),
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
              <div class="horizontal-scroll">
                <ul class="list1">
                    <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
                  <li>
                    <div class="card1">
                      <div class="inner-wrapper">
                        <ul class="details-list mb-0">
                          <li>
                            <div class="d-flex">
                              <span class="title">ID:</span>
                              <span class="text"><?= get_field('id');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Date:</span>
                               <?php $date = get_field('date');?>
                              <span class="text"><?= date("d F Y", strtotime($date));?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Trainer</span>
                              <?php $trainer_id = get_field('trainer');
                    $author_obj = get_user_by('id', $trainer_id);?>
                              <span class="text"><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></span>
                            </div>
                          </li>
                        </ul>
                        <div class="row align-items-center">
                        <div class="col-auto">
                            <button type="button" class="view-measurement" data-index="<?= $query->post->ID;?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri();?>/assets/images/eye-icon.svg" alt="icon"></i></button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                 <?php endwhile;?>
                </ul>
              </div>
              <div class="text-md-end">
                <a href="<?= home_url('/customer-measurements/');?>" class="link1">View all recent measurements</a>
              </div>
            <?php else:?>
              <p>No measurement found</p>
              <?php endif;?>
            </div>
          </div>
        </div>
        <!-- /section -->
      </div>


        <div class="modal2 modal fade" id="view-subcription-details" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered small">
            <div class="modal-content">
              <header class="modal-header">
                <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                <h3>View subscription details</h3>
              </header>
              <div class="modal-body">
                <div class="box1 px-4 shadow-none bg-color3">
                  <ul class="details-list d-flex mb-0">
                    
                  </ul>
                </div>
              </div>
              <footer class="modal-footer d-md-none justify-content-center">
                
              </footer>
            </div>
          </div>
        </div>
        
        <div class="modal2 modal fade" id="view-measurement-details" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered small">
            <div class="modal-content">
              <header class="modal-header">
                <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                <h3>View measurement details</h3>
              </header>
              <div class="modal-body">
                <div class="box1 px-4 shadow-none bg-color3">
                  <ul class="details-list d-flex mb-0">
                    
                  </ul>
                </div>
              </div>
              <footer class="modal-footer d-md-none justify-content-center">
                
              </footer>
            </div>
          </div>
        </div>
        
        <div class="modal2 modal fade" id="view-class-details" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered small">
            <div class="modal-content">
              <header class="modal-header">
                <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                <h3>View class details</h3>
              </header>
              <div class="modal-body">
                <div class="box1 px-4 shadow-none bg-color3">
                  <ul class="details-list d-flex mb-0">
                   
                  </ul>
                </div>
                <div class="text-end d-none d-md-block pt-4">
                  <button class="btn button1 rounded book-class popup-book" type="button">Join the class</button>
                </div>
              </div>
              <footer class="modal-footer d-md-none justify-content-center">
                <div class="button-group">
                  <button data-bs-dismiss="modal" class="btn button6 rounded">Cancel</button>
                  <button class="btn button1 rounded book-class popup-book" type="button">Join</button>
                </div>
              </footer>
            </div>
          </div>
        </div>
        
        <div class="modal2 modal fade" id="confirm-booking" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered small">
            <div class="modal-content">
              <header class="modal-header">
                <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                <h3>Confirm Booking</h3>
              </header>
              <div class="modal-body">
                <div class="box1 px-4 shadow-none bg-color3">
                  <h3>Are you sure want to join the class?</h3>
                </div>
              </div>
              <footer class="modal-footer justify-content-center">
                <div class="button-group">
                  <button data-bs-dismiss="modal" class="btn button6 rounded">No</button>
                  <button class="btn button1 rounded submit-booking" type="button">Yes</button>
                  <input type="hidden" class="class_id">
                </div>
              </footer>
            </div>
          </div>
        </div>
        
            <div class="modal2 modal fade" id="cancel-booking" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered small">
            <div class="modal-content">
              <header class="modal-header">
                <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                <h3>Cancel Booking</h3>
              </header>
              <div class="modal-body">
                <div class="box1 px-4 shadow-none bg-color3">
                  <h3>Are you sure want to cancel the booking?</h3>
                </div>
              </div>
              <footer class="modal-footer justify-content-center">
                <div class="button-group">
                  <button data-bs-dismiss="modal" class="btn button6 rounded">No</button>
                  <button class="btn button1 rounded submit-cancel" type="button">Yes</button>
                  <input type="hidden" class="cancel_id">
                </div>
              </footer>
            </div>
          </div>
        </div>
        
         <div class="modal2 modal fade" id="workout-popup" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <header class="modal-header">
                <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                <h3 class="workout-title">Workout 001</h3>
                <ul class="details-list d-flex border-0 text-white mb-0">
                  <li>
                    <div class="d-flex align-items-center">
                      <span class="title">ID:</span>
                      <span class="text workout-id">HSSL_001</span>
                    </div>
                  </li>
                </ul>
              </header>
              <div class="video-wrapper rounded2 overflow-hidden">
                <div class="video-box">
                  <iframe src="" class="workout-video" style="width:100%; height:400px;"></iframe>
                </div>
                <div class="description">
                  <h4>Description:</h4>
                  <p class="workout-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor sed perferendis distinctio cum, beatae quibusdam nulla quasi incidunt laudantium odio totam at vel autem iste quidem porro iusto. Esse, provident.</p>
                </div>
              </div>
            </div>
          </div>
        </div>

<?php get_footer('customer');?>
<script>
    jQuery(".view-subscription").click(function(e){
            e.preventDefault();
                
                    var user_id = $(this).data('index');
                     
                    // process the form
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=view_customer_subscription_action&user_id='+user_id,
                        data: '',
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                            var obj = JSON.parse(result);     
                            jQuery('#view-subcription-details ul').html(obj.html); 
                            jQuery('#view-subcription-details').modal('show');
                      


                    });
            

            return false;
        });
        
            jQuery(".view-measurement").click(function(e){
            e.preventDefault();
                
                    var user_id = $(this).data('index');
                     
                    // process the form
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=view_customer_measurement_action&user_id='+user_id,
                        data: '',
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                            var obj = JSON.parse(result);     
                            jQuery('#view-measurement-details ul').html(obj.html); 
                            jQuery('#view-measurement-details').modal('show');
                      


                    });
            

            return false;
        });
        
         jQuery(".view-class").click(function(e){
            e.preventDefault();
                
                    var user_id = $(this).data('index');
                     jQuery('.popup-book').attr('data-index', user_id);

                    // process the form
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=view_customer_class_action&user_id='+user_id,
                        data: '',
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                            var obj = JSON.parse(result);     
                            jQuery('#view-class-details ul').html(obj.html); 
                            jQuery('#view-class-details').modal('show');
                      


                    });
            

            return false;
        });
        
        jQuery(".book-class").click(function(e){
             e.preventDefault();
                
                    var user_id = $(this).data('index');
                    jQuery('#view-class-details').modal('hide');

                    jQuery('.class_id').val(user_id);
                    jQuery('#confirm-booking').modal('show');
            

            return false;
            
        });
        
        jQuery(".submit-booking").click(function(e){
             e.preventDefault();
                
                    var user_id = jQuery('.class_id').val();
                    jQuery('#confirm-booking').modal('hide');
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=customer_book_class_action&user_id='+user_id,
                        data: '',
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                            location.reload();
                      


                    });
            

            return false;
            
        });
        
        jQuery(".cancel-booking").click(function(e){
             e.preventDefault();
                
                    var user_id = $(this).data('index');
                    jQuery('.cancel_id').val(user_id);
                    jQuery('#cancel-booking').modal('show');
            

            return false;
            
        });
        
        jQuery(".submit-cancel").click(function(e){
             e.preventDefault();
                
                    var user_id = jQuery('.cancel_id').val();
                    jQuery('#cancel-booking').modal('hide');
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=customer_cancel_booking_action&id='+user_id,
                        data: '',
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                            location.reload();
                      


                    });
            

            return false;
            
        });
        
         $( document ).on( "click", ".workout-show", function() {
        $('.workout-title').val($(this).data('name'));
        $('.workout-id').val($(this).data('id'));
        $('.workout-description').val($(this).data('description'));
        var youtube_id = youtube_parser($(this).data('video'));
        console.log(youtube_id);
        console.log($(this).data('video'));
        $('.workout-video').attr('src', '//www.youtube.com/embed/'+youtube_id);
        $('#workout-popup').modal('show');
        return false;
        
    });
    
    function youtube_parser(url){
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    var match = url.match(regExp);
    return (match&&match[7])? match[7] : false;
}
</script>