<?php /* Template name: Trainer Measurement */
if (is_user_logged_in()) {
  if (current_user_can('trainer')) {
    $current_user = wp_get_current_user();
  } elseif (current_user_can('administrator')) {
    wp_redirect(get_home_url('', '/admin-dashboard/'));
  } elseif (current_user_can('customer')) {
    wp_redirect(get_home_url('', '/customer-dashboard/'));
  } else {
    wp_redirect(get_home_url());
  }
} else {
  wp_redirect(get_home_url());
}
get_header('trainer'); ?>


<div id="content">
  <!-- section -->
  <div class="section dashboard">
    <div class="container">
      <div class="row align-items-center flex-row-reverse mb-30 mb-md-5">
        <div class="col-md-6 mb-4 mb-md-0 d-none d-md-block">
          <ul class="breadcrumb alt justify-content-end mb-0">
            <li><a href="#">Home</a></li>
            <li>Measurement</li>
          </ul>
        </div>
        <div class="col-md-6">
          <div class="text-center text-md-start">
            <h2 class="dashboard-title d-none d-md-block">Measurement</h2>
            <h2 class="dashboard-title mb-4 mb-md-0 d-md-none">Measurement</h2>
            <figure class="d-md-none">
              <img src="<?= get_template_directory_uri(); ?>/assets/images/img1.jpg" alt="img" class="w-100 rounded1 shadow2">
            </figure>
          </div>
        </div>
      </div>
      <div class="box1 px-md-4 mb-30 bg-color3 border border-color1 shadow1">
        <div class="form1 alt">
          <form action="#" class="filter-form">
            <div class="row align-items-end">
              <div class="col">
                <div class="row">
                  <div class="col-xxl-auto col-12 mb-4 mb-xxl-0 align-self-center d-none d-md-block">
                    <img src="<?= get_template_directory_uri(); ?>/assets/images/filter-gray.svg" alt="icon">
                  </div>
                  <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                    <div class="date-picker">
                      <input type="text" class="form-control" name="start_date" placeholder="Select from date">
                    </div>
                  </div>
                  <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                    <div class="date-picker">
                      <input type="text" class="form-control" name="end_date" placeholder="Select to date">
                    </div>
                  </div>
                  <div class="col-lg col-md-6 mb-md-4 mb-0 mb-xxl-0">
                    <input type="text" class="form-control" name="trainer_keyword" placeholder="Search by Customer">
                  </div>
                  <div class="col-xxl-auto col-12 d-none d-md-block">
                    <button class="btn button1 rounded px-3 py-2 h-100" type="submit"><i class="fa fa-search fs-4"></i></button>
                  </div>
                </div>
              </div>
              <div class="col-auto d-md-none">
                <button class="btn button1 rounded px-3 py-2 h-100" type="submit"><i class="fa fa-search fs-4"></i></button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="row mb-4 align-items-center">
        <div class="col-6 text-md-end">
          <button class="btn button5 px-4 py-1 d-md-inline-flex align-items-center d-none" data-bs-toggle="modal" data-bs-target="#add-customer">
            <span class="fs-2 me-2">&plus;</span>
            Add Measurement
          </button>
          <button class="link d-inline-flex align-items-center d-md-none" data-bs-toggle="modal" data-bs-target="#add-customer">
            <span class="fs-2 me-2">&plus;</span>
            Add Measurement
          </button>
        </div>
        <div class="col-6 order-md-first">

        </div>
      </div>
      <div class="more-results">
        <?php
        $args = array(
          'post_type' => 'measurement',
          'posts_per_page' => -1,
          'meta_query' => array(
            array(
              'key'     => 'trainer',
              'value'   => $current_user->ID,
              'compare' => '=',
            )
          ),
        );
        $query = new WP_Query($args);
        if ($query->have_posts()) :
        ?>
          <ul class="list1">
            <?php while ($query->have_posts()) : $query->the_post();
                 $customer_id = get_field('customer');
            ?>
              <li>
                <div class="card1">
                  <div class="inner-wrapper">
                    <ul class="details-list mb-0">
                      <li>
                        <div class="d-flex">
                          <span class="title">ID:</span>
                          <span class="text"><?= get_field('id'); ?></span>
                        </div>
                      </li>
                      <li>
                        <div class="d-flex">
                          <span class="title">Date:</span>
                          <?php $date = get_field('date'); ?>
                          <span class="text"><?= date("d F Y", strtotime($date)); ?></span>
                        </div>
                      </li>
                      <li>
                        <div class="d-flex">
                          <span class="title">Customer</span>
                          <?php $trainer_id = get_field('customer');
                          $author_obj = get_user_by('id', $trainer_id); ?>
                          <span class="text"><?= $author_obj->user_firstname . ' ' . $author_obj->user_lastname; ?></span>
                        </div>
                      </li>
                    </ul>
                    <div class="row align-items-center options">
                      <?php
                      $data = array(
                        'customer' => $customer_id,
                        'trainer' => $trainer_id,
                        'fasting' => get_field('fasting'),
                        'height'  => get_field('height'),
                        'weight'  => get_field('weight'),
                        'performance_index'  => get_field('performance_index'),
                        'smm'  => get_field('smm'),
                        'notes'  => get_field('notes'),
                        'notes_ar'  => get_field('notes_in_arabic'),
                      ); ?>
                      <div class="col-auto">
                        <button type="button" class="edit-user" data-index="<?= $query->post->ID; ?>" data-details='<?php echo json_encode($data); ?>'><i class="circle-icon1"><img src="<?= get_template_directory_uri(); ?>/assets/images/edit.svg" alt="icon"></i></button>
                      </div>
                      <div class="col-auto">
                        <button type="button" class="delete-user" data-index="<?= $query->post->ID; ?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri(); ?>/assets/images/delete.svg" alt="icon"></i></button>
                      </div>
                      <div class="col-auto">
                        <button type="button" class="view-measurement" data-index="<?= $query->post->ID; ?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri(); ?>/assets/images/eye-icon.svg" alt="icon"></i></button>
                      </div>
                      <?php $disable = get_field('is_disable'); ?>
                      <div class="col text-md-end">
                        <label class="toggle1 alt">
                          <input type="checkbox" class="disable-user" <?php if (!$disable) {
                                                                        echo 'checked';
                                                                      } ?> value="<?= $query->post->ID; ?>">
                          <span class="slider"></span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            <?php endwhile; ?>
          </ul>
        <?php else : ?>
          <div class="alert alert-info" role="alert"><?php _e('No measurement found matching the search criteria. Please try again.'); ?> </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <!-- /section -->
</div>


<div class="modal2 modal fade" id="add-customer" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>Add Measurement</h3>
      </header>
      <div class="modal-body">
        <div class="form1">
          <form action="#" class="add-user-form">
            <div class="row">
              <div class="col-12 form-group">
                <label class="form-label">Choose Customer</label>
                <div class="selectbox sel-customer">
                  <select class="form-control add_customer" name="customer" id="add_customer">
                    <option value="">Choose Customer</option>
                    <?php
                    $team_presidents = get_users(array('role__in' => array('customer'), 'number' => 9999));
                    foreach ($team_presidents as $team_president) :
                    ?>
                      <option value="<?= $team_president->ID; ?>"><?= $team_president->user_firstname . ' ' . $team_president->user_lastname; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="col-12 form-group">
                <label class="form-label">Fasting</label>
                <div class="row">
                  <div class="col-auto">
                    <label class="radio1">
                      <input type="radio" name="fasting" value="Yes">
                      <span class="inner">
                        <i class="fa fa-solid fa-thumbs-up"></i>
                      </span>
                    </label>
                  </div>
                  <div class="col-auto">
                    <label class="radio1">
                      <input type="radio" name="fasting" value="No">
                      <span class="inner">
                        <i class="fa fa-solid fa-thumbs-down"></i>
                      </span>
                    </label>
                  </div>
                </div>
                <label for="fasting" class="error"></label>
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">Height (cm)</label>
                <input type="number" class="form-control decimal_input" step="any" pattern="[0-9]*" name="height" placeholder="Enter Height (cm)">
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">Weight (kg)</label>
                <input type="number" class="form-control decimal_input" step="any" pattern="[0-9]*" name="weight" placeholder="Enter Weight (kg)">
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">Performance Index</label>
                <input type="text" class="form-control" name="performance_index" placeholder="Enter Performance Index">
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">SMM</label>
                <input type="text" class="form-control" name="smm" placeholder="Enter SMM">
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">Note (EN)</label>
                <textarea class="form-control" name="note" placeholder="Enter Note"></textarea>
              </div>
              <div class="col-md-6 form-group ar">
                <label class="form-label">Note (AR)</label>
                <textarea class="form-control" name="note_ar" placeholder="Enter Note"></textarea>
              </div>



            </div>
            <div class="button-group d-none d-md-flex">
              <input type="hidden" class="form-control" name="trainer" value="<?= $current_user->ID; ?>">
              <button class="btn button6-outline rounded" type="button" data-bs-dismiss="modal">Cancel</button>
              <button class="btn button1 rounded submit-btn" type="submit">Add</button>
            </div>
          </form>
        </div>
      </div>
      <footer class="modal-footer d-md-none justify-content-center">
        <div class="button-group">
          <button class="btn button6 rounded">Cancel</button>
          <button class="btn button1 rounded">Add</button>
        </div>
      </footer>
    </div>
  </div>
</div>


<div class="modal2 modal fade" id="edit-customer" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>Edit Measurement</h3>
      </header>
      <div class="modal-body">
        <div class="form1">
          <form action="#" class="edit-user-form">
            <div class="row">
              <div class="col-12 form-group">
                <label class="form-label">Choose Customer</label>
                <div class="selectbox sel-edit-customer">
                  <select class="form-control edit_customer" name="edit_customer" id="edit_customer">
                    <option value="">Choose Customer</option>
                    <?php
                    $team_presidents = get_users(array('role__in' => array('customer'), 'number' => 9999));
                    foreach ($team_presidents as $team_president) :
                    ?>
                      <option value="<?= $team_president->ID; ?>"><?= $team_president->user_firstname . ' ' . $team_president->user_lastname; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="col-12 form-group">
                <label class="form-label">Fasting</label>
                <div class="row">
                  <div class="col-auto">
                    <label class="radio1">
                      <input type="radio" class="edit_fasting" name="edit_fasting" value="Yes">
                      <span class="inner">
                        <i class="fa fa-solid fa-thumbs-up"></i>
                      </span>
                    </label>
                  </div>
                  <div class="col-auto">
                    <label class="radio1">
                      <input type="radio" class="edit_fasting" name="edit_fasting" value="No">
                      <span class="inner">
                        <i class="fa fa-solid fa-thumbs-down"></i>
                      </span>
                    </label>
                  </div>
                </div>
                <label for="edit_fasting" class="error"></label>
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">Height (cm)</label>
                <input type="number" class="form-control edit_height decimal_input" step="any" pattern="[0-9]*" name="edit_height" placeholder="Enter Height (cm)">
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">Weight (kg)</label>
                <input type="number" class="form-control edit_weight decimal_input" step="any" pattern="[0-9]*" name="edit_weight" placeholder="Enter Weight (kg)">
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">Performance Index</label>
                <input type="text" class="form-control edit_performance_index" name="edit_performance_index" placeholder="Enter Performance Index">
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">SMM</label>
                <input type="text" class="form-control edit_smm" name="edit_smm" placeholder="Enter SMM">
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">Note (EN)</label>
                <textarea class="form-control edit_note" name="edit_note" placeholder="Enter Note"></textarea>
              </div>
              <div class="col-md-6 form-group ar">
                <label class="form-label">Note (AR)</label>
                <textarea class="form-control edit_note_ar" name="edit_note_ar" placeholder="Enter Note"></textarea>
              </div>
            </div>
            <div class="button-group d-none d-md-flex">
              <input type="hidden" class="form-control" name="edit_trainer" value="<?= $current_user->ID; ?>">
              <input type="hidden" name="user_id" class="edit_user_id" value="">
              <button class="btn button6-outline rounded" type="button" data-bs-dismiss="modal">Cancel</button>
              <button class="btn button1 rounded submit-btn" type="submit">Update</button>
            </div>
          </form>
        </div>
      </div>
      <footer class="modal-footer d-md-none justify-content-center">
        <div class="button-group">
          <button class="btn button6 rounded" type="button" data-bs-dismiss="modal">Cancel</button>
          <button class="btn button1 rounded submit-btn" type="submit">Update</button>
        </div>
      </footer>
    </div>
  </div>
</div>

<div class="modal2 modal fade" id="disable-enable" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <h3></h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <div class="content-body"></div>
        </div>
      </div>
      <footer class="modal-footer justify-content-center">
        <div class="button-group">
          <button class="btn button6 rounded disable-enable-close" type="button">No</button>
          <button class="btn button1 rounded submit-booking disable-enable-submit" type="button">Yes</button>
          <input type="hidden" class="disable-user-id" value="">
          <input type="hidden" class="disable-enable-id" value="">
        </div>
      </footer>
    </div>
  </div>
</div>

<div class="modal2 modal fade" id="disable-enable-success" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3></h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <div class="content-body"></div>
        </div>
      </div>
      <footer class="modal-footer justify-content-center">
        <div class="button-group">
          <button class="btn button6 rounded" data-bs-dismiss="modal" type="button">OK</button>
        </div>
      </footer>
    </div>
  </div>
</div>

<div class="modal2 modal fade" id="delete-confirm" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>Delete Measurement</h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <h3>Are you sure want to delete this Measurment ?</h3>
        </div>
      </div>
      <footer class="modal-footer justify-content-center">
        <div class="button-group">
          <button data-bs-dismiss="modal" class="btn button6 rounded">No</button>
          <button class="btn button1 rounded delete-submit" type="button">Yes</button>
          <input type="hidden" class="delete-user-id">
        </div>
      </footer>
    </div>
  </div>
</div>

<div class="modal2 modal fade" id="delete-success" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>Delete Measurement</h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <h3>Measurement has been successfully deleted</h3>
        </div>
      </div>
      <footer class="modal-footer justify-content-center">
        <div class="button-group">
          <button data-bs-dismiss="modal" class="btn button6 rounded">Ok</button>
        </div>
      </footer>
    </div>
  </div>
</div>

<div class="modal2 modal fade" id="edit-user-success" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>Edit Measurement</h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <h3>Measurement has been successfully updated</h3>
        </div>
      </div>
      <footer class="modal-footer justify-content-center">
        <div class="button-group">
          <button data-bs-dismiss="modal" class="btn button6 rounded">Ok</button>
        </div>
      </footer>
    </div>
  </div>
</div>

<div class="modal2 modal fade" id="view-measurement-details" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>View measurement details</h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <ul class="details-list d-flex mb-0">

          </ul>
        </div>
      </div>
      <footer class="modal-footer d-md-none justify-content-center">

      </footer>
    </div>
  </div>
</div>

<?php get_footer('trainer');?>
<script>
  $(document).ready(function() {
    $(".add_customer").select2({
      dropdownParent: $(".sel-customer")
    });
    $(".edit_customer").select2({
      dropdownParent: $(".sel-edit-customer")
    });
  });
  $.validator.addMethod('filesize', function(value, element, param) {
    return this.optional(element) || (element.files[0].size <= param * 1000000)
  }, 'File size must be less than {0} MB');
  $.validator.addMethod('decimal', function(value, element) {
    return this.optional(element) || /^((\d+(\\.\d{0,2})?)|((\d*(\.\d{1,2}))))$/.test(value);
  }, "Please enter a correct number");
  var abc = jQuery('.add-user-form').validate({
    // ignore: ".ignore",
    // onfocusout: function(element) {
    //   $(element).valid()
    // },
    // onkeyup: false,
    ignore: [],
    rules: {
      trainer: {

        required: true,
      },
      customer: {

        required: true,
      },
      fasting: {
        required: true,
      },
      height: {
        required: true,
        decimal: true,
      },
      weight: {
        required: true,
        decimal: true,
      },
      // performance_index: {
      //   required: true,
      // },
      // smm: {
      //   required: true,
      // },
      // note: {
      //   required: true,
      // }
    },
    messages: {
      customer: {
        required: "<?php _e('Customer is required', 'sidf'); ?>"
      },
      fasting: {
        required: "<?php _e('Fasting is required', 'sidf'); ?>"
      },
    },
    errorElement: "label",
    errorPlacement: function(error, element) {
      if (element.hasClass("select2-hidden-accessible")) {
        element = $("#select2-" + element.attr("id") + "-container").parent();
        error.insertAfter(element);
      } else {
        error.insertAfter(element);
      }
    },

  });

  jQuery(".add-user-form").submit(function(e) {
    e.preventDefault();
    if (abc.form() && abc.form()) {

      jQuery('#loader').show();
      jQuery('.submit-btn').attr('disabled', 'disabled');

      var fd = new FormData(jQuery('.add-user-form')[0]);
      // process the form
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_measurement_action',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          location.reload();


        });
    }

    return false;
  });

  jQuery(".filter-form").submit(function(e) {
    e.preventDefault();

    jQuery('#loader').show();

    var fd = new FormData(jQuery('.filter-form')[0]);
    // process the form
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=trainer_filter_measurement_action&page=1',
        data: fd,
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        var obj = JSON.parse(result);
        jQuery('.more-results').html(obj.html);
        jQuery('#loader').hide();

      });

    return false;
  });

  $(document).on("click", "a.page-numbers", function() {
    jQuery('#loader').show();
    var href = $(this).attr('href');
    var this1 = $(this);
    var fd = new FormData(jQuery('.filter-form')[0]);
    // process the form
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>' + href + '&action=admin_filter_class_action',
        data: fd,
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        var obj = JSON.parse(result);
        jQuery('.more-results').html(obj.html);
        jQuery('#loader').hide();
      });

    return false;
  });

  $(document).on('change', '.disable-user', function() {
    var disable = 1;
    $('#disable-enable h3').html('Disable Class');
    $('#disable-enable .content-body').html('<h3>Are you sure to disable this Class ?</h3>');
    $('#disable-enable-success h3').html('Disable Class');
    $('#disable-enable-success .content-body').html('<h3>Class is successfully disabled</h3>');
    if (this.checked) {
      disable = 0;
      $('#disable-enable h3').html('Enable Class');
      $('#disable-enable .content-body').html('<h3>Are you sure to enable this Class ?</h3>');
      $('#disable-enable-success h3').html('Enable Class');
      $('#disable-enable-success .content-body').html('<h3>Class is successfully enabled</h3>');

    }
    var user_id = this.value;
    $('.disable-user-id').val(user_id);
    $('.disable-enable-id').val(disable);
    $('#disable-enable').modal('show');

  });

  $(document).on("click", ".disable-enable-close", function() {
    var user_id = $('.disable-user-id').val();
    var disable = $('.disable-enable-id').val();
    if (disable == 1) {
      $("input[type=checkbox][value=" + user_id + "]").prop("checked", true);
    } else {
      $("input[type=checkbox][value=" + user_id + "]").prop("checked", false);
    }
    $('#disable-enable').modal('hide');
  });

  $(document).on("click", ".disable-enable-submit", function() {
    var user_id = $('.disable-user-id').val();
    var disable = $('.disable-enable-id').val();
    $('#disable-enable').modal('hide');
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_enable_disable_member_action&postid=' + user_id + '&disable=' + disable,
        data: '',
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        $('#disable-enable-success').modal('show');

      });
  });

  $(document).on("click", ".delete-user", function() {
    var user_id = $(this).data('index');
    $('.delete-user-id').val(user_id);
    $('#delete-confirm').modal('show');
  });

  $(document).on("click", ".delete-submit", function() {
    var user_id = $('.delete-user-id').val();
    $('#delete-confirm').modal('hide');
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=delete_member_action&id=' + user_id,
        data: '',
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        $('#delete-success').modal('show');

      });
  });

  jQuery('#delete-success').on('hidden.bs.modal', function() {
    location.reload();
  });


  $(document).on("click", ".edit-user", function() {
    var user_id = $(this).data('index');
    var details = $(this).data('details');
    $('.edit_user_id').val(user_id);
    $('.edit_customer').val(details.customer).trigger('change');
    $('input[name=edit_fasting][value="' + details.fasting + '"]').prop("checked", true);
    $('.edit_height').val(details.height);
    $('.edit_weight').val(details.weight);
    $('.edit_performance_index').val(details.performance_index);
    $('.edit_smm').val(details.smm);
    $('.edit_note').val(details.notes);
    $('.edit_note_ar').val(details.notes_ar);
    $('#edit-customer').modal('show');

  });

  $(".decimal_input").keydown(function(e){
      if ([69, 187, 188, 189].includes(e.keyCode)) {
        e.preventDefault();
      }
    });

  var abcd = jQuery('.edit-user-form').validate({
    // ignore: ".ignore",
    // onfocusout: function(element) {
    //   $(element).valid()
    // },
    // onkeyup: false,
    ignore: [],
    rules: {
      edit_trainer: {

        required: true,
      },
      edit_customer: {

        required: true,
      },
      edit_fasting: {
        required: true,
      },
      edit_height: {
        required: true,
        decimal: true,
      },
      edit_weight: {
        required: true,
        decimal: true,
      },
      // edit_performance_index: {
      //   required: true,
      // },
      // edit_smm: {
      //   required: true,
      // },
      // edit_note: {
      //   required: true,
      // }

    },
    messages: {
      edit_customer: {
        required: "<?php _e('Customer is required', 'sidf'); ?>"
      },
      edit_fasting: {
        required: "<?php _e('Fasting is required', 'sidf'); ?>"
      },
    },
    errorElement: "label",
    errorPlacement: function(error, element) {
      if (element.hasClass("select2-hidden-accessible")) {
        element = $("#select2-" + element.attr("id") + "-container").parent();
        error.insertAfter(element);
      } else {
        error.insertAfter(element);
      }
    },

  });

  jQuery(".edit-user-form").submit(function(e) {
    e.preventDefault();
    if (abcd.form() && abcd.form()) {
      $('#edit-customer').modal('hide');
      jQuery('#loader').show();
      jQuery('.submit-btn').attr('disabled', 'disabled');

      var fd = new FormData(jQuery('.edit-user-form')[0]);
      // process the form
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_edit_measurement_action',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          jQuery('#loader').hide();
          $('#edit-user-success').modal('show');



        });
    }

    return false;
  });
  jQuery('#edit-user-success').on('hidden.bs.modal', function() {
    location.reload();
  });

  jQuery(document).on("click", ".view-measurement", function(e) {
    e.preventDefault();

    var user_id = $(this).data('index');

    // process the form
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=view_trainer_measurement_action&user_id=' + user_id,
        data: '',
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        var obj = JSON.parse(result);
        jQuery('#view-measurement-details ul').html(obj.html);
        jQuery('#view-measurement-details').modal('show');



      });


    return false;
  });
</script>