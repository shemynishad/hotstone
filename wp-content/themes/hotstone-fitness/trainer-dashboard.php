<?php /* Template name: Trainer Dashboard */
if (is_user_logged_in()) {
  if (current_user_can('trainer')) {
    $current_user = wp_get_current_user();
  } elseif (current_user_can('administrator')) {
    wp_redirect(get_home_url('', '/admin-dashboard/'));
  } elseif (current_user_can('customer')) {
    wp_redirect(get_home_url('', '/customer-dashboard/'));
  } else {
    wp_redirect(get_home_url());
  }
} else {
  wp_redirect(get_home_url());
}
get_header('trainer'); ?>


<div id="content">
  <!-- section -->
  <div class="section dashboard">
    <div class="container">
      <div class="d-md-none mb-30">
        <h2 class="dashboard-title text-center mb-4">Trainer Dashboard Home</h2>
        <figure>
          <img src="<?= get_template_directory_uri(); ?>/assets/images/img1.jpg" alt="img" class="w-100 rounded1 shadow2">
        </figure>
      </div>
      <div class="row align-items-center flex-row-reverse">
        <div class="col-md-6 mb-4 mb-md-0 d-none d-md-block">
          <ul class="breadcrumb alt justify-content-end mb-0">
            <li>Home</li>
          </ul>
        </div>
        <div class="col-md-6">
          <h2 class="dashboard-title mb-2 mb-md-4">Upcoming Classes</h2>
        </div>
      </div>
      <div class="mb-30">
        <?php
        $class_id_array = array();
        $today = date('Ymd');
        $args = array(
          'post_type' => 'class',
          'posts_per_page' => 6,
          'meta_query' => array(
            array(
              'key'     => 'date',
              'compare' => '>=',
              'value'   => $today,
            ),
            array(
              'key'     => 'trainer',
              'value'   => $current_user->ID,
              'compare' => '=',
            )
          ),
        );
        $query = new WP_Query($args);
        if ($query->have_posts()) :
        ?>
          <div class="horizontal-scroll">
            <ul class="list1">
              <?php while ($query->have_posts()) : $query->the_post();
                $class_id_array[] = $query->post->ID;
              ?>
                <li>
                  <div class="card1">
                    <div class="inner-wrapper">
                      <ul class="details-list mb-0">
                        <li>
                          <div class="d-flex">
                            <span class="title">ID:</span>
                            <span class="text"><?= get_field('id'); ?></span>
                          </div>
                        </li>
                        <li>
                          <div class="d-flex">
                            <span class="title">Date:</span>
                            <?php $date = get_field('date'); ?>
                            <span class="text"><?= date("d F Y", strtotime($date)); ?></span>
                          </div>
                        </li>
                        <li>
                          <div class="d-flex">
                            <span class="title">Time:</span>
                            <span class="text"><?= get_field('start_time'); ?> - <?= get_field('end_time'); ?></span>
                          </div>
                        </li>
                        <li>
                          <div class="d-flex">
                            <span class="title">Workouts:</span>
                            <?php $workout_titles = array();
                            $workouts = explode(",", get_field('workouts'));
                            foreach ($workouts as $workout) :
                              $workout_titles[] = get_field('name', $workout);
                            endforeach; ?>
                            <span class="text"><?= implode(",", $workout_titles); ?></span>
                          </div>
                        </li>
                        <li>
                          <div class="d-flex">
                            <span class="title">Class:</span>
                            <span class="text"><?= get_field('class_title'); ?></span>
                          </div>
                        </li>
                        <li>
                          <div class="d-flex">
                            <span class="title">Type:</span>
                            <span class="text"><?= get_field('name', get_field('membership')); ?></span>
                          </div>
                        </li>
                        <li>
                          <div class="d-flex">
                            <span class="title">Bookings:</span>
                            <?php
                            $class_array = array(
                              'key'     => 'class',
                              'value'   => $query->post->ID,
                              'compare' => '='
                            );

                            $args = array(
                              'post_type' => 'booking',
                              'numberposts' => -1,
                              'meta_query' => array(
                                $class_array
                              )

                            );
                            $latest_class = get_posts($args);
                            ?>
                            <span class="text"><?= count($latest_class); ?>/<?= get_field('no_of_slots'); ?></span>
                          </div>
                        </li>
                      </ul>
                      <div class="row align-items-center options">
                        <?php
                        $formatted_date =  date("m/d/Y", strtotime($date));
                        $data = array(
                          'class_title' => get_field('class_title'),
                          'class_title_ar' => get_field('class_title_in_arabic'),
                          'date' => $formatted_date,
                          'start_time' => get_field('start_time'),
                          'end_time' => get_field('end_time'),
                          'type' => get_field('membership'),
                          'no_of_slots' => get_field('no_of_slots'),
                          'trainer' => get_field('trainer'),
                          'workouts' => get_field('workouts')
                        ); ?>
                        <div class="col-auto">
                          <button type="button" class="edit-class" data-index="<?= $query->post->ID; ?>" data-details='<?php echo json_encode($data); ?>'><i class="circle-icon1"><img src="<?= get_template_directory_uri(); ?>/assets/images/edit.svg" alt="icon"></i></button>
                        </div>
                        <div class="col-auto">
                          <button type="button" class="delete-class" data-index="<?= $query->post->ID; ?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri(); ?>/assets/images/delete.svg" alt="icon"></i></button>
                        </div>
                        <div class="col-auto">
                          <a href="<?php the_permalink(); ?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri(); ?>/assets/images/eye-icon.svg" alt="icon"></i></a>
                        </div>
                        <?php $disable = get_field('is_disable'); ?>
                        <div class="col text-md-end">
                          <label class="toggle1 alt">
                            <input type="checkbox" class="disable-class" <?php if (!$disable) {
                                                                            echo 'checked';
                                                                          } ?> value="<?= $query->post->ID; ?>">
                            <span class="slider"></span>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              <?php endwhile; ?>
            </ul>
          </div>
          <div class="text-md-end">
            <a href="<?= home_url('/trainer-class/'); ?>" class="link1">View all upcoming classes</a>
          </div>
        <?php else : ?>
          <p>No upcoming class found</p>
        <?php endif; ?>
      </div>
      <div class="mb-30">
        <h2 class="dashboard-title mb-2 mb-md-4">Upcoming bookings</h2>
        <?php

        $today = date('Ymd');
        $args = [];
        if(count($class_id_array) > 0){
          $args = array(
            'post_type' => 'booking',
            'posts_per_page' => 6,
            'meta_query' => array(
              array(
                'key'     => 'class',
                'value'   => $class_id_array,
                'compare' => 'IN'
              )
            ),
          );
        }
        $query = new WP_Query($args);
        if ($query->have_posts()) :
        ?>
          <div class="horizontal-scroll">
            <ul class="list1">
              <?php while ($query->have_posts()) : $query->the_post();

              ?>
                <li>
                  <div class="card1 check-item">
                    <div class="inner-wrapper">

                      <ul class="details-list mb-3 mb-md-0">
                        <li>
                          <div class="d-flex">
                            <span class="title">ID:</span>
                            <span class="text"><?= get_field('id'); ?></span>
                          </div>
                        </li>
                        <li>
                          <div class="d-flex">
                            <span class="title">Customer:</span>
                            <?php $customer_id = get_field('customer');
                            $author_obj = get_user_by('id', $customer_id); ?>
                            <span class="text"><?= $author_obj->user_firstname . ' ' . $author_obj->user_lastname; ?></span>

                          </div>
                        </li>
                        <li>
                          <div class="d-flex">
                            <span class="title">Booking date:</span>
                            <?php $booking_date = get_field('booking_date'); ?>
                            <span class="text"><?= date("d F Y", strtotime($booking_date)); ?></span>

                          </div>
                        </li>
                        <li>
                          <div class="d-flex">
                            <span class="title">Class:</span>
                            <span class="text"><?= get_field('id', get_field('class')); ?></span>
                          </div>
                        </li>
                        <li>
                        <div class="d-flex">
                          <span class="title">Class Title:</span>
                          <span class="text"><?= get_field('class_title', get_field('class')); ?></span>
                        </div>
                      </li>
                        <li>
                          <div class="d-flex">
                            <span class="title">Type:</span>
                            <span class="text"><?= get_field('name', get_field('membership', get_field('class'))); ?></span>
                          </div>
                        </li>
                        <li>
                          <div class="d-flex">
                            <span class="title">Created by:</span>
                            <span class="text"><?= get_field('created_by'); ?></span>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </li>
              <?php endwhile; ?>
            </ul>
          </div>
          <div class="text-md-end">
            <a href="<?= home_url('/trainer-bookings/'); ?>" class="link1">View all upcoming bookings</a>
          </div>
        <?php else : ?>
          <p>No upcoming bookings found</p>
        <?php endif; ?>
      </div>
      <div class="mb-30">
        <h2 class="dashboard-title mb-2 mb-md-4">Upcoming measurements</h2>
        <?php
        $args = array(
          'post_type' => 'measurement',
          'posts_per_page' => 6,
          'meta_query' => array(
            array(
              'key'     => 'trainer',
              'value'   => $current_user->ID,
              'compare' => '=',
            ),
          ),
        );
        $query = new WP_Query($args);
        if ($query->have_posts()) :
        ?>
          <div class="horizontal-scroll">
            <ul class="list1">
              <?php while ($query->have_posts()) : $query->the_post();
                $customer_id = get_field('customer');
              ?>
                <li>
                  <div class="card1">
                    <div class="inner-wrapper">
                      <ul class="details-list mb-0">
                        <li>
                          <div class="d-flex">
                            <span class="title">ID:</span>
                            <span class="text"><?= get_field('id'); ?></span>
                          </div>
                        </li>
                        <li>
                          <div class="d-flex">
                            <span class="title">Date:</span>
                            <?php $date = get_field('date'); ?>
                            <span class="text"><?= date("d F Y", strtotime($date)); ?></span>
                          </div>
                        </li>
                        <li>
                          <div class="d-flex">
                            <span class="title">Customer</span>
                            <?php $trainer_id = get_field('customer');
                            $author_obj = get_user_by('id', $trainer_id); ?>
                            <span class="text"><?= $author_obj->user_firstname . ' ' . $author_obj->user_lastname; ?></span>
                          </div>
                        </li>
                      </ul>
                      <div class="row align-items-center options">
                        <?php
                        $data = array(
                          'customer' => $customer_id,
                          'trainer' => $trainer_id,
                          'fasting' => get_field('fasting'),
                          'height'  => get_field('height'),
                          'weight'  => get_field('weight'),
                          'performance_index'  => get_field('performance_index'),
                          'smm'  => get_field('smm'),
                          'notes'  => get_field('notes'),
                          'notes_ar'  => get_field('notes_in_arabic'),
                        ); ?>
                        <div class="col-auto">
                          <button type="button" class="edit-user" data-index="<?= $query->post->ID; ?>" data-details='<?php echo json_encode($data); ?>'><i class="circle-icon1"><img src="<?= get_template_directory_uri(); ?>/assets/images/edit.svg" alt="icon"></i></button>
                        </div>
                        <div class="col-auto">
                          <button type="button" class="delete-user" data-index="<?= $query->post->ID; ?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri(); ?>/assets/images/delete.svg" alt="icon"></i></button>
                        </div>
                        <div class="col-auto">
                          <button type="button" class="view-measurement" data-index="<?= $query->post->ID; ?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri(); ?>/assets/images/eye-icon.svg" alt="icon"></i></button>
                        </div>
                        <?php $disable = get_field('is_disable'); ?>
                        <div class="col text-md-end">
                          <label class="toggle1 alt">
                            <input type="checkbox" class="disable-user" <?php if (!$disable) {
                                                                          echo 'checked';
                                                                        } ?> value="<?= $query->post->ID; ?>">
                            <span class="slider"></span>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              <?php endwhile; ?>
            </ul>
          </div>
          <div class="text-md-end">
            <a href="<?= home_url('/trainer-measurement/'); ?>" class="link1">View all recent measurements</a>
          </div>
        <?php else : ?>
          <p>No measurement found</p>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <!-- /section -->
</div>




<div class="modal2 modal fade" id="view-measurement-details" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>View measurement details</h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <ul class="details-list d-flex mb-0">

          </ul>
        </div>
      </div>
      <footer class="modal-footer d-md-none justify-content-center">

      </footer>
    </div>
  </div>
</div>


<div class="modal2 modal fade" id="edit-customer-class" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>Add class</h3>
      </header>
      <div class="modal-body">
        <div class="form1">
          <form action="#" class="edit-class-form">
            <div class="row">
              <div class="col-md-6 form-group">
                <label class="form-label">Class Title (EN)</label>
                <input type="text" class="form-control edit_class_title" name="edit_class_title" placeholder="Enter Class Title">
              </div>
              <div class="col-md-6 form-group ar">
                <label class="form-label">Class Title (AR)</label>
                <input type="text" class="form-control edit_class_title_ar" name="edit_class_title_ar" placeholder="Enter Class title">
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">Select Date</label>
                <div class="date-picker end-date-picker further-date">
                  <input type="text" class="form-control edit_date" name="edit_date" placeholder="Select Date">
                </div>
              </div>
              <div class="col-md-3 form-group">
                <label class="form-label">Start Time</label>
                <div class="selectbox">
                  <select class="form-control edit_start_time" name="edit_start_time">
                    <option value="">Select Time</option>
                    <option value="00:00">00:00</option>
                    <option value="00:30">00:30</option>
                    <option value="01:00">01:00</option>
                    <option value="01:30">01:30</option>
                    <option value="02:00">02:00</option>
                    <option value="02:30">02:30</option>
                    <option value="03:00">03:00</option>
                    <option value="03:30">03:30</option>
                    <option value="04:00">04:00</option>
                    <option value="04:30">04:30</option>
                    <option value="05:00">05:00</option>
                    <option value="05:30">05:30</option>
                    <option value="06:00">06:00</option>
                    <option value="06:30">06:30</option>
                    <option value="07:00">07:00</option>
                    <option value="07:30">07:30</option>
                    <option value="08:00">08:00</option>
                    <option value="08:30">08:30</option>
                    <option value="09:00">09:00</option>
                    <option value="09:30">09:30</option>
                    <option value="10:00">10:00</option>
                    <option value="10:30">10:30</option>
                    <option value="11:00">11:00</option>
                    <option value="11:30">11:30</option>
                    <option value="12:00">12:00</option>
                    <option value="12:30">12:30</option>
                    <option value="13:00">13:00</option>
                    <option value="13:30">13:30</option>
                    <option value="14:00">14:00</option>
                    <option value="14:30">14:30</option>
                    <option value="15:00">15:00</option>
                    <option value="15:30">15:30</option>
                    <option value="16:00">16:00</option>
                    <option value="16:30">16:30</option>
                    <option value="17:00">17:00</option>
                    <option value="17:30">17:30</option>
                    <option value="18:00">18:00</option>
                    <option value="18:30">18:30</option>
                    <option value="19:00">19:00</option>
                    <option value="19:30">19:30</option>
                    <option value="20:00">20:00</option>
                    <option value="20:30">20:30</option>
                    <option value="21:00">21:00</option>
                    <option value="21:30">21:30</option>
                    <option value="22:00">22:00</option>
                    <option value="22:30">22:30</option>
                    <option value="23:00">23:00</option>
                    <option value="23:30">23:30</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3 form-group">
                <label class="form-label">End Time</label>
                <div class="selectbox">
                  <select class="form-control edit_end_time" name="edit_end_time">
                    <option value="">Select Time</option>
                    <option value="00:00">00:00</option>
                    <option value="00:30">00:30</option>
                    <option value="01:00">01:00</option>
                    <option value="01:30">01:30</option>
                    <option value="02:00">02:00</option>
                    <option value="02:30">02:30</option>
                    <option value="03:00">03:00</option>
                    <option value="03:30">03:30</option>
                    <option value="04:00">04:00</option>
                    <option value="04:30">04:30</option>
                    <option value="05:00">05:00</option>
                    <option value="05:30">05:30</option>
                    <option value="06:00">06:00</option>
                    <option value="06:30">06:30</option>
                    <option value="07:00">07:00</option>
                    <option value="07:30">07:30</option>
                    <option value="08:00">08:00</option>
                    <option value="08:30">08:30</option>
                    <option value="09:00">09:00</option>
                    <option value="09:30">09:30</option>
                    <option value="10:00">10:00</option>
                    <option value="10:30">10:30</option>
                    <option value="11:00">11:00</option>
                    <option value="11:30">11:30</option>
                    <option value="12:00">12:00</option>
                    <option value="12:30">12:30</option>
                    <option value="13:00">13:00</option>
                    <option value="13:30">13:30</option>
                    <option value="14:00">14:00</option>
                    <option value="14:30">14:30</option>
                    <option value="15:00">15:00</option>
                    <option value="15:30">15:30</option>
                    <option value="16:00">16:00</option>
                    <option value="16:30">16:30</option>
                    <option value="17:00">17:00</option>
                    <option value="17:30">17:30</option>
                    <option value="18:00">18:00</option>
                    <option value="18:30">18:30</option>
                    <option value="19:00">19:00</option>
                    <option value="19:30">19:30</option>
                    <option value="20:00">20:00</option>
                    <option value="20:30">20:30</option>
                    <option value="21:00">21:00</option>
                    <option value="21:30">21:30</option>
                    <option value="22:00">22:00</option>
                    <option value="22:30">22:30</option>
                    <option value="23:00">23:00</option>
                    <option value="23:30">23:30</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6 form-group mb-0">
                <label class="form-label">Select Type</label>
                <div class="selectbox">
                  <select class="form-control edit_membership" disabled="disabled" name="edit_membership">
                    <!-- <option value="">Select Membership Type</option> -->
                    <?php
                    $args = array(
                      'post_type' => 'membership_types',
                      'posts_per_page' => -1
                    );
                    $query = new WP_Query($args);
                    if ($query->have_posts()) :
                      while ($query->have_posts()) : $query->the_post();
                    ?>
                        <option value="<?= $query->post->ID; ?>"><?= get_field('name'); ?></option>

                    <?php endwhile;
                    endif; ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">No of Slots</label>
                <input type="text" class="form-control edit_no_of_slots" name="edit_no_of_slots" placeholder="Enter No of Slots">
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">Select Workouts</label>
                <div class="custom-selectbox">
                  <div class="form-control p-0 h-auto">
                    <select class="chosen-select edit_workout" multiple name="edit_workout[]" data-placeholder="Type workouts name">
                      <!-- <option value="">Select Workouts</option> -->
                      <?php
                      $args = array(
                        'numberposts' => -1,
                        'post_type'   => 'workout'
                      );

                      $latest_books = get_posts($args);
                      foreach ($latest_books as $member) :
                      ?>
                        <option value="<?= $member->ID; ?>"><?= $member->post_title; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="button-group d-none d-md-flex">
              <input type="hidden" class="form-control" name="edit_trainer" value="<?= $current_user->ID; ?>">
              <input type="hidden" name="user_id" class="edit_user_id" value="">
              <button class="btn button6-outline rounded" type="button" data-bs-dismiss="modal">Cancel</button>
              <button class="btn button1 rounded submit-btn" type="submit">Update</button>
            </div>
          </form>
        </div>
      </div>
      <footer class="modal-footer d-md-none justify-content-center">
        <div class="button-group">
          <button class="btn button6 rounded" type="button" data-bs-dismiss="modal">Cancel</button>
          <button class="btn button1 rounded submit-btn" type="submit">Update</button>
        </div>
      </footer>
    </div>
  </div>
</div>


<div class="modal2 modal fade" id="disable-enable" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <h3></h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <div class="content-body"></div>
        </div>
      </div>
      <footer class="modal-footer justify-content-center">
        <div class="button-group">
          <button class="btn button6 rounded disable-enable-close" type="button">No</button>
          <button class="btn button1 rounded submit-booking disable-enable-submit" type="button">Yes</button>
          <input type="hidden" class="disable-user-id" value="">
          <input type="hidden" class="disable-enable-id" value="">
        </div>
      </footer>
    </div>
  </div>
</div>

<div class="modal2 modal fade" id="disable-enable-success" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3></h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <div class="content-body"></div>
        </div>
      </div>
      <footer class="modal-footer justify-content-center">
        <div class="button-group">
          <button class="btn button6 rounded" data-bs-dismiss="modal" type="button">OK</button>
        </div>
      </footer>
    </div>
  </div>
</div>

<div class="modal2 modal fade" id="delete-class-confirm" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>Delete Class</h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <h3>Are you sure want to delete this Class ?</h3>
        </div>
      </div>
      <footer class="modal-footer justify-content-center">
        <div class="button-group">
          <button data-bs-dismiss="modal" class="btn button6 rounded">No</button>
          <button class="btn button1 rounded delete-class-submit" type="button">Yes</button>
          <input type="hidden" class="delete-class-id">
        </div>
      </footer>
    </div>
  </div>
</div>

<div class="modal2 modal fade" id="delete-class-success" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>Delete Class</h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <h3>Class has been successfully deleted</h3>
        </div>
      </div>
      <footer class="modal-footer justify-content-center">
        <div class="button-group">
          <button data-bs-dismiss="modal" class="btn button6 rounded">Ok</button>
        </div>
      </footer>
    </div>
  </div>
</div>

<div class="modal2 modal fade" id="edit-class-success" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>Edit Class</h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <h3>Class has been successfully updated</h3>
        </div>
      </div>
      <footer class="modal-footer justify-content-center">
        <div class="button-group">
          <button data-bs-dismiss="modal" class="btn button6 rounded">Ok</button>
        </div>
      </footer>
    </div>
  </div>
</div>


<div class="modal2 modal fade" id="edit-customer-measurement" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>Edit Measurement</h3>
      </header>
      <div class="modal-body">
        <div class="form1">
          <form action="#" class="edit-measurement-form">
            <div class="row">
              <div class="col-12 form-group">
                <label class="form-label">Choose Customer</label>
                <div class="selectbox sel-edit-customer">
                  <select class="form-control edit_customer" name="edit_customer" id="edit_customer">
                    <option value="">Choose Customer</option>
                    <?php
                    $team_presidents = get_users(array('role__in' => array('customer'), 'number' => 9999));
                    foreach ($team_presidents as $team_president) :
                    ?>
                      <option value="<?= $team_president->ID; ?>"><?= $team_president->user_firstname . ' ' . $team_president->user_lastname; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="col-12 form-group">
                <label class="form-label">Fasting</label>
                <div class="row">
                  <div class="col-auto">
                    <label class="radio1">
                      <input type="radio" class="edit_fasting" name="edit_fasting" value="Yes">
                      <span class="inner">
                        <i class="fa fa-solid fa-thumbs-up"></i>
                      </span>
                    </label>
                  </div>
                  <div class="col-auto">
                    <label class="radio1">
                      <input type="radio" class="edit_fasting" name="edit_fasting" value="No">
                      <span class="inner">
                        <i class="fa fa-solid fa-thumbs-down"></i>
                      </span>
                    </label>
                  </div>
                </div>
                <label for="edit_fasting" class="error"></label>
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">Height</label>
                <input type="text" class="form-control edit_height" name="edit_height" placeholder="Enter Height">
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">Weight</label>
                <input type="text" class="form-control edit_weight" name="edit_weight" placeholder="Enter Weight">
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">Performance Index</label>
                <input type="text" class="form-control edit_performance_index" name="edit_performance_index" placeholder="Enter Performance Index">
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">SMM</label>
                <input type="text" class="form-control edit_smm" name="edit_smm" placeholder="Enter SMM">
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">Note (EN)</label>
                <textarea class="form-control edit_note" name="edit_note" placeholder="Enter Note"></textarea>
              </div>
              <div class="col-md-6 form-group ar">
                <label class="form-label">Note (AR)</label>
                <textarea class="form-control edit_note_ar" name="edit_note_ar" placeholder="Enter Note"></textarea>
              </div>
            </div>
            <div class="button-group d-none d-md-flex">
              <input type="hidden" class="form-control" name="edit_trainer" value="<?= $current_user->ID; ?>">
              <input type="hidden" name="user_id" class="edit_user_id" value="">
              <button class="btn button6-outline rounded" type="button" data-bs-dismiss="modal">Cancel</button>
              <button class="btn button1 rounded submit-btn" type="submit">Update</button>
            </div>
          </form>
        </div>
      </div>
      <footer class="modal-footer d-md-none justify-content-center">
        <div class="button-group">
          <button class="btn button6 rounded" type="button" data-bs-dismiss="modal">Cancel</button>
          <button class="btn button1 rounded submit-btn" type="submit">Update</button>
        </div>
      </footer>
    </div>
  </div>
</div>


<div class="modal2 modal fade" id="edit-measurement-success" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>Edit Measurement</h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <h3>Measurement has been successfully updated</h3>
        </div>
      </div>
      <footer class="modal-footer justify-content-center">
        <div class="button-group">
          <button data-bs-dismiss="modal" class="btn button6 rounded">Ok</button>
        </div>
      </footer>
    </div>
  </div>
</div>

<div class="modal2 modal fade" id="delete-confirm" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>Delete Measurement</h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <h3>Are you sure want to delete this Measurment ?</h3>
        </div>
      </div>
      <footer class="modal-footer justify-content-center">
        <div class="button-group">
          <button data-bs-dismiss="modal" class="btn button6 rounded">No</button>
          <button class="btn button1 rounded delete-submit" type="button">Yes</button>
          <input type="hidden" class="delete-user-id">
        </div>
      </footer>
    </div>
  </div>
</div>

<div class="modal2 modal fade" id="delete-success" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>Delete Measurement</h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <h3>Measurement has been successfully deleted</h3>
        </div>
      </div>
      <footer class="modal-footer justify-content-center">
        <div class="button-group">
          <button data-bs-dismiss="modal" class="btn button6 rounded">Ok</button>
        </div>
      </footer>
    </div>
  </div>
</div>



<?php get_footer('trainer');?>
<script>
  jQuery(".view-subscription").click(function(e) {
    e.preventDefault();

    var user_id = $(this).data('index');

    // process the form
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=view_customer_subscription_action&user_id=' + user_id,
        data: '',
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        var obj = JSON.parse(result);
        jQuery('#view-subcription-details ul').html(obj.html);
        jQuery('#view-subcription-details').modal('show');



      });


    return false;
  });

  $('.disable-class').change(function() {
    var disable = 1;
    $('#disable-enable h3').html('Disable Class');
    $('#disable-enable .content-body').html('<h3>Are you sure to disable this Class ?</h3>');
    $('#disable-enable-success h3').html('Disable Class');
    $('#disable-enable-success .content-body').html('<h3>Class is successfully disabled</h3>');
    if (this.checked) {
      disable = 0;
      $('#disable-enable h3').html('Enable Class');
      $('#disable-enable .content-body').html('<h3>Are you sure to enable this Class ?</h3>');
      $('#disable-enable-success h3').html('Enable Class');
      $('#disable-enable-success .content-body').html('<h3>Class is successfully enabled</h3>');

    }
    var user_id = this.value;
    $('.disable-user-id').val(user_id);
    $('.disable-enable-id').val(disable);
    $('#disable-enable').modal('show');

  });

  $(document).on("click", ".disable-enable-close", function() {
    var user_id = $('.disable-user-id').val();
    var disable = $('.disable-enable-id').val();
    if (disable == 1) {
      $("input[type=checkbox][value=" + user_id + "]").prop("checked", true);
    } else {
      $("input[type=checkbox][value=" + user_id + "]").prop("checked", false);
    }
    $('#disable-enable').modal('hide');
  });

  $(document).on("click", ".disable-enable-submit", function() {
    var user_id = $('.disable-user-id').val();
    var disable = $('.disable-enable-id').val();
    $('#disable-enable').modal('hide');
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_enable_disable_member_action&postid=' + user_id + '&disable=' + disable,
        data: '',
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        $('#disable-enable-success').modal('show');

      });
  });

  $(document).on("click", ".delete-class", function() {
    var user_id = $(this).data('index');
    $('.delete-class-id').val(user_id);
    $('#delete-class-confirm').modal('show');
  });

  $(document).on("click", ".delete-class-submit", function() {
    var user_id = $('.delete-class-id').val();
    $('#delete-class-confirm').modal('hide');
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=delete_member_action&id=' + user_id,
        data: '',
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        $('#delete-class-success').modal('show');

      });
  });

  jQuery('#delete-class-success').on('hidden.bs.modal', function() {
    location.reload();
  });


  $(document).on("click", ".edit-class", function() {
    var user_id = $(this).data('index');
    var details = $(this).data('details');
    $('.edit-class-form .edit_user_id').val(user_id);
    $('.edit_class_title').val(details.class_title);
    $('.edit_class_title_ar').val(details.class_title_ar);
    $('.edit_date').val(details.date);
    $('.edit_start_time').val(details.start_time);
    $('.edit_end_time').val(details.end_time);
    //$('input[name=edit_type][value="'+details.type+'"]').prop("checked",true);
    $('.edit_membership').val(details.type);
    $('.edit_no_of_slots').val(details.no_of_slots);
    var str_array = details.workouts.split(',');
    $(".edit_workout").val(str_array).trigger("chosen:updated");
    $('#edit-customer-class').modal('show');

  });

  var abcd = jQuery('.edit-class-form').validate({
    // ignore: ".ignore",
    // onfocusout: function(element) {
    //   $(element).valid()
    // },
    // onkeyup: false,
    ignore: [],
    rules: {
      edit_trainer: {

        required: true,
      },
      edit_class_title: {

        required: true,
      },
      edit_date: {
        required: true,
      },
      edit_start_time: {
        required: true,
      },
      edit_end_time: {
        required: true,
      },
      edit_type: {
        required: true,
      },
      edit_no_of_slots: {
        required: true,
      },
      'edit_workout[]': {
        required: true,
      }

    }

  });

  jQuery(".edit-class-form").submit(function(e) {
    e.preventDefault();
    if (abcd.form() && abcd.form()) {
      $('#edit-customer-class').modal('hide');
      jQuery('#loader').show();
      jQuery('.submit-btn').attr('disabled', 'disabled');

      var fd = new FormData(jQuery('.edit-class-form')[0]);
      // process the form
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_edit_class_action',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          jQuery('#loader').hide();
          $('#edit-class-success').modal('show');



        });
    }

    return false;
  });
  jQuery('#edit-class-success').on('hidden.bs.modal', function() {
    location.reload();
  });



  $(document).on("click", ".edit-user", function() {
    var user_id = $(this).data('index');
    var details = $(this).data('details');
    $('.edit-measurement-form .edit_user_id').val(user_id);
    $('.edit-measurement-form .edit_customer').val(details.customer).trigger('change');
    $('input[name=edit_fasting][value="' + details.fasting + '"]').prop("checked", true);
    $('.edit_height').val(details.height);
    $('.edit_weight').val(details.weight);
    $('.edit_performance_index').val(details.performance_index);
    $('.edit_smm').val(details.smm);
    $('.edit_note').val(details.notes);
    $('.edit_note_ar').val(details.notes_ar);
    $('#edit-customer-measurement').modal('show');

  });
  $(document).ready(function() {
    $(".edit_customer").select2({
      dropdownParent: $(".sel-edit-customer")
    });
  });
  $.validator.addMethod('filesize', function(value, element, param) {
    return this.optional(element) || (element.files[0].size <= param * 1000000)
  }, 'File size must be less than {0} MB');
  $.validator.addMethod('decimal', function(value, element) {
    return this.optional(element) || /^((\d+(\\.\d{0,2})?)|((\d*(\.\d{1,2}))))$/.test(value);
  }, "Please enter a correct number");
  var abcde = jQuery('.edit-measurement-form').validate({
    // ignore: ".ignore",
    // onfocusout: function(element) {
    //   $(element).valid()
    // },
    // onkeyup: false,
    ignore: [],
    rules: {
      edit_trainer: {

        required: true,
      },
      edit_customer: {

        required: true,
      },
      edit_fasting: {
        required: true,
      },
      edit_height: {
        required: true,
        decimal: true,
      },
      edit_weight: {
        required: true,
        decimal: true,
      },
      // edit_performance_index: {
      //   required: true,
      // },
      // edit_smm: {
      //   required: true,
      // },
      // edit_note: {
      //   required: true,
      // }

    },
    messages: {
      edit_customer: {
        required: "<?php _e('Customer is required', 'sidf'); ?>"
      },
      edit_fasting: {
        required: "<?php _e('Fasting is required', 'sidf'); ?>"
      },
    },
    errorElement: "label",
    errorPlacement: function(error, element) {
      if (element.hasClass("select2-hidden-accessible")) {
        element = $("#select2-" + element.attr("id") + "-container").parent();
        error.insertAfter(element);
      } else {
        error.insertAfter(element);
      }
    },

  });

  jQuery(".edit-measurement-form").submit(function(e) {
    e.preventDefault();
    if (abcde.form() && abcde.form()) {
      $('#edit-customer-measurement').modal('hide');
      jQuery('#loader').show();
      jQuery('.submit-btn').attr('disabled', 'disabled');

      var fd = new FormData(jQuery('.edit-measurement-form')[0]);
      // process the form
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_edit_measurement_action',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          jQuery('#loader').hide();
          $('#edit-measurement-success').modal('show');



        });
    }

    return false;
  });
  jQuery('#edit-measurement-success').on('hidden.bs.modal', function() {
    location.reload();
  });


  $(document).on('change', '.disable-user', function() {
    var disable = 1;
    $('#disable-enable h3').html('Disable Class');
    $('#disable-enable .content-body').html('<h3>Are you sure to disable this Measurement ?</h3>');
    $('#disable-enable-success h3').html('Disable Class');
    $('#disable-enable-success .content-body').html('<h3>Measurement is successfully disabled</h3>');
    if (this.checked) {
      disable = 0;
      $('#disable-enable h3').html('Enable Class');
      $('#disable-enable .content-body').html('<h3>Are you sure to enable this Class ?</h3>');
      $('#disable-enable-success h3').html('Enable Class');
      $('#disable-enable-success .content-body').html('<h3>Measurement is successfully enabled</h3>');

    }
    var user_id = this.value;
    $('.disable-user-id').val(user_id);
    $('.disable-enable-id').val(disable);
    $('#disable-enable').modal('show');

  });

  $(document).on("click", ".disable-enable-close", function() {
    var user_id = $('.disable-user-id').val();
    var disable = $('.disable-enable-id').val();
    if (disable == 1) {
      $("input[type=checkbox][value=" + user_id + "]").prop("checked", true);
    } else {
      $("input[type=checkbox][value=" + user_id + "]").prop("checked", false);
    }
    $('#disable-enable').modal('hide');
  });

  $(document).on("click", ".disable-enable-submit", function() {
    var user_id = $('.disable-user-id').val();
    var disable = $('.disable-enable-id').val();
    $('#disable-enable').modal('hide');
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_enable_disable_member_action&postid=' + user_id + '&disable=' + disable,
        data: '',
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        $('#disable-enable-success').modal('show');

      });
  });

  $(document).on("click", ".delete-user", function() {
    var user_id = $(this).data('index');
    $('.delete-user-id').val(user_id);
    $('#delete-confirm').modal('show');
  });

  $(document).on("click", ".delete-submit", function() {
    var user_id = $('.delete-user-id').val();
    $('#delete-confirm').modal('hide');
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=delete_member_action&id=' + user_id,
        data: '',
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        $('#delete-success').modal('show');

      });
  });

  jQuery('#delete-success').on('hidden.bs.modal', function() {
    location.reload();
  });
  jQuery(document).on("click", ".view-measurement", function(e) {
    e.preventDefault();

    var user_id = $(this).data('index');

    // process the form
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=view_trainer_measurement_action&user_id=' + user_id,
        data: '',
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        var obj = JSON.parse(result);
        jQuery('#view-measurement-details ul').html(obj.html);
        jQuery('#view-measurement-details').modal('show');



      });


    return false;
  });
</script>