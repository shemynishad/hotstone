<?php /* Template name: Customer Login */
if ( is_user_logged_in() ) {
        wp_redirect(get_home_url('','/customer-dashboard/'));     
}
get_header('customer');?>


      <!-- content -->
      <div id="content">
        <!-- section -->
        <div class="section">
          <div class="container py-xl-5">
            <div class="col-xxl-10 mx-auto px-xxl-4">
              <div class="box2 user-login-mobile h-100">
                <div class="row g-0">
                  <div class="col-md-6">
                    <div class="inner-wrapper h-100 d-flex flex-column">
                      <figure class="logo text-center text-md-start">
                        <img src="<?= get_template_directory_uri();?>/assets/images/logo.svg" alt="icon">
                      </figure>
                      <div class="form1 alt my-auto py-4">
                        <div class="text-center mb-30">
                          <h3 class="mb-3">User login</h3>
                          <div class="d-md-none">
                            <p>Welcome back,<br> Sign in to continue loem ipsum dolor sit amet</p>
                          </div>
                        </div>
                         <?= customer_form_login();?>
                      </div>
                      <!-- <p class="mb-0">Don’t have an account? <a href="#" class="link">Create account</a></p> -->
                    </div>
                  </div>
                  <div class="col-md-6 d-none d-md-block">
                    <figure class="h-100">
                      <img src="<?= get_template_directory_uri();?>/assets/images/img2.jpg" alt="img" class="w-100 h-100 object-cover">
                    </figure>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /section -->
      </div>
      <?php get_footer('customer');?>
<script>
     jQuery(window).on('load', function(){ 
<?php if(isset($_REQUEST['login'])){?>
<?php if($_REQUEST['login']=="failed"){?>
    jQuery("#loginerror").css("display", "block");
    jQuery("#loginerror").html("<?php _e('Invalid username or password','sidf');?>");
    <?php } ?>
    <?php if($_REQUEST['login']=="disabled"){?>
    jQuery("#loginerror").css("display", "block");
    jQuery("#loginerror").html("You account have been disabled");
    <?php } ?>
    <?php } ?>
    var $src = $('.src'),
        $dst = $('.dest');
    $src.on('input', function () {
        $dst.val($src.val());
    });
    var $src1 = $('.src1'),
        $dst1 = $('.dest1');
    $src1.on('input', function () {
        $dst1.val($src1.val());
    });
        jQuery('#login-form').validate({
        onfocusout: false,
        onkeyup: false,
        ignore: [],
        rules:
        {
            log:
            {

                required: true



            },
            pwd:
            {

                required: true

            }
        }

    });
});
    
</script>

