<?php

//Theme Support
add_theme_support('post-thumbnails');
add_theme_support('woocommerce');
add_theme_support('automatic-feed-links');
add_image_size( 'hotstone-thumbnail', 312, 312, array( 'left', 'top' ) ); 
add_image_size( 'boystown-thumbnail', 100, 80, array( 'left', 'top' ) ); 
add_image_size( 'boystown-event-thumbnail', 253, 193, array( 'center', 'center' ) ); 

// Register Navigation Menus
if (function_exists('register_nav_menus'))
    register_nav_menus(array('main_menu'=>'Main Menu','footer_left_menu'=>'Footer Left Menu','footer_middle_menu'=>'Footer Middle Menu'));

/* Add Css to nav menu */    
	
  function theme_nav_menu_css_class($classes, $item = null, $args = null) {
	if (in_array('current-menu-item', $classes, true))
		$classes[] = 'active';
	if (in_array('current-post-parent', $classes, true))
		$classes[] = 'active';
    if (in_array('current-menu-parent', $classes, true))
		$classes[] = 'active';
	if (in_array('menu-item-has-children', $classes, true))
		$classes[] = 'parent';
	if(is_singular('groups')):
	    if (in_array('menu-item-547', $classes, true)):
		$classes[] = 'active';
		endif;
	endif;
	return $classes;
}
add_filter('nav_menu_css_class', 'theme_nav_menu_css_class'); 


register_sidebar(array(
'name' => 'Sidebar',
'id' => 'sidebar',
'description' => 'Sidebar',
'class' => 'Sidebar',
'before_widget' => '<div id="%1$s" class="widget %2$s">',
'after_widget' => '</div>',
'before_title' => '<h3>',
'after_title' => '</h3>',

));


/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'hotstone_scripts' );
function hotstone_scripts() {
    if(ICL_LANGUAGE_CODE=='ar'):
    wp_enqueue_style( 'bootstrap-rtl', get_template_directory_uri() . '/assets/stylesheets/bootstrap-rtl.min.css', array(), '', 'all' );
	else:
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/stylesheets/bootstrap.min.css', array(), '', 'all' );
	endif;	
	wp_enqueue_style( 'Noto-rtl', 'https://fonts.googleapis.com/css2?family=Noto+Kufi+Arabic:wght@100;300;400;500;600;700;800;900&display=swap', array(), '', 'all' );
    if(is_front_page()):
    wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/assets/stylesheets/font-awesome.min.css', array(), '', 'all' );
    wp_enqueue_style( 'owl-carousel-css', get_template_directory_uri() . '/assets/stylesheets/owl.carousel-min.css', array(), '', 'all' );
    wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/assets/stylesheets/fancybox.css', array(), '', 'all' );
    wp_enqueue_style( 'fontawesome-css', get_template_directory_uri() . '/assets/stylesheets/font-awesome.min.css', array(), '', 'all' );
	if(ICL_LANGUAGE_CODE=='ar'):
    wp_enqueue_style( 'home-arcss', get_template_directory_uri() . '/assets/stylesheets/home-rtl.css', array(), '', 'all' );
	else:
	wp_enqueue_style( 'home-css', get_template_directory_uri() . '/assets/stylesheets/home.css', array(), '', 'all' );
	endif;
    else:
    wp_enqueue_style( 'jquery-ui', get_template_directory_uri() . '/assets/stylesheets/jquery-ui.min.css', array(), '', 'all' );
	wp_enqueue_style( 'fontawesome-css', get_template_directory_uri() . '/assets/stylesheets/font-awesome.min.css', array(), '', 'all' );
	wp_enqueue_style( 'chosen-css', get_template_directory_uri() . '/assets/stylesheets/chosen.min.css', array(), '', 'all' );
	if(ICL_LANGUAGE_CODE=='ar'):
    wp_enqueue_style( 'main-ar', get_template_directory_uri() . '/assets/stylesheets/main-rtl.css', array(), '', 'all' );
	else:
	wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/stylesheets/main.css', array(), '', 'all' );
	endif;
    if(is_page_template('trainer-dashboard.php')  || is_page_template('trainer-profile.php')  || is_page_template('trainer-measurement.php')  || is_page_template('trainer-bookings.php')  || (is_singular('class') && current_user_can('trainer'))  || is_page_template('trainer-schedule.php')  || is_page_template('trainer-login.php')  || is_page_template('customer-notifications.php')  ||is_page_template('customer-login.php')  || is_page_template('customer-dashboard.php') || is_page_template('customer-schedule.php') || is_page_template('customer-booking.php') || is_page_template('customer-completed-booking.php')|| is_page_template('customer-subscription.php') || is_page_template('customer-measurements.php') || is_page_template('customer-profile.php')):
        if(ICL_LANGUAGE_CODE=='ar'):
		wp_enqueue_style( 'app-ar', get_template_directory_uri() . '/assets/stylesheets/app-rtl.css', array(), '', 'all' );   
		wp_enqueue_style( 'style-ar', get_template_directory_uri() . '/style-rtl.css', array(), '', 'all' );    
		else:
		wp_enqueue_style( 'app', get_template_directory_uri() . '/assets/stylesheets/app.css', array(), '', 'all' );    
		wp_enqueue_style( 'hotstone-style', get_stylesheet_uri() );
		endif;        
    endif;
    endif;
    wp_enqueue_style( 'select2css', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css', false, '', 'all' );
    wp_enqueue_style( 'hotstone-style', get_stylesheet_uri() );

    wp_enqueue_script( 'head-core', get_template_directory_uri() . '/assets/javascripts/vendor/head.core.js', array(), '', true );

    wp_enqueue_script( 'JQuery', get_template_directory_uri() . '/assets/javascripts/vendor/jquery-3.2.1.min.js', array(), '', true );
    wp_enqueue_script( 'matchheight', get_template_directory_uri() . '/assets/javascripts/vendor/jquery.matchHeight-min.js', array(), '', true );
    wp_enqueue_script( 'popper', get_template_directory_uri() . '/assets/javascripts/vendor/popper.min.js', array(), '', true );
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/assets/javascripts/vendor/bootstrap.min.js', array(), '', true );
    wp_enqueue_script( 'ofi-bowser', get_template_directory_uri() . '/assets/javascripts/vendor/ofi.browser.js', array(), '', true );
    wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/assets/javascripts/vendor/fancybox.js', array(), '', true );
    wp_enqueue_script( 'owl-js', get_template_directory_uri() . '/assets/javascripts/vendor/owl.carousel-min.js', array(), '', true );
    wp_enqueue_script( 'jquery-ui-js', get_template_directory_uri() . '/assets/javascripts/vendor/jquery-ui.min.js', array(), '', true );
    wp_enqueue_script( 'validate', get_template_directory_uri() . '/includes/jquery.validate.min.js', array(), '', true );
    wp_enqueue_script( 'custom-validator', get_template_directory_uri() . '/includes/custom-validator.js', array(), '', true );
    wp_enqueue_script( 'chosen-js', get_template_directory_uri() . '/assets/javascripts/chosen.jquery.min.js', array(), '', true );
    
    if(ICL_LANGUAGE_CODE=='ar'):
   		wp_enqueue_script( 'main-ar-js', get_template_directory_uri() . '/assets/javascripts/main-rtl.js', array(), '', true );
	else:
		wp_enqueue_script( 'main-js', get_template_directory_uri() . '/assets/javascripts/main.js', array(), '', true );
	endif;	
    wp_enqueue_script( 'select2', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', array(), '3.2.1', true );
    wp_enqueue_script( 'chart', 'https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js', array(), '2.9.3', true );
}

 
function add_body_class( $classes )
{
    global $post;
    if ( isset( $post ) ) {
        
    }
    if(is_page_template('customer-login.php') || is_page_template('trainer-login.php')){
        $classes[] = 'user-login';
    }

    return $classes;
}
add_filter( 'body_class', 'add_body_class' );

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}

function excerpt($limit) {

 $excerpt = explode(' ', get_the_excerpt(), $limit);

 if (count($excerpt)>=$limit) {

 array_pop($excerpt);

 $excerpt = implode(" ",$excerpt).'...';

 } else {

 $excerpt = implode(" ",$excerpt);

 }

 $excerpt = preg_replace('`[[^]]*]`','',$excerpt);

 return $excerpt;

}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}

include 'includes/functions/callback-email.php';
include 'includes/functions/callback-add-front-office.php';
include 'includes/functions/callback-filter-front-office.php';
include 'includes/functions/callback-enable-disable-user.php';
include 'includes/functions/callback-delete-user.php';
include 'includes/functions/callback-send-user-details.php';
include 'includes/functions/callback-edit-front-office.php';
include 'includes/functions/callback-add-trainer.php';
include 'includes/functions/callback-filter-trainer.php';
include 'includes/functions/callback-add-customer.php';
include 'includes/functions/callback-add-note.php';
include 'includes/functions/callback-filter-customer.php';
include 'includes/functions/callback-view-note.php';
include 'includes/functions/callback-edit-customer.php';
include 'includes/functions/callback-edit-trainer.php';
include 'includes/functions/callback-add-membership-type.php';
include 'includes/functions/callback-enable-disable-member.php';
include 'includes/functions/callback-delete-member.php';
include 'includes/functions/callback-edit-membership.php';
include 'includes/functions/callback-filter-membership.php';
include 'includes/functions/callback-add-subscriptions.php';
include 'includes/functions/callback-edit-subscription.php';
include 'includes/functions/callback-view-log.php';
include 'includes/functions/callback-filter-subscription.php';
include 'includes/functions/callback-add-workout.php';
include 'includes/functions/callback-edit-workout.php';
include 'includes/functions/callback-filter-workout.php';
include 'includes/functions/callback-add-measurement.php';
include 'includes/functions/callback-edit-measurement.php';
include 'includes/functions/callback-view-measurement.php';
include 'includes/functions/callback-filter-measurement.php';
include 'includes/functions/callback-add-class.php';
include 'includes/functions/callback-edit-class.php';
include 'includes/functions/callback-filter-class.php';
include 'includes/functions/callback-add-booking.php';
include 'includes/functions/callback-edit-booking.php';
include 'includes/functions/callback-view-booking-log.php';
include 'includes/functions/callback-filter-booking.php';
include 'includes/functions/callback-add-notification.php';
include 'includes/functions/callback-view-notification.php';
include 'includes/functions/callback-view-customer-subscription.php';
include 'includes/functions/callback-view-customer-measurement.php';
include 'includes/functions/callback-view-customer-class.php';
include 'includes/functions/callback-customer-book-class.php';
include 'includes/functions/callback-customer-cancel-booking.php';
include 'includes/functions/callback-customer-fliter-class.php';
include 'includes/functions/callback-customer-filter-measurement.php';
include 'includes/functions/callback-edit-customer-profile.php';
include 'includes/functions/callback-customer-password.php';
include 'includes/functions/callback-view-customer-notification.php';
include 'includes/functions/callback-trainer-filter-class.php';
include 'includes/functions/callback-view-trainer-measurement.php';
include 'includes/functions/callback-trainer-filter-measurement.php';
include 'includes/functions/callback-edit-trainer-profile.php';
include 'includes/functions/callback-admin-booking-customer.php';
include 'includes/functions/callback-trainer-attendance.php';
include 'includes/functions/callback-admin-booking-class.php';
add_action( 'wp_login_failed', 'custom_login_failed' );
function custom_login_failed( $user )
{
    $referrer = wp_get_referer();

    if ( $referrer && ! strstr($referrer, 'wp-login') && ! strstr($referrer,'wp-admin') )
    {              
        $referrer = remove_query_arg( 'action', $referrer ) ;
        wp_redirect( add_query_arg('login', 'failed', $referrer) );
        exit;
    }
}


function myplugin_authenticate_on_hold($user)
{
    // username and password are correct
    if ($user instanceof WP_User) {
        $is_disabled =  get_user_meta( $user->ID, 'disable', true );
        $user_meta=get_userdata($user->ID);
        $user_roles=$user_meta->roles;
        if (in_array("front_office_user", $user_roles) || in_array("trainer", $user_roles) || in_array("customer", $user_roles)){
        if ($is_disabled ) {
            $referrer = wp_get_referer();

			if ( $referrer && ! strstr($referrer, 'wp-login') && ! strstr($referrer,'wp-admin') )
			{              
				$referrer = remove_query_arg( 'action', $referrer ) ;
				wp_redirect( add_query_arg('login', 'disabled', $referrer) );
				exit;
			}
          }
        }
      
    }

    return $user;
}

add_filter('authenticate', 'myplugin_authenticate_on_hold', 21);

function form_login( $args = array() ) {
$defaults = array(
                'echo' => true,
                 'redirect' =>  isset($_GET['redirect_to']) ? $_GET['redirect_to'] : home_url('/admin-dashboard/'), // Default redirect is back to the current page
                    'form_id' => 'login-form',
                    'label_username' => __( 'Username' ),
                'label_password' => __( 'Password' ),
                'label_remember' => __( 'Remember Password' ),
                'label_log_in' => __( 'login' ),
                'id_username' => 'user_login',
                'id_password' => 'user_pass',
                'id_remember' => 'remember',
                'id_submit' => 'wp-submit',
                'remember' => false,
                    'value_username' => '',
                    'value_remember' => false, // Set this to true to default the "Remember me" checkbox to checked
        );
            $args = wp_parse_args( $args, apply_filters( 'login_form_defaults', $defaults ) );
            
            $form = '
                <form name="' . $args['form_id'] . '" id="' . $args['form_id'] . '" action="' . esc_url( site_url( 'wp-login.php', 'login_post' ) ) . '" method="post">
                ' . apply_filters( 'login_form_top', '', $args ) . '
                <div class="form-group">
                      <label class="form-label">Email</label>
                      <input type="text" id="' . esc_attr( $args['id_username'] ) . '" class="form-control" name="log" placeholder="Enter your email address"> 
                    </div>
                <div class="form-group password">
                    <label class="form-label">Password</label>
                    <div class="input-wrapper">
                      <input type="password" name="pwd" id="' . esc_attr( $args['id_password'] ) . '" class="form-control" placeholder="Password">
                      <button type="button" class="show-hide-pass">
                        <img src="'.get_template_directory_uri().'/assets/images/show-password-eye.svg" alt="icon" class="show-password d-none">
                        <img src="'.get_template_directory_uri().'/assets/images/hide-password-eye.svg" alt="icon" class="hide-password">
                      </button>
                    </div>
                    <label id="loginerror" class="error" style="display:none"></label>
                </div>
                <div class="form-group">
                  <a href="'.esc_url( wp_lostpassword_url() ).'" class="link" target="_blank">Forgot Password?</a>
                </div>
        ' . apply_filters( 'login_form_middle', '', $args ) . '
        
        <input type="hidden" name="redirect_to" value="' . esc_url( $args['redirect'] ) . '" />
            <button type="submit" name="wp-submit" class="btn button1 w-100 btn-lg">'. __('Sign In','sidf'). '</button>
        
               
                  ' . apply_filters( 'login_form_bottom', '', $args ) . '
               </form>';
    
            if ( $args['echo'] )
                    echo $form;
            else
                    return $form;
    }
    
    
    
function customer_form_login( $args = array() ) {
$defaults = array(
                'echo' => true,
                 'redirect' =>  isset($_GET['redirect_to']) ? $_GET['redirect_to'] : home_url('/customer-dashboard/'), // Default redirect is back to the current page
                    'form_id' => 'login-form',
                    'label_username' => __( 'Username' ),
                'label_password' => __( 'Password' ),
                'label_remember' => __( 'Remember Password' ),
                'label_log_in' => __( 'login' ),
                'id_username' => 'user_login',
                'id_password' => 'user_pass',
                'id_remember' => 'remember',
                'id_submit' => 'wp-submit',
                'remember' => false,
                    'value_username' => '',
                    'value_remember' => false, // Set this to true to default the "Remember me" checkbox to checked
        );
            $args = wp_parse_args( $args, apply_filters( 'login_form_defaults', $defaults ) );
            
            $form = '
                <form name="' . $args['form_id'] . '" id="' . $args['form_id'] . '" action="' . esc_url( site_url( 'wp-login.php', 'login_post' ) ) . '" method="post">
                ' . apply_filters( 'login_form_top', '', $args ) . '
                <div class="mb-5 mb-md-0">
                                      

                            <div class="form-group">
                              <input type="text" class="form-control" name="log" placeholder="Email">
                            </div>
                            <div class="form-group">
                              <input type="password" class="form-control" name="pwd" placeholder="Password">
                            </div>
                            <label id="loginerror" class="error" style="display:none"></label>
                            <div class="form-group">
                              <div class="row">
                                <div class="col-lg-6 mb-4 mb-xl-0 d-none d-md-block">
                                  <label class="check1">
                                    <input type="checkbox" name="rememberme" value="forever" id="rememberme" tabindex="13" />
                                    <span class="inner">
                                      Remember me
                                    </span>
                                  </label>
                                </div>';
                                // <div class="col-lg-6 text-xl-end">
                                //   <a href="#" class="link">Forgot Password?</a>
                                // </div>
                                $form .= '</div>
                            </div>
                          </div>
        ' . apply_filters( 'login_form_middle', '', $args ) . '
        
        <input type="hidden" name="redirect_to" value="' . esc_url( $args['redirect'] ) . '" />
            <button type="submit" name="wp-submit" class="btn button1 w-100 btn-lg text-normal rounded">'. __('Sign In','sidf'). '</button>
        
               
                  ' . apply_filters( 'login_form_bottom', '', $args ) . '
               </form>';
    
            if ( $args['echo'] )
                    echo $form;
            else
                    return $form;
    }
    add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
  if (in_array('current-menu-item', $classes) ){
    $classes[] = 'active ';
  }
  return $classes;
}