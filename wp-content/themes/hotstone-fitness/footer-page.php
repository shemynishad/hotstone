<footer id="footer">
            <div class="container container-md">
                <div class="row align-items-center">
                    <div class="col-lg-8 col-md-12 column">
                        <div class="row align-items-center">
                            <div class="col-md-auto pe-xl-4">
                                <div class="logo">
                                    <a href="#">
                                        <img src="<?= get_template_directory_uri();?>/assets/images/logo.svg" alt="logo">
                                    </a>
                                </div>
                            </div>
                            <div class="col ps-xl-4">
                                <ul class="footer-navigation">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#about_div">About</a></li>
                                    <li><a href="#membership_div">Membership & Pricing</a></li>
                                    <li><a href="#gallery_div">Gallery</a></li>
                                    <li><a href="#class_div">Class Schedule</a></li>
                                    <li><a href="#contact_div">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5 column text-lg-end buttons-wrapper">
                        <a href="<?php echo home_url('/trainer-login/');?>" class="btn button1">
                            <i class="icon"><img src="<?= get_template_directory_uri();?>/assets/images/trainer-icon.svg" alt="icon"></i>
                            Trainer login
                        </a>
                        <a href="<?php echo home_url('/customer-login/');?>" class="btn button1 ms-2">
                            <i class="icon"><img src="<?= get_template_directory_uri();?>/assets/images/user-icon.svg" alt="icon"></i>
                            Customer login
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-7 column">
                        <p class="copyright">© 2022 Hot Stone Fitness. <a href="#">Terms & Conditions</a> . <a
                                href="#">Privacy policy</a></p>
                    </div>
                </div>
            </div>
            <figure class="figure1 absolute">
                <img class="w-100" src="<?= get_template_directory_uri();?>/assets/images/footer-bg.jpg" alt="img">
            </figure>
        </footer>
        <!-- /footer -->
        <a href="#" id="back-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
        <!-- Javascript -->
      <?php wp_footer();?>
    </div>
</body>

</html>