<?php /* Template name: Customer Class */
if ( is_user_logged_in() ) {
    if (current_user_can('customer')) {
        $current_user = wp_get_current_user();
    }elseif (current_user_can('administrator')) {
        wp_redirect(get_home_url('','/admin-dashboard/'));   
    }else {
        wp_redirect(get_home_url());     
    }
}else{
wp_redirect(get_home_url()); 
} 
get_header('customer');?>


<div id="content">
        <!-- section -->
        <div class="section dashboard">
          <div class="container">
            <div class="row align-items-center flex-row-reverse mb-30 mb-md-5">
              <div class="col-md-6 mb-4 mb-md-0 d-none d-md-block">
                <ul class="breadcrumb alt justify-content-end mb-0">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">Classes</a></li>
                  <li>Schedule</li>
                </ul>
              </div>
              <div class="col-md-6">
                <div class="text-center text-md-start">
                  <h2 class="dashboard-title">Schedule</h2>
                </div>
              </div>
            </div>
             <?php 
                $active_subcription = 0;
                $end_date_formatted = date("Ymd");
                  $args = array(
                    	'post_type' => 'subscription',
                    	'posts_per_page' => -1,
                    	// 'meta_query' => array(
                    	// 	array(
                    	// 		'key'     => 'customer',
                    	// 		'value'   => $current_user->ID,
                    	// 		'compare' => '=',
                    	// 	),
                    	// 	array(
                    	// 		'key'     => 'is_disable',
                    	// 		'value'   => 1,
                    	// 		'compare' => '!=',
                    	// 	),
                    	// 	array(
                      //           'key'     => 'end_date',
                      //           'compare' => '>=',
                      //           'value'   => $end_date_formatted,
                      //       ),
                      //        array(
                      //           'key'     => 'remaining_sessions',
                      //           'compare' => '>=',
                      //           'value'   => 0,
                      //          )
                    	// ),
                      'meta_query' => array(
                        array(
                    			'key'     => 'customer',
                    			'value'   => $current_user->ID,
                    			'compare' => '=',
                    		),
                    		array(
                    			'key'     => 'is_disable',
                    			'value'   => 1,
                    			'compare' => '!=',
                    		),
                        'relation' => 'AND',
                        array(
                          'relation' => 'OR',
                          array(
                            'key'     => 'end_date',
                            'compare' => '>=',
                            'value'   => $end_date_formatted,
                        ),
                           array(
                              'key'     => 'remaining_sessions',
                              'compare' => '>=',
                              'value'   => 0,
                             )
                        ),
                      ),
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
                <?php while($query->have_posts()): $query->the_post();
                       $active_subcription = get_field('membership_type');
                       endwhile; endif;
                    ?>
            <div class="box1 px-md-4 mb-30 bg-color3 border border-color1 shadow1">
              <div class="form1">
                <form action="#" class="filter-form">
                  <div class="row align-items-end">
                    <div class="col">
                      <div class="row">
                        <div class="col-xxl-auto col-12 mb-4 mb-xxl-0 align-self-center d-none d-md-block">
                          <img src="<?= get_template_directory_uri();?>/assets/images/filter-gray.svg" alt="icon">
                        </div>
                        <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                          <div class="date-picker end-date-picker further-date">
                            <input type="text" class="form-control" name="start_date" placeholder="Select from date">
                          </div>
                        </div>
                        <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                          <div class="date-picker end-date-picker further-date">
                            <input type="text" class="form-control" name="end_date" placeholder="Select to date">
                          </div>
                        </div>
                        <input type="hidden" name="membership" value="<?= $active_subcription;?>">
                        <!--<div class="col-lg col-md-6 mb-4 mb-xxl-0">
                            <div class="selectbox">
                               <select class="form-control" name="type">
                            <option value="">Type</option>
                            <option value="Individual">Individual</option>
                            <option value="Group">Group</option>
                            <option value="Online">Online</option>
                            
                          </select>
                            </div>
                        </div>-->
                        <div class="col-lg col-md-6 mb-md-4 mb-0 mb-xxl-0">
                            <input type="text" class="form-control" name="keyword" placeholder="Search by Trainer">
                        </div>
                        <div class="col-xxl-auto col-12 d-none d-md-block">
                          <button class="btn button1 rounded px-3 py-2 h-100" type="submit"><i class="fa fa-search fs-4"></i></button>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto d-md-none">
                      <button class="btn button1 rounded px-3 py-2 h-100" type="submit"><i class="fa fa-search fs-4"></i></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!-- <div class="button-group mb-30 justify-content-end justify-content-md-start">
              <button class="btn button4 d-inline-flex align-items-center text-normal p-md-3 p-2">
                <i class="btn-icon me-md-2">
                  <svg viewBox="0 0 31 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.9169 17.913C10.9169 17.509 10.6029 17.1818 10.2155 17.1818H7.76771C7.38071 17.1818 7.06665 17.509 7.06665 17.913V20.4671C7.06665 20.8716 7.38071 21.199 7.76771 21.199H10.2155C10.6029 21.199 10.9169 20.8716 10.9169 20.4671V17.913Z" fill="#1C1D21"/>
                    <path d="M17.0335 17.9129C17.0335 17.5089 16.7194 17.1816 16.3327 17.1816H13.8847C13.4977 17.1816 13.1836 17.5089 13.1836 17.9129V20.467C13.1836 20.8715 13.4977 21.1989 13.8847 21.1989H16.3327C16.7194 21.1989 17.0335 20.8715 17.0335 20.467V17.9129Z" fill="#1C1D21"/>
                    <path d="M23.151 17.9129C23.151 17.5089 22.837 17.1816 22.45 17.1816H20.0022C19.6148 17.1816 19.3008 17.5089 19.3008 17.9129V20.467C19.3008 20.8715 19.6148 21.1989 20.0022 21.1989H22.45C22.837 21.1989 23.151 20.8715 23.151 20.467V17.9129Z" fill="#1C1D21"/>
                    <path d="M10.9167 24.2981C10.9167 23.8934 10.6027 23.5664 10.2152 23.5664H7.76747C7.38047 23.5664 7.06641 23.8934 7.06641 24.2981V26.8517C7.06641 27.2559 7.38047 27.5833 7.76747 27.5833H10.2152C10.6027 27.5833 10.9167 27.2558 10.9167 26.8517V24.2981Z" fill="#1C1D21"/>
                    <path d="M17.0335 24.2981C17.0335 23.8934 16.7194 23.5664 16.3327 23.5664H13.8847C13.4977 23.5664 13.1836 23.8934 13.1836 24.2981V26.8517C13.1836 27.2559 13.4977 27.5833 13.8847 27.5833H16.3327C16.7194 27.5833 17.0335 27.2558 17.0335 26.8517V24.2981Z" fill="#1C1D21"/>
                    <path d="M23.151 24.2981C23.151 23.8934 22.837 23.5664 22.4503 23.5664H20.0022C19.6148 23.5664 19.3008 23.8934 19.3008 24.2981V26.8517C19.3008 27.2559 19.6148 27.5833 20.0022 27.5833H22.4503C22.837 27.5833 23.151 27.2558 23.151 26.8517V24.2981Z" fill="#1C1D21"/>
                    <path d="M27.5285 3.71127V7.61238C27.5285 9.37564 26.1578 10.7966 24.4684 10.7966H22.538C20.8485 10.7966 19.4596 9.37564 19.4596 7.61238V3.69727H10.7587V7.61238C10.7587 9.37564 9.36985 10.7966 7.68057 10.7966H5.74978C4.06044 10.7966 2.68978 9.37564 2.68978 7.61238V3.71127C1.21328 3.75771 0 5.03269 0 6.59981V29.4622C0 31.0587 1.24005 32.37 2.76999 32.37H27.4483C28.9759 32.37 30.2182 31.056 30.2182 29.4622V6.59981C30.2182 5.03269 29.005 3.75771 27.5285 3.71127ZM26.6319 28.0385C26.6319 28.7284 26.0958 29.2882 25.4344 29.2882H4.73111C4.06968 29.2882 3.5336 28.7284 3.5336 28.0385V16.2292C3.5336 15.539 4.06961 14.9792 4.73111 14.9792H25.4343C26.0958 14.9792 26.6318 15.539 26.6318 16.2292L26.6319 28.0385Z" fill="#1C1D21"/>
                    <path d="M5.74481 8.70584H7.65427C8.23385 8.70584 8.70377 8.21617 8.70377 7.61135V1.21598C8.70377 0.611094 8.23385 0.121094 7.65427 0.121094H5.74481C5.16518 0.121094 4.69531 0.611094 4.69531 1.21598V7.61135C4.69531 8.21617 5.16518 8.70584 5.74481 8.70584Z" fill="#1C1D21"/>
                    <path d="M22.5143 8.70584H24.4237C25.0029 8.70584 25.4729 8.21617 25.4729 7.61135V1.21598C25.4729 0.611094 25.003 0.121094 24.4237 0.121094H22.5143C21.9348 0.121094 21.4648 0.611094 21.4648 1.21598V7.61135C21.4648 8.21617 21.9348 8.70584 22.5143 8.70584Z" fill="#1C1D21"/>
                  </svg>
                </i> 
                <span class="d-none d-md-inline-block">Calendar view</span>
              </button>
              <button class="btn button4 d-inline-flex align-items-center text-normal p-md-3 p-2 active">
                <i class="btn-icon me-md-2">
                  <svg viewBox="0 0 25 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect width="11.4244" height="12.2707" rx="1" fill="#1C1D21"/>
                    <rect y="14.0234" width="11.4244" height="12.2707" rx="1" fill="#1C1D21"/>
                    <rect x="13.0586" width="11.4244" height="12.2707" rx="1" fill="#1C1D21"/>
                    <rect x="13.0586" y="14.0234" width="11.4244" height="12.2707" rx="1" fill="#1C1D21"/>
                  </svg>                      
                </i> 
                <span class="d-none d-md-inline-block">Card view</span>
              </button>
            </div> -->
            <div class="more-results">
                
                <?php 
                $class_id_array = array();
                  $today = date('Ymd');
                  $args = array(
                    	'post_type' => 'booking',
                    	'posts_per_page' => -1,
                    	'meta_query' => array(
                             array(
                                'key'     => 'booking_date',
                                'compare' => '>=',
                                'value'   => $today,
                            ),
                            array(
                    			'key'     => 'customer',
                    			'value'   => $current_user->ID,
                    			'compare' => '=',
                    		),
                    		array(
                    			'key'     => 'is_disable',
                    			'value'   => 1,
                    			'compare' => '!=',
                    		)
                        ),
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                      while($query->have_posts()): $query->the_post();
                          $class = get_field('class');
                          $class_id_array[] = $class;
                      endwhile;
                  endif;
               $today = date('Ymd');
                  $args = array(
                    	'post_type' => 'class',
                    	'posts_per_page' => -1,
                    	// 'post__not_in' => $class_id_array,
                    	'meta_query' => array(
                             array(
                                'key'     => 'date',
                                'compare' => '>=',
                                'value'   => $today,
                            ),
                    		array(
                    			'key'     => 'is_disable',
                    			'value'   => 1,
                    			'compare' => '!=',
                    		),
                    		array(
                    			'key'     => 'membership',
                    			'value'   => $active_subcription,
                    			'compare' => '=',
                    		)
                        ),
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
            <ul class="list1">
               <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
                  <li>
                    <div class="card1">
                      <div class="inner-wrapper">
                        <ul class="details-list mb-0">
                          <li>
                            <div class="d-flex">
                              <span class="title">ID:</span>
                              <span class="text"><?= get_field('id');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Date:</span>
                              <?php $date = get_field('date');?>
                              <span class="text"><?= date("d F Y", strtotime($date));?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Time:</span>
                              <span class="text"><?= get_field('start_time');?> - <?= get_field('end_time');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Workouts:</span>
                              <?php $workout_titles = array();
                    $workouts = explode(",",get_field('workouts'));
                    foreach($workouts as $workout):
                         $workout_titles[] = '<a href="#" class="workout-show" data-name="'.get_field('name', $workout).'" data-id="'.get_field('id', $workout).'" data-description="'.get_field('description', $workout).'" data-video="'.get_field('video_url', $workout).'">'.get_field('name',$workout).'</a>';
                    endforeach;?>
                              <span class="text"><?= implode(",",$workout_titles);?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Trainer</span>
                              <?php $trainer_id = get_field('trainer');
                    $author_obj = get_user_by('id', $trainer_id);?>
                              <span class="text"><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Class:</span>
                              <span class="text"><?= get_field('class_title');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Type:</span>
                              <span class="text"><?= get_field('name',get_field('membership'));?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Bookings:</span>
                              <?php 
                    $class_array = array(
                           'key'     => 'class',
                           'value'   => $query->post->ID,
                           'compare' => '='
                           );
                  $args = array(
                    	'post_type' => 'booking',
                    	'numberposts' => -1,
                    	'meta_query' => array(
                            $class_array
                        )
                        
                    );
                    $latest_class = get_posts( $args );
//for class display
                    $customer_check = array(
                      'key'     => 'customer',
                      'value'   => $current_user->ID,
                      'compare' => '=',
                     );
                    $args1 = array(
                    	'post_type' => 'booking',
                    	'numberposts' => -1,
                    	'meta_query' => array(
                            $class_array,
                            $customer_check
                        )
                        
                    );
                    $booking_alredy_done = get_posts( $args1 );
                    $schedule_id = isset($booking_alredy_done[0]->ID) ? $booking_alredy_done[0]->ID : '';
                    ?>
                              <span class="text"><?= count($latest_class);?>/<?= get_field('no_of_slots');?></span>
                            </div>
                          </li>
                        </ul>
                        <div class="row align-items-center">
                        <div class="col-auto">
                            <button type="button" class="view-class" data-index="<?= $query->post->ID;?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri();?>/assets/images/eye-icon.svg" alt="icon"></i></button>
                          </div>
                          <?php if(!$schedule_id){?>
                          <div class="col-auto">
                            <button type="button" data-index="<?= $query->post->ID;?>" class="btn button5 px-md-4 py-md-3 p-2 btn-lg text-normal book-class">Join</button>
                          </div>
                          <?php }?>
                        </div>
                      </div>
                    </div>
                  </li>
                 <?php endwhile;?>
            </ul>
            <?php else:?>
            <div class="alert alert-info" role="alert"><?php _e('No class found matching the search criteria. Please try again.');?> </div>
            <?php endif;?>
          </div>
        </div>
        <!-- /section -->
      </div>

<div class="modal2 modal fade" id="view-class-details" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered small">
            <div class="modal-content">
              <header class="modal-header">
                <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                <h3>View class details</h3>
              </header>
              <div class="modal-body">
                <div class="box1 px-4 shadow-none bg-color3">
                  <ul class="details-list d-flex mb-0">
                   
                  </ul>
                </div>
                <!-- <div class="text-end d-none d-md-block pt-4">
                  <button class="btn button1 rounded book-class popup-book" type="button">Join the class</button>
                </div> -->
              </div>
              <footer class="modal-footer d-md-none justify-content-center">
                <div class="button-group">
                  <button data-bs-dismiss="modal" class="btn button6 rounded">Cancel</button>
                  <button class="btn button1 rounded book-class popup-book" type="button">Join</button>
                </div>
              </footer>
            </div>
          </div>
        </div>
        
        <div class="modal2 modal fade" id="confirm-booking" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered small">
            <div class="modal-content">
              <header class="modal-header">
                <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                <h3>Confirm Booking</h3>
              </header>
              <div class="modal-body">
                <div class="box1 px-4 shadow-none bg-color3">
                  <h3>Are you sure want to join the class?</h3>
                </div>
              </div>
              <footer class="modal-footer justify-content-center">
                <div class="button-group">
                  <button data-bs-dismiss="modal" class="btn button6 rounded">No</button>
                  <button class="btn button1 rounded submit-booking" type="button">Yes</button>
                  <input type="hidden" class="class_id">
                </div>
              </footer>
            </div>
          </div>
        </div>
        
         <div class="modal2 modal fade" id="workout-popup" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <header class="modal-header">
                <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                <h3 class="workout-title">Workout 001</h3>
                <ul class="details-list d-flex border-0 text-white mb-0">
                  <li>
                    <div class="d-flex align-items-center">
                      <span class="title">ID:</span>
                      <span class="text workout-id">HSSL_001</span>
                    </div>
                  </li>
                </ul>
              </header>
              <div class="video-wrapper rounded2 overflow-hidden">
                <div class="video-box">
                  <iframe src="" class="workout-video" style="width:100%; height:400px;"></iframe>
                </div>
                <div class="description">
                  <h4>Description:</h4>
                  <p class="workout-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor sed perferendis distinctio cum, beatae quibusdam nulla quasi incidunt laudantium odio totam at vel autem iste quidem porro iusto. Esse, provident.</p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <?php get_footer('customer');?>
<script>
        jQuery( document ).on( "click", ".view-class", function(e) {
            e.preventDefault();
                
                    var user_id = $(this).data('index');
                     jQuery('.popup-book').attr('data-index', user_id);

                    // process the form
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=view_customer_class_action&user_id='+user_id,
                        data: '',
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                            var obj = JSON.parse(result);     
                            jQuery('#view-class-details ul').html(obj.html); 
                            jQuery('#view-class-details').modal('show');
                      


                    });
            

            return false;
        });
        
    
        jQuery( document ).on( "click", ".book-class", function(e) {
             e.preventDefault();
                
                    var user_id = $(this).data('index');
                    jQuery('#view-class-details').modal('hide');

                    jQuery('.class_id').val(user_id);
                    jQuery('#confirm-booking').modal('show');
            

            return false;
            
        });
        
        jQuery(".submit-booking").click(function(e){
             e.preventDefault();
                
                    var user_id = jQuery('.class_id').val();
                    jQuery('#confirm-booking').modal('hide');
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=customer_book_class_action&user_id='+user_id,
                        data: '',
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                            location.reload();
                      


                    });
            

            return false;
            
        });
        
        jQuery(".filter-form").submit(function(e) {
    e.preventDefault();
      
        jQuery('#loader').show();
      
        var fd= new FormData(jQuery('.filter-form')[0]);
        // process the form
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>?action=customer_filter_class_action&page=1',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            var obj = JSON.parse(result);     
            jQuery('.more-results').html(obj.html); 
             jQuery('#loader').hide();
             
          });
        
        return false;
    });
          $( document ).on( "click", ".workout-show", function() {
        $('.workout-title').val($(this).data('name'));
        $('.workout-id').val($(this).data('id'));
        $('.workout-description').val($(this).data('description'));
        var youtube_id = youtube_parser($(this).data('video'));
        console.log(youtube_id);
        console.log($(this).data('video'));
        $('.workout-video').attr('src', '//www.youtube.com/embed/'+youtube_id);
        $('#workout-popup').modal('show');
        return false;
        
    });
    
    function youtube_parser(url){
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    var match = url.match(regExp);
    return (match&&match[7])? match[7] : false;
}
       
</script>