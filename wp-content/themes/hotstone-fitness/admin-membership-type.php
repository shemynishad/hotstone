<?php /* Template name: Admin Membership Type */
if ( is_user_logged_in() ) {
    if (current_user_can('administrator') || current_user_can('front_office_user')) {
        $current_user = wp_get_current_user();
    } else {
        wp_redirect(get_home_url());     
    }
}else{
wp_redirect(get_home_url()); 
} 
get_header();?>

<div id="content">
        <!-- section -->
        <div class="section dashboard">
          <div class="container">
            <div class="row align-items-center flex-row-reverse mb-30 mb-md-5">
              <div class="col-md-6 mb-4 mb-md-0">
                <ul class="breadcrumb justify-content-end mb-0">
                  <li><a href="#">Dashboard</a></li>
                  <li><a href="#">Memberships</a></li>
                  <li>Membership Types</li>
                </ul>
              </div>
              <div class="col-md-6">
                <p class="welcome-title">Hello, Welcome <?= $current_user->display_name;?>.</p>
              </div>
            </div>
            <div class="text-center text-lg-end mb-4">
              <button class="btn button1 btn-sm" data-bs-toggle="modal" data-bs-target="#add-customer">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.999 23.003" class="me-2">
                  <path id="Path_35" data-name="Path 35" d="M16.515,21.525a.709.709,0,0,0,.135.013.721.721,0,0,0,.709-.585,3.918,3.918,0,0,0,.073-.74,4.337,4.337,0,0,0-2.63-3.889,3.158,3.158,0,0,0,1.087-2.38,3.251,3.251,0,0,0-6.5,0,3.16,3.16,0,0,0,1.105,2.394,4.331,4.331,0,0,0-2.6,3.874,4.027,4.027,0,0,0,.072.737.723.723,0,1,0,1.422-.261,2.621,2.621,0,0,1-.049-.476A3.143,3.143,0,0,1,12.664,17.3a3.142,3.142,0,0,1,3.324,2.917,2.528,2.528,0,0,1-.048.471.717.717,0,0,0,.57.841Zm-3.876-9.338a1.757,1.757,0,1,1-1.8,1.756,1.784,1.784,0,0,1,1.8-1.756Z" transform="translate(-4.334 -6.458)" fill="#fff"/>
                  <path id="Path_36" data-name="Path 36" d="M29.611,12.2a.72.72,0,0,0,.722-.718h0V9.328a2.883,2.883,0,0,0-2.889-2.87H7.222a2.882,2.882,0,0,0-2.888,2.87V22.96a2.882,2.882,0,0,0,2.888,2.87h5.563a.73.73,0,0,1,.511.211l3.016,3a1.449,1.449,0,0,0,2.041,0l3.017-3a.73.73,0,0,1,.511-.211h5.562a2.882,2.882,0,0,0,2.89-2.87V15.067a.722.722,0,1,0-1.445,0V22.96a1.442,1.442,0,0,1-1.444,1.44H21.882a2.16,2.16,0,0,0-1.532.628l-3.017,3-3.017-3a2.159,2.159,0,0,0-1.532-.63H7.222a1.442,1.442,0,0,1-1.444-1.435V9.328A1.442,1.442,0,0,1,7.222,7.893H27.444a1.442,1.442,0,0,1,1.444,1.435V11.48a.72.72,0,0,0,.722.718Z" transform="translate(-4.334 -6.458)" fill="#fff"/>
                  <path id="Path_37" data-name="Path 37" d="M22.388,19.372a.719.719,0,0,0,.722-.716h0V16.5h2.167a.718.718,0,1,0,0-1.435H23.111v-2.15a.723.723,0,0,0-1.444,0v2.152H19.5a.718.718,0,1,0,0,1.435h2.167v2.152a.719.719,0,0,0,.722.717Z" transform="translate(-4.334 -6.458)" fill="#fff"/>
                </svg>
                Add Membership          
              </button>
            </div>
            <div class="more-results">
                  <?php 
                  $args = array(
                    	'post_type' => 'membership_types',
                    	'posts_per_page' => 8
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
            <div class="table1 mb-4">
              <table class="table mb-0">
                <thead>
                  <tr>
                    <td>ID</td>
                    <td>Name</td>                    
                    <td>Amount</td>                    
                    <td>Session</td>  
                    <!-- <td>No. of Session</td>  
                    <td>Validity (In Days)</td>   -->
                    <td></td>                    
                  </tr>
                </thead>
                <tbody>
                <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
                  <tr>
                    <td><?= get_field('id');?></td>
                    <td><?= get_field('name');?></td>
                    <td><?= get_field('amount');?> SAR</td>
                    <td><?= get_field('session_type');?></td>
                    <!-- <td><?= get_field('no_of_sessions');?></td>
                    <td><?= get_field('validity');?></td> -->
                    <td>
                      <ul class="user-options mb-0">
                      <?php $disable = get_field('is_disable');?>
                        <li>
                          <label class="toggle1">
                            <input type="checkbox" class="disable-user" <?php if(!$disable){ echo 'checked';}?> value="<?= $query->post->ID;?>">
                            <span class="slider"></span>
                          </label>
                        </li>
                        <?php 
                         $photo_url = '';
                         if(get_field('photo', $query->post->ID)){
                            $photo_id = get_field('photo', $query->post->ID);
                            $photo_url = wp_get_attachment_url($photo_id);
                         }
                         $data = array(   
                              'membership_name_en' => get_field('name', $query->post->ID),
                              'membership_name_ar' => get_field('name_in_arabic', $query->post->ID),
                              'no_of_session' => get_field('no_of_sessions', $query->post->ID),
                              'amount' => get_field('amount', $query->post->ID),
                              'validity' => get_field('validity', $query->post->ID),
                              'session_type' => get_field('session_type', $query->post->ID),
                              'photo_url' => $photo_url,
                              
                        );?>
                        <li>
                          <button type="button" class="option edit-user" data-index="<?= $query->post->ID;?>" data-details='<?php echo json_encode($data);?>'><img src="<?= get_template_directory_uri();?>/assets/images/pencil.svg" alt="icon"></button>
                        </li>
                        <li>
                          <button type="button" class="option delete-user" data-index="<?= $query->post->ID;?>"><img src="<?= get_template_directory_uri();?>/assets/images/bin.svg" alt="icon"></button>
                        </li>
                      </ul>
                    </td>
                  </tr>
                  <?php endwhile;?>
                </tbody>
              </table>
            </div>
            <?php
            $pages = paginate_links( array(
                'base' => '%_%',
                'format' => '?page=%#%',
                'current' => max( 1, get_query_var('paged')  ),
                'total' => $query->max_num_pages,
                'end_size' => 1,
                'type'  => 'array',
                'mid_size' => 2,
                'next_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                'prev_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ) );
            ?>            
            <?php if( is_array( $pages ) ) :?>
            <ul class="pagination justify-content-center justify-content-lg-end">
            <?php foreach ( $pages as $page ):?>
                        <li>
                            <?php echo $page;?>
                        </li>
                        <?php endforeach;?>
                    </ul>
            <?php endif;?>
            <?php else:?>
                <div class="alert alert-info" role="alert"><?php _e('No Membership Types found');?> </div>
            <?php endif;?>
          </div>
        </div>
        <!-- /section -->
      </div>

      <div class="modal1 modal fade" id="add-customer" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Add Membership Type</h3>
              </header>
              <div class="modal-body p-0">
                <div class="form1">
                  <form action="#" class="add-user-form">
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label class="form-label">Membership name (EN)</label>
                        <input type="text" class="form-control" name="membership_name_en" placeholder="Enter membership name">
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">Membership name (AR)</label>
                        <input type="text" class="form-control" name="membership_name_ar" placeholder="Enter membership name">
                      </div>
                      <!-- <div class="col-md-6 form-group">
                        <label class="form-label">No. of Sessions</label>
                        <input type="text" class="form-control" name="no_of_session" placeholder="Enter No. of Sessions">
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Validity (days)</label>
                        <input type="text" class="form-control" name="validity" placeholder="Enter Validity">
                      </div> -->
                      <div class="col-md-6 form-group">
                        <label class="form-label">Amount</label>
                        <input type="text" class="form-control" name="amount" placeholder="Enter Amount">
                      </div>
                      
                      
                      <div class="col-6 form-group">
                        <label class="form-label">Session Type</label>
                        <div class="row">
                          <div class="col-auto">
                            <label class="radio1">
                              <input type="radio" name="session_type" value="Individual">
                              <span class="inner">
                                <i class="fa fa-solid fa-user"></i>
                                Individual
                              </span>
                            </label>
                          </div>
                          <div class="col-auto">
                            <label class="radio1">
                              <input type="radio" name="session_type" value="Group">
                              <span class="inner">
                                <i class="fa fa-solid fa-users"></i>
                                Group
                              </span>
                            </label>
                          </div>
                          <div class="col-auto">
                            <label class="radio1">
                              <input type="radio" name="session_type" value="Online">
                              <span class="inner">
                                <i class="fa fa-regular fa-play-circle"></i>
                                Online
                              </span>
                            </label>
                          </div>
                        </div>
                        <label for="session_type" class="error"></label>
                      </div>
                       <div class="col-md-6 form-group">
                        <label class="form-label">Photo</label>
                        <div class="file-upload">
                          <input type="file" id="photo" class="single-upload" name="photo" accept="image/png, image/jpg, image/jpeg">
                          <label for="photo" class="inner">&plus;</label>
                          <img src="" class="image-src hide">
                        </div>
                        <label for="photo" class="error"></label>
                        <span class="allowed-type">Allowed file : png,jpg,jpeg</span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 mb-4 mb-md-0">
                        <button class="btn button2 w-100 btn-lg" type="button" data-bs-dismiss="modal">Cancel</button>
                      </div>
                      <div class="col-md-6">
                        <button class="btn button1 w-100 btn-lg submit-btn" type="submit">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="edit-customer" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Edit Membership Type</h3>
              </header>
              <div class="modal-body p-0">
                <div class="form1">
                  <form action="#" class="edit-user-form">
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label class="form-label">Membership name (EN)</label>
                        <input type="text" class="form-control edit_membership_name_en" name="edit_membership_name_en" placeholder="Enter membership name">
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">Membership name (AR)</label>
                        <input type="text" class="form-control edit_membership_name_ar" name="edit_membership_name_ar" placeholder="Enter membership name">
                      </div>
                      <!-- <div class="col-md-6 form-group">
                        <label class="form-label">No. of Sessions</label>
                        <input type="text" class="form-control edit_no_of_session" name="edit_no_of_session" placeholder="Enter No. of Sessions">
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Validity (days)</label>
                        <input type="text" class="form-control edit_validity" name="edit_validity" placeholder="Enter Validity">
                      </div> -->
                      <div class="col-md-6 form-group">
                        <label class="form-label">Amount</label>
                        <input type="text" class="form-control edit_amount" name="edit_amount" placeholder="Enter Amount">
                      </div>
                      <div class="col-6 form-group">
                        <label class="form-label">Session Type</label>
                        <div class="row">
                          <div class="col-auto">
                            <label class="radio1">
                              <input type="radio" class="edit_session_type" name="edit_session_type" value="Individual">
                              <span class="inner">
                                <i class="fa fa-solid fa-user"></i>
                                Individual
                              </span>
                            </label>
                          </div>
                          <div class="col-auto">
                            <label class="radio1">
                              <input type="radio" class="edit_session_type" name="edit_session_type" value="Group">
                              <span class="inner">
                                <i class="fa fa-solid fa-users"></i>
                                Group
                              </span>
                            </label>
                          </div>
                          <div class="col-auto">
                            <label class="radio1">
                              <input type="radio" class="edit_session_type" name="edit_session_type" value="Online">
                              <span class="inner">
                                <i class="fa fa-regular fa-play-circle"></i>
                                Online
                              </span>
                            </label>
                          </div>
                        </div>
                        <label for="edit_type" class="error"></label>
                      </div>
                       <div class="col-md-6 form-group">
                        <label class="form-label">Photo</label>
                        <div class="file-upload">
                          <input type="file" id="edit_photo" class="single-upload" name="photo" accept="image/png, image/jpg, image/jpeg">
                          <label for="edit_photo" class="inner">&plus;</label>
                          <img src="" class="edit_image image-src hide">
                        </div>
                        <label for="photo" class="error"></label>
                        <span class="allowed-type">Allowed file : png,jpg,jpeg</span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 mb-4 mb-md-0">
                        <button class="btn button2 w-100 btn-lg" type="button" data-bs-dismiss="modal">Cancel</button>
                      </div>
                      <div class="col-md-6">
                          <input type="hidden" name="user_id" class="edit_user_id" value="">
                        <button class="btn button1 w-100 btn-lg submit-btn" type="submit">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="modal1 modal fade" id="disable-enable" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1"></h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"></div>
                <div class="row">
                  <div class="col-md-6 mb-4 mb-md-0">
                    <button class="btn button2 w-100 btn-lg disable-enable-close" type="button">No</button>
                  </div>
                  <div class="col-md-6">
                    <button class="btn button1 w-100 btn-lg disable-enable-submit" type="button">Yes</button>
                  </div>
                </div>
              </div>
              <input type="hidden" class="disable-user-id" value="">
              <input type="hidden" class="disable-enable-id" value="">
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="disable-enable-success" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1"></h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"></div>
                <div class="row">
                  <div class="col-md-6">
                    <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="delete-confirm" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Delete Membership Type</h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"><p>Are you sure want to delete this membership type ?</p></div>
                <div class="row">
                  <div class="col-md-6 mb-4 mb-md-0">
                    <button class="btn button2 w-100 btn-lg" data-bs-dismiss="modal" type="button">No</button>
                  </div>
                  <div class="col-md-6">
                    <button class="btn button1 w-100 btn-lg delete-submit" type="button">Yes</button>
                  </div>
                </div>
              </div>
              <input type="hidden" class="delete-user-id" value="">
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="delete-success" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Delete Membership Type</h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"><p>Membership Type has been successfully deleted</p></div>
                <div class="row">
                  <div class="col-md-6 mb-4 mb-md-0">
                   <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

       

       

        <div class="modal1 modal fade" id="edit-user-success" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Edit Membership Type</h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"><p>Membership Type has been successfully updated</p></div>
                <div class="row">
                  <div class="col-md-6 mb-4 mb-md-0">
                   <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>



<?php get_footer();?>
<script>
 $.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param * 1000000)
}, 'File size must be less than {0} MB');
       var abc = jQuery('.add-user-form').validate({
            // ignore: ".ignore",
            // onfocusout: function(element) {$(element).valid()},
            // onkeyup: false,
            ignore: [],
            rules:
            {
                membership_name_en:
                {

                    required: true,  
                },
                membership_name_ar:
                {

                    required: true, 
                },
                 validity:
                {
         	        digits: true
                },
                amount:
                {
                    required: true,  
         	        digits: true
                },
                no_of_session:
                {
                    required: true,  
        			digits: true
                },
                // photo:
                // {
                //     required: true, 
                //     filesize : 1.5
                // },
                session_type:
                {
                    required: true, 
                },
            }

        });

        jQuery(".add-user-form").submit(function(e){
            e.preventDefault();
            if(abc.form()  && abc.form())
            {
                
                 jQuery('#loader').show();
                 jQuery('.submit-btn').attr('disabled','disabled');
                
                    var fd= new FormData(jQuery('.add-user-form')[0]);
                    // process the form
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=admin_membership_type_action',
                        data: fd,
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                           location.reload();                      


                    });
            }

            return false;
        });

       

    $( document ).on( "click", "a.page-numbers", function() {
        jQuery('#loader').show();
        var href = $(this).attr('href');
        var this1 = $(this);
        
        // process the form
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>'+href+'&action=admin_filter_membership_action',
          data: '',
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            var obj = JSON.parse(result);     
            jQuery('.more-results').html(obj.html); 
             jQuery('#loader').hide();       
          });
        
        return false;
    });

    $(document).on('change', '.disable-user', function() {
        var disable = 1;
            $('#disable-enable h3').html('Disable Membership Type');
            $('#disable-enable .content-body').html('<p>Are you sure to disable this membership type ?</p>');
            $('#disable-enable-success h3').html('Disable Membership Type');
            $('#disable-enable-success .content-body').html('<p>Membership Type is successfully disabled</p>');
        if(this.checked) {
        disable = 0;
        $('#disable-enable h3').html('Enable Membership Type');
        $('#disable-enable .content-body').html('<p>Are you sure to enable this Membership Type ?</p>');
        $('#disable-enable-success h3').html('Enable Membership Type');
        $('#disable-enable-success .content-body').html('<p>Membership Type is successfully enabled</p>');
            
        }
        var user_id = this.value;
        $('.disable-user-id').val(user_id);
        $('.disable-enable-id').val(disable);
        $('#disable-enable').modal('show');
         
    });

    $( document ).on( "click", ".disable-enable-close", function() {
        var user_id = $('.disable-user-id').val();
        var disable = $('.disable-enable-id').val();
        if(disable==1){
          $("input[type=checkbox][value="+user_id+"]").prop("checked",true);
        }else{
          $("input[type=checkbox][value="+user_id+"]").prop("checked",false);
        }
        $('#disable-enable').modal('hide');
    });
    
    $( document ).on( "click", ".disable-enable-submit", function() {
        var user_id = $('.disable-user-id').val();
        var disable = $('.disable-enable-id').val();
        $('#disable-enable').modal('hide');
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>?action=admin_enable_disable_member_action&postid='+user_id+'&disable='+disable,
          data: '',
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            $('#disable-enable-success').modal('show'); 
             
          });
    });

    $( document ).on( "click", ".delete-user", function() {
        var user_id = $(this).data('index');
        $('.delete-user-id').val(user_id);
        $('#delete-confirm').modal('show'); 
    });

    $( document ).on( "click", ".delete-submit", function() {
        var user_id = $('.delete-user-id').val();
        $('#delete-confirm').modal('hide');
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>?action=delete_member_action&id='+user_id,
          data: '',
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            $('#delete-success').modal('show'); 
             
          });
    });

    jQuery('#delete-success').on('hidden.bs.modal', function () {
      location.reload();
    });

   

    $( document ).on( "click", ".edit-user", function() {
        var user_id = $(this).data('index');
        var details = $(this).data('details');
        $('.edit_user_id').val(user_id);
        $('.edit_membership_name_en').val(details.membership_name_en);
        $('.edit_membership_name_ar').val(details.membership_name_ar);
        $('.edit_no_of_session').val(details.no_of_session);
        $('.edit_amount').val(details.amount);
        
        $('input[name=edit_session_type][value="'+details.session_type+'"]').prop("checked",true);
        $('.edit_validity').val(details.validity);
        if(details.photo_url){
          $('.edit_image').attr('src',details.photo_url);
        }
        $('#edit-customer').modal('show');
        
    });

    var abcd = jQuery('.edit-user-form').validate({
            // ignore: ".ignore",
            // onfocusout: function(element) {$(element).valid()},
            // onkeyup: false,
            ignore: [],
            rules:
            {
                edit_membership_name_en:
                {

                    required: true,  
                },
                edit_membership_name_ar:
                {

                    required: true, 
                },
                 edit_validity:
                {
                     
         	        digits: true
                },
                edit_amount:
                {
                    required: true,  
         	        digits: true
                },
                edit_no_of_session:
                {
                    required: true,  
        			digits: true
                },
                // photo:
                // {
                    
                //     filesize : 1.5
                // },
                edit_session_type:
                {
                    required: true, 
                },
                
            }

        });

        jQuery(".edit-user-form").submit(function(e){
            e.preventDefault();
            if(abcd.form()  && abcd.form())
            {
              $('#edit-customer').modal('hide');
                 jQuery('#loader').show();
                 jQuery('.submit-btn').attr('disabled','disabled');
                
                    var fd= new FormData(jQuery('.edit-user-form')[0]);
                    // process the form
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=admin_edit_membership_action',
                        data: fd,
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                            jQuery('#loader').hide();
                          $('#edit-user-success').modal('show');
                      


                    });
            }

            return false;
        });
        jQuery('#edit-user-success').on('hidden.bs.modal', function () {
      location.reload();
    });
</script>
