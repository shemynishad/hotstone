<!-- footer -->
<div class="modal2 modal fade" id="view-logout-details" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered small">
            <div class="modal-content">
              <div class="modal-body">
                <div class="col-lg-10 mx-auto text-center py-md-5 py-4">
                  <h3 class="mb-lg-5 mb-4">Are you sure your want to logout?</h3>
                  <div class="button-group justify-content-center">
                    <button data-bs-dismiss="modal" class="btn button6-outline rounded">No</button>
                    <a href="<?php echo wp_logout_url( home_url('/customer-login') ); ?>" class="btn button1 rounded">Yes</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<footer id="footer">
        <div class="container">
          <p>© <?php echo date('Y'); ?>, Hot Stone Fitness. All rights reserved.</p>
        </div>
      </footer>
      <!-- /footer -->
    </main>
    <!-- Javascript -->
    <?php wp_footer();?>
  </div>
</body>

</html>