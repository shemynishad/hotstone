<?php /* Template name: Trainer Profile */
if (is_user_logged_in()) {
  if (current_user_can('trainer')) {
    $current_user = wp_get_current_user();
  } elseif (current_user_can('administrator')) {
    wp_redirect(get_home_url('', '/admin-dashboard/'));
  } elseif (current_user_can('customer')) {
    wp_redirect(get_home_url('', '/customer-dashboard/'));
  } else {
    wp_redirect(get_home_url());
  }
} else {
  wp_redirect(get_home_url());
}
get_header('trainer'); ?>

<div id="content">
  <div class="section">
    <div id="loader"></div>
    <div class="container">
      <div class="form1 mb-5">
        <h3 class="mb-4">Update Profile</h3>
        <form action="#" class="edit-user-form">
          <div class="row">
            <div class="col-md-6 form-group">
              <label class="form-label">First name (EN)</label>
              <input type="text" class="form-control" name="first_name_en" placeholder="Enter first name" value="<?= $current_user->user_firstname; ?>">
            </div>
            <div class="col-md-6 form-group ar">
              <label class="form-label">First name (AR)</label>
              <input type="text" class="form-control" name="first_name_ar" placeholder="Enter first name" value="<?= get_field('first_name_arabic', 'user_' . $current_user->ID); ?>">
            </div>
            <div class="col-md-6 form-group">
              <label class="form-label">Last name (EN)</label>
              <input type="text" class="form-control" name="last_name_en" placeholder="Enter last name" value="<?= $current_user->user_lastname; ?>">
            </div>
            <div class="col-md-6 form-group ar">
              <label class="form-label">Last name (AR)</label>
              <input type="text" class="form-control" placeholder="Enter last name" name="last_name_ar" value="<?= get_field('last_name_arabic', 'user_' . $current_user->ID); ?>">
            </div>
            <div class="col-md-6 form-group">
              <label class="form-label">Email</label>
              <input type="email" class="form-control" placeholder="Enter email address" readonly value="<?= $current_user->user_email; ?>">
            </div>
            <div class="col-md-6 form-group">
              <label class="form-label">Phone</label>
              <input type="text" class="form-control" placeholder="Enter phone number" name="phone" value="<?= get_field('phone', 'user_' . $current_user->ID); ?>">
            </div>
            <div class="col-md-6 form-group">
              <label class="form-label">Date of Birth</label>
              <div class="date-picker">
                <input type="text" class="form-control" placeholder="Select date of birth" name="date_of_birth" value="<?= get_field('dob', 'user_' . $current_user->ID); ?>">
              </div>
            </div>
            <div class="col-12 form-group">
              <?php $gender = get_field('gender', 'user_' . $current_user->ID); ?>
              <label class="form-label">Gender</label>
              <div class="row">
                <div class="col-auto">
                  <label class="radio1">
                    <input type="radio" name="gender" value="male" <?php if ($gender = 'male') {
                                                                      echo 'checked';
                                                                    } ?>>
                    <span class="inner">
                      <img src="<?= get_template_directory_uri(); ?>/assets/images/male.svg" alt="icon">
                    </span>
                  </label>
                </div>
                <div class="col-auto">
                  <label class="radio1">
                    <input type="radio" name="gender" value="female" <?php if ($gender = 'female') {
                                                                        echo 'checked';
                                                                      } ?>>
                    <span class="inner">
                      <img src="<?= get_template_directory_uri(); ?>/assets/images/female.svg" alt="icon">
                    </span>
                  </label>
                </div>
              </div>
            </div>
            <div class="col-md-6 form-group">
              <label class="form-label">ID no</label>
              <input type="text" class="form-control" name="id_no" placeholder="Enter ID no" value="<?= get_field('id_no', 'user_' . $current_user->ID); ?>">
            </div>
            <div class="col-md-6 form-group">
              <label class="form-label">Employee no</label>
              <input type="text" class="form-control" name="employee_no" placeholder="Enter Employee no" value="<?= get_field('employee_no', 'user_' . $current_user->ID); ?>">
            </div>
            <div class="col-md-6 form-group">
              <label class="form-label">Passport no</label>
              <input type="text" class="form-control" name="passport_no" placeholder="Enter Passport no" value="<?= get_field('passport_no', 'user_' . $current_user->ID); ?>">
            </div>
            <div class="col-md-6 form-group">
              <label class="form-label">Date of expiry of ID</label>
              <div class="date-picker">
                <input type="text" class="form-control" name="date_of_id" placeholder="Select date of hire" value="<?= get_field('date_of_expiry_of_id', 'user_' . $current_user->ID); ?>">
              </div>
            </div>

            <div class="col-md-12 form-group">
              <label class="form-label">Youtube Video URL</label>
              <input type="text" class="form-control" name="youtube_video" placeholder="Enter Youtube Video URL" value="<?= get_field('youtube_url', 'user_' . $current_user->ID); ?>">
            </div>
            <?php
            $photo_url = '';
            if (get_field('photo', 'user_' . $current_user->ID)) {
              $photo_id = get_field('photo', 'user_' . $current_user->ID);

              $photo_url = wp_get_attachment_url($photo_id);
            }
            ?>
            <div class="col-12 form-group">
              <label class="form-label">Photo</label>
              <div class="file-upload">
                <input type="file" id="photo" class="single-upload" name="photo" accept="image/png, image/jpg, image/jpeg">
                <label for="photo" class="inner">&plus;</label>
                <img src="<?= $photo_url; ?>" class="image-src">
              </div>
              <label for="photo" class="error"></label>
              <span class="allowed-type">Allowed file types: png, jpg, jpeg</span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <button class="btn button1 btn-lg" type="submit">Update</button>
            </div>
          </div>
        </form>
      </div>
      <div class="form1">
        <h3 class="mb-4">Change Password</h3>
        <form action="#" class="change-password">
          <div class="row">
            <div class="col-md-6 form-group">
              <label class="form-label">Password</label>
              <input type="text" class="form-control" name="password" id="password" placeholder="Password">
            </div>
            <div class="col-md-6 form-group">
              <label class="form-label">Confirm Password</label>
              <input type="text" class="form-control" name="confirm_password" placeholder="Confirm Password">
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <button class="btn button1 btn-lg" type="submit">Update</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="modal2 modal fade" id="edit-user-success" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>Edit Profile</h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <h3>Profile has been successfully updated</h3>
        </div>
      </div>
      <footer class="modal-footer justify-content-center">

      </footer>
    </div>
  </div>
</div>

<div class="modal2 modal fade" id="edit-password-success" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>Edit Profile</h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <h3>Password has been successfully updated</h3>
        </div>
      </div>
      <footer class="modal-footer justify-content-center">

      </footer>
    </div>
  </div>
</div>
<?php get_footer('trainer'); ?>
<script>
  $.validator.addMethod('filesize', function(value, element, param) {
    return this.optional(element) || (element.files[0].size <= param * 1000000)
  }, 'File size must be less than {0} MB');
  var abcd = jQuery('.edit-user-form').validate({
    // ignore: ".ignore",
    // onfocusout: function(element) {
    //   $(element).valid()
    // },
    // onkeyup: false,
    ignore: [],
    rules: {
      first_name_en: {

        required: true,
      },
      last_name_en: {

        required: true,
      },
      phone: {

        required: true,
        digits: true,
        minlength: 10,
        maxlength: 10,
      },
      // date_of_birth: {
      //   required: true,
      // },
      height: {
        required: true,
        digits: true,
      },
      gender: {
        required: true,
      },
      photo: {
        filesize: 1.5
      },

    }

  });

  jQuery(".edit-user-form").submit(function(e) {
    e.preventDefault();
    if (abcd.form() && abcd.form()) {
      $('#edit-customer').modal('hide');
      jQuery('#loader').show();
      jQuery('.submit-btn').attr('disabled', 'disabled');

      var fd = new FormData(jQuery('.edit-user-form')[0]);
      // process the form
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>?action=edit_trainer_action',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          jQuery('#loader').hide();
          $('#edit-user-success').modal('show');



        });
    }

    return false;
  });

  var abc = jQuery('.change-password').validate({
    // ignore: ".ignore",
    // onfocusout: function(element) {
    //   $(element).valid()
    // },
    // onkeyup: false,
    ignore: [],
    rules: {
      password: {

        required: true,
      },
      confirm_password: {

        required: true,
        equalTo: "#password"
      },
    }
  });

  jQuery(".change-password").submit(function(e) {
    e.preventDefault();
    if (abc.form() && abc.form()) {
      jQuery('#loader').show();
      jQuery('.submit-btn').attr('disabled', 'disabled');

      var fd = new FormData(jQuery('.change-password')[0]);
      // process the form
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>?action=edit_customer_password_action',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          jQuery('#loader').hide();
          $('#edit-password-success').modal('show');



        });
    }

    return false;
  });
</script>