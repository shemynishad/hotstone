<?php /* Template name: Trainer Bookings */
if (is_user_logged_in()) {
  if (current_user_can('trainer')) {
    $current_user = wp_get_current_user();
  } elseif (current_user_can('administrator')) {
    wp_redirect(get_home_url('', '/admin-dashboard/'));
  } elseif (current_user_can('customer')) {
    wp_redirect(get_home_url('', '/customer-dashboard/'));
  } else {
    wp_redirect(get_home_url());
  }
} else {
  wp_redirect(get_home_url());
}
get_header('trainer'); ?>


<div id="content">
  <!-- section -->
  <div class="section dashboard">
    <div class="container">
      <div class="row align-items-center flex-row-reverse mb-30 mb-md-5">
        <div class="col-md-6 mb-4 mb-md-0 d-none d-md-block">
          <ul class="breadcrumb alt justify-content-end mb-0">
            <li><a href="<?php home_url('/trainer-dashboard'); ?>">Home</a></li>
            <li><a href="<?php home_url('/trainer-bookings'); ?>">Classes</a></li>
            <li>Bookings</li>
          </ul>
        </div>
        <div class="col-md-6">
          <div class="text-center text-md-start">
            <h2 class="dashboard-title d-none d-md-block">Bookings</h2>
            <h2 class="dashboard-title mb-4 mb-md-0 d-md-none">My Classes / Bookings</h2>
            <figure class="d-md-none">
              <img src="<?= get_template_directory_uri(); ?>/assets/images/img1.jpg" alt="img" class="w-100 rounded1 shadow2">
            </figure>
          </div>
        </div>
        <div class="col-6 text-md-end">
          <button class="btn button5 px-4 py-1 d-md-inline-flex align-items-center d-none" data-bs-toggle="modal" data-bs-target="#add-booking">
            <span class="fs-2 me-2">&plus;</span>
            Add Booking
          </button>
          <button class="link d-inline-flex align-items-center d-md-none" data-bs-toggle="modal" data-bs-target="#add-booking">
            <span class="fs-2 me-2">&plus;</span>
            Add Booking
          </button>
        </div>
      </div>

      <div class="more-results">
        <?php $today = date('Ymd');
        $class_id_array = array();
        $args = array(
          'post_type' => 'class',
          'posts_per_page' => -1,
          'meta_query' => array(
            array(
              'key'     => 'date',
              'compare' => '>=',
              'value'   => $today,
            ),
            array(
              'key'     => 'trainer',
              'value'   => $current_user->ID,
              'compare' => '=',
            )
          ),
        );
        $query = new WP_Query($args);
        if ($query->have_posts()) :
          while ($query->have_posts()) : $query->the_post();
            $class_id_array[] = $query->post->ID;
          endwhile;
        endif;
        $class_array = array(
          'key'     => 'class',
          'value'   => $class_id_array,
          'compare' => 'IN'
        );
        $args = [];
        if (count($class_id_array) > 0) {
          $args = array(
            'post_type' => 'booking',
            'posts_per_page' => -1,
            'meta_query' => array(
              $class_array
            ),
          );
        }
        $query = new WP_Query($args);
        if ($query->have_posts()) :
        ?>
          <ul class="list1 four-cols">
            <?php while ($query->have_posts()) : $query->the_post();

            ?>
              <li>
                <div class="card1 check-item">
                  <div class="inner-wrapper">

                    <ul class="details-list mb-3 mb-md-0">
                      <li>
                        <div class="d-flex">
                          <span class="title">ID:</span>
                          <span class="text"><?= get_field('id'); ?></span>
                        </div>
                      </li>
                      <li>
                        <div class="d-flex">
                          <span class="title">Customer:</span>
                          <?php $customer_id = get_field('customer');
                          $author_obj = get_user_by('id', $customer_id); ?>
                          <span class="text"><?= $author_obj->user_firstname . ' ' . $author_obj->user_lastname; ?></span>

                        </div>
                      </li>
                      <li>
                        <div class="d-flex">
                          <span class="title">Booking date:</span>
                          <?php $booking_date = get_field('booking_date'); ?>
                          <span class="text"><?= date("d F Y", strtotime($booking_date)); ?></span>

                        </div>
                      </li>
                      <li>
                        <div class="d-flex">
                          <span class="title">Class:</span>
                          <span class="text"><?= get_field('id', get_field('class')); ?></span>
                        </div>
                      </li>
                      <li>
                        <div class="d-flex">
                          <span class="title">Class Title:</span>
                          <span class="text"><?= get_field('class_title', get_field('class')); ?></span>
                        </div>
                      </li>
                      <li>
                        <div class="d-flex">
                          <span class="title">Type:</span>
                          <span class="text"><?= get_field('name', get_field('membership', get_field('class'))); ?></span>
                        </div>
                      </li>
                      <li>
                        <div class="d-flex">
                          <span class="title">Created by:</span>
                          <span class="text"><?= get_field('created_by'); ?></span>
                        </div>
                      </li>
                    </ul>

                    <div class="row align-items-center options">
                      <?php
                      $formatted_date =  date("m/d/Y", strtotime($booking_date));
                      $data = array(
                        'customer' => $customer_id,
                        'date' => $formatted_date,
                        'class' => get_field('class'),
                      ); ?>
                      <div class="col-auto">
                        <button type="button" class="edit-booking" data-index="<?= $query->post->ID; ?>" data-details='<?php echo json_encode($data); ?>'><i class="circle-icon1"><img src="<?= get_template_directory_uri(); ?>/assets/images/edit.svg" alt="icon"></i></button>
                      </div>
                      <div class="col-auto">
                        <button type="button" class="delete-user" data-index="<?= $query->post->ID; ?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri(); ?>/assets/images/delete.svg" alt="icon"></i></button>
                      </div>
                      <div class="col-auto">
                        <button type="button" class="option view-log" data-index="<?= $query->post->ID; ?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri(); ?>/assets/images/eye-icon.svg" alt="icon"></i></button>
                      </div>
                      <?php $disable = get_field('is_disable'); ?>
                      <div class="col text-md-end">
                        <label class="toggle1 alt">
                          <input type="checkbox" class="disable-user" <?php if (!$disable) {
                                                                        echo 'checked';
                                                                      } ?> value="<?= $query->post->ID; ?>">
                          <span class="slider"></span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            <?php endwhile; ?>
          </ul>
        <?php else : ?>
          <div class="alert alert-info" role="alert">
            No upcoming booking found matching the search criteria. Please try again.
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <!-- /section -->
</div>


<div class="modal1 modal fade" id="add-booking" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
      <header class="mb-4 pe-4">
        <h3 class="text-uppercase text-color1">Add Booking</h3>
      </header>
      <div class="modal-body p-0">
        <div class="form1">
          <form action="#" class="add-user-form">
            <div class="row">
              <div class="col-md-6 form-group date-field">
                <label class="form-label">Choose Date</label>
                <div class="date-picker end-date-picker further-date">
                  <input type="text" class="form-control date-picker-field" name="date" id="date" placeholder="Select Date" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6 form-group custom-selectbox">
                <label class="form-label">Choose Class</label>
                <div class="selectbox">
                  <select class="form-control class-field" name="class">
                    <option value="">Choose Class</option>
                    <?php
                    $today = date('Ymd');
                    $args = array(
                      'numberposts' => -1,
                      'post_type'   => 'class',
                      'meta_query' => array(
                        array(
                           'key'     => 'date',
                           'compare' => '>=',
                           'value'   => $today,
                       ),
                   array(
                     'key'     => 'trainer',
                     'value'   => $current_user->ID,
                     'compare' => '=',
                   )
                   ),
                    );

                    $latest_books = get_posts($args);
                    foreach ($latest_books as $member) :
                    ?>
                      <option value="<?= $member->ID; ?>"><?= get_field('id', $member->ID); ?> ( <?= $member->post_title; ?> )</option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6 form-group d-none">
                <label class="form-label">Choose Customer</label>
                <div class="selectbox sel-customer">
                  <select class="form-control customer-field add_booking" name="customer" id="customer">
                    <option value="">Choose Customer</option>
                    <?php
                    $team_presidents = get_users(array('role__in' => array('customer'), 'number' => 9999));
                    foreach ($team_presidents as $team_president) :
                    ?>
                      <option value="<?= $team_president->ID; ?>"><?= $team_president->user_firstname . ' ' . $team_president->user_lastname; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>



            </div>

            <div class="row">
              <div class="col-md-6 mb-4 mb-md-0">
                <button class="btn button2 w-100 btn-lg" type="button" data-bs-dismiss="modal">Cancel</button>
              </div>
              <div class="col-md-6">
                <button class="btn button1 w-100 btn-lg submit-btn" type="submit">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>



<div class="modal1 modal fade" id="edit-customer" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
      <header class="mb-4 pe-4">
        <h3 class="text-uppercase text-color1">Edit Booking</h3>
      </header>
      <div class="modal-body p-0">
        <div class="form1">
          <form action="#" class="edit-booking-form">
            <div class="row">
              <div class="col-md-6 form-group">
                <label class="form-label">Choose Date</label>
                <div class="date-picker end-date-picker further-date">
                  <input type="text" class="form-control edit_date" name="edit_date" placeholder="Select Date">
                </div>
              </div>
              <div class="col-md-6 form-group custom-selectbox">
                <label class="form-label">Choose Class</label>
                <div class="selectbox">
                  <select class="form-control edit_class" disabled="disabled" name="edit_class">
                    <option value="">Choose Class</option>
                    <?php
                    $args = array(
                      'numberposts' => -1,
                      'post_type'   => 'class',
                      'meta_query' => array(
                        array(
                          'key'     => 'trainer',
                          'value'   => $current_user->ID,
                          'compare' => '=',
                        )
                      ),
                    );

                    $latest_books = get_posts($args);
                    foreach ($latest_books as $member) :
                    ?>
                      <option value="<?= $member->ID; ?>"><?= get_field('id', $member->ID); ?> ( <?= $member->post_title; ?> )</option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6 form-group">
                <label class="form-label">Choose Customer</label>
                <div class="selectbox sel-edit-customer">
                  <select class="form-control edit_customer" disabled="disabled" name="edit_customer" id="edit_customer">
                    <option value="">Choose Customer</option>
                    <?php
                    $team_presidents = get_users(array('role__in' => array('customer'), 'number' => 9999));
                    foreach ($team_presidents as $team_president) :
                    ?>
                      <option value="<?= $team_president->ID; ?>"><?= $team_president->user_firstname . ' ' . $team_president->user_lastname; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 mb-4 mb-md-0">
                <button class="btn button2 w-100 btn-lg" type="button" data-bs-dismiss="modal">Cancel</button>
              </div>
              <div class="col-md-6">
                <input type="hidden" name="user_id" class="edit_user_id" value="">
                <button class="btn button1 w-100 btn-lg" type="submit">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal1 modal fade" id="disable-enable" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <header class="mb-4 pe-4">
        <h3 class="text-uppercase text-color1"></h3>
      </header>
      <div class="modal-body p-0">
        <div class="content-body"></div>
        <div class="row">
          <div class="col-md-6 mb-4 mb-md-0">
            <button class="btn button2 w-100 btn-lg disable-enable-close" type="button">No</button>
          </div>
          <div class="col-md-6">
            <button class="btn button1 w-100 btn-lg disable-enable-submit" type="button">Yes</button>
          </div>
        </div>
      </div>
      <input type="hidden" class="disable-user-id" value="">
      <input type="hidden" class="disable-enable-id" value="">
    </div>
  </div>
</div>

<div class="modal1 modal fade" id="disable-enable-success" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
      <header class="mb-4 pe-4">
        <h3 class="text-uppercase text-color1"></h3>
      </header>
      <div class="modal-body text-center p-0">
        <div class="content-body"></div>
        <div class="row">
          <div class="col-md-6 mx-auto">
            <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal1 modal fade" id="delete-confirm" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
      <header class="mb-4 pe-4">
        <h3 class="text-uppercase text-color1">Delete Booking</h3>
      </header>
      <div class="modal-body p-0">
        <div class="content-body">
          <p>Are you sure want to delete this Booking ?</p>
        </div>
        <div class="row">
          <div class="col-md-6 mb-4 mb-md-0">
            <button class="btn button2 w-100 btn-lg" data-bs-dismiss="modal" type="button">No</button>
          </div>
          <div class="col-md-6">
            <button class="btn button1 w-100 btn-lg delete-submit" type="button">Yes</button>
          </div>
        </div>
      </div>
      <input type="hidden" class="delete-user-id" value="">
    </div>
  </div>
</div>

<div class="modal1 modal fade" id="delete-success" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
      <header class="mb-4 pe-4">
        <h3 class="text-uppercase text-color1">Delete Booking</h3>
      </header>
      <div class="modal-body p-0">
        <div class="content-body">
          <p>Booking has been successfully deleted</p>
        </div>
        <div class="row">
          <div class="col-md-6 mb-4 mb-md-0">
            <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>





<div class="modal1 modal fade" id="edit-booking-success" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
      <header class="mb-4 pe-4">
        <h3 class="text-uppercase text-color1">Edit Booking</h3>
      </header>
      <div class="modal-body p-0">
        <div class="content-body">
          <p>Booking has been successfully updated</p>
        </div>
        <div class="row">
          <div class="col-md-6 mb-4 mb-md-0">
            <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<div class="modal1 modal fade" id="view-log" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
      <header class="mb-4 pe-4">
        <h3 class="text-uppercase text-color1">View Note</h3>
      </header>
      <div class="modal-body p-0">

      </div>
    </div>
  </div>
</div>

<?php get_footer('trainer'); ?>
<script>
  $(document).ready(function() {
    $(".add_booking").select2({
      dropdownParent: $(".sel-customer")
    });
    $(".edit_customer").select2({
      dropdownParent: $(".sel-edit-customer")
    });
  });

  $.validator.addMethod('filesize', function(value, element, param) {
    return this.optional(element) || (element.files[0].size <= param * 1000000)
  }, 'File size must be less than {0} MB');
  var abc = jQuery('.add-user-form').validate({
    ignore: ".ignore",
    onfocusout: function(element) {
      $(element).valid()
    },
    onkeyup: false,
    rules: {
      date: {

        required: true,
      },
      customer: {

        required: true,
      },
      class: {
        required: true,
      }
    },
    messages: {
      customer: {
        required: "<?php _e('Customer is required', 'sidf'); ?>"
      },
    },
    errorElement: "label",
    errorPlacement: function(error, element) {
      if (element.hasClass("select2-hidden-accessible")) {

        element = $("#select2-" + element.attr("id") + "-container").parent();
        error.insertAfter(element);
      } else {
        error.insertAfter(element);
      }
    },

  });

  jQuery(".add-user-form").submit(function(e) {
    e.preventDefault();
    if (abc.form() && abc.form()) {

      jQuery('#loader').show();
      jQuery('.submit-btn').attr('disabled', 'disabled');

      var fd = new FormData(jQuery('.add-user-form')[0]);
      // process the form
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_booking_action',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          location.reload();


        });
    }

    return false;
  });

  jQuery(".filter-form").submit(function(e) {
    e.preventDefault();

    jQuery('#loader').show();

    var fd = new FormData(jQuery('.filter-form')[0]);
    // process the form
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_filter_booking_action&page=1',
        data: fd,
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        var obj = JSON.parse(result);
        jQuery('.more-results').html(obj.html);
        jQuery('#loader').hide();

      });

    return false;
  });

  $(document).on("click", "a.page-numbers", function() {
    jQuery('#loader').show();
    var href = $(this).attr('href');
    var this1 = $(this);
    var fd = new FormData(jQuery('.filter-form')[0]);
    // process the form
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>' + href + '&action=admin_filter_booking_action',
        data: fd,
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        var obj = JSON.parse(result);
        jQuery('.more-results').html(obj.html);
        jQuery('#loader').hide();
      });

    return false;
  });

  $(document).on('change', '.disable-user', function() {
    var disable = 1;
    $('#disable-enable h3').html('Disable Booking');
    $('#disable-enable .content-body').html('<p>Are you sure to disable this Booking ?</p>');
    $('#disable-enable-success h3').html('Disable Booking');
    $('#disable-enable-success .content-body').html('<p>Booking is successfully disabled</p>');
    if (this.checked) {
      disable = 0;
      $('#disable-enable h3').html('Enable Booking');
      $('#disable-enable .content-body').html('<p>Are you sure to enable this Booking ?</p>');
      $('#disable-enable-success h3').html('Enable Booking');
      $('#disable-enable-success .content-body').html('<p>Booking is successfully enabled</p>');

    }
    var user_id = this.value;
    $('.disable-user-id').val(user_id);
    $('.disable-enable-id').val(disable);
    $('#disable-enable').modal('show');

  });

  $(document).on("click", ".disable-enable-close", function() {
    var user_id = $('.disable-user-id').val();
    var disable = $('.disable-enable-id').val();
    if (disable == 1) {
      $("input[type=checkbox][value=" + user_id + "]").prop("checked", true);
    } else {
      $("input[type=checkbox][value=" + user_id + "]").prop("checked", false);
    }
    $('#disable-enable').modal('hide');
  });

  $(document).on("click", ".disable-enable-submit", function() {
    var user_id = $('.disable-user-id').val();
    var disable = $('.disable-enable-id').val();
    $('#disable-enable').modal('hide');
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_enable_disable_member_action&postid=' + user_id + '&disable=' + disable,
        data: '',
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        $('#disable-enable-success').modal('show');

      });
  });

  $(document).on("click", ".delete-user", function() {
    var user_id = $(this).data('index');
    $('.delete-user-id').val(user_id);
    $('#delete-confirm').modal('show');
  });

  $(document).on("click", ".delete-submit", function() {
    var user_id = $('.delete-user-id').val();
    $('#delete-confirm').modal('hide');
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=delete_member_action&id=' + user_id,
        data: '',
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        $('#delete-success').modal('show');

      });
  });

  jQuery('#delete-success').on('hidden.bs.modal', function() {
    location.reload();
  });


  $(document).on("click", ".edit-booking", function() {
    var user_id = $(this).data('index');
    var details = $(this).data('details');
    $('.edit_user_id').val(user_id);
    $('.edit_customer').val(details.customer).trigger('change');
    $('.edit_date').val(details.date);
    $('.edit_class').val(details.class);
    $('#edit-customer').modal('show');

  });

  var abcd = jQuery('.edit-booking-form').validate({
    ignore: ".ignore",
    onfocusout: function(element) {
      $(element).valid()
    },
    onkeyup: false,
    rules: {
      edit_date: {

        required: true,
      },
      edit_customer: {

        required: true,
      },
      edit_class: {
        required: true,
      }

    },
    messages: {
      edit_customer: {
        required: "<?php _e('Customer is required', 'sidf'); ?>"
      },
    },
    errorElement: "label",
    errorPlacement: function(error, element) {
      if (element.hasClass("select2-hidden-accessible")) {
        element = $("#select2-" + element.attr("id") + "-container").parent();
        error.insertAfter(element);
      } else {
        error.insertAfter(element);
      }
    },

  });

  jQuery(".edit-booking-form").submit(function(e) {
    e.preventDefault();
    if (abcd.form() && abcd.form()) {
      $('#edit-customer').modal('hide');
      jQuery('#loader').show();
      jQuery('.submit-btn').attr('disabled', 'disabled');

      var fd = new FormData(jQuery('.edit-booking-form')[0]);
      // process the form
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_edit_booking_action',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          jQuery('#loader').hide();
          $('#edit-booking-success').modal('show');



        });
    }

    return false;
  });
  jQuery('#edit-booking-success').on('hidden.bs.modal', function() {
    location.reload();
  });



  jQuery(".view-log").click(function(e) {
    e.preventDefault();

    var user_id = $(this).data('index');

    // process the form
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=view_booking_log_action&user_id=' + user_id,
        data: '',
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        var obj = JSON.parse(result);
        jQuery('#view-log .modal-body').html(obj.html);
        jQuery('#view-log').modal('show');



      });


    return false;
  });

  $(document).on("change", ".class-field", function() {

    var class_id = $(this).val();
    jQuery('#loader').show();

    // process the form
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_booking_customer_action&class_id=' + class_id,
        data: '',
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        var obj = JSON.parse(result);
        jQuery('.customer-field').html(obj.html);
        jQuery('.customer-field').parents('.form-group').removeClass('d-none');
        jQuery('#loader').hide();


      });

  });

  $(document).on("change", ".date-picker-field", function() {

    var date = $(this).val();
    jQuery('#loader').show();

    // process the form
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_booking_class_action&date=' + date,
        data: '',
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        var obj = JSON.parse(result);
        jQuery('.class-field').html(obj.html);
        jQuery('.class-field').parents('.form-group').removeClass('d-none');
        jQuery('#loader').hide();


      });

  });
</script>