<?php /* Template name: Admin Class */
if ( is_user_logged_in() ) {
    if (current_user_can('administrator') || current_user_can('front_office_user')) {
        $current_user = wp_get_current_user();
    } else {
        wp_redirect(get_home_url());     
    }
}else{
wp_redirect(get_home_url()); 
} 
get_header();?>

<div id="content">
        <!-- section -->
        <div class="section dashboard">
            <div id="loader"></div>
          <div class="container">
            <div class="row align-items-center flex-row-reverse mb-30 mb-md-5">
              <div class="col-md-6 mb-4 mb-md-0">
                <ul class="breadcrumb justify-content-end mb-0">
                  <li><a href="#">Dashboard</a></li>
                  <li><a href="#">Class</a></li>
                  <li>Schedule</li>
                </ul>
              </div>
              <div class="col-md-6">
                <p class="welcome-title">Hello, Welcome <?= $current_user->display_name;?>.</p>
              </div>
            </div>
            <div class="box1 py-4 mb-30">
              <div class="form1">
                <form action="#" class="filter-form">
                  <div class="row align-items-end">
                    <div class="col-xxl-auto col-12 mb-4 mb-xxl-0">
                      <i class="icon"><img src="<?= get_template_directory_uri();?>/assets/images/filter.svg" alt="icon"></i>
                    </div>
                    <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                      <label class="form-label">Date From</label>
                      <div class="date-picker">
                        <input type="text" class="form-control all-date" name="start_date" placeholder="Select from date">
                      </div>
                    </div>
                    <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                      <label class="form-label">To</label>
                      <div class="date-picker">
                        <input type="text" class="form-control all-date" name="end_date" placeholder="Select to date">
                      </div>
                    </div>
                    <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                      <label class="form-label">Type</label>
                        <div class="selectbox">
                          <select class="form-control" name="membership">
                            <option value="">Type</option>
                            <?php 
                                  $args = array(
                                    	'post_type' => 'membership_types',
                                    	'posts_per_page' => -1
                                    );
                                    $query = new WP_Query( $args );
                                  if($query->have_posts()):
                                      while($query->have_posts()): $query->the_post();
                                  ?>
                                <option value="<?= $query->post->ID;?>"><?= get_field('name');?></option>
                                
                                 <?php endwhile; endif;?>
                            
                          </select>
                        </div>
                    </div>
                    <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                      <label class="form-label">Trainer</label>
                      <div>
                        <input type="text" class="form-control" name="keyword" placeholder="Search by Name">
                      </div>
                    </div>
                    <div class="col-xxl-auto col-12">
                      <button class="btn button3" type="submit">Search</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="text-center text-lg-end mb-4">
              <button class="btn button1 btn-sm" data-bs-toggle="modal" data-bs-target="#add-customer">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.999 23.003" class="me-2">
                  <path id="Path_35" data-name="Path 35" d="M16.515,21.525a.709.709,0,0,0,.135.013.721.721,0,0,0,.709-.585,3.918,3.918,0,0,0,.073-.74,4.337,4.337,0,0,0-2.63-3.889,3.158,3.158,0,0,0,1.087-2.38,3.251,3.251,0,0,0-6.5,0,3.16,3.16,0,0,0,1.105,2.394,4.331,4.331,0,0,0-2.6,3.874,4.027,4.027,0,0,0,.072.737.723.723,0,1,0,1.422-.261,2.621,2.621,0,0,1-.049-.476A3.143,3.143,0,0,1,12.664,17.3a3.142,3.142,0,0,1,3.324,2.917,2.528,2.528,0,0,1-.048.471.717.717,0,0,0,.57.841Zm-3.876-9.338a1.757,1.757,0,1,1-1.8,1.756,1.784,1.784,0,0,1,1.8-1.756Z" transform="translate(-4.334 -6.458)" fill="#fff"/>
                  <path id="Path_36" data-name="Path 36" d="M29.611,12.2a.72.72,0,0,0,.722-.718h0V9.328a2.883,2.883,0,0,0-2.889-2.87H7.222a2.882,2.882,0,0,0-2.888,2.87V22.96a2.882,2.882,0,0,0,2.888,2.87h5.563a.73.73,0,0,1,.511.211l3.016,3a1.449,1.449,0,0,0,2.041,0l3.017-3a.73.73,0,0,1,.511-.211h5.562a2.882,2.882,0,0,0,2.89-2.87V15.067a.722.722,0,1,0-1.445,0V22.96a1.442,1.442,0,0,1-1.444,1.44H21.882a2.16,2.16,0,0,0-1.532.628l-3.017,3-3.017-3a2.159,2.159,0,0,0-1.532-.63H7.222a1.442,1.442,0,0,1-1.444-1.435V9.328A1.442,1.442,0,0,1,7.222,7.893H27.444a1.442,1.442,0,0,1,1.444,1.435V11.48a.72.72,0,0,0,.722.718Z" transform="translate(-4.334 -6.458)" fill="#fff"/>
                  <path id="Path_37" data-name="Path 37" d="M22.388,19.372a.719.719,0,0,0,.722-.716h0V16.5h2.167a.718.718,0,1,0,0-1.435H23.111v-2.15a.723.723,0,0,0-1.444,0v2.152H19.5a.718.718,0,1,0,0,1.435h2.167v2.152a.719.719,0,0,0,.722.717Z" transform="translate(-4.334 -6.458)" fill="#fff"/>
                </svg>
                Add Schedule          
              </button>
            </div>
            <div class="more-results">
                  <?php 
                  $args = array(
                    	'post_type' => 'class',
                    	'posts_per_page' => 8
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
            <div class="table1 mb-4">
              <table class="table mb-0">
                <thead>
                  <tr>
                    <td>ID</td>
                    <td>Date</td>     
                    <td>Time(Start-End)</td>
                    <td>Duration</td>
                    <td>Trainer</td>
                    <td>Workouts</td>
                    <td>Schedule</td>
                    <td>Type</td>
                    <td>Bookings</td>
                    <td></td>                    
                  </tr>
                </thead>
                <tbody>
                <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
                  <tr>
                    <td><?= get_field('id');?></td>
                    <?php $date = get_field('date');?>
                    <td><?= date("d F Y", strtotime($date));?></td>
                    <td><?= get_field('start_time').'-'.get_field('end_time');?></td>
                    <?php $to_time = strtotime("2008-12-13 ".get_field('end_time').":00");
                        $from_time = strtotime("2008-12-13 ".get_field('start_time').":00");
                        $hours = round(abs($to_time - $from_time) / 3600,2);?>
                    <td><?= $hours.' hours';?></td>
                    <?php $trainer_id = get_field('trainer');
                    $author_obj = get_user_by('id', $trainer_id);?>
                    <td><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></td>
                    <?php $workout_titles = array();
                    $workouts = explode(",",get_field('workouts'));
                    foreach($workouts as $workout):
                        $workout_titles[] = get_the_title($workout);
                    endforeach;?>
                    <td><?= implode(",",$workout_titles);?></td>
                    <td><?= get_field('class_title');?></td>
                    <td><?= get_field('name',get_field('membership'));?></td>
                    <?php 
                    $class_array = array(
                           'key'     => 'class',
                           'value'   => $query->post->ID,
                           'compare' => '='
                           );
     
                  $args = array(
                    	'post_type' => 'booking',
                    	'numberposts' => -1,
                    	'meta_query' => array(
                            $class_array
                        )
                        
                    );
                    $latest_class = get_posts( $args );
                    ?>
                    <td><?= count($latest_class);?>/<?= get_field('no_of_slots');?></td>
                    <td>
                      <ul class="user-options mb-0">
                      <?php $disable = get_field('is_disable');?>
                        <li>
                          <label class="toggle1">
                            <input type="checkbox" class="disable-user" <?php if(!$disable){ echo 'checked';}?> value="<?= $query->post->ID;?>">
                            <span class="slider"></span>
                          </label>
                        </li>
                        <?php 
                        $formatted_date =  date("m/d/Y", strtotime($date));
                         $data = array(   
                            'class_title' => get_field('class_title'),
                            'class_title_ar' => get_field('class_title_in_arabic'),
                            'date' => $formatted_date,
                            'start_time' => get_field('start_time'),
                            'end_time' => get_field('end_time'),
                            'type' => get_field('membership'),
                            'no_of_slots' => get_field('no_of_slots'),
                            'trainer' => get_field('trainer'),
                            'workouts' =>get_field('workouts')
                        );?>
                        <li>
                          <button type="button" class="option edit-user" data-index="<?= $query->post->ID;?>" data-details='<?php echo json_encode($data);?>'><img src="<?= get_template_directory_uri();?>/assets/images/pencil.svg" alt="icon"></button>
                        </li>
                        <li>
                          <button type="button" class="option delete-user" data-index="<?= $query->post->ID;?>"><img src="<?= get_template_directory_uri();?>/assets/images/bin.svg" alt="icon"></button>
                        </li>
                        <li>
                          <a href="<?php the_permalink();?>" class="option" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                        </li>
                      </ul>
                    </td>
                  </tr>
                  <?php endwhile;?>
                </tbody>
              </table>
            </div>
            <?php
            $pages = paginate_links( array(
                'base' => '%_%',
                'format' => '?page=%#%',
                'current' => max( 1, get_query_var('paged')  ),
                'total' => $query->max_num_pages,
                'end_size' => 1,
                'type'  => 'array',
                'mid_size' => 2,
                'next_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                'prev_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ) );
            ?>            
            <?php if( is_array( $pages ) ) :?>
            <ul class="pagination justify-content-center justify-content-lg-end">
            <?php foreach ( $pages as $page ):?>
                        <li>
                            <?php echo $page;?>
                        </li>
                        <?php endforeach;?>
                    </ul>
            <?php endif;?>
            <?php else:?>
                <div class="alert alert-info" role="alert">
				  No schedules found matching the search criteria. Please try again.
				</div>
            <?php endif;?>
          </div>
        </div>
        <!-- /section -->
      </div>

      <div class="modal1 modal fade" id="add-customer" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Add Schedule</h3>
              </header>
              <div class="modal-body p-0">
                <div class="form1">
                  <form action="#" class="add-user-form">
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label class="form-label">Schedule Title (EN)</label>                        
                        <input type="text" class="form-control" name="class_title" placeholder="Enter Schedule Title">                        
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">Class Schedule (AR)</label>                        
                        <input type="text" class="form-control" name="class_title_ar" placeholder="Enter Schedule title">                        
                      </div>
                      <div class="col-md-6 form-group date-field">
                        <label class="form-label">Select Date</label>
                        <div class="date-picker end-date-picker further-date">
                          <input type="text" class="form-control date_class" name="date[]" placeholder="Select Date">
                        </div>
						  <button type="button" class="option add-new link1 d-flex align-items-center ms-auto mt-1">
                          <span class="fs-1 me-2">&plus;</span>
                          Add more date & time
                        </button>
                      </div>
                       <div class="col-md-3 form-group start-time-field">
                        <label class="form-label">Start Time</label>
                        <div class="selectbox start_class">
                          <select class="form-control start_time" name="start_time[]">
                            <option value="">Select Time</option>
                            <option value="00:00">00:00</option>
                            <option value="00:30">00:30</option>
                            <option value="01:00">01:00</option>
                            <option value="01:30">01:30</option>
                            <option value="02:00">02:00</option>
                            <option value="02:30">02:30</option>
                            <option value="03:00">03:00</option>
                            <option value="03:30">03:30</option>
                            <option value="04:00">04:00</option>
                            <option value="04:30">04:30</option>
                            <option value="05:00">05:00</option>
                            <option value="05:30">05:30</option>
                            <option value="06:00">06:00</option>
                            <option value="06:30">06:30</option>
                            <option value="07:00">07:00</option>
                            <option value="07:30">07:30</option>
                            <option value="08:00">08:00</option>
                            <option value="08:30">08:30</option>
                            <option value="09:00">09:00</option>
                            <option value="09:30">09:30</option>
                            <option value="10:00">10:00</option>
                            <option value="10:30">10:30</option>
                            <option value="11:00">11:00</option>
                            <option value="11:30">11:30</option>
                            <option value="12:00">12:00</option>
                            <option value="12:30">12:30</option>
                            <option value="13:00">13:00</option>
                            <option value="13:30">13:30</option>
                            <option value="14:00">14:00</option>
                            <option value="14:30">14:30</option>
                            <option value="15:00">15:00</option>
                            <option value="15:30">15:30</option>
                            <option value="16:00">16:00</option>
                            <option value="16:30">16:30</option>
                            <option value="17:00">17:00</option>
                            <option value="17:30">17:30</option>
                            <option value="18:00">18:00</option>
                            <option value="18:30">18:30</option>
                            <option value="19:00">19:00</option>
                            <option value="19:30">19:30</option>
                            <option value="20:00">20:00</option>
                            <option value="20:30">20:30</option>
                            <option value="21:00">21:00</option>
                            <option value="21:30">21:30</option>
                            <option value="22:00">22:00</option>
                            <option value="22:30">22:30</option>
                            <option value="23:00">23:00</option>
                            <option value="23:30">23:30</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3 form-group end-time-field">
                        <label class="form-label">End Time</label>
                        <div class="selectbox end_class">
                          <select class="form-control end_time" name="end_time[]">
                            <option value="">Select Time</option>
                            <option value="00:00">00:00</option>
                            <option value="00:30">00:30</option>
                            <option value="01:00">01:00</option>
                            <option value="01:30">01:30</option>
                            <option value="02:00">02:00</option>
                            <option value="02:30">02:30</option>
                            <option value="03:00">03:00</option>
                            <option value="03:30">03:30</option>
                            <option value="04:00">04:00</option>
                            <option value="04:30">04:30</option>
                            <option value="05:00">05:00</option>
                            <option value="05:30">05:30</option>
                            <option value="06:00">06:00</option>
                            <option value="06:30">06:30</option>
                            <option value="07:00">07:00</option>
                            <option value="07:30">07:30</option>
                            <option value="08:00">08:00</option>
                            <option value="08:30">08:30</option>
                            <option value="09:00">09:00</option>
                            <option value="09:30">09:30</option>
                            <option value="10:00">10:00</option>
                            <option value="10:30">10:30</option>
                            <option value="11:00">11:00</option>
                            <option value="11:30">11:30</option>
                            <option value="12:00">12:00</option>
                            <option value="12:30">12:30</option>
                            <option value="13:00">13:00</option>
                            <option value="13:30">13:30</option>
                            <option value="14:00">14:00</option>
                            <option value="14:30">14:30</option>
                            <option value="15:00">15:00</option>
                            <option value="15:30">15:30</option>
                            <option value="16:00">16:00</option>
                            <option value="16:30">16:30</option>
                            <option value="17:00">17:00</option>
                            <option value="17:30">17:30</option>
                            <option value="18:00">18:00</option>
                            <option value="18:30">18:30</option>
                            <option value="19:00">19:00</option>
                            <option value="19:30">19:30</option>
                            <option value="20:00">20:00</option>
                            <option value="20:30">20:30</option>
                            <option value="21:00">21:00</option>
                            <option value="21:30">21:30</option>
                            <option value="22:00">22:00</option>
                            <option value="22:30">22:30</option>
                            <option value="23:00">23:00</option>
                            <option value="23:30">23:30</option>
                          </select>
                        </div>
                      </div>
                       <div class="col-md-6 form-group mb-0">
                        <label class="form-label">Select Membership Type</label>
                        <div class="selectbox">
                            <select class="form-control" name="membership">
                                <!-- <option value="">Select Membership Type</option> -->
                                <?php 
                                  $args = array(
                                    	'post_type' => 'membership_types',
                                    	'posts_per_page' => -1
                                    );
                                    $query = new WP_Query( $args );
                                  if($query->have_posts()):
                                      while($query->have_posts()): $query->the_post();
                                  ?>
                                <option value="<?= $query->post->ID;?>"><?= get_field('name');?></option>
                                
                                 <?php endwhile; endif;?>
                            </select>
                        </div>
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">No of Slots</label>                        
                        <input type="number" class="form-control" name="no_of_slots" placeholder="Enter No of Slots">                        
                      </div>
                     
                      <div class="col-md-6 form-group">
                        <label class="form-label">Select Trainer</label>
                        <div class="selectbox sel-trainer">
                          <select class="form-control add_trainer" name="trainer" id="trainer">
                            <option value="">Select Trainer</option>
                            <?php 
                            $team_presidents = get_users( array( 'role__in' => array( 'trainer'), 'number' =>9999 ) );
                            foreach($team_presidents as $team_president):
                            ?>
                            <option value="<?= $team_president->ID;?>"><?= $team_president->user_firstname.' '.$team_president->user_lastname;?></option>
                            <?php endforeach;?>
                          </select>
                        </div>
                      </div>
                      
                      <div class="col-md-6 form-group">
                        <label class="form-label">Select Workouts</label>
                        <div class="custom-selectbox">
                          <div class="form-control p-0 h-auto">
                            <select class="chosen-select" multiple name="workout[]" data-placeholder="Type workouts name">
                              <!-- <option value="">Select Workouts</option> -->
                               <?php 
                            $args = array(
                              'numberposts' => -1,
                              'post_type'   => 'workout'
                            );
                            
                            $latest_books = get_posts( $args );
                            foreach($latest_books as $member):
                            ?>
                            <option value="<?= $member->ID;?>"><?= $member->post_title;?></option>
                            <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                      </div>
                     

                    </div>

                    <div class="row">
                      <div class="col-md-6 mb-4 mb-md-0">
                        <button class="btn button2 w-100 btn-lg" type="button" data-bs-dismiss="modal">Cancel</button>
                      </div>
                      <div class="col-md-6">
                        <button class="btn button1 w-100 btn-lg submit-btn" type="submit">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

        

        <div class="modal1 modal fade" id="edit-customer" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Edit Schedule</h3>
              </header>
              <div class="modal-body p-0">
                <div class="form1">
                  <form action="#" class="edit-user-form">
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label class="form-label">Class Schedule (EN)</label>                        
                        <input type="text" class="form-control edit_class_title" name="edit_class_title" placeholder="Enter Schedule Title">                        
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">Class Schedule (AR)</label>                        
                        <input type="text" class="form-control edit_class_title_ar" name="edit_class_title_ar" placeholder="Enter Schedule title">                        
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Select Date</label>
                        <div class="date-picker end-date-picker further-date">
                          <input type="text" class="form-control edit_date" name="edit_date" placeholder="Select Date">
                        </div>
                      </div>
                       <div class="col-md-3 form-group">
                        <label class="form-label">Start Time</label>
                        <div class="selectbox">
                          <select class="form-control edit_start_time" name="edit_start_time" id="edit_start">
                            <option value="">Select Time</option>
                            <option value="00:00">00:00</option>
                            <option value="00:30">00:30</option>
                            <option value="01:00">01:00</option>
                            <option value="01:30">01:30</option>
                            <option value="02:00">02:00</option>
                            <option value="02:30">02:30</option>
                            <option value="03:00">03:00</option>
                            <option value="03:30">03:30</option>
                            <option value="04:00">04:00</option>
                            <option value="04:30">04:30</option>
                            <option value="05:00">05:00</option>
                            <option value="05:30">05:30</option>
                            <option value="06:00">06:00</option>
                            <option value="06:30">06:30</option>
                            <option value="07:00">07:00</option>
                            <option value="07:30">07:30</option>
                            <option value="08:00">08:00</option>
                            <option value="08:30">08:30</option>
                            <option value="09:00">09:00</option>
                            <option value="09:30">09:30</option>
                            <option value="10:00">10:00</option>
                            <option value="10:30">10:30</option>
                            <option value="11:00">11:00</option>
                            <option value="11:30">11:30</option>
                            <option value="12:00">12:00</option>
                            <option value="12:30">12:30</option>
                            <option value="13:00">13:00</option>
                            <option value="13:30">13:30</option>
                            <option value="14:00">14:00</option>
                            <option value="14:30">14:30</option>
                            <option value="15:00">15:00</option>
                            <option value="15:30">15:30</option>
                            <option value="16:00">16:00</option>
                            <option value="16:30">16:30</option>
                            <option value="17:00">17:00</option>
                            <option value="17:30">17:30</option>
                            <option value="18:00">18:00</option>
                            <option value="18:30">18:30</option>
                            <option value="19:00">19:00</option>
                            <option value="19:30">19:30</option>
                            <option value="20:00">20:00</option>
                            <option value="20:30">20:30</option>
                            <option value="21:00">21:00</option>
                            <option value="21:30">21:30</option>
                            <option value="22:00">22:00</option>
                            <option value="22:30">22:30</option>
                            <option value="23:00">23:00</option>
                            <option value="23:30">23:30</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3 form-group">
                        <label class="form-label">End Time</label>
                        <div class="selectbox">
                          <select class="form-control edit_end_time" name="edit_end_time">
                            <option value="">Select Time</option>
                            <option value="00:00">00:00</option>
                            <option value="00:30">00:30</option>
                            <option value="01:00">01:00</option>
                            <option value="01:30">01:30</option>
                            <option value="02:00">02:00</option>
                            <option value="02:30">02:30</option>
                            <option value="03:00">03:00</option>
                            <option value="03:30">03:30</option>
                            <option value="04:00">04:00</option>
                            <option value="04:30">04:30</option>
                            <option value="05:00">05:00</option>
                            <option value="05:30">05:30</option>
                            <option value="06:00">06:00</option>
                            <option value="06:30">06:30</option>
                            <option value="07:00">07:00</option>
                            <option value="07:30">07:30</option>
                            <option value="08:00">08:00</option>
                            <option value="08:30">08:30</option>
                            <option value="09:00">09:00</option>
                            <option value="09:30">09:30</option>
                            <option value="10:00">10:00</option>
                            <option value="10:30">10:30</option>
                            <option value="11:00">11:00</option>
                            <option value="11:30">11:30</option>
                            <option value="12:00">12:00</option>
                            <option value="12:30">12:30</option>
                            <option value="13:00">13:00</option>
                            <option value="13:30">13:30</option>
                            <option value="14:00">14:00</option>
                            <option value="14:30">14:30</option>
                            <option value="15:00">15:00</option>
                            <option value="15:30">15:30</option>
                            <option value="16:00">16:00</option>
                            <option value="16:30">16:30</option>
                            <option value="17:00">17:00</option>
                            <option value="17:30">17:30</option>
                            <option value="18:00">18:00</option>
                            <option value="18:30">18:30</option>
                            <option value="19:00">19:00</option>
                            <option value="19:30">19:30</option>
                            <option value="20:00">20:00</option>
                            <option value="20:30">20:30</option>
                            <option value="21:00">21:00</option>
                            <option value="21:30">21:30</option>
                            <option value="22:00">22:00</option>
                            <option value="22:30">22:30</option>
                            <option value="23:00">23:00</option>
                            <option value="23:30">23:30</option>
                          </select>
                        </div>
                      </div><div class="col-md-6 form-group mb-0">
                        <label class="form-label">Select Type</label>
                        <div class="selectbox">
                            <select class="form-control edit_membership" disabled="disabled" name="edit_membership">
                                <!-- <option value="">Select Membership Type</option> -->
                                <?php 
                                  $args = array(
                                    	'post_type' => 'membership_types',
                                    	'posts_per_page' => -1
                                    );
                                    $query = new WP_Query( $args );
                                  if($query->have_posts()):
                                      while($query->have_posts()): $query->the_post();
                                  ?>
                                <option value="<?= $query->post->ID;?>"><?= get_field('name');?></option>
                                
                                 <?php endwhile; endif;?>
                            </select>
                        </div>
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">No of Slots</label>                        
                        <input type="number" class="form-control edit_no_of_slots" name="edit_no_of_slots" placeholder="Enter No of Slots">                        
                      </div>
                     
                      <div class="col-md-6 form-group">
                        <label class="form-label">Select Trainer</label>
                        <div class="selectbox sel-edit-trainer">
                          <select class="form-control edit_trainer" name="edit_trainer" id="edit_trainer">
                            <option value="">Select Trainer</option>
                            <?php 
                            $team_presidents = get_users( array( 'role__in' => array( 'trainer'), 'number' =>9999 ) );
                            foreach($team_presidents as $team_president):
                            ?>
                            <option value="<?= $team_president->ID;?>"><?= $team_president->user_firstname.' '.$team_president->user_lastname;?></option>
                            <?php endforeach;?>
                          </select>
                        </div>
                      </div>
                      
                      <div class="col-md-6 form-group">
                        <label class="form-label">Select Workouts</label>
                        <div class="custom-selectbox">
                          <div class="form-control p-0 h-auto">
                            <select class="chosen-select edit_workout" multiple name="edit_workout[]" data-placeholder="Type workouts name">
                              <!-- <option value="">Select Workouts</option> -->
                               <?php 
                            $args = array(
                              'numberposts' => -1,
                              'post_type'   => 'workout'
                            );
                            
                            $latest_books = get_posts( $args );
                            foreach($latest_books as $member):
                            ?>
                            <option value="<?= $member->ID;?>"><?= $member->post_title;?></option>
                            <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 mb-4 mb-md-0">
                        <button class="btn button2 w-100 btn-lg" type="button" data-bs-dismiss="modal">Cancel</button>
                      </div>
                      <div class="col-md-6">
                        <input type="hidden" name="user_id" class="edit_user_id" value="">
                        <button class="btn button1 w-100 btn-lg" type="submit">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="modal1 modal fade" id="disable-enable" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1"></h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"></div>
                <div class="row">
                  <div class="col-md-6 mb-4 mb-md-0">
                    <button class="btn button2 w-100 btn-lg disable-enable-close" type="button">No</button>
                  </div>
                  <div class="col-md-6">
                    <button class="btn button1 w-100 btn-lg disable-enable-submit" type="button">Yes</button>
                  </div>
                </div>
              </div>
              <input type="hidden" class="disable-user-id" value="">
              <input type="hidden" class="disable-enable-id" value="">
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="disable-enable-success" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1"></h3>
              </header>
              <div class="modal-body text-center p-0">
                <div class="content-body"></div>
                <div class="row">
                  <div class="col-md-6 mx-auto">
                    <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="delete-confirm" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Delete Schedule</h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"><p>Are you sure want to delete this Schedule ?</p></div>
                <div class="row">
                  <div class="col-md-6 mb-4 mb-md-0">
                    <button class="btn button2 w-100 btn-lg" data-bs-dismiss="modal" type="button">No</button>
                  </div>
                  <div class="col-md-6">
                    <button class="btn button1 w-100 btn-lg delete-submit" type="button">Yes</button>
                  </div>
                </div>
              </div>
              <input type="hidden" class="delete-user-id" value="">
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="delete-success" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Delete Schedule</h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"><p>Schedule has been successfully deleted</p></div>
                <div class="row">
                  <div class="col-md-6 mb-4 mb-md-0">
                   <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        

        

        <div class="modal1 modal fade" id="edit-user-success" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Edit Schedule</h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"><p>Schedule has been successfully updated</p></div>
                <div class="row">
                  <div class="col-md-6 mb-4 mb-md-0">
                   <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        

        <div class="modal1 modal fade" id="view-log" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">View Note</h3>
              </header>
              <div class="modal-body p-0">
                
              </div>
            </div>
          </div>
        </div>



<?php get_footer();?>
<script>
  $(document).ready(function() {
      $(".edit_trainer").select2({
        dropdownParent: $(".sel-edit-trainer")
      });
      $(".add_trainer").select2({
        dropdownParent: $(".sel-trainer")
      });
    });

      $.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param * 1000000)
}, 'File size must be less than {0} MB');
       var abc = jQuery('.add-user-form').validate({
            // ignore: ".ignore",
            // onfocusout: function(element) {$(element).valid()},
            // onkeyup: false,
            ignore: [],
            rules:
            {
                trainer:
                {

                    required: true,  
                },
                class_title:
                {

                    required: true, 
                },
                'date[]':
                {
                    required: true, 
                },
                'start_time[]':
                {
                    required: true, 
                },
               'end_time[]':
                {
                    required: true,
                },
                membership:
                {
                    required: true,
                },
                no_of_slots:
                {
                    required: true,
					          digits:true
                },
                'workout[]':
                {
                    required: true,
                }
            },
            messages: {
              
              trainer: {
                required: "<?php _e('Trainer is required', 'sidf'); ?>"
              },
              
            },
            errorElement: "label",
            errorPlacement: function(error, element) {
              if (element.hasClass("select2-hidden-accessible")) {
                element = $("#select2-" + element.attr("id") + "-container").parent();
                error.insertAfter(element);
              } else {
                error.insertAfter(element);
              }
            },
           

        });

        jQuery(".add-user-form").submit(function(e){
            e.preventDefault();
            $('input.date_class').each(function() {
              if($(this).val() == ''){
                $(this).addClass('error');
              }else{
                $(this).removeClass('error');
              }
            }); 
            $('select.start_time').each(function() {
              if($(this).val() == ''){
                $(this).addClass('error');
              }else{
                $(this).removeClass('error');
              }
            });
            $('select.end_time').each(function() {
              if($(this).val() == ''){
                $(this).addClass('error');
              }else{
                $(this).removeClass('error');
              }
            }); 
           if($('.chosen-select').val() == ''){
            $('.chosen-select').parent().addClass('error');
           }else{
            $('.chosen-select').parent().removeClass('error');
           }

           var start_time = $('select[name="start_time[]"]').map(function() {
                if(this.value){
                  return this.value;
                }
            }).get();

            var end_time = $('select[name="end_time[]"]').map(function() {
                if(this.value){
                  return this.value;
                }
            }).get();

            var isValid = true;

            if(start_time.length > 0){
              $.each(start_time, function(index, start) {
                var end = end_time[index];
                if (end <= start) {
                  isValid = false;
                  return false;
                }
              });

              if (!isValid) {
                $('#time_error').remove();
                var errorDiv = $('<div id="time_error" style="color:red">').text('Invalid time range');
                $('.start-time-field').append(errorDiv);
                return false;
              }else{
                $('#time_error').remove();
              }
            }

            if(abc.form()  && abc.form())
            {
                
                 jQuery('#loader').show();
                 jQuery('.submit-btn').attr('disabled','disabled');
                
                    var fd= new FormData(jQuery('.add-user-form')[0]);
                    // process the form
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=admin_class_action',
                        data: fd,
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                           location.reload();                      


                    });
            }

            return false;
        });

        jQuery(".filter-form").submit(function(e) {
    e.preventDefault();
      
        jQuery('#loader').show();
      
        var fd= new FormData(jQuery('.filter-form')[0]);
        // process the form
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>?action=admin_filter_class_action&page=1',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            var obj = JSON.parse(result);     
            jQuery('.more-results').html(obj.html); 
             jQuery('#loader').hide();
             
          });
        
        return false;
    });

    $( document ).on( "click", "a.page-numbers", function() {
        jQuery('#loader').show();
        var href = $(this).attr('href');
        var this1 = $(this);
        var fd= new FormData(jQuery('.filter-form')[0]);
        // process the form
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>'+href+'&action=admin_filter_class_action',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            var obj = JSON.parse(result);     
            jQuery('.more-results').html(obj.html); 
             jQuery('#loader').hide();          
          });
        
        return false;
    });

    $(document).on('change', '.disable-user', function() {
        var disable = 1;
            $('#disable-enable h3').html('Disable Schedule');
            $('#disable-enable .content-body').html('<p>Are you sure to disable this Schedule ?</p>');
            $('#disable-enable-success h3').html('Disable Schedule');
            $('#disable-enable-success .content-body').html('<p>Schedule is successfully disabled</p>');
        if(this.checked) {
        disable = 0;
        $('#disable-enable h3').html('Enable Schedule');
        $('#disable-enable .content-body').html('<p>Are you sure to enable this Schedule ?</p>');
        $('#disable-enable-success h3').html('Enable Schedule');
        $('#disable-enable-success .content-body').html('<p>Schedule is successfully enabled</p>');
            
        }
        var user_id = this.value;
        $('.disable-user-id').val(user_id);
        $('.disable-enable-id').val(disable);
        $('#disable-enable').modal('show');
         
    });

    $( document ).on( "click", ".disable-enable-close", function() {
        var user_id = $('.disable-user-id').val();
        var disable = $('.disable-enable-id').val();
        if(disable==1){
          $("input[type=checkbox][value="+user_id+"]").prop("checked",true);
        }else{
          $("input[type=checkbox][value="+user_id+"]").prop("checked",false);
        }
        $('#disable-enable').modal('hide');
    });

    $( document ).on( "click", ".disable-enable-submit", function() {
        var user_id = $('.disable-user-id').val();
        var disable = $('.disable-enable-id').val();
        $('#disable-enable').modal('hide');
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>?action=admin_enable_disable_member_action&postid='+user_id+'&disable='+disable,
          data: '',
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            $('#disable-enable-success').modal('show'); 
             
          });
    });

    $( document ).on( "click", ".delete-user", function() {
        var user_id = $(this).data('index');
        $('.delete-user-id').val(user_id);
        $('#delete-confirm').modal('show'); 
    });

    $( document ).on( "click", ".delete-submit", function() {
        var user_id = $('.delete-user-id').val();
        $('#delete-confirm').modal('hide');
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>?action=delete_member_action&id='+user_id,
          data: '',
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            $('#delete-success').modal('show'); 
             
          });
    });

    jQuery('#delete-success').on('hidden.bs.modal', function () {
      location.reload();
    });

    
    $( document ).on( "click", ".edit-user", function() {
        var user_id = $(this).data('index');
        var details = $(this).data('details');
        $('.edit_user_id').val(user_id);
        $('.edit_class_title').val(details.class_title);
        $('.edit_class_title_ar').val(details.class_title_ar);
        $('.edit_date').val(details.date);
        $('.edit_start_time').val(details.start_time);
        $('.edit_end_time').val(details.end_time);
        //$('input[name=edit_type][value="'+details.type+'"]').prop("checked",true);
        $('.edit_membership').val(details.type);
        $('.edit_no_of_slots').val(details.no_of_slots);
        $('.edit_trainer').val(details.trainer).trigger('change');
        var str_array = details.workouts.split(',');
        $(".edit_workout").val(str_array).trigger("chosen:updated");
        $('#edit-customer').modal('show');
        
    });

    var abcd = jQuery('.edit-user-form').validate({
            // ignore: ".ignore",
            // onfocusout: function(element) {$(element).valid()},
            // onkeyup: false,
            ignore: [],
            rules:
            {
                edit_trainer:
                {

                    required: true,  
                },
                edit_class_title:
                {

                    required: true, 
                },
                edit_date:
                {
                    required: true, 
                },
                edit_start_time:
                {
                    required: true, 
                },
                edit_end_time:
                {
                    required: true,
                    greaterThan: "#edit_start"
                },
                edit_membership:
                {
                    required: true,
                },
                edit_no_of_slots:
                {
                    required: true,
					digits:true
                },
                'edit_workout[]':
                {
                    required: true,
                }
                
            },
            messages: {
              edit_trainer: {
                required: "<?php _e('Trainer is required', 'sidf'); ?>"
              },
              edit_end_time: {
                      greaterThan : "Invalid time range"
                  },
            },
            errorElement: "label",
            errorPlacement: function(error, element) {
              if (element.hasClass("select2-hidden-accessible")) {
                element = $("#select2-" + element.attr("id") + "-container").parent();
                error.insertAfter(element);
              }else if(element.attr("name") == "edit_end_time" ) {
                  error.insertAfter("#edit_start");
              }else {
                error.insertAfter(element);
              }
            },

        });

        $.validator.addMethod("greaterThan", function(value, element, params) {

        if (value > $(params).val()) {
            return value
        } 
        },'Must be greater than {0}.');

        jQuery(".edit-user-form").submit(function(e){
            e.preventDefault();
            if($('.chosen-select').val() == ''){
              $('.chosen-select').parent().addClass('error');
            }else{
              $('.chosen-select').parent().removeClass('error');
            }
            if(abcd.form()  && abcd.form())
            {
              $('#edit-customer').modal('hide');
                 jQuery('#loader').show();
                 jQuery('.submit-btn').attr('disabled','disabled');
                
                    var fd= new FormData(jQuery('.edit-user-form')[0]);
                    // process the form
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=admin_edit_class_action',
                        data: fd,
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                        jQuery('#loader').hide();
                          $('#edit-user-success').modal('show');
                      


                    });
            }

            return false;
        });
        jQuery('#edit-user-success').on('hidden.bs.modal', function () {
      location.reload();
    });

    

        jQuery(".view-log").click(function(e){
            e.preventDefault();
                
                    var user_id = $(this).data('index');
                     
                    // process the form
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=view_measurement_action&user_id='+user_id,
                        data: '',
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                            var obj = JSON.parse(result);     
                            jQuery('#view-log .modal-body').html(obj.html); 
                            jQuery('#view-log').modal('show');
                      


                    });
            

            return false;
        });
        
        jQuery( document ).on( "click", ".add-new", function() {
            $(this).hide();
            var body = '<div class="date-picker end-date-picker further-date"><input type="text" class="form-control date_class" name="date[]" placeholder="Select Date"><button type="button" class="option add-new"><i class="fa fa-solid fa-plus"></i></button></div>';
            var start_time_section = '<div class="selectbox start_class" style="margin-top:15px"><select class="form-control start_time" name="start_time[]">';
            start_time_section += '<option value="">Select Time</option><option value="00:00">00:00</option><option value="00:30">00:30</option><option value="01:00">01:00</option><option value="01:30">01:30</option><option value="02:00">02:00</option><option value="02:30">02:30</option><option value="03:00">03:00</option><option value="03:30">03:30</option><option value="04:00">04:00</option>';
            start_time_section += '<option value="04:30">04:30</option><option value="05:00">05:00</option><option value="05:30">05:30</option><option value="06:00">06:00</option><option value="06:30">06:30</option><option value="07:00">07:00</option><option value="07:30">07:30</option><option value="08:00">08:00</option>';
            start_time_section += '<option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option>';
            start_time_section += '<option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option>';
            start_time_section += '<option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option><option value="18:30">18:30</option><option value="19:00">19:00</option><option value="19:30">19:30</option><option value="20:00">20:00</option><option value="20:30">20:30</option><option value="21:00">21:00</option><option value="21:30">21:30</option>';
            start_time_section += '<option value="22:00">22:00</option><option value="22:30">22:30</option><option value="23:00">23:00</option><option value="23:30">23:30</option>';
            start_time_section += '</select></div>';

            var end_time_section = '<div class="selectbox end_class" style="margin-top:15px"><select class="form-control end_time" name="end_time[]">';
            end_time_section += '<option value="">Select Time</option><option value="00:00">00:00</option><option value="00:30">00:30</option><option value="01:00">01:00</option><option value="01:30">01:30</option><option value="02:00">02:00</option><option value="02:30">02:30</option><option value="03:00">03:00</option><option value="03:30">03:30</option><option value="04:00">04:00</option>';
            end_time_section += '<option value="04:30">04:30</option><option value="05:00">05:00</option><option value="05:30">05:30</option><option value="06:00">06:00</option><option value="06:30">06:30</option><option value="07:00">07:00</option><option value="07:30">07:30</option><option value="08:00">08:00</option>';
            end_time_section += '<option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option>';
            end_time_section += '<option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option>';
            end_time_section += '<option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option><option value="18:30">18:30</option><option value="19:00">19:00</option><option value="19:30">19:30</option><option value="20:00">20:00</option><option value="20:30">20:30</option><option value="21:00">21:00</option><option value="21:30">21:30</option>';
            end_time_section += '<option value="22:00">22:00</option><option value="22:30">22:30</option><option value="23:00">23:00</option><option value="23:30">23:30</option>';
            end_time_section += '</select></div>';

            jQuery('.date-field').append(body);
            jQuery('.start-time-field').append(start_time_section);
            jQuery('.end-time-field').append(end_time_section);

            $(".further-date").each(function () {
        		$(this).find(".form-control").datepicker({
        			changeMonth: true,
        			changeYear: true,
        			showOn: "both",
        			buttonImage: "../wp-content/themes/hotstone-fitness/assets/images/calendar-icon.svg",
        			buttonImageOnly: true,
        			buttonText: "Select date",
        			minDate: "+1d",
        			yearRange: "c:2030"
        		});
        	});
            
        });

        jQuery('.start_time').change(function(){
			jQuery('.end_time').val('');
			var valueindex = jQuery(this).prop('selectedIndex');
			$('.end_time option').each(function(index,value) {
				if(index <= valueindex) {
					$(this).hide();
				} else{
					$(this).show();
				}
			});
			
		});
	
	jQuery('.edit_start_time').change(function(){
			jQuery('.edit_end_time').val('');
			var valueindex = jQuery(this).prop('selectedIndex');
			$('.edit_end_time option').each(function(index,value) {
				if(index <= valueindex) {
					$(this).hide();
				} else{
					$(this).show();
				}
			});
			
		});
        
</script>
