<?php get_header('page');?>
       <?php if(get_field('slideshow')):?>
       <div id="banner" class="animated-box">
            <div class="row g-0">
                <div class="col left-col relative">
                    <div class="owl-carousel owl-theme">
                        <?php while(has_sub_field('slideshow')):?>
                        <div class="item">
                            <figure class="figure1 cover">
                                <img class="w-100" src="<?php the_sub_field('image');?>" alt="img">
                            </figure>
                            <div class="description">
                                <h2><?php the_sub_field('title');?></h2>
                                <?php the_sub_field('content');?>
                                <footer>
                                    <a href="<?php the_sub_field('link');?>" class="btn button1">read more</a>
                                </footer>
                            </div>
                        </div>
                        <?php endwhile;?>
                    </div>
                </div>
                <div class="col-md-auto right-col">
                    <?php if(get_field('right_sections')):?>
                    <ul class="classes-option">
                        <?php while(has_sub_field('right_sections')):?>
                        <li>
                            <figure class="figure1">
                                <img class="w-100" src="<?php the_sub_field('image');?>" alt="img">
                            </figure>
                            <h5><a href="<?php the_sub_field('link');?>"><?php the_sub_field('title');?></a></h5>
                            <p><?php the_sub_field('content');?></p>
                        </li>
                        <?php endwhile;?>
                    </ul>
                    <?php endif;?>
                </div>
            </div>

        </div>
        <?php endif;?>
        <!-- /banner -->
        <div id="content">
            <!-- section -->
            <div class="section pull-top pt-lg-0 pb-0 pb-md-5 animated-box" id="class_div">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-5 col-md-5 column">
                            <figure class="figure1 c-shadow">
                                <img class="w-100" src="<?php the_field('about_image');?>" alt="img">
                            </figure>
                        </div>
                        <div class="col-xl-5 col-md-7 d-flex flex-wrap ps-lg-5">
                            <div class="my-auto w-100">
                                <h1><?php the_field('about_title');?></h1>
                                <?php if(get_field('stats')):?>
                                <ul class="stats">
                                    <?php while(has_sub_field('stats')):?>
                                    <li>
                                        <span class="count"><?php the_sub_field('number');?></span>
                                        <span class="content"><?php the_sub_field('title');?> </span>
                                    </li>
                                    <?php endwhile;?>
                                </ul>
                                <?php endif;?>
                                <?php the_field('about_content');?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /section -->
            <!-- section -->
            <?php if(get_field('offers')):?>
            <div class="section we-offer pb-0 pb-md-5 animated-box">
                <div class="container container-md">
                    <header>
                        <h2><?php the_field('what_title');?></h2>
                    </header>
                    <div class="row">
                        <?php $i=1; while(has_sub_field('offers')):?>
                        <div class="col-md-6 column relative <?php if($i%2==0): echo 'right-col'; else: echo 'left-col'; endif;?>">
                            <?php if($i%2==0):?>
                            <div class="image">
                                <img src="<?php the_sub_field('image');?>" alt="img">
                            </div>
                            <?php endif;?>
                            <div class="offer">
                                <h3><?php the_sub_field('title');?></h3>
                                <?php if(get_sub_field('features')):?>
                                <ul class="list">
                                    <?php while(has_sub_field('features')):?>
                                    <li><?php the_sub_field('feature');?></li>
                                    <?php endwhile;?>
                                  
                                </ul>
                                <?php endif;?>
                                <span class="triangle"></span>
                            </div>
                            <?php if($i%2!=0):?>
                            <div class="image">
                                <img src="<?php the_sub_field('image');?>" alt="img">
                            </div>
                            <?php endif;?>
                        </div>
                        <?php $i++; endwhile;?>
                        
                    </div>
                </div>
            </div>
            <?php endif;?>
            <!-- /section -->
            <!-- section -->
            <div class="section team pb-0 pb-md-5 animated-box" id="about_div">
                <div class="container container-md">
                    <div class="row align-items-center">
                        <div class="col-md-4 pe-lg-5 column">
                            <h2><?php the_field('member_title');?></h2>
                        </div>
                        <div class="col-md-4 relative column">
                            <figure class="figure1 c-shadow">
                                <img class="w-100" src="<?php the_field('member_image');?>" alt="img">
                            </figure>
                            <footer>
                                <a href="<?php the_field('youtube_link');?>" data-fancybox class="btn button2">watch video <i class="fa fa-youtube-play ms-1 mt-1"
                                        aria-hidden="true"></i></a>
                            </footer>
                        </div>
                        <div class="col-md-4 ps-lg-5 column">
                            <h4><?php the_field('name');?></h4>
                            <p class="position"><?php the_field('designation');?></p>
                            <?php the_field('details');?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /section -->
            <!-- section -->
            <?php if(get_field('memberships')):?>
            <div class="section" id="membership_div">
                <div class="container container-md animated-box">
                    <header>
                        <h2><?php the_field('membership_title');?></h2>
                    </header>
                    <div class="row">
                        <?php while(has_sub_field('memberships')):?>
                        <div class="col-md-4 column">
                            <div class="card">
                                <figure class="figure1">
                                    <img class="w-100" src="<?php the_sub_field('image');?>" alt="img">
                                </figure>
                                <div class="content">
                                    <h3>
                                        <i class="icon">
                                            <img src="<?php the_sub_field('icon');?>" alt="icon">
                                        </i>
                                        <?php the_sub_field('title');?>
                                    </h3>
                                    <p><?php the_sub_field('description');?></p>
                                    <p class="price">
                                        sar <span><?php the_sub_field('amount');?></span>
                                    </p>
                                    <p><?php the_sub_field('number_of_session');?></p>
                                </div>
                                <footer>
                                    <a href="<?php the_sub_field('link');?>" class="btn button2 text-uppercase">get started</a>
                                </footer>
                            </div>
                        </div>
                        <?php endwhile;?>
                        
                    </div>
                </div>
            </div>
            <?php endif;?>
            <!-- /section -->
            <!-- section -->
            <?php if(get_field('galleries')):?>
            <div class="section gallery py-0 animated-box" id="gallery_div">
                <div class="container-fluid">
                    <header>
                        <h2><?php the_field('gallery_title');?></h2>
                    </header>
                    <div class="row">
                        <?php while(has_sub_field('galleries')):?>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-6 px-0">
                            <a href="<?php the_sub_field('image');?>" data-fancybox>
                                <img src="<?php the_sub_field('image');?>" alt="img">
                            </a>
                        </div>
                        <?php endwhile;?>
                  
                    </div>
                </div>
            </div>
            <?php endif;?>
            <!-- /section -->
            <!-- section -->
            <div class="section animated-box d-none">
                <div class="container container-md">
                    <header>
                        <h2>Classes & Schedules</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
                    </header>
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/schedule.jpg" alt="img">
                </div>
            </div>
            <!-- /section -->
            <!-- section -->
            <div class="section bg-color1 contact-us py-0 animated-box" id="contact_div">
                <div class="container-fluid px-0">
                    <div class="row g-0">
                        <div class="col-lg-7 col-md-6">
                            <div class="map h-100">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14497.943608819718!2d46.6634148!3d24.7101934!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x55bcce979e4f06d9!2sHot%20Stone%20Spa%20Women!5e0!3m2!1sen!2sin!4v1662633177893!5m2!1sen!2sin" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6 d-flex flex-wrap text-white">
                            <div class="inner">
                                <h3>
                                    Send <br>
                                    your queries
                                </h3>
                                <div class="form">
                                    <?php echo do_shortcode(get_field('form'));?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /section -->
        </div>

<?php get_footer('page');?>