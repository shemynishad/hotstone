<?php /* Template name: Admin Subscriptions */
if (is_user_logged_in()) {
  if (current_user_can('administrator') || current_user_can('front_office_user')) {
    $current_user = wp_get_current_user();
  } else {
    wp_redirect(get_home_url());
  }
} else {
  wp_redirect(get_home_url());
}
get_header(); ?>

<div id="content">
  <!-- section -->
  <div class="section dashboard">
    <div id="loader"></div>
    <div class="container">
      <div class="row align-items-center flex-row-reverse mb-30 mb-md-5">
        <div class="col-md-6 mb-4 mb-md-0">
          <ul class="breadcrumb justify-content-end mb-0">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Memberships</a></li>
            <li>Subscriptions</li>
          </ul>
        </div>
        <div class="col-md-6">
          <p class="welcome-title">Hello, Welcome <?= $current_user->display_name; ?>.</p>
        </div>
      </div>
      <div class="box1 py-4 mb-30">
        <div class="form1">
          <form action="#" class="filter-form">
            <div class="row align-items-end">
              <div class="col-xxl-auto col-12 mb-4 mb-xxl-0">
                <i class="icon"><img src="<?= get_template_directory_uri(); ?>/assets/images/filter.svg" alt="icon"></i>
              </div>
              <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                <label class="form-label">Membership Type</label>
                <div class="selectbox">
                  <select class="form-control" name="membership">
                    <option value="">Membership Type</option>
                    <?php
                    $args = array(
                      'numberposts' => -1,
                      'post_type'   => 'membership_types'
                    );

                    $latest_books = get_posts($args);
                    foreach ($latest_books as $member) :
                    ?>
                      <option value="<?= $member->ID; ?>"><?= $member->post_title; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                <label class="form-label">Customer</label>
                <div>
                  <input type="text" class="form-control" name="keyword" placeholder="Search by Name">
                </div>
              </div>
              <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                <label class="form-label">Registration From</label>
                <div class="date-picker">
                  <input type="text" class="form-control all-date" name="start_date" placeholder="Select from date">
                </div>
              </div>
              <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                <label class="form-label">To</label>
                <div class="date-picker">
                  <input type="text" class="form-control all-date" name="end_date" placeholder="Select to date">
                </div>
              </div>
              <div class="col-xxl-auto col-12">
                <button class="btn button3" type="submit">Search</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="text-center text-lg-end mb-4">
        <button class="btn button1 btn-sm" data-bs-toggle="modal" data-bs-target="#add-customer">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.999 23.003" class="me-2">
            <path id="Path_35" data-name="Path 35" d="M16.515,21.525a.709.709,0,0,0,.135.013.721.721,0,0,0,.709-.585,3.918,3.918,0,0,0,.073-.74,4.337,4.337,0,0,0-2.63-3.889,3.158,3.158,0,0,0,1.087-2.38,3.251,3.251,0,0,0-6.5,0,3.16,3.16,0,0,0,1.105,2.394,4.331,4.331,0,0,0-2.6,3.874,4.027,4.027,0,0,0,.072.737.723.723,0,1,0,1.422-.261,2.621,2.621,0,0,1-.049-.476A3.143,3.143,0,0,1,12.664,17.3a3.142,3.142,0,0,1,3.324,2.917,2.528,2.528,0,0,1-.048.471.717.717,0,0,0,.57.841Zm-3.876-9.338a1.757,1.757,0,1,1-1.8,1.756,1.784,1.784,0,0,1,1.8-1.756Z" transform="translate(-4.334 -6.458)" fill="#fff" />
            <path id="Path_36" data-name="Path 36" d="M29.611,12.2a.72.72,0,0,0,.722-.718h0V9.328a2.883,2.883,0,0,0-2.889-2.87H7.222a2.882,2.882,0,0,0-2.888,2.87V22.96a2.882,2.882,0,0,0,2.888,2.87h5.563a.73.73,0,0,1,.511.211l3.016,3a1.449,1.449,0,0,0,2.041,0l3.017-3a.73.73,0,0,1,.511-.211h5.562a2.882,2.882,0,0,0,2.89-2.87V15.067a.722.722,0,1,0-1.445,0V22.96a1.442,1.442,0,0,1-1.444,1.44H21.882a2.16,2.16,0,0,0-1.532.628l-3.017,3-3.017-3a2.159,2.159,0,0,0-1.532-.63H7.222a1.442,1.442,0,0,1-1.444-1.435V9.328A1.442,1.442,0,0,1,7.222,7.893H27.444a1.442,1.442,0,0,1,1.444,1.435V11.48a.72.72,0,0,0,.722.718Z" transform="translate(-4.334 -6.458)" fill="#fff" />
            <path id="Path_37" data-name="Path 37" d="M22.388,19.372a.719.719,0,0,0,.722-.716h0V16.5h2.167a.718.718,0,1,0,0-1.435H23.111v-2.15a.723.723,0,0,0-1.444,0v2.152H19.5a.718.718,0,1,0,0,1.435h2.167v2.152a.719.719,0,0,0,.722.717Z" transform="translate(-4.334 -6.458)" fill="#fff" />
          </svg>
          Add Subscriptions
        </button>
      </div>
      <div class="more-results">
        <?php
        $args = array(
          'post_type' => 'subscription',
          'posts_per_page' => 8
        );
        $query = new WP_Query($args);
        if ($query->have_posts()) :
        ?>
          <div class="table1 mb-4">
            <table class="table mb-0">
              <thead>
                <tr>
                  <td>ID</td>
                  <td>Customer</td>
                  <td>Membership</td>
                  <td>Registration date</td>
                  <td>Expiry date</td>
                  <td>Rem. Sessions</td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                <?php while ($query->have_posts()) : $query->the_post();

                ?>
                  <tr>
                    <td><?= get_field('id'); ?></td>
                    <?php $customer_id = get_field('customer');
                    $author_obj = get_user_by('id', $customer_id); ?>
                    <td><?= $author_obj->user_firstname . ' ' . $author_obj->user_lastname; ?></td>
                    <?php $membership = get_field('membership_type'); ?>
                    <td><?= get_field('name', $membership); ?></td>
                    <?php $start_date = get_field('start_date'); ?>
                    <td><?= date("d F Y", strtotime($start_date)); ?></td>
                    <?php $end_date = get_field('end_date'); ?>
                    <?php
                    if($end_date == ''){
                      echo '<td>N/A</td>';
                    }else{
                      echo '<td>'.date("d F Y", strtotime($end_date)).'</td>';
                    }
                    ?>
                    <?php
                        if (get_field('options') == 'session') {
                          $no_of_sessions = get_field('remaining_sessions');
                        }else{
                          $no_of_sessions = 'N/A';
                        }
                        ?>
                        <td><?= $no_of_sessions; ?></td>
                    <td>
                      <ul class="user-options mb-0">
                        <?php $disable = get_field('is_disable'); ?>
                        <li>
                          <label class="toggle1">
                            <input type="checkbox" class="disable-user" <?php if (!$disable) {
                                                                          echo 'checked';
                                                                        } ?> value="<?= $query->post->ID; ?>">
                            <span class="slider"></span>
                          </label>
                        </li>
                        <?php
                        $attachment_name = '';
                        if (get_field('invoice')) {
                          $photo_id = get_field('invoice');
                          $attachment_url = wp_get_attachment_url($photo_id);
                          $attachment_name = '<img src="' . get_template_directory_uri() . '/includes/doc.png"><span class="file-name">' . basename($attachment_url) . '</span>';
                          $filetype = wp_check_filetype_and_ext($attachment_url, $attachment_url);
                          if ($filetype['ext'] == 'pdf') {
                            $attachment_name = '<img src="' . get_template_directory_uri() . '/includes/pdf.png"><span class="file-name">' . basename($attachment_url) . '</span>';
                          }
                        }
                        $formatted_start_date =  date("m/d/Y", strtotime($start_date));
                        $formatted_end_date =  date("m/d/Y", strtotime($end_date));
                        $data = array(
                          'customer' => $customer_id,
                          'membership' => $membership,
                          'start_date' => $formatted_start_date,
                          'type' => get_field('type'),
                          'options' => get_field('options'),
                          'end_date'   => $formatted_end_date,
                          'remaining_session' => get_field('remaining_sessions'),
                          'attachment_name' => '<a href="' . $attachment_url . '" target="_blank">' . $attachment_name . '</a>'
                        ); ?>
                        <li>
                          <button type="button" class="option edit-user" data-index="<?= $query->post->ID; ?>" data-details='<?php echo json_encode($data); ?>'><img src="<?= get_template_directory_uri(); ?>/assets/images/pencil.svg" alt="icon"></button>
                        </li>
                        <li>
                          <button type="button" class="option delete-user" data-index="<?= $query->post->ID; ?>"><img src="<?= get_template_directory_uri(); ?>/assets/images/bin.svg" alt="icon"></button>
                        </li>
                        <li>
                          <button type="button" class="option view-log" data-index="<?= $query->post->ID; ?>"><img src="<?= get_template_directory_uri(); ?>/assets/images/icon4.svg" alt="icon"></button>
                        </li>
                      </ul>
                    </td>
                  </tr>
                <?php endwhile; ?>
              </tbody>
            </table>
          </div>
          <?php
          $pages = paginate_links(array(
            'base' => '%_%',
            'format' => '?page=%#%',
            'current' => max(1, get_query_var('paged')),
            'total' => $query->max_num_pages,
            'end_size' => 1,
            'type'  => 'array',
            'mid_size' => 2,
            'next_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            'prev_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>'
          ));
          ?>
          <?php if (is_array($pages)) : ?>
            <ul class="pagination justify-content-center justify-content-lg-end">
              <?php foreach ($pages as $page) : ?>
                <li>
                  <?php echo $page; ?>
                </li>
              <?php endforeach; ?>
            </ul>
          <?php endif; ?>
        <?php else : ?>
          <p>No Subscriptions Found</p>
        <?php endif; ?>
      </div>
    </div>
    <!-- /section -->
  </div>

  <div class="modal1 modal fade" id="add-customer" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <header class="mb-4 pe-4">
          <h3 class="text-uppercase text-color1">Add Subscription</h3>
        </header>
        <div class="modal-body p-0">
          <div class="form1">
            <form action="#" class="add-user-form">
              <div class="row">
                <div class="col-md-6 form-group">
                  <label class="form-label">Select Membership</label>
                  <div class="selectbox sel-member">
                    <select class="form-control add_membership" name="membership" id="membership">
                      <option value="">Select Membership</option>
                      <?php
                      $args = array(
                        'numberposts' => -1,
                        'post_type'   => 'membership_types'
                      );

                      $latest_books = get_posts($args);
                      foreach ($latest_books as $member) :
                      ?>
                        <option value="<?= $member->ID; ?>"><?= $member->post_title; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-6 form-group">
                  <div class="text-center text-lg-end mb-4">
                    <a href="<?= home_url('/admin-customer/'); ?>" class="btn button1 btn-sm">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.999 23.003" class="me-2">
                        <path id="Path_35" data-name="Path 35" d="M16.515,21.525a.709.709,0,0,0,.135.013.721.721,0,0,0,.709-.585,3.918,3.918,0,0,0,.073-.74,4.337,4.337,0,0,0-2.63-3.889,3.158,3.158,0,0,0,1.087-2.38,3.251,3.251,0,0,0-6.5,0,3.16,3.16,0,0,0,1.105,2.394,4.331,4.331,0,0,0-2.6,3.874,4.027,4.027,0,0,0,.072.737.723.723,0,1,0,1.422-.261,2.621,2.621,0,0,1-.049-.476A3.143,3.143,0,0,1,12.664,17.3a3.142,3.142,0,0,1,3.324,2.917,2.528,2.528,0,0,1-.048.471.717.717,0,0,0,.57.841Zm-3.876-9.338a1.757,1.757,0,1,1-1.8,1.756,1.784,1.784,0,0,1,1.8-1.756Z" transform="translate(-4.334 -6.458)" fill="#fff" />
                        <path id="Path_36" data-name="Path 36" d="M29.611,12.2a.72.72,0,0,0,.722-.718h0V9.328a2.883,2.883,0,0,0-2.889-2.87H7.222a2.882,2.882,0,0,0-2.888,2.87V22.96a2.882,2.882,0,0,0,2.888,2.87h5.563a.73.73,0,0,1,.511.211l3.016,3a1.449,1.449,0,0,0,2.041,0l3.017-3a.73.73,0,0,1,.511-.211h5.562a2.882,2.882,0,0,0,2.89-2.87V15.067a.722.722,0,1,0-1.445,0V22.96a1.442,1.442,0,0,1-1.444,1.44H21.882a2.16,2.16,0,0,0-1.532.628l-3.017,3-3.017-3a2.159,2.159,0,0,0-1.532-.63H7.222a1.442,1.442,0,0,1-1.444-1.435V9.328A1.442,1.442,0,0,1,7.222,7.893H27.444a1.442,1.442,0,0,1,1.444,1.435V11.48a.72.72,0,0,0,.722.718Z" transform="translate(-4.334 -6.458)" fill="#fff" />
                        <path id="Path_37" data-name="Path 37" d="M22.388,19.372a.719.719,0,0,0,.722-.716h0V16.5h2.167a.718.718,0,1,0,0-1.435H23.111v-2.15a.723.723,0,0,0-1.444,0v2.152H19.5a.718.718,0,1,0,0,1.435h2.167v2.152a.719.719,0,0,0,.722.717Z" transform="translate(-4.334 -6.458)" fill="#fff" />
                      </svg>
                      Add Customer
                    </a>
                  </div>
                </div>
                <div class="col-md-6 form-group">
                  <label class="form-label">Choose Customer</label>
                  <div class="selectbox sel-customer">
                    <select class="form-control add_customer" name="customer" id="customer">
                      <option value="">Choose Customer</option>
                      <?php
                      $team_presidents = get_users(array('role__in' => array('customer'), 'number' => 9999));
                      foreach ($team_presidents as $team_president) :
                      ?>
                        <?php
                        $active_subcription = 0;
                        $end_date_formatted = date("Ymd");
                        $args = array(
                          'post_type' => 'subscription',
                          'posts_per_page' => 1,
                          'meta_query' => array(
                            array(
                              'key'     => 'customer',
                              'value'   => $team_president->ID,
                              'compare' => '=',
                            ),
                            array(
                              'key'     => 'is_disable',
                              'value'   => 1,
                              'compare' => '!=',
                            ),
                            // array(
                            //   'key'     => 'end_date',
                            //   'compare' => '>=',
                            //   'value'   => $end_date_formatted,
                            // ),
                            // array(
                            //   'key'     => 'remaining_sessions',
                            //   'compare' => '>=',
                            //   'value'   => 0,
                            // )
                          ),
                        );
                        $query = new WP_Query($args);
                        if ($query->have_posts()) :
                        ?>
                        <?php while ($query->have_posts()) : $query->the_post();
                            $active_subcription = get_field('membership_type');
                          endwhile;
                        endif;
                        ?>
                        <?php if ($active_subcription == 0) : ?>
                          <option value="<?= $team_president->ID; ?>"><?= $team_president->user_firstname . ' ' . $team_president->user_lastname; ?></option>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-6 form-group">
                  <label class="form-label">Select Type</label>
                  <div class="row">
                    <div class="col-auto">
                      <label class="radio1">
                        <input type="radio" name="type" value="Individual">
                        <span class="inner">
                          <i class="fa fa-solid fa-user"></i>
                          Individual
                        </span>
                      </label>
                    </div>
                    <div class="col-auto">
                      <label class="radio1">
                        <input type="radio" name="type" value="Group">
                        <span class="inner">
                          <i class="fa fa-solid fa-users"></i>
                          Group
                        </span>
                      </label>
                    </div>
                    <div class="col-auto">
                      <label class="radio1">
                        <input type="radio" name="type" value="Online">
                        <span class="inner">
                          <i class="fa fa-regular fa-play-circle"></i>
                          Online
                        </span>
                      </label>
                    </div>
                  </div>
                  <label for="type" class="error"></label>
                </div>

                <div class="col-12 form-group">
                  <label class="form-label">Select Options</label>
                  <div class="row">
                    <div class="col-auto">
                      <label class="custom-radio1">
                        <input type="radio" name="options" value="validity" checked>
                        <span class="custom-inner">
                          Validity(Days)
                        </span>
                      </label>
                    </div>
                    <div class="col-auto">
                      <label class="custom-radio1">
                        <input type="radio" name="options" value="session">
                        <span class="custom-inner">
                          No.of Session
                        </span>
                      </label>
                    </div>
                  </div>
                  <label for="options" class="error"></label>
                </div>

                <div class="col-md-6 form-group">
                  <label class="form-label">Start Date</label>
                  <div class="date-picker end-date-picker further-date">
                    <input type="text" class="form-control" name="start_date" placeholder="Select Start date" autocomplete="off">
                  </div>
                </div>
                <div class="col-md-6 form-group" id="div_end_date">
                  <label class="form-label">End Date</label>
                  <div class="date-picker end-date-picker further-date">
                    <input type="text" class="form-control" name="end_date" placeholder="Select End date" autocomplete="off">
                  </div>
                </div>
                <div class="col-md-6 form-group" id="div_no_sessions" style="display: none;">
                  <label class="form-label">No. of Sessions</label>
                  <input type="text" class="form-control" name="remaining_session" placeholder="Enter No. of Sessions">
                </div>
                <div class="col-md-6 form-group">
                  <label class="form-label">Invoice</label>
                  <div class="file-upload">
                    <input type="file" id="attachment" class="single-upload-attachment" name="attachment" accept="application/msword, application/pdf">
                    <label for="attachment" class="inner">&plus;</label>
                    <span class="attachment-name"></span>
                  </div>
                  <label for="attachment" class="error"></label>
                  <span class="allowed-type">Allowed file types: pdf, doc, docx</span>
                </div>



              </div>

              <div class="row">
                <div class="col-md-6 mb-4 mb-md-0">
                  <button class="btn button2 w-100 btn-lg" type="button" data-bs-dismiss="modal">Cancel</button>
                </div>
                <div class="col-md-6">
                  <button class="btn button1 w-100 btn-lg submit-btn" type="submit">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>



  <div class="modal1 modal fade" id="edit-customer" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <header class="mb-4 pe-4">
          <h3 class="text-uppercase text-color1">Edit Subscription</h3>
        </header>
        <div class="modal-body p-0">
          <div class="form1">
            <form action="#" class="edit-user-form">
              <div class="row">
                <div class="col-md-6 form-group">
                  <label class="form-label">Select Membership</label>
                  <div class="selectbox sel-edit-member">
                    <select class="form-control edit_membership" name="edit_membership">
                      <option value="">Select Membership</option>
                      <?php
                      $args = array(
                        'numberposts' => -1,
                        'post_type'   => 'membership_types'
                      );

                      $latest_books = get_posts($args);
                      foreach ($latest_books as $member) :
                      ?>
                        <option value="<?= $member->ID; ?>"><?= $member->post_title; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-6 form-group">
                  <label class="form-label">Choose Customer</label>
                  <div class="selectbox sel-edit-customer">
                    <select class="form-control edit_customer" name="edit_customer" disabled>
                      <option value="">Choose Customer</option>
                      <?php
                      $team_presidents = get_users(array('role__in' => array('customer'), 'number' => 9999));
                      foreach ($team_presidents as $team_president) :
                      ?>
                        <option value="<?= $team_president->ID; ?>"><?= $team_president->user_firstname . ' ' . $team_president->user_lastname; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>

                <div class="col-6 form-group">
                  <label class="form-label">Select Type</label>
                  <div class="row">
                    <div class="col-auto">
                      <label class="radio1">
                        <input type="radio" class="edit_type" name="edit_type" value="Individual">
                        <span class="inner">
                          <i class="fa fa-solid fa-user"></i>
                          Individual
                        </span>
                      </label>
                    </div>
                    <div class="col-auto">
                      <label class="radio1">
                        <input type="radio" class="edit_type" name="edit_type" value="Group">
                        <span class="inner">
                          <i class="fa fa-solid fa-users"></i>
                          Group
                        </span>
                      </label>
                    </div>
                    <div class="col-auto">
                      <label class="radio1">
                        <input type="radio" class="edit_type" name="edit_type" value="Online">
                        <span class="inner">
                          <i class="fa fa-regular fa-play-circle"></i>
                          Online
                        </span>
                      </label>
                    </div>
                  </div>
                  <label for="edit_type" class="error"></label>
                </div>
                <div class="col-12 form-group">
                  <label class="form-label">Select Options</label>
                  <div class="row">
                    <div class="col-auto">
                      <label class="custom-radio1">
                        <input type="radio" class="edit_options" name="edit_options" value="validity">
                        <span class="custom-inner">
                          Validity(Days)
                        </span>
                      </label>
                    </div>
                    <div class="col-auto">
                      <label class="custom-radio1">
                        <input type="radio" class="edit_options" name="edit_options" value="session">
                        <span class="custom-inner">
                          No.of Session
                        </span>
                      </label>
                    </div>
                  </div>
                  <label for="edit_options" class="error"></label>
                </div>

                <div class="col-md-6 form-group">
                  <label class="form-label">Start Date</label>
                  <div class="date-picker end-date-picker further-date">
                    <input type="text" class="form-control edit_start_date" name="edit_start_date" placeholder="Select Start date" autocomplete="off">
                  </div>
                </div>
                <div class="col-md-6 form-group" id="div_edit_end_date">
                  <label class="form-label">End Date</label>
                  <div class="date-picker end-date-picker further-date">
                    <input type="text" class="form-control edit_end_date" name="edit_end_date" placeholder="Select End date" autocomplete="off">
                  </div>
                </div>
                <div class="col-md-6 form-group" id="div_edit_no_sessions">
                  <label class="form-label">No. of Sessions</label>
                  <input type="text" class="form-control edit_remaining_session" name="edit_remaining_session" placeholder="Enter Remaining Sessions">
                </div>
                <div class="col-md-6 form-group">
                  <label class="form-label">Invoice</label>
                  <div class="file-upload">
                    <input type="file" id="edit_attachment" class="single-upload-attachment" name="attachment" accept="application/msword, application/pdf">
                    <label for="edit_attachment" class="inner">&plus;</label>
                    <span class="attachment-name edit-attachment-name"></span>
                  </div>
                  <label for="attachment" class="error"></label>
                  <span class="allowed-type">Allowed file types: pdf, doc, docx</span>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 mb-4 mb-md-0">
                  <button class="btn button2 w-100 btn-lg" type="button" data-bs-dismiss="modal">Cancel</button>
                </div>
                <div class="col-md-6">
                  <input type="hidden" name="user_id" class="edit_user_id" value="">
                  <button class="btn button1 w-100 btn-lg" type="submit">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="modal1 modal fade" id="disable-enable" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <header class="mb-4 pe-4">
          <h3 class="text-uppercase text-color1"></h3>
        </header>
        <div class="modal-body p-0">
          <div class="content-body"></div>
          <div class="row">
            <div class="col-md-6 mb-4 mb-md-0">
              <button class="btn button2 w-100 btn-lg disable-enable-close" type="button">No</button>
            </div>
            <div class="col-md-6">
              <button class="btn button1 w-100 btn-lg disable-enable-submit" type="button">Yes</button>
            </div>
          </div>
        </div>
        <input type="hidden" class="disable-user-id" value="">
        <input type="hidden" class="disable-enable-id" value="">
      </div>
    </div>
  </div>

  <div class="modal1 modal fade" id="disable-enable-success" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <header class="mb-4 pe-4">
          <h3 class="text-uppercase text-color1"></h3>
        </header>
        <div class="modal-body text-center p-0">
          <div class="content-body"></div>
          <div class="row">
            <div class="col-md-6 mx-auto">
              <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal1 modal fade" id="delete-confirm" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <header class="mb-4 pe-4">
          <h3 class="text-uppercase text-color1">Delete Subscription</h3>
        </header>
        <div class="modal-body p-0">
          <div class="content-body">
            <p>Are you sure want to delete this Subscription ?</p>
          </div>
          <div class="row">
            <div class="col-md-6 mb-4 mb-md-0">
              <button class="btn button2 w-100 btn-lg" data-bs-dismiss="modal" type="button">No</button>
            </div>
            <div class="col-md-6">
              <button class="btn button1 w-100 btn-lg delete-submit" type="button">Yes</button>
            </div>
          </div>
        </div>
        <input type="hidden" class="delete-user-id" value="">
      </div>
    </div>
  </div>

  <div class="modal1 modal fade" id="delete-success" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <header class="mb-4 pe-4">
          <h3 class="text-uppercase text-color1">Delete Subscription</h3>
        </header>
        <div class="modal-body p-0">
          <div class="content-body">
            <p>Subscription has been successfully deleted</p>
          </div>
          <div class="row">
            <div class="col-md-6 mb-4 mb-md-0">
              <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal1 modal fade" id="edit-user-success" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <header class="mb-4 pe-4">
          <h3 class="text-uppercase text-color1">Edit Subscription</h3>
        </header>
        <div class="modal-body p-0">
          <div class="content-body">
            <p>Subscription has been successfully updated</p>
          </div>
          <div class="row">
            <div class="col-md-6 mb-4 mb-md-0">
              <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



  <div class="modal1 modal fade" id="view-log" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <header class="mb-4 pe-4">
          <h3 class="text-uppercase text-color1">View Log</h3>
        </header>
        <div class="modal-body p-0">
          <div class="content-body"></div>
        </div>
      </div>
    </div>
  </div>



  <?php get_footer(); ?>
  <style>
    .custom-radio1 {
      position: relative;
      padding-left: 31px;
    }

    .custom-radio1 input {
      position: absolute;
      opacity: 0;
      pointer-events: none;
    }

    .custom-radio1 input:checked~.custom-inner::before {
      background: #ce8a29;
    }

    .custom-radio1 .custom-inner::before {
      content: '';
      width: 19px;
      height: 19px;
      border-radius: 100px;
      outline: 1px solid #525355;
      border: 4px solid #ffffff;
      position: absolute;
      left: 0px;
      top: 50%;
      -webkit-transform: translateY(-50%);
      -ms-transform: translateY(-50%);
      transform: translateY(-50%);
    }
  </style>
  <script>
    $('input[type=radio][name=options]').change(function() {
      if (this.value == 'validity') {
        $("#div_no_sessions").css("display", "none");
        $("#div_end_date").css("display", "block");
      } else {
        $("#div_no_sessions").css("display", "block");
        $("#div_end_date").css("display", "none");
      }
    });
    $('input[type=radio][name=edit_options]').change(function() {
      if (this.value == 'validity') {
        $("#div_edit_no_sessions").css("display", "none");
        $("#div_edit_end_date").css("display", "block");
      } else {
        $("#div_edit_no_sessions").css("display", "block");
        $("#div_edit_end_date").css("display", "none");
      }
    });
    $(document).ready(function() {
      $(".add_membership").select2({
        dropdownParent: $(".sel-member")
      });
      $(".edit_membership").select2({
        dropdownParent: $(".sel-edit-member")
      });
      $(".add_customer").select2({
        dropdownParent: $(".sel-customer")
      });
      $(".edit-customer").select2({
        dropdownParent: $(".sel-edit-member")
      });
    });
    $.validator.addMethod('filesize', function(value, element, param) {
      return this.optional(element) || (element.files[0].size <= param * 1000000)
    }, 'File size must be less than {0} MB');
    var abc = jQuery('.add-user-form').validate({
      // ignore: ".ignore",
      // onfocusout: function(element) {
      //   $(element).valid()
      // },
      // onkeyup: false,
      ignore: [],
      rules: {
        membership: {

          required: true,
        },
        customer: {

          required: true,
        },
        type: {

          required: true,
        },
        options: {
          required: true,
        },
        start_date: {
          required: true,
        },
        end_date: {
          required: {
            depends: function() {
              return $('input[name=options]:checked').val() == 'validity';
            }
          }
        },
        remaining_session: {
          required: {
            depends: function() {
              return $('input[name=options]:checked').val() == 'session';
            }
          },
          digits: true,
        },
        // attachment: {
        //   required: true,
        //   filesize: 1.5
        // }
      },
      messages: {
        membership: {
          required: "<?php _e('Membership is required', 'sidf'); ?>"
        },
        customer: {
          required: "<?php _e('Customer is required', 'sidf'); ?>"
        },
        type: {
          required: "<?php _e('Type is required', 'sidf'); ?>"
        },
        options: {
          required: "<?php _e('Options is required', 'sidf'); ?>"
        },
        // attachment: {
        //   required: "<?php _e('Invoice is required', 'sidf'); ?>"
        // },
      },
      errorElement: "label",
      errorPlacement: function(error, element) {
        if (element.hasClass("select2-hidden-accessible")) {
          element = $("#select2-" + element.attr("id") + "-container").parent();
          error.insertAfter(element);
        } else {
          error.insertAfter(element);
        }
      },

    });

    jQuery(".add-user-form").submit(function(e) {
      e.preventDefault();
      if (abc.form() && abc.form()) {

        jQuery('#loader').show();
        jQuery('.submit-btn').attr('disabled', 'disabled');

        var fd = new FormData(jQuery('.add-user-form')[0]);
        // process the form
        jQuery.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_subscription_action',
            data: fd,
            processData: false,
            contentType: false,
          })
          // using the done promise callback
          .done(function(result) {
            if(result.success == false){
              jQuery('#loader').hide();
              jQuery('.submit-btn').removeAttr('disabled');
              $('#exist-customer').modal('show');
            }else{
              location.reload();
            }
          });
      }

      return false;
    });

    jQuery(".filter-form").submit(function(e) {
      e.preventDefault();

      jQuery('#loader').show();

      var fd = new FormData(jQuery('.filter-form')[0]);
      // process the form
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_filter_subscription_action&page=1',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          var obj = JSON.parse(result);
          jQuery('.more-results').html(obj.html);
          jQuery('#loader').hide();

        });

      return false;
    });

    $(document).on("click", "a.page-numbers", function() {
      jQuery('#loader').show();
      var href = $(this).attr('href');
      var this1 = $(this);
      var fd = new FormData(jQuery('.filter-form')[0]);
      // process the form
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>' + href + '&action=admin_filter_subscription_action',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          var obj = JSON.parse(result);
          jQuery('.more-results').html(obj.html);
          jQuery('#loader').hide();
        });

      return false;
    });

    $(document).on('change', '.disable-user', function() {
      var disable = 1;
      $('#disable-enable h3').html('Disable Subscription');
      $('#disable-enable .content-body').html('<p>Are you sure to disable this Subscription ?</p>');
      $('#disable-enable-success h3').html('Disable Subscription');
      $('#disable-enable-success .content-body').html('<p>Subscription is successfully disabled</p>');
      if (this.checked) {
        disable = 0;
        $('#disable-enable h3').html('Enable Subscription');
        $('#disable-enable .content-body').html('<p>Are you sure to enable this Subscription ?</p>');
        $('#disable-enable-success h3').html('Enable Subscription');
        $('#disable-enable-success .content-body').html('<p>Subscription is successfully enabled</p>');

      }
      var user_id = this.value;
      $('.disable-user-id').val(user_id);
      $('.disable-enable-id').val(disable);
      $('#disable-enable').modal('show');

    });

    $(document).on("click", ".disable-enable-close", function() {
      var user_id = $('.disable-user-id').val();
      var disable = $('.disable-enable-id').val();
      if (disable == 1) {
        $("input[type=checkbox][value=" + user_id + "]").prop("checked", true);
      } else {
        $("input[type=checkbox][value=" + user_id + "]").prop("checked", false);
      }
      $('#disable-enable').modal('hide');
    });

    $(document).on("click", ".disable-enable-submit", function() {
      var user_id = $('.disable-user-id').val();
      var disable = $('.disable-enable-id').val();
      $('#disable-enable').modal('hide');
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_enable_disable_member_action&postid=' + user_id + '&disable=' + disable,
          data: '',
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          $('#disable-enable-success').modal('show');

        });
    });

    $(document).on("click", ".delete-user", function() {
      var user_id = $(this).data('index');
      $('.delete-user-id').val(user_id);
      $('#delete-confirm').modal('show');
    });

    $(document).on("click", ".delete-submit", function() {
      var user_id = $('.delete-user-id').val();
      $('#delete-confirm').modal('hide');
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>?action=delete_member_action&id=' + user_id,
          data: '',
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          $('#delete-success').modal('show');

        });
    });

    jQuery('#delete-success').on('hidden.bs.modal', function() {
      location.reload();
    });

    jQuery('#disable-enable-success').on('hidden.bs.modal', function() {
      location.reload();
    });


    $(document).on("click", ".edit-user", function() {
      var user_id = $(this).data('index');
      var details = $(this).data('details');
      console.log(details); 
      $('.edit_user_id').val(user_id);
      $('.edit_customer').val(details.customer).trigger('change');
      $('.edit_membership').val(details.membership).trigger('change');
      $('.edit_start_date').val(details.start_date);
      if(details.end_date != "01/01/1970"){
        $('.edit_end_date').val(details.end_date);
      }else{
        $('.edit_end_date').val('');
      }
      
      $('.edit_remaining_session').val(details.remaining_session);
      if (details.attachment_name) {
        $('.edit-attachment-name').html(details.attachment_name);
      }
      $('input[name=edit_type][value="' + details.type + '"]').prop("checked", true);
      $('input[name=edit_options][value="' + details.options + '"]').prop("checked", true);
      if(details.options == 'validity'){
        $("#div_edit_no_sessions").css("display", "none");
        $("#div_edit_end_date").css("display", "block");
      } else {
        $("#div_edit_no_sessions").css("display", "block");
        $("#div_edit_end_date").css("display", "none");
        
      }
      $('#edit-customer').modal('show');

    });

    var abcd = jQuery('.edit-user-form').validate({
      // ignore: ".ignore",
      // onfocusout: function(element) {
      //   $(element).valid()
      // },
      // onkeyup: false,
      ignore: [],
      rules: {
        edit_membership: {

          required: true,
        },
        edit_customer: {

          required: true,
        },
        edit_type: {
          required: true,
        },
        edit_options: {
          required: true,
        },
        edit_start_date: {
          required: true,
        },
        edit_end_date: {
          required: {
            depends: function() {
              return $('input[name=edit_options]:checked').val() == 'validity';
            }
          }
        },
        edit_remaining_session: {
          required: {
            depends: function() {
              return $('input[name=edit_options]:checked').val() == 'session';
            }
          },
          digits: true,
        },
        // attachment: {
        //   filesize: 1.5
        // }

      },
      messages: {
        edit_membership: {
          required: "<?php _e('Membership is required', 'sidf'); ?>"
        },
        edit_customer: {
          required: "<?php _e('Customer is required', 'sidf'); ?>"
        },
        edit_type: {
          required: "<?php _e('Type is required', 'sidf'); ?>"
        },
        edit_options: {
          required: "<?php _e('Options is required', 'sidf'); ?>"
        },
        // attachment: {
        //   required: "<?php _e('Invoice is required', 'sidf'); ?>"
        // },
      },
      errorElement: "label",
      errorPlacement: function(error, element) {
        if (element.hasClass("select2-hidden-accessible")) {
          element = $("#select2-" + element.attr("id") + "-container").parent();
          error.insertAfter(element);
        } else {
          error.insertAfter(element);
        }
      },

    });

    jQuery(".edit-user-form").submit(function(e) {
      e.preventDefault();
      if (abcd.form() && abcd.form()) {
        $('#edit-customer').modal('hide');
        jQuery('#loader').show();
        jQuery('.submit-btn').attr('disabled', 'disabled');

        var fd = new FormData(jQuery('.edit-user-form')[0]);
        // process the form
        jQuery.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_edit_subscription_action',
            data: fd,
            processData: false,
            contentType: false,
          })
          // using the done promise callback
          .done(function(result) {
            jQuery('#loader').hide();
            $('#edit-user-success').modal('show');



          });
      }

      return false;
    });
    jQuery('#edit-user-success').on('hidden.bs.modal', function() {
      location.reload();
    });



    jQuery(".view-log").click(function(e) {
      e.preventDefault();

      var user_id = $(this).data('index');

      // process the form
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>?action=view_log_action&user_id=' + user_id,
          data: '',
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          var obj = JSON.parse(result);
          jQuery('#view-log .content-body').html(obj.html);
          jQuery('#view-log').modal('show');



        });


      return false;
    });
  </script>