$(window).on("load", function () {
  	 $('body').fadeIn();

	$(".same-height, .list1>li").matchHeight();
  
	document.querySelectorAll('.animated-box').forEach((i) => {
		if (i) {
			const observer = new IntersectionObserver((entries) => {
				observerCallback(entries, observer, i)
			},
			{threshold: 0});    
			observer.observe(i);
		}
	})

	const observerCallback = (entries, observer, header) => {
		entries.forEach((entry, i) => {
			// If the element is visible
			if (entry.isIntersecting) {
				// Add the animation class
				entry.target.classList.add('fade-in');
			}
		});
	};

	objectFitImages();

	$('.open-menu-btn').click(function () {
		$('body').toggleClass('open-menu');
		return false;
	});

	if ($(window).width() > 992) {
		$("body").addClass("menu-open");
	} else {
		$("body").removeClass("menu-open");
	}

	$('.close-menu-btn').click(function () {
		$('body').removeClass('open-menu');
		return false;
	});

	$(".down-control").on("click", function () {
		var $Position = $($(this).attr("href")).position().top;
		$("html, body").animate({
			scrollTop: $Position,
		}, 1000);
		return false;
	});

	$('.owl-carousel').owlCarousel({
		loop: true,
		nav: false,
		autoplay: true,
		autoplayTimeout: 3000,
		dots: false,
		autoplayHoverPause: false,
		animateOut: 'fadeOut',
		items: 1
	});

	$("#back-to-top").click(function () {
		$("html, body").animate({
			scrollTop: 0
		}, 1000);
		return false;
	});

	if ($(window).width() > 992) {
		$("body").addClass("menu-open");
	} else {
		$("body").removeClass("menu-open");
	}

	$(".open-menu-btn").on("click", function () {
		if ($("body").hasClass("menu-open")) {
			$("body").removeClass("menu-open");
		} else {
			$("body").toggleClass("menu-open");
		}
	});

	$(".close-menu-btn").on("click", function () {
		$("body").removeClass("menu-open");
	});

	$(".date-picker:not(.end-date-picker)").each(function () {
		$(this).find(".form-control:not(.all-date)").datepicker({
			changeMonth: true,
			changeYear: true,
			showOn: "both",
			buttonImage: "../wp-content/themes/hotstone-fitness/assets/images/calendar-icon.svg",
			buttonImageOnly: true,
			buttonText: "Select date",
			maxDate: "0",
			yearRange: "1950:c"
		});
	});
	
	$(".date-picker:not(.end-date-picker)").each(function () {
		$(this).find(".all-date").datepicker({
			changeMonth: true,
			changeYear: true,
			showOn: "both",
			buttonImage: "../wp-content/themes/hotstone-fitness/assets/images/calendar-icon.svg",
			buttonImageOnly: true,
			buttonText: "Select date",
			yearRange: "1950:2030"
		});
	});
	
	$(".end-date-picker:not(.further-date)").each(function () {
		$(this).find(".form-control").datepicker({
			changeMonth: true,
			changeYear: true,
			showOn: "both",
			buttonImage: "../wp-content/themes/hotstone-fitness/assets/images/calendar-icon.svg",
			buttonImageOnly: true,
			buttonText: "Select date",
			minDate: "-1d",
			yearRange: "c:2030"
		});
	});
	
	$(".further-date").each(function () {
		$(this).find(".form-control").datepicker({
			changeMonth: true,
			changeYear: true,
			showOn: "both",
			buttonImage: "../wp-content/themes/hotstone-fitness/assets/images/calendar-icon.svg",
			buttonImageOnly: true,
			buttonText: "Select date",
			minDate: "0",
			yearRange: "c:2030"
		});
	});
	
		$('.single-upload').change(function(){
		var input = this;
		var url = $(this).val();
		var $input = $(this);
		var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
		if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
		 {
			var reader = new FileReader();
	
			reader.onload = function (e) {
				
				
				$input.parent().find('.image-src').removeClass('hide');
			    $input.parent().find('.image-src').attr('src', e.target.result);
			}
		   reader.readAsDataURL(input.files[0]);
		}
		else
		{
			$input.parent().find('.image-src').addClass('hide');
		}
	  });
	  
	  $('.single-upload-attachment').change(function(e){
		$('.file-name').empty();
		var input = this;
		var url = $(this).val();
		var $input = $(this);
		var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
		var image = "<img src='../wp-content/themes/hotstone-fitness/includes/doc.png'>";
		if(ext == 'pdf'){
		    image = "<img src='../wp-content/themes/hotstone-fitness/includes/pdf.png'>";    
		}
		$input.parent().find('.attachment-name').html(image);
		$('<p class="file-name">'+e.target.files[0].name+'</p>').insertAfter(".attachment-name");
	  });
	  
	   $(".custom-selectbox").each(function () {
        $(this).find(".chosen-select").chosen({
            width: "100%",
        });
    });

	$(window).on("resize", function () {
		if ($(window).width() > 992) {
			$("body").addClass("menu-open");
		} else {
			$("body").removeClass("menu-open");
		}
	});

	$(window).on("scroll", function () {
		var scroll = $(window).scrollTop();
		if (scroll <= 500) {
			$("body").addClass("scroller");
		}
	});
	
	$(".ui-datepicker a").removeAttr("href").css('cursor', 'pointer'); 

});
