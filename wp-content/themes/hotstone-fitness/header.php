<!DOCTYPE html>
<html>

<head>
  <!-- Meta Tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />
  <meta name="description" content="" />
  <!-- Page Title & Favicon -->
<link rel="icon" type="image/png" sizes="32x32" href="<?= get_template_directory_uri();?>/favicon-32x32.png">
  <title><?php
  /*
   * Print the <title> tag based on what is being viewed.
   */
  global $page, $paged;
    
  wp_title( '|', true, 'right' );
  // Add the blog name.
  bloginfo( 'name' );

  // Add the blog description for the home/front page.
  $site_description = get_bloginfo( 'description', 'display' );
  if ( $site_description && ( is_home() || is_front_page() ) )
    echo " | $site_description";

  // Add a page number if necessary:
  if ( $paged >= 2 || $page >= 2 )
    echo ' | ' . sprintf( __( 'Page %s', 'belgravia' ), max( $paged, $page ) );

  ?>
</title>
    <!-- Stylesheets -->
  <?php wp_head();?>
</head>

<body <?php body_class();?>>
  <div id="container">
    <aside id="sidebar">
      <div class="logo">
        <a href="#"><img src="<?= get_template_directory_uri();?>/assets/images/logo.svg" alt="Logo"></a>
      </div>
      <?php if(is_user_logged_in()):?>
      <nav id="main-navigation">
        <button class="close-menu-btn d-lg-none" type="button">&times;</button>
        <div class="inner-wrapper">
          <ul class="menu">
            <li class="active">
              <a href="<?php echo home_url('/admin-dashboard/');?>">Dashboard</a>
            </li>
            <li>
              <a href="#">User</a>
              <ul>
                <li><a href="<?php echo home_url('/admin-front-office-user/');?>">Front Office</a></li>
                <li><a href="<?php echo home_url('/admin-customer/');?>">Customers</a></li>
                <li><a href="<?php echo home_url('/admin-trainer/');?>">Trainers</a></li>
              </ul>
            </li>
            <li>
              <a href="#">Memberships</a>
              <ul>
                <li><a href="<?php echo home_url('/admin-membership-types/');?>">Membership Types</a></li>
                <li><a href="<?php echo home_url('/admin-subscriptions/');?>">Subscriptions</a></li>
              </ul>
            </li>
            <li>
              <a href="<?php echo home_url('/admin-workout/');?>">Workouts</a>
            </li>
            <li>
              <a href="#">Classes</a>
              <ul>
                <li><a href="<?php echo home_url('/admin-class/');?>">Schedule</a></li>
                <li><a href="<?php echo home_url('/admin-booking/');?>">Bookings</a></li>
              </ul>
            </li>
            <li>
              <a href="<?php echo home_url('/admin-measurement/');?>">Measurments</a>
            </li>
            <li>
              <a href="#">Settings</a>
              <ul>
                <!-- <li><a href="#">General Settings</a></li> -->
                <li><a href="<?php echo home_url('/admin-notification/');?>">Notifications</a></li>
              </ul>
            </li>
            <li>
              <a href="<?php echo wp_logout_url( home_url('login') ); ?>">Logout</a>
            </li>
          </ul>
        </div>
      </nav>
      <?php endif;?>
    </aside>
    <main id="main">
      <!-- header -->
      <header id="header">
        <div class="container">
          <div class="row align-items-center justify-content-between flex-nowrap">
            <div class="col-lg-2 col-auto">
              <button class="open-menu-btn" type="button">
                <img src="<?= get_template_directory_uri();?>/assets/images/bars.svg" alt="icon">
              </button>
            </div>
            <div class="col-lg-4 col d-lg-none">
              <div class="logo text-center">
                <a href="#"><img src="<?= get_template_directory_uri();?>/assets/images/logo.svg" alt="Logo"></a>
              </div>
            </div>
            <div class="col-lg-3 col-auto">
            <?php if(is_user_logged_in()):
              $current_user = wp_get_current_user();
              $avatar_url = get_avatar_url( $current_user->user_email, array( 'size' => 100 ) );
              ?>
              <div class="user">
                <i class="avatar"><img src="<?= $avatar_url;?>" alt="img"></i>
                <span class="name"><?= $current_user->display_name; ?></span>
              </div>
              <?php endif;?>
            </div>
          </div>
        </div>
      </header>