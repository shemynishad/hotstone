<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <!-- Meta Tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <meta name="description" content="" />
    <!-- Page Title & Favicon -->
	<link rel="icon" type="image/png" sizes="32x32" href="<?= get_template_directory_uri();?>/favicon-32x32.png">
    <title><?php
  /*
   * Print the <title> tag based on what is being viewed.
   */
  global $page, $paged;
    
  wp_title( '|', true, 'right' );
  // Add the blog name.
  bloginfo( 'name' );

  // Add the blog description for the home/front page.
  $site_description = get_bloginfo( 'description', 'display' );
  if ( $site_description && ( is_home() || is_front_page() ) )
    echo " | $site_description";

  // Add a page number if necessary:
  if ( $paged >= 2 || $page >= 2 )
    echo ' | ' . sprintf( __( 'Page %s', 'belgravia' ), max( $paged, $page ) );

  ?>
</title>
    <!-- Stylesheets -->
  <?php wp_head();?>
</head>

<body <?php body_class();?>>

    <!--[if lt IE 9]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
    <div id="container">
        <!-- header -->
        <header id="header">
            <div class="container-fluid">
                <div class="row align-items-center order-lg-1">
                    <div class="col-xxl-2 col-lg-3 col-5">
                        <div class="logo">
                            <a href="#">
                                <img src="<?= get_template_directory_uri();?>/assets/images/logo.svg" alt="logo">
                            </a>
                        </div>
                    </div>
                    <div class="col-xxl-2 col-lg-3 col-7 text-end order-lg-3">
                        <ul id="user-options">
                            <li>
                                <a href="<?php echo home_url('/customer-login/');?>">
                                    <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                                    <span>Customer login</span>
                                </a>
                            </li>
                            <?php $languages = icl_get_languages('skip_missing=1'); foreach($languages as $l){ ?>
							<li class="lang <?php if($l['active']){ echo 'current';}?>"><a href="<?php echo $l['url'];?>"><?php echo $l['native_name'];?></a></li>
							<?php } ?>
                            <li class="d-lg-none">
                                <a class="open-menu-btn"><i class="fa fa-bars" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xxl-8 col-lg-6 text-center order-lg-2">
                        <nav id="main-navigation">
                            <div class="scrollable-content">
                                <a class="close-menu-btn d-lg-none"><i class="fa fa-times" aria-hidden="true"></i></a>
                                <ul class="menu">
                                    <li class="current">
                                        <a href="#">Home</a>
                                    </li>
                                    <li>
                                        <a href="#about_div">About</a>
                                    </li>
                                    <li>
                                        <a href="#membership_div">Membership & Pricing</a>
                                    </li>
                                    <li>
                                        <a href="#gallery_div">Gallery</a>
                                    </li>
                                    <li>
                                        <a href="#class_div">Class Schedule</a>
                                    </li>
                                    <li>
                                        <a href="#contact_div">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>

                </div>
            </div>
        </header>