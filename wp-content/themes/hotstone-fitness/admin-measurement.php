<?php /* Template name: Admin Measuremet */
if (is_user_logged_in()) {
  if (current_user_can('administrator') || current_user_can('front_office_user')) {
    $current_user = wp_get_current_user();
  } else {
    wp_redirect(get_home_url());
  }
} else {
  wp_redirect(get_home_url());
}
get_header(); ?>

<div id="content">
  <!-- section -->
  <div class="section dashboard">
    <div id="loader"></div>
    <div class="container">
      <div class="row align-items-center flex-row-reverse mb-30 mb-md-5">
        <div class="col-md-6 mb-4 mb-md-0">
          <ul class="breadcrumb justify-content-end mb-0">
            <li><a href="#">Dashboard</a></li>
            <li>Measurement</li>
          </ul>
        </div>
        <div class="col-md-6">
          <p class="welcome-title">Hello, Welcome <?= $current_user->display_name; ?>.</p>
        </div>
      </div>
      <div class="box1 py-4 mb-30">
        <div class="form1">
          <form action="#" class="filter-form">
            <div class="row align-items-end">
              <div class="col-xxl-auto col-12 mb-4 mb-xxl-0">
                <i class="icon"><img src="<?= get_template_directory_uri(); ?>/assets/images/filter.svg" alt="icon"></i>
              </div>
              <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                <label class="form-label">Date From</label>
                <div class="date-picker">
                  <input type="text" class="form-control" name="start_date" placeholder="Select from date">
                </div>
              </div>
              <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                <label class="form-label">To</label>
                <div class="date-picker">
                  <input type="text" class="form-control" name="end_date" placeholder="Select to date">
                </div>
              </div>
              <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                <label class="form-label">Customer</label>
                <div>
                  <input type="text" class="form-control" name="keyword" placeholder="Search by Name">
                </div>
              </div>
              <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                <label class="form-label">Trainer</label>
                <div>
                  <input type="text" class="form-control" name="trainer_keyword" placeholder="Search by Name">
                </div>
              </div>
              <div class="col-xxl-auto col-12">
                <button class="btn button3" type="submit">Search</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="text-center text-lg-end mb-4">
        <button class="btn button1 btn-sm" data-bs-toggle="modal" data-bs-target="#add-customer">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.999 23.003" class="me-2">
            <path id="Path_35" data-name="Path 35" d="M16.515,21.525a.709.709,0,0,0,.135.013.721.721,0,0,0,.709-.585,3.918,3.918,0,0,0,.073-.74,4.337,4.337,0,0,0-2.63-3.889,3.158,3.158,0,0,0,1.087-2.38,3.251,3.251,0,0,0-6.5,0,3.16,3.16,0,0,0,1.105,2.394,4.331,4.331,0,0,0-2.6,3.874,4.027,4.027,0,0,0,.072.737.723.723,0,1,0,1.422-.261,2.621,2.621,0,0,1-.049-.476A3.143,3.143,0,0,1,12.664,17.3a3.142,3.142,0,0,1,3.324,2.917,2.528,2.528,0,0,1-.048.471.717.717,0,0,0,.57.841Zm-3.876-9.338a1.757,1.757,0,1,1-1.8,1.756,1.784,1.784,0,0,1,1.8-1.756Z" transform="translate(-4.334 -6.458)" fill="#fff" />
            <path id="Path_36" data-name="Path 36" d="M29.611,12.2a.72.72,0,0,0,.722-.718h0V9.328a2.883,2.883,0,0,0-2.889-2.87H7.222a2.882,2.882,0,0,0-2.888,2.87V22.96a2.882,2.882,0,0,0,2.888,2.87h5.563a.73.73,0,0,1,.511.211l3.016,3a1.449,1.449,0,0,0,2.041,0l3.017-3a.73.73,0,0,1,.511-.211h5.562a2.882,2.882,0,0,0,2.89-2.87V15.067a.722.722,0,1,0-1.445,0V22.96a1.442,1.442,0,0,1-1.444,1.44H21.882a2.16,2.16,0,0,0-1.532.628l-3.017,3-3.017-3a2.159,2.159,0,0,0-1.532-.63H7.222a1.442,1.442,0,0,1-1.444-1.435V9.328A1.442,1.442,0,0,1,7.222,7.893H27.444a1.442,1.442,0,0,1,1.444,1.435V11.48a.72.72,0,0,0,.722.718Z" transform="translate(-4.334 -6.458)" fill="#fff" />
            <path id="Path_37" data-name="Path 37" d="M22.388,19.372a.719.719,0,0,0,.722-.716h0V16.5h2.167a.718.718,0,1,0,0-1.435H23.111v-2.15a.723.723,0,0,0-1.444,0v2.152H19.5a.718.718,0,1,0,0,1.435h2.167v2.152a.719.719,0,0,0,.722.717Z" transform="translate(-4.334 -6.458)" fill="#fff" />
          </svg>
          Add Measurement
        </button>
      </div>
      <div class="more-results">
        <?php
        $args = array(
          'post_type' => 'measurement',
          'posts_per_page' => 20
        );
        $query = new WP_Query($args);
        if ($query->have_posts()) :
        ?>
          <div class="table1 mb-4">
            <table class="table mb-0">
              <thead>
                <tr>
                  <td>ID</td>
                  <td>Customer</td>
                  <td>Date</td>
                  <td>Trainer</td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                <?php while ($query->have_posts()) : $query->the_post();

                ?>
                  <tr>
                    <td><?= get_field('id'); ?></td>
                    <?php $customer_id = get_field('customer');
                    $author_obj = get_user_by('id', $customer_id); ?>
                    <td><?php if (!empty($author_obj->user_firstname)) echo $author_obj->user_firstname . ' ' . $author_obj->user_lastname; ?></td>
                    <?php $date = get_field('date'); ?>
                    <td><?= date("d F Y", strtotime($date)); ?></td>
                    <?php $trainer_id = get_field('trainer');
                    $author_obj = get_user_by('id', $trainer_id); ?>
                    <td><?= $author_obj->user_firstname . ' ' . $author_obj->user_lastname; ?></td>
                    <td>
                      <ul class="user-options mb-0">
                        <?php $disable = get_field('is_disable'); ?>
                        <li>
                          <label class="toggle1">
                            <input type="checkbox" class="disable-user" <?php if (!$disable) {
                                                                          echo 'checked';
                                                                        } ?> value="<?= $query->post->ID; ?>">
                            <span class="slider"></span>
                          </label>
                        </li>
                        <?php
                        $data = array(
                          'customer' => $customer_id,
                          'trainer' => $trainer_id,
                          'fasting' => get_field('fasting'),
                          'height'  => get_field('height'),
                          'weight'  => get_field('weight'),
                          'performance_index'  => get_field('performance_index'),
                          'smm'  => get_field('smm'),
                          'notes'  => get_field('notes'),
                          'notes_ar'  => get_field('notes_in_arabic'),
                        ); ?>
                        <li>
                          <button type="button" class="option edit-user" data-index="<?= $query->post->ID; ?>" data-details='<?php echo json_encode($data); ?>'><img src="<?= get_template_directory_uri(); ?>/assets/images/pencil.svg" alt="icon"></button>
                        </li>
                        <li>
                          <button type="button" class="option delete-user" data-index="<?= $query->post->ID; ?>"><img src="<?= get_template_directory_uri(); ?>/assets/images/bin.svg" alt="icon"></button>
                        </li>
                        <li>
                          <button type="button" class="option view-log" data-index="<?= $query->post->ID; ?>"><i class="fa fa-eye" aria-hidden="true"></i></button>
                        </li>
                      </ul>
                    </td>
                  </tr>
                <?php endwhile; ?>
              </tbody>
            </table>
          </div>
          <?php
          $pages = paginate_links(array(
            'base' => '%_%',
            'format' => '?page=%#%',
            'current' => max(1, get_query_var('paged')),
            'total' => $query->max_num_pages,
            'end_size' => 1,
            'type'  => 'array',
            'mid_size' => 2,
            'next_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            'prev_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>'
          ));
          ?>
          <?php if (is_array($pages)) : ?>
            <ul class="pagination justify-content-center justify-content-lg-end">
              <?php foreach ($pages as $page) : ?>
                <li>
                  <?php echo $page; ?>
                </li>
              <?php endforeach; ?>
            </ul>
          <?php endif; ?>
        <?php else : ?>
          <div class="alert alert-info" role="alert"><?php _e('No Measurement found'); ?> </div>
        <?php endif; ?>
      </div>
    </div>
    <!-- /section -->
  </div>

  <div class="modal1 modal fade" id="add-customer" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <header class="mb-4 pe-4">
          <h3 class="text-uppercase text-color1">Add Measurement</h3>
        </header>
        <div class="modal-body p-0">
          <div class="form1">
            <form action="#" class="add-user-form">
              <div class="row">
                <div class="col-md-6 form-group">
                  <label class="form-label">Choose Customer</label>
                  <div class="selectbox sel-customer">
                    <select class="form-control add_customer" name="customer" id="select-customer">
                      <option value="">Choose Customer</option>
                      <?php
                      $team_presidents = get_users(array('role__in' => array('customer'), 'number' => 9999));
                      foreach ($team_presidents as $team_president) :
                      ?>
                        <option value="<?= $team_president->ID; ?>"><?= $team_president->user_firstname . ' ' . $team_president->user_lastname; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-6 form-group">
                  <label class="form-label">Select Trainer</label>
                  <div class="selectbox sel-trainer">
                    <select class="form-control add_trainer" name="trainer" id="select-trainer">
                      <option value="">Select Trainer</option>
                      <?php
                      $team_presidents = get_users(array('role__in' => array('trainer'), 'number' => 9999));
                      foreach ($team_presidents as $team_president) :
                      ?>
                        <option value="<?= $team_president->ID; ?>"><?= $team_president->user_firstname . ' ' . $team_president->user_lastname; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-12 form-group">
                  <label class="form-label">Fasting</label>
                  <div class="row">
                    <div class="col-auto">
                      <label class="radio1">
                        <input type="radio" name="fasting" value="Yes">
                        <span class="inner">
                          <i class="fa fa-solid fa-thumbs-up"></i>
                        </span>
                      </label>
                    </div>
                    <div class="col-auto">
                      <label class="radio1">
                        <input type="radio" name="fasting" value="No">
                        <span class="inner">
                          <i class="fa fa-solid fa-thumbs-down"></i>
                        </span>
                      </label>
                    </div>
                  </div>
                  <label for="fasting" class="error"></label>
                </div>
                <div class="col-md-6 form-group">
                  <label class="form-label">Height (cm)</label>
                  <input type="number" class="form-control decimal_input" step="any" pattern="[0-9]*" name="height" placeholder="Enter Height (cm)">
                </div>
                <div class="col-md-6 form-group">
                  <label class="form-label">Weight (kg)</label>
                  <input type="number" class="form-control decimal_input" step="any" pattern="[0-9]*" name="weight" placeholder="Enter Weight (kg)">
                </div>
                <div class="col-md-6 form-group">
                  <label class="form-label">Performance Index</label>
                  <input type="text" class="form-control" name="performance_index" placeholder="Enter Performance Index">
                </div>
                <div class="col-md-6 form-group">
                  <label class="form-label">SMM</label>
                  <input type="text" class="form-control" name="smm" placeholder="Enter SMM">
                </div>
                <div class="col-md-6 form-group">
                  <label class="form-label">Note (EN)</label>
                  <textarea class="form-control" name="note" placeholder="Enter Note"></textarea>
                </div>
                <div class="col-md-6 form-group ar">
                  <label class="form-label">Note (AR)</label>
                  <textarea class="form-control" name="note_ar" placeholder="Enter Note"></textarea>
                </div>



              </div>

              <div class="row">
                <div class="col-md-6 mb-4 mb-md-0">
                  <button class="btn button2 w-100 btn-lg" type="button" data-bs-dismiss="modal">Cancel</button>
                </div>
                <div class="col-md-6">
                  <button class="btn button1 w-100 btn-lg submit-btn" type="submit">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>



  <div class="modal1 modal fade" id="edit-customer" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <header class="mb-4 pe-4">
          <h3 class="text-uppercase text-color1">Edit Measurement</h3>
        </header>
        <div class="modal-body p-0">
          <div class="form1">
            <form action="#" class="edit-user-form">
              <div class="row">
                <div class="col-md-6 form-group">
                  <label class="form-label">Choose Customer</label>
                  <div class="selectbox sel-edit-customer">
                    <select class="form-control edit_customer" name="edit_customer" id="edit_customer">
                      <option value="">Choose Customer</option>
                      <?php
                      $team_presidents = get_users(array('role__in' => array('customer'), 'number' => 9999));
                      foreach ($team_presidents as $team_president) :
                      ?>
                        <option value="<?= $team_president->ID; ?>"><?= $team_president->user_firstname . ' ' . $team_president->user_lastname; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-6 form-group">
                  <label class="form-label">Select Trainer</label>
                  <div class="selectbox sel-edit-trainer">
                    <select class="form-control edit_trainer" name="edit_trainer" id="edit_trainer">
                      <option value="">Select Trainer</option>
                      <?php
                      $team_presidents = get_users(array('role__in' => array('trainer'), 'number' => 9999));
                      foreach ($team_presidents as $team_president) :
                      ?>
                        <option value="<?= $team_president->ID; ?>"><?= $team_president->user_firstname . ' ' . $team_president->user_lastname; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-12 form-group">
                  <label class="form-label">Fasting</label>
                  <div class="row">
                    <div class="col-auto">
                      <label class="radio1">
                        <input type="radio" class="edit_fasting" name="edit_fasting" value="Yes">
                        <span class="inner">
                          <i class="fa fa-solid fa-thumbs-up"></i>
                        </span>
                      </label>
                    </div>
                    <div class="col-auto">
                      <label class="radio1">
                        <input type="radio" class="edit_fasting" name="edit_fasting" value="No">
                        <span class="inner">
                          <i class="fa fa-solid fa-thumbs-down"></i>
                        </span>
                      </label>
                    </div>
                  </div>
                  <label for="edit_fasting" class="error"></label>
                </div>
                <div class="col-md-6 form-group">
                  <label class="form-label">Height (cm)</label>
                  <input type="number" class="form-control edit_height decimal_input" step="any" pattern="[0-9]*" name="edit_height" placeholder="Enter Height (cm)">
                </div>
                <div class="col-md-6 form-group">
                  <label class="form-label">Weight (kg)</label>
                  <input type="number" class="form-control edit_weight decimal_input" step="any" pattern="[0-9]*" name="edit_weight" placeholder="Enter Weight (kg)">
                </div>
                <div class="col-md-6 form-group">
                  <label class="form-label">Performance Index</label>
                  <input type="text" class="form-control edit_performance_index" name="edit_performance_index" placeholder="Enter Performance Index">
                </div>
                <div class="col-md-6 form-group">
                  <label class="form-label">SMM</label>
                  <input type="text" class="form-control edit_smm" name="edit_smm" placeholder="Enter SMM">
                </div>
                <div class="col-md-6 form-group">
                  <label class="form-label">Note (EN)</label>
                  <textarea class="form-control edit_note" name="edit_note" placeholder="Enter Note"></textarea>
                </div>
                <div class="col-md-6 form-group ar">
                  <label class="form-label">Note (AR)</label>
                  <textarea class="form-control edit_note_ar" name="edit_note_ar" placeholder="Enter Note"></textarea>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 mb-4 mb-md-0">
                  <button class="btn button2 w-100 btn-lg" type="button" data-bs-dismiss="modal">Cancel</button>
                </div>
                <div class="col-md-6">
                  <input type="hidden" name="user_id" class="edit_user_id" value="">
                  <button class="btn button1 w-100 btn-lg" type="submit">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="modal1 modal fade" id="disable-enable" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <header class="mb-4 pe-4">
          <h3 class="text-uppercase text-color1"></h3>
        </header>
        <div class="modal-body p-0">
          <div class="content-body"></div>
          <div class="row">
            <div class="col-md-6 mb-4 mb-md-0">
              <button class="btn button2 w-100 btn-lg disable-enable-close" type="button">No</button>
            </div>
            <div class="col-md-6">
              <button class="btn button1 w-100 btn-lg disable-enable-submit" type="button">Yes</button>
            </div>
          </div>
        </div>
        <input type="hidden" class="disable-user-id" value="">
        <input type="hidden" class="disable-enable-id" value="">
      </div>
    </div>
  </div>

  <div class="modal1 modal fade" id="disable-enable-success" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <header class="mb-4 pe-4">
          <h3 class="text-uppercase text-color1"></h3>
        </header>
        <div class="modal-body text-center p-0">
          <div class="content-body"></div>
          <div class="row">
            <div class="col-md-6 mx-auto">
              <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal1 modal fade" id="delete-confirm" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <header class="mb-4 pe-4">
          <h3 class="text-uppercase text-color1">Delete Measurement</h3>
        </header>
        <div class="modal-body p-0">
          <div class="content-body">
            <p>Are you sure want to delete this Measurement ?</p>
          </div>
          <div class="row">
            <div class="col-md-6 mb-4 mb-md-0">
              <button class="btn button2 w-100 btn-lg" data-bs-dismiss="modal" type="button">No</button>
            </div>
            <div class="col-md-6">
              <button class="btn button1 w-100 btn-lg delete-submit" type="button">Yes</button>
            </div>
          </div>
        </div>
        <input type="hidden" class="delete-user-id" value="">
      </div>
    </div>
  </div>

  <div class="modal1 modal fade" id="delete-success" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <header class="mb-4 pe-4">
          <h3 class="text-uppercase text-color1">Delete Measurement</h3>
        </header>
        <div class="modal-body p-0">
          <div class="content-body">
            <p>Measurement has been successfully deleted</p>
          </div>
          <div class="row">
            <div class="col-md-6 mb-4 mb-md-0">
              <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>





  <div class="modal1 modal fade" id="edit-user-success" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <header class="mb-4 pe-4">
          <h3 class="text-uppercase text-color1">Edit Measurement</h3>
        </header>
        <div class="modal-body p-0">
          <div class="content-body">
            <p>Measurement has been successfully updated</p>
          </div>
          <div class="row">
            <div class="col-md-6 mb-4 mb-md-0">
              <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



  <div class="modal1 modal fade" id="view-log" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <header class="mb-4 pe-4">
          <h3 class="text-uppercase text-color1">View Note</h3>
        </header>
        <div class="modal-body p-0">

        </div>
      </div>
    </div>
  </div>



  <?php get_footer(); ?>
  <script>
    $(document).ready(function() {
      $(".add_customer").select2({
        dropdownParent: $(".sel-customer")
      });
      $(".edit_customer").select2({
        dropdownParent: $(".sel-edit-customer")
      });
      $(".add_trainer").select2({
        dropdownParent: $(".sel-trainer")
      });
      $(".edit_trainer").select2({
        dropdownParent: $(".sel-edit-trainer")
      });
    });
    $(".decimal_input").keydown(function(e){
      if ([69, 187, 188, 189].includes(e.keyCode)) {
        e.preventDefault();
      }
    });
    $.validator.addMethod('filesize', function(value, element, param) {
      return this.optional(element) || (element.files[0].size <= param * 1000000)
    }, 'File size must be less than {0} MB');
    $.validator.addMethod('decimal', function(value, element) {
            return this.optional(element) || /^((\d+(\\.\d{0,2})?)|((\d*(\.\d{1,2}))))$/.test(value);
        }, "Please enter a correct number");
    var abc = jQuery('.add-user-form').validate({
      ignore: ".ignore",
      // onfocusout: function(element) {
      //   $(element).valid()
      // },
      // onkeyup: false,
      ignore: [],
      rules: {
        trainer: {

          required: true,
        },
        customer: {

          required: true,
        },
        fasting: {
          required: true,
        },
        height: {
          required: true,
          decimal: true,
        },
        weight: {
          required: true,
          decimal: true,
        },
        // performance_index:
        // {
        //     required: true,
        // },
        // smm:
        // {
        //     required: true,
        // },
        // note:
        // {
        //     required: true,
        // }
      },
      messages: {
        customer: {
          required: "<?php _e('Customer is required', 'sidf'); ?>"
        },
        trainer: {
          required: "<?php _e('Trainer is required', 'sidf'); ?>"
        },
        fasting: {
          required: "<?php _e('Fasting is required', 'sidf'); ?>"
        },
      },
      errorElement: "label",
      errorPlacement: function(error, element) {
        if (element.hasClass("select2-hidden-accessible")) {
          element = $("#select2-" + element.attr("id") + "-container").parent();
          error.insertAfter(element);
        } else {
          error.insertAfter(element);
        }
      },

    });

    jQuery(".add-user-form").submit(function(e) {
      e.preventDefault();
      if (abc.form() && abc.form()) {

        jQuery('#loader').show();
        jQuery('.submit-btn').attr('disabled', 'disabled');

        var fd = new FormData(jQuery('.add-user-form')[0]);
        // process the form
        jQuery.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_measurement_action',
            data: fd,
            processData: false,
            contentType: false,
          })
          // using the done promise callback
          .done(function(result) {
            location.reload();


          });
      }

      return false;
    });

    jQuery(".filter-form").submit(function(e) {
      e.preventDefault();

      jQuery('#loader').show();

      var fd = new FormData(jQuery('.filter-form')[0]);
      // process the form
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_filter_measurement_action&page=1',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          var obj = JSON.parse(result);
          jQuery('.more-results').html(obj.html);
          jQuery('#loader').hide();

        });

      return false;
    });

    $(document).on("click", "a.page-numbers", function() {
      jQuery('#loader').show();
      var href = $(this).attr('href');
      var this1 = $(this);
      var fd = new FormData(jQuery('.filter-form')[0]);
      // process the form
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>' + href + '&action=admin_filter_measurement_action',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          var obj = JSON.parse(result);
          jQuery('.more-results').html(obj.html);
          jQuery('#loader').hide();
        });

      return false;
    });

    $(document).on('change', '.disable-user', function() {
      var disable = 1;
      $('#disable-enable h3').html('Disable Measurement');
      $('#disable-enable .content-body').html('<p>Are you sure to disable this Measurement ?</p>');
      $('#disable-enable-success h3').html('Disable Measurement');
      $('#disable-enable-success .content-body').html('<p>Measurement is successfully disabled</p>');
      if (this.checked) {
        disable = 0;
        $('#disable-enable h3').html('Enable Measurement');
        $('#disable-enable .content-body').html('<p>Are you sure to enable this Measurement ?</p>');
        $('#disable-enable-success h3').html('Enable Measurement');
        $('#disable-enable-success .content-body').html('<p>Measurement is successfully enabled</p>');

      }
      var user_id = this.value;
      $('.disable-user-id').val(user_id);
      $('.disable-enable-id').val(disable);
      $('#disable-enable').modal('show');

    });

    $(document).on("click", ".disable-enable-close", function() {
      var user_id = $('.disable-user-id').val();
      var disable = $('.disable-enable-id').val();
      if (disable == 1) {
        $("input[type=checkbox][value=" + user_id + "]").prop("checked", true);
      } else {
        $("input[type=checkbox][value=" + user_id + "]").prop("checked", false);
      }
      $('#disable-enable').modal('hide');
    });

    $(document).on("click", ".disable-enable-submit", function() {
      var user_id = $('.disable-user-id').val();
      var disable = $('.disable-enable-id').val();
      $('#disable-enable').modal('hide');
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_enable_disable_member_action&postid=' + user_id + '&disable=' + disable,
          data: '',
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          $('#disable-enable-success').modal('show');

        });
    });

    $(document).on("click", ".delete-user", function() {
      var user_id = $(this).data('index');
      $('.delete-user-id').val(user_id);
      $('#delete-confirm').modal('show');
    });

    $(document).on("click", ".delete-submit", function() {
      var user_id = $('.delete-user-id').val();
      $('#delete-confirm').modal('hide');
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>?action=delete_member_action&id=' + user_id,
          data: '',
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          $('#delete-success').modal('show');

        });
    });

    jQuery('#delete-success').on('hidden.bs.modal', function() {
      location.reload();
    });


    $(document).on("click", ".edit-user", function() {
      var user_id = $(this).data('index');
      var details = $(this).data('details');
      $('.edit_user_id').val(user_id);
      $('.edit_customer').val(details.customer).trigger('change');
      $('.edit_trainer').val(details.trainer).trigger('change');
      $('input[name=edit_fasting][value="' + details.fasting + '"]').prop("checked", true);
      $('.edit_height').val(details.height);
      $('.edit_weight').val(details.weight);
      $('.edit_performance_index').val(details.performance_index);
      $('.edit_smm').val(details.smm);
      $('.edit_note').val(details.notes);
      $('.edit_note_ar').val(details.notes_ar);
      $('#edit-customer').modal('show');

    });

    var abcd = jQuery('.edit-user-form').validate({
      // ignore: ".ignore",
      // onfocusout: function(element) {
      //   $(element).valid()
      // },
      // onkeyup: false,
      ignore: [],
      rules: {
        edit_trainer: {

          required: true,
        },
        edit_customer: {

          required: true,
        },
        edit_fasting: {
          required: true,
        },
        edit_height: {
          required: true,
          decimal: true,
        },
        edit_weight: {
          required: true,
          decimal: true,
        },
        // edit_performance_index:
        // {
        //     required: true,
        // },
        // edit_smm:
        // {
        //     required: true,
        // },
        // edit_note:
        // {
        //     required: true,
        // }

      },
      messages: {
        edit_customer: {
          required: "<?php _e('Customer is required', 'sidf'); ?>"
        },
        edit_trainer: {
          required: "<?php _e('Trainer is required', 'sidf'); ?>"
        },
        edit_fasting: {
          required: "<?php _e('Fasting is required', 'sidf'); ?>"
        },
      },
      errorElement: "label",
      errorPlacement: function(error, element) {
        if (element.hasClass("select2-hidden-accessible")) {
          element = $("#select2-" + element.attr("id") + "-container").parent();
          error.insertAfter(element);
        } else {
          error.insertAfter(element);
        }
      },

    });

    jQuery(".edit-user-form").submit(function(e) {
      e.preventDefault();
      if (abcd.form() && abcd.form()) {
        $('#edit-customer').modal('hide');
        jQuery('#loader').show();
        jQuery('.submit-btn').attr('disabled', 'disabled');

        var fd = new FormData(jQuery('.edit-user-form')[0]);
        // process the form
        jQuery.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: '<?php echo admin_url('admin-ajax.php'); ?>?action=admin_edit_measurement_action',
            data: fd,
            processData: false,
            contentType: false,
          })
          // using the done promise callback
          .done(function(result) {
            jQuery('#loader').hide();
            $('#edit-user-success').modal('show');



          });
      }

      return false;
    });
    jQuery('#edit-user-success').on('hidden.bs.modal', function() {
      location.reload();
    });



    jQuery(".view-log").click(function(e) {
      e.preventDefault();

      var user_id = $(this).data('index');

      // process the form
      jQuery.ajax({
          type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url: '<?php echo admin_url('admin-ajax.php'); ?>?action=view_measurement_action&user_id=' + user_id,
          data: '',
          processData: false,
          contentType: false,
        })
        // using the done promise callback
        .done(function(result) {
          var obj = JSON.parse(result);
          jQuery('#view-log .modal-body').html(obj.html);
          jQuery('#view-log').modal('show');



        });


      return false;
    });
  </script>