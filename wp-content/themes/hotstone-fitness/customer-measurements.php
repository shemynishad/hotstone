<?php /* Template name: Customer Measurements */
if (is_user_logged_in()) {
  if (current_user_can('customer')) {
    $current_user = wp_get_current_user();
  } elseif (current_user_can('administrator')) {
    wp_redirect(get_home_url('', '/admin-dashboard/'));
  } else {
    wp_redirect(get_home_url());
  }
} else {
  wp_redirect(get_home_url());
}
get_header('customer'); ?>


<div id="content">
  <!-- section -->
  <div class="section dashboard">
    <div class="container">
      <div class="row align-items-center flex-row-reverse mb-30 mb-md-5">
        <div class="col-md-6 mb-4 mb-md-0 d-none d-md-block">
          <ul class="breadcrumb alt justify-content-end mb-0">
            <li><a href="#">Home</a></li>
            <li>Measurements</li>
          </ul>
        </div>
        <div class="col-md-6">
          <div class="text-center text-md-start">
            <h2 class="dashboard-title">Measurements</h2>
          </div>
        </div>
      </div>
      <div class="box1 px-md-4 mb-30 bg-color3 border border-color1 shadow1">
        <div class="form1">
          <form action="#" class="filter-form">
            <div class="row align-items-end">
              <div class="col">
                <div class="row">
                  <div class="col-xxl-auto col-12 mb-4 mb-xxl-0 align-self-center d-none d-md-block">
                    <img src="<?= get_template_directory_uri(); ?>/assets/images/filter-gray.svg" alt="icon">
                  </div>
                  <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                    <div class="date-picker">
                      <input type="text" class="form-control" name="start_date" placeholder="Select from date">
                    </div>
                  </div>
                  <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                    <div class="date-picker">
                      <input type="text" class="form-control" name="end_date" placeholder="Select to date">
                    </div>
                  </div>
                  <div class="col-lg col-md-6 mb-md-4 mb-0 mb-xxl-0">
                    <input type="text" class="form-control" name="trainer_keyword" placeholder="Search by Trainer">
                  </div>
                  <div class="col-xxl-auto col-12 d-none d-md-block">
                    <button class="btn button1 rounded px-3 py-2 h-100" type="submit"><i class="fa fa-search fs-4"></i></button>
                  </div>
                </div>
              </div>
              <div class="col-auto d-md-none">
                <button class="btn button1 rounded px-3 py-2 h-100" type="submit"><i class="fa fa-search fs-4"></i></button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="more-results">
        <?php
        $args = array(
          'post_type' => 'measurement',
          'posts_per_page' => -1,
          'meta_query' => array(
            array(
              'key'     => 'customer',
              'value'   => $current_user->ID,
              'compare' => '=',
            ),
            array(
              'key'     => 'is_disable',
              'value'   => 1,
              'compare' => '!=',
            ),
          ),
        );
        $query = new WP_Query($args);
        if ($query->have_posts()) :
        ?>
          <ul class="list1">
            <?php while ($query->have_posts()) : $query->the_post();

            ?>
              <li>
                <div class="card1">
                  <div class="inner-wrapper">
                    <ul class="details-list mb-0">
                      <li>
                        <div class="d-flex">
                          <span class="title">ID:</span>
                          <span class="text"><?= get_field('id'); ?></span>
                        </div>
                      </li>
                      <li>
                        <div class="d-flex">
                          <span class="title">Date:</span>
                          <?php $date = get_field('date'); ?>
                          <span class="text"><?= date("d F Y", strtotime($date)); ?></span>
                        </div>
                      </li>
                      <li>
                        <div class="d-flex">
                          <span class="title">Trainer</span>
                          <?php $trainer_id = get_field('trainer');
                          $author_obj = get_user_by('id', $trainer_id); ?>
                          <span class="text"><?= $author_obj->user_firstname . ' ' . $author_obj->user_lastname; ?></span>
                        </div>
                      </li>
                    </ul>
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <button type="button" class="view-measurement" data-index="<?= $query->post->ID; ?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri(); ?>/assets/images/eye-icon.svg" alt="icon"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            <?php endwhile; ?>
          </ul>
        <?php else : ?>
          <div class="alert alert-info" role="alert"><?php _e('No class found matching the search criteria. Please try again.'); ?> </div>
        <?php endif; ?>
      </div>
      <div class="row align-items-center flex-row-reverse mb-30 mb-md-5">
        <div class="col-md-6 mb-4 mb-md-0 d-none d-md-block">
          <ul class="breadcrumb alt justify-content-end mb-0">
          </ul>
        </div>
        <div class="col-md-6">
          <div class="text-center text-md-start">
            <h2 class="dashboard-title">Progress Chart</h2>
          </div>
        </div>
        <div class="col-md-12">
          <?php
          $args = array(
            'post_type' => 'measurement',
            'posts_per_page' => -1,
            'orderby' => 'date',
            'order' => 'ASC',
            'meta_query' => array(
              array(
                'key'     => 'customer',
                'value'   => $current_user->ID,
                'compare' => '=',
              ),
              array(
                'key'     => 'is_disable',
                'value'   => 1,
                'compare' => '!=',
              ),
            ),
          );
          $query = new WP_Query($args);
          $data = array();
          if ($query->have_posts()) {
            while ($query->have_posts()) {
              $query->the_post();
              $custom_field_value = get_post_meta(get_the_ID(), 'weight', true);
              $date_value = get_field('date');
              $data[] = array(
                'label' => get_the_title(),
                'value' => $custom_field_value,
                'date' => date("d-m-Y", strtotime($date_value)),
              );
            }
          }
          wp_reset_postdata();
          ?>
          <canvas id="line-chart">

          </canvas>
        </div>
      </div>
    </div>
  </div>
  <!-- /section -->
</div>

<div class="modal2 modal fade" id="view-measurement-details" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered small">
    <div class="modal-content">
      <header class="modal-header">
        <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
        <h3>View measurement details</h3>
      </header>
      <div class="modal-body">
        <div class="box1 px-4 shadow-none bg-color3">
          <ul class="details-list d-flex mb-0">

          </ul>
        </div>
      </div>
      <footer class="modal-footer d-md-none justify-content-center">

      </footer>
    </div>
  </div>
</div>
<?php get_footer('customer'); ?>
<script>
  jQuery(document).on("click", ".view-measurement", function(e) {
    e.preventDefault();

    var user_id = $(this).data('index');

    // process the form
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=view_customer_measurement_action&user_id=' + user_id,
        data: '',
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        var obj = JSON.parse(result);
        jQuery('#view-measurement-details ul').html(obj.html);
        jQuery('#view-measurement-details').modal('show');



      });


    return false;
  });



  jQuery(".filter-form").submit(function(e) {
    e.preventDefault();

    jQuery('#loader').show();

    var fd = new FormData(jQuery('.filter-form')[0]);
    // process the form
    jQuery.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: '<?php echo admin_url('admin-ajax.php'); ?>?action=customer_filter_measurement_action&page=1',
        data: fd,
        processData: false,
        contentType: false,
      })
      // using the done promise callback
      .done(function(result) {
        var obj = JSON.parse(result);
        jQuery('.more-results').html(obj.html);
        jQuery('#loader').hide();

      });

    return false;
  });
</script>
<script>
  var ctx = document.getElementById('line-chart').getContext('2d');
  var chart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: [<?php foreach ($data as $datum) {
                  echo '"' . $datum['date'] . '",';
                } ?>],
      datasets: [{
        label: 'Weight (Kg)',
        backgroundColor: 'rgba(225,149,0,0.2)',
        borderColor: 'rgba(225,149,0,1)',
        borderWidth: 1,
        data: [<?php foreach ($data as $datum) {
                  echo $datum['value'] . ',';
                } ?>]
      }]
    },
    options: {
      responsive: true,
      scales: {
        xAxes: [{
          ticks: {
            beginAtZero: true
          },
          scaleLabel: {
            display: true,
            labelString: 'Date'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            stepSize: 10,
            max: 200,
            min: 0,
          },
          scaleLabel: {
            display: true,
            labelString: 'Weight in Kg'
          }
        }]
      }
    }
  });
</script>