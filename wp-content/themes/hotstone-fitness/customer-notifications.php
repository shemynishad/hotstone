<?php /* Template name: Customer Notifications */
if ( is_user_logged_in() ) {
    if (current_user_can('customer')) {
        $current_user = wp_get_current_user();
    }elseif (current_user_can('administrator')) {
        wp_redirect(get_home_url('','/admin-dashboard/'));   
    }else {
        wp_redirect(get_home_url());     
    }
}else{
wp_redirect(get_home_url()); 
} 
get_header('customer');?>

<div id="content">
        <div class="section">
            <div id="loader"></div>
          <div class="container">
            <div class="form1 mb-5">
              <h3 class="mb-4">Notification</h3>
              <?php 
                  $args = array(
                    	'post_type' => 'notification',
                    	'posts_per_page' => -1,
                    	'meta_query' => array(
                            array(
                    			'key'     => 'customer',
                    			'value'   => $current_user->ID,
                    			'compare' => '=',
                    		)
                        ),
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
            <div class="table1 mb-4">
              <table class="table mb-0">
                <thead>
                  <tr>
                    <td>ID</td>
                    <td>Date</td> 
                    <td>Subject</td>                    
                    <td></td>                    
                  </tr>
                </thead>
                <tbody>
                <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
                  <tr>
                    <td><?= get_field('id');?></td>
                    <?php $date = get_field('date');?>
                    <td><?= date("d F Y", strtotime($date));?></td>
                    <td><?= get_field('subject');?></td>
                    <td>
                      <ul class="user-options mb-0">
                        <li>
                          <button type="button" class="option view-log" data-index="<?= $query->post->ID;?>"><i class="fa fa-eye" aria-hidden="true"></i></button>
                        </li>
                      </ul>
                    </td>
                  </tr>
                  <?php endwhile;?>
                </tbody>
              </table>
            </div>
            <?php
            $pages = paginate_links( array(
                'base' => '%_%',
                'format' => '?page=%#%',
                'current' => max( 1, get_query_var('paged')  ),
                'total' => $query->max_num_pages,
                'end_size' => 1,
                'type'  => 'array',
                'mid_size' => 2,
                'next_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                'prev_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ) );
            ?>            
            <?php if( is_array( $pages ) ) :?>
            <ul class="pagination justify-content-center justify-content-lg-end">
            <?php foreach ( $pages as $page ):?>
                        <li>
                            <?php echo $page;?>
                        </li>
                        <?php endforeach;?>
                    </ul>
            <?php endif;?>
            <?php else:?>
                <p>No Notification Found</p>
            <?php endif;?>
            </div>
          </div>
        </div>
      </div>
      
      <div class="modal2 modal fade" id="view-measurement-details" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered small">
            <div class="modal-content">
              <header class="modal-header">
                <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
                <h3>View measurement details</h3>
              </header>
              <div class="modal-body">
                <div class="box1 px-4 shadow-none bg-color3">
                  <ul class="details-list d-flex mb-0">
                    
                  </ul>
                </div>
              </div>
              <footer class="modal-footer d-md-none justify-content-center">
                
              </footer>
            </div>
          </div>
        </div>
      
      
        <?php get_footer('customer');?>
<script>
    
    jQuery( document ).on( "click", ".view-log", function(e) {
            e.preventDefault();
                
                    var user_id = $(this).data('index');
                     
                    // process the form
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=view_customer_notification_action&user_id='+user_id,
                        data: '',
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                            var obj = JSON.parse(result);     
                            jQuery('#view-measurement-details ul').html(obj.html); 
                            jQuery('#view-measurement-details').modal('show');
                      


                    });
            

            return false;
        });
</script>
