<?php /* Template name: Customer Completed Booking */
if ( is_user_logged_in() ) {
    if (current_user_can('customer')) {
        $current_user = wp_get_current_user();
    }elseif (current_user_can('administrator')) {
        wp_redirect(get_home_url('','/admin-dashboard/'));   
    }else {
        wp_redirect(get_home_url());     
    }
}else{
wp_redirect(get_home_url()); 
} 
get_header('customer');?>


<div id="content">
        <!-- section -->
        <div class="section dashboard">
          <div class="container">
            <div class="row align-items-center flex-row-reverse mb-30 mb-md-5">
              <div class="col-md-6 mb-4 mb-md-0 d-none d-md-block">
                <ul class="breadcrumb alt justify-content-end mb-0">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">Classes</a></li>
                  <li>Booking</li>
                </ul>
              </div>
              <div class="col-md-6">
                <div class="text-center text-md-start">
                  <h2 class="dashboard-title">Booking</h2>
                </div>
              </div>
            </div>
            <div class="box1 px-md-4 mb-30 bg-color3 border border-color1 shadow1 d-none">
              <div class="form1 alt">
                <form action="#" class="filter-form">
                  <div class="row align-items-end">
                    <div class="col">
                      <div class="row">
                        <div class="col-xxl-auto col-12 mb-4 mb-xxl-0 align-self-center d-none d-md-block">
                          <img src="<?= get_template_directory_uri();?>/assets/images/filter-gray.svg" alt="icon">
                        </div>
                        <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                          <div class="date-picker end-date-picker further-date">
                            <input type="text" class="form-control" name="start_date" placeholder="Select from date">
                          </div>
                        </div>
                        <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                          <div class="date-picker end-date-picker further-date">
                            <input type="text" class="form-control" name="end_date" placeholder="Select to date">
                          </div>
                        </div>
                        <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                            <div class="selectbox">
                               <select class="form-control" name="type">
                            <option value="">Type</option>
                            <option value="Individual">Individual</option>
                            <option value="Group">Group</option>
                            <option value="Online">Online</option>
                            
                          </select>
                            </div>
                        </div>
                        <div class="col-lg col-md-6 mb-md-4 mb-0 mb-xxl-0">
                            <input type="text" class="form-control" name="keyword" placeholder="Search by Trainer">
                        </div>
                        <div class="col-xxl-auto col-12 d-none d-md-block">
                          <button class="btn button1 rounded px-3 py-2 h-100" type="submit"><i class="fa fa-search fs-4"></i></button>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto d-md-none">
                      <button class="btn button1 rounded px-3 py-2 h-100" type="submit"><i class="fa fa-search fs-4"></i></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="button-group mb-30 justify-content-end justify-content-md-start">
              <a href="/customer-booking" class="btn button4 d-inline-flex align-items-center text-normal p-md-3 p-2">
                <span class="d-inline-block">Upcoming Bookings</span>
              </a>
              <a href="/customer-completed-booking" class="btn button4 d-inline-flex align-items-center text-normal p-md-3 p-2 active">
                <span class="d-inline-block">Completed Bookings</span>
              </a>
            </div>
            <div class="more-results">
                <?php 
               $today = date('Ymd');
                  $args = array(
                    	'post_type' => 'booking',
                    	'posts_per_page' => -1,
                    	'meta_query' => array(
                             array(
                                'key'     => 'booking_date',
                                'compare' => '<',
                                'value'   => $today,
                            ),
                            array(
                    			'key'     => 'customer',
                    			'value'   => $current_user->ID,
                    			'compare' => '=',
                    		),
                    		array(
                    			'key'     => 'is_disable',
                    			'value'   => 1,
                    			'compare' => '!=',
                    		)
                        ),
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
            <ul class="list1">
               <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
                  <li>
                    <div class="card1">
                      <div class="inner-wrapper">
                        <ul class="details-list mb-0">
                          <li>
                            <div class="d-flex">
                              <span class="title">ID:</span>
                              <span class="text"><?= get_field('id');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Booking date:</span>
                              <?php $booking_date = get_field('booking_date');?>
                              <span class="text"><?= date("d F Y", strtotime($booking_date));?></span>
                            </div>
                          </li>
                          <?php $class = get_field('class');
                          $class_id_array[] = $class;
                          ?>
                          <?php $workout_titles = array();
                    $workouts = explode(",",get_field('workouts',$class));
                    foreach($workouts as $workout):
                        $workout_titles[] = get_the_title($workout);
                    endforeach;?>
                          <li>
                            <div class="d-flex">
                              <span class="title">Workout:</span>
                              <span class="text"><?= implode(",",$workout_titles);?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Date:</span>
                              <?php $created_date = get_field('created_date');?>
                              <span class="text"><?= date("d F Y", strtotime($created_date));?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Trainer:</span>
                              <?php $trainer_id = get_field('trainer',$class);
                    $author_obj = get_user_by('id', $trainer_id);?>
                              <span class="text"><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Type:</span>
                              <span class="text"><?= get_field('name',get_field('membership',$class));?></span>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </li>
                 <?php endwhile;?>
            </ul>
            <?php else:?>
            <div class="alert alert-info" role="alert"><?php _e('No completed booking found');?> </div>
            <?php endif;?>
          </div>
        </div>
        <!-- /section -->
      </div>

      <?php get_footer('customer');?>