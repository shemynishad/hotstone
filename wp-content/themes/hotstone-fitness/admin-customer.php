<?php /* Template name: Admin Customer */
if ( is_user_logged_in() ) {
    if (current_user_can('administrator') || current_user_can('front_office_user')) {
        $current_user = wp_get_current_user();
    } else {
        wp_redirect(get_home_url());     
    }
}else{
wp_redirect(get_home_url()); 
} 
get_header();?>

<div id="content">
        <!-- section -->
        <div class="section dashboard">
            <div id="loader"></div>
          <div class="container">
            <div class="row align-items-center flex-row-reverse mb-30 mb-md-5">
              <div class="col-md-6 mb-4 mb-md-0">
                <ul class="breadcrumb justify-content-end mb-0">
                  <li><a href="#">Dashboard</a></li>
                  <li><a href="#">User</a></li>
                  <li>Customer</li>
                </ul>
              </div>
              <div class="col-md-6">
                <p class="welcome-title">Hello, Welcome <?= $current_user->display_name;?>.</p>
              </div>
            </div>
            <div class="box1 py-4 mb-30">
              <div class="form1">
                <form action="#" class="filter-form">
                  <div class="row align-items-end">
                    <div class="col-xxl-auto col-12 mb-4 mb-xxl-0">
                      <i class="icon"><img src="<?= get_template_directory_uri();?>/assets/images/filter.svg" alt="icon"></i>
                    </div>
                    <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                      <label class="form-label">Search by Name/Email/Phone</label>
                      <div>
                        <input type="text" class="form-control" name="keyword" placeholder="Search by Name/Email/Phone">
                      </div>
                    </div>
                    <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                      <label class="form-label">Registration From</label>
                      <div class="date-picker">
                        <input type="text" class="form-control" name="start_date" placeholder="Select from date">
                      </div>
                    </div>
                    <div class="col-lg col-md-6 mb-4 mb-xxl-0">
                      <label class="form-label">To</label>
                      <div class="date-picker">
                        <input type="text" class="form-control" name="end_date" placeholder="Select to date">
                      </div>
                    </div>
                    <div class="col-xxl-auto col-12">
                      <button class="btn button3" type="submit">Search</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="text-center text-lg-end mb-4">
              <button class="btn button1 btn-sm" data-bs-toggle="modal" data-bs-target="#add-customer">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.999 23.003" class="me-2">
                  <path id="Path_35" data-name="Path 35" d="M16.515,21.525a.709.709,0,0,0,.135.013.721.721,0,0,0,.709-.585,3.918,3.918,0,0,0,.073-.74,4.337,4.337,0,0,0-2.63-3.889,3.158,3.158,0,0,0,1.087-2.38,3.251,3.251,0,0,0-6.5,0,3.16,3.16,0,0,0,1.105,2.394,4.331,4.331,0,0,0-2.6,3.874,4.027,4.027,0,0,0,.072.737.723.723,0,1,0,1.422-.261,2.621,2.621,0,0,1-.049-.476A3.143,3.143,0,0,1,12.664,17.3a3.142,3.142,0,0,1,3.324,2.917,2.528,2.528,0,0,1-.048.471.717.717,0,0,0,.57.841Zm-3.876-9.338a1.757,1.757,0,1,1-1.8,1.756,1.784,1.784,0,0,1,1.8-1.756Z" transform="translate(-4.334 -6.458)" fill="#fff"/>
                  <path id="Path_36" data-name="Path 36" d="M29.611,12.2a.72.72,0,0,0,.722-.718h0V9.328a2.883,2.883,0,0,0-2.889-2.87H7.222a2.882,2.882,0,0,0-2.888,2.87V22.96a2.882,2.882,0,0,0,2.888,2.87h5.563a.73.73,0,0,1,.511.211l3.016,3a1.449,1.449,0,0,0,2.041,0l3.017-3a.73.73,0,0,1,.511-.211h5.562a2.882,2.882,0,0,0,2.89-2.87V15.067a.722.722,0,1,0-1.445,0V22.96a1.442,1.442,0,0,1-1.444,1.44H21.882a2.16,2.16,0,0,0-1.532.628l-3.017,3-3.017-3a2.159,2.159,0,0,0-1.532-.63H7.222a1.442,1.442,0,0,1-1.444-1.435V9.328A1.442,1.442,0,0,1,7.222,7.893H27.444a1.442,1.442,0,0,1,1.444,1.435V11.48a.72.72,0,0,0,.722.718Z" transform="translate(-4.334 -6.458)" fill="#fff"/>
                  <path id="Path_37" data-name="Path 37" d="M22.388,19.372a.719.719,0,0,0,.722-.716h0V16.5h2.167a.718.718,0,1,0,0-1.435H23.111v-2.15a.723.723,0,0,0-1.444,0v2.152H19.5a.718.718,0,1,0,0,1.435h2.167v2.152a.719.719,0,0,0,.722.717Z" transform="translate(-4.334 -6.458)" fill="#fff"/>
                </svg>
                Add Customer          
              </button>
            </div>
            <div class="more-results">
                  <?php 
                  $count_args  = array(
                        'role'      => 'customer',
                        'fields'    => 'all_with_meta',
                        'number'    => 999999      
                    );
                    $user_count_query = new WP_User_Query($count_args);
                    $user_count = $user_count_query->get_results();
                    
                    // count the number of users found in the query
                    $total_users = $user_count ? count($user_count) : 1;
                    $users_per_page = 8;
                    $page=  1;
                    // calculate the total number of pages.
                    $total_pages = 1;
                    $offset = $users_per_page * ($page - 1);
                    $total_pages = ceil($total_users / $users_per_page);
                  
                  $team_presidents = get_users( array( 'role__in' => array( 'customer'), 'number' =>8 ) );
                  if($team_presidents):
                  ?>
            <div class="table1 mb-4">
              <table class="table mb-0">
                <thead>
                  <tr>
                    <td>ID</td>
                    <td>First Name</td>       
                    <td>Last Name</td>      
                    <td>DOB</td>    
                    <td>Gender</td>               
                    <td>Email</td>                    
                    <td>Phone</td>                
                    <td></td>                    
                  </tr>
                </thead>
                <tbody>
                <?php foreach($team_presidents as $team_president):
                       
                    ?>
                  <tr>
                    <td><?= get_field('user_id', 'user_'.$team_president->ID);?></td>
                    <td><?= $team_president->user_firstname;?></td>
                    <td><?= $team_president->user_lastname;?></td>
                    <td><?= get_field('dob', 'user_'.$team_president->ID);?></td>
                    <td><?= ucfirst(get_field('gender', 'user_'.$team_president->ID));?></td>
                    <td><?= $team_president->user_email;?></td>
                    <td><?= get_field('phone', 'user_'.$team_president->ID);?></td>
                    <td>
                      <ul class="user-options mb-0">
                      <?php $disable = get_field('disable', 'user_'.$team_president->ID);?>
                        <li>
                          <label class="toggle1">
                            <input type="checkbox" class="disable-user" <?php if(!$disable){ echo 'checked';}?> value="<?= $team_president->ID;?>">
                            <span class="slider"></span>
                          </label>
                        </li>
                        <?php 
                         $photo_url = '';
                         if(get_field('photo', 'user_'.$team_president->ID)){
                            $photo_id = get_field('photo', 'user_'.$team_president->ID);
                            $photo_url = wp_get_attachment_url($photo_id);
                         }
                         $formatted_date =  date("m/d/Y", strtotime(get_field('registration_date', 'user_'.$team_president->ID)));
                         $data = array(   
                              'first_name_en' => $team_president->user_firstname,
                              'last_name_en' => $team_president->user_lastname,
                              'first_name_ar' => get_field('first_name_arabic', 'user_'.$team_president->ID),
                              'last_name_ar' => get_field('last_name_arabic', 'user_'.$team_president->ID),
                              'email' => $team_president->user_email,
                              'phone' => get_field('phone', 'user_'.$team_president->ID),
                              'dob' => get_field('dob', 'user_'.$team_president->ID),
                              'height' => get_field('height', 'user_'.$team_president->ID),
                               'gender' => get_field('gender', 'user_'.$team_president->ID),
                              'company_name' => get_field('company_name', 'user_'.$team_president->ID),
                              'company_name_arabic' => get_field('company_name_arabic', 'user_'.$team_president->ID),
                              'position' => get_field('position', 'user_'.$team_president->ID),
                              'position_arabic' => get_field('position_arabic', 'user_'.$team_president->ID),
                              'address' => get_field('address', 'user_'.$team_president->ID),
                              'address_arabic' => get_field('address_arabic', 'user_'.$team_president->ID),
                              'registration_date' => $formatted_date,
                              'photo_url' => $photo_url,
                              'source_referral' => get_field('source_referral', 'user_'.$team_president->ID)
                              
                        );?>
                        <li>
                          <button type="button" class="option edit-user" data-index="<?= $team_president->ID;?>" data-details='<?php echo json_encode($data);?>'><img src="<?= get_template_directory_uri();?>/assets/images/pencil.svg" alt="icon"></button>
                        </li>
                        <?php if(!current_user_can('front_office_user')){ ?>
                        <li>
                          <button type="button" class="option delete-user" data-index="<?= $team_president->ID;?>"><img src="<?= get_template_directory_uri();?>/assets/images/bin.svg" alt="icon"></button>
                        </li>
                        <?php } ?>
                        <li>
                          <button type="button" class="option add-note" data-index="<?= $team_president->ID;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                        </li>
                        <li>
                          <button type="button" class="option send-details" data-index="<?= $team_president->ID;?>"><img src="<?= get_template_directory_uri();?>/assets/images/icon4.svg" alt="icon"></button>
                        </li>
                        <li>
                          <button type="button" class="option view-note" data-index="<?= $team_president->ID;?>"><i class="fa fa-eye" aria-hidden="true"></i></button>
                        </li>
                      </ul>
                    </td>
                  </tr>
                  <?php endforeach;?>
                </tbody>
              </table>
            </div>
            <?php
            $pages = paginate_links( array(
                'base' => '%_%',
                'format' => '?page=%#%',
                'current' => max( 1, get_query_var('paged')  ),
                'total' => $total_pages,
                'end_size' => 1,
                'type'  => 'array',
                'mid_size' => 2,
                'next_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                'prev_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ) );
            ?>            
            <?php if( is_array( $pages ) ) :?>
            <ul class="pagination justify-content-center justify-content-lg-end">
            <?php foreach ( $pages as $page ):?>
                        <li>
                            <?php echo $page;?>
                        </li>
                        <?php endforeach;?>
                    </ul>
            <?php endif;?>
            <?php else:?>
                <p>No Customer User Found</p>
            <?php endif;?>
          </div>
        </div>
        <!-- /section -->
      </div>

      <div class="modal1 modal fade" id="add-customer" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Add Customer</h3>
              </header>
              <div class="modal-body p-0">
                <div class="form1">
                  <form action="#" class="add-user-form">
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label class="form-label">First name (EN)</label>
                        <input type="text" class="form-control" name="first_name_en" placeholder="Enter first name">
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">First name (AR)</label>
                        <input type="text" class="form-control" name="first_name_ar" placeholder="Enter first name">
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Last name (EN)</label>
                        <input type="text" class="form-control" name="last_name_en" placeholder="Enter last name">
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">Last name (AR)</label>
                        <input type="text" class="form-control" name="last_name_ar" placeholder="Enter last name">
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Email</label>
                        <input type="email" class="form-control" name="email" placeholder="Enter email address">
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Phone</label>
                        <input type="text" class="form-control" name="phone" placeholder="Enter phone number">
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Date of Birth</label>
                        <div class="date-picker">
                          <input type="text" class="form-control" name="date_of_birth" placeholder="Select date of birth">
                        </div>
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Height (in CM)</label>                        
                        <input type="text" class="form-control" name="height" placeholder="Enter Height">                        
                      </div>
                      <div class="col-12 form-group">
                        <label class="form-label">Source of Referral</label>
                        <div class="selectbox">
                          <select class="form-control" name="source_referral">
                            <option value="">Select source of referral</option>
                            <option value="Option 1">Social Media</option>
                            <option value="Option 2">Google</option>
                            <option value="Option 3">Friends</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-12 form-group">
                        <label class="form-label">Gender</label>
                        <div class="row">
                          <div class="col-auto">
                            <label class="radio1">
                              <input type="radio" name="gender" value="male">
                              <span class="inner">
                                <img src="<?= get_template_directory_uri();?>/assets/images/male.svg" alt="icon">
                              </span>
                            </label>
                          </div>
                          <div class="col-auto">
                            <label class="radio1">
                              <input type="radio" name="gender" value="female">
                              <span class="inner">
                                <img src="<?= get_template_directory_uri();?>/assets/images/female.svg" alt="icon">
                              </span>
                            </label>
                          </div>
                        </div>
                        <label for="gender" class="error"></label>
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Company (EN)</label>
                        <input type="text" class="form-control" name="company_en" placeholder="Enter Company">
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">Company (AR)</label>
                        <input type="text" class="form-control" name="company_ar" placeholder="Enter Company">
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Position (EN)</label>
                        <input type="text" class="form-control" name="position_en" placeholder="Enter Position">
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">Position (AR)</label>
                        <input type="text" class="form-control" name="position_ar" placeholder="Enter Position">
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Address (EN)</label>
                        <textarea class="form-control" name="address" placeholder="Enter Address"></textarea>
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">Address (AR)</label>
                        <textarea class="form-control" name="address_ar" placeholder="Enter Address"></textarea>
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Photo</label>
                        <div class="file-upload">
                          <input type="file" id="photo" class="single-upload" name="photo" accept="image/png, image/jpg, image/jpeg">
                          <label for="photo" class="inner">&plus;</label>
                          <img src="" class="image-src hide">
                        </div>
                        <label for="photo" class="error"></label>
                        <span class="allowed-type">Allowed file types: png, jpg, jpeg</span>
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Registration Date</label>
                        <div class="date-picker">
                          <input type="text" class="form-control" name="registration_date" value="<?= date('m/d/Y');?>" placeholder="Select date of birth">
                        </div>
                      </div>
                     

                    </div>

                    <div class="row">
                      <div class="col-md-6 mb-4 mb-md-0">
                        <button class="btn button2 w-100 btn-lg" type="button" data-bs-dismiss="modal">Cancel</button>
                      </div>
                      <div class="col-md-6">
                        <button class="btn button1 w-100 btn-lg submit-btn" type="submit">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="add-note" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Add Note</h3>
              </header>
              <div class="modal-body p-0">
                <div class="form1">
                  <form action="#" class="add-note-form">
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label class="form-label">Note (EN)</label>
                        <textarea class="form-control" name="note" placeholder="Enter Note"></textarea>
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">Note (AR)</label>
                        <textarea class="form-control" name="note_ar" placeholder="Enter Note"></textarea>
                      </div> 
                    </div>

                    <div class="row">
                        <input type="hidden" class="note_user_id" name="user_id">
                      <div class="col-md-6 mb-4 mb-md-0">
                        <button class="btn button2 w-100 btn-lg"  type="button" data-bs-dismiss="modal">Cancel</button>
                      </div>
                      <div class="col-md-6">
                        <button class="btn button1 w-100 btn-lg" type="submit">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="edit-customer" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Edit Customer</h3>
              </header>
              <div class="modal-body p-0">
                <div class="form1">
                  <form action="#" class="edit-user-form">
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label class="form-label">First name (EN)</label>
                        <input type="text" class="form-control edit_first_name_en" name="edit_first_name_en" placeholder="Enter first name"> 
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">First name (AR)</label>
                        <input type="text" class="form-control edit_first_name_ar" name="edit_first_name_ar" placeholder="Enter first name">
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Last name (EN)</label>
                        <input type="text" class="form-control edit_last_name_en" name="edit_last_name_en" placeholder="Enter last name">
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">Last name (AR)</label>
                        <input type="text" class="form-control edit_last_name_ar" name="edit_last_name_ar" placeholder="Enter last name">
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Phone</label>
                        <input type="text" class="form-control edit_phone" name="edit_phone" placeholder="Enter phone number">
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Date of Birth</label>
                        <div class="date-picker">
                          <input type="text" class="form-control edit_date_of_birth" name="edit_date_of_birth" placeholder="Select date of birth">
                        </div>
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Height (in CM)</label>                        
                        <input type="text" class="form-control edit_height" name="edit_height" placeholder="Enter Height">                        
                      </div>
                      <div class="col-12 form-group">
                        <label class="form-label">Source of Referral</label>
                        <div class="selectbox">
                          <select class="form-control edit_source_referral" name="edit_source_referral">
                            <option value="">Select source of referral</option>
                            <option value="Option 1">Social Media</option>
                            <option value="Option 2">Google</option>
                            <option value="Option 3">Friends</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-12 form-group">
                        <label class="form-label">Gender</label>
                        <div class="row">
                          <div class="col-auto">
                            <label class="radio1">
                              <input type="radio" class="edit_gender" name="edit_gender" value="male">
                              <span class="inner">
                                <img src="<?= get_template_directory_uri();?>/assets/images/male.svg" alt="icon">
                              </span>
                            </label>
                          </div>
                          <div class="col-auto">
                            <label class="radio1">
                              <input type="radio" class="edit_gender" name="edit_gender" value="female">
                              <span class="inner">
                                <img src="<?= get_template_directory_uri();?>/assets/images/female.svg" alt="icon">
                              </span>
                            </label>
                          </div>
                        </div>
                        <label for="edit_gender" class="error"></label>
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Company (EN)</label>
                        <input type="text" class="form-control edit_company_en" name="edit_company_en" placeholder="Enter Company">
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">Company (AR)</label>
                        <input type="text" class="form-control edit_company_ar" name="edit_company_ar" placeholder="Enter Company">
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Position (EN)</label>
                        <input type="text" class="form-control edit_position_en" name="edit_position_en" placeholder="Enter Position">
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">Position (AR)</label>
                        <input type="text" class="form-control edit_position_ar" name="edit_position_ar" placeholder="Enter Position">
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Address (EN)</label>
                        <textarea class="form-control edit_address" name="edit_address" placeholder="Enter Address"></textarea>
                      </div>
                      <div class="col-md-6 form-group ar">
                        <label class="form-label">Address (AR)</label>
                        <textarea class="form-control edit_address_ar" name="edit_address_ar" placeholder="Enter Address"></textarea>
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Photo</label>
                        <div class="file-upload">
                          <input type="file" id="edit_photo" class="single-upload" name="photo" accept="image/png, image/jpg, image/jpeg">
                          <label for="edit_photo" class="inner">&plus;</label>
                          <img src="" class="edit_image image-src hide">
                        </div>
                        <label for="photo" class="error"></label>
                        <span class="allowed-type">Allowed file types: png, jpg, jpeg</span>
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="form-label">Registration Date</label>
                        <div class="date-picker">
                          <input type="text" class="form-control edit_registration_date" name="edit_registration_date" value="" placeholder="Select date of birth">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 mb-4 mb-md-0">
                        <button class="btn button2 w-100 btn-lg" type="button" data-bs-dismiss="modal">Cancel</button>
                      </div>
                      <div class="col-md-6">
                        <input type="hidden" name="user_id" class="edit_user_id" value="">
                        <button class="btn button1 w-100 btn-lg" type="submit">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="modal1 modal fade" id="disable-enable" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1"></h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"></div>
                <div class="row">
                  <div class="col-md-6 mb-4 mb-md-0">
                    <button class="btn button2 w-100 btn-lg disable-enable-close" type="button">No</button>
                  </div>
                  <div class="col-md-6">
                    <button class="btn button1 w-100 btn-lg disable-enable-submit" type="button">Yes</button>
                  </div>
                </div>
              </div>
              <input type="hidden" class="disable-user-id" value="">
              <input type="hidden" class="disable-enable-id" value="">
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="disable-enable-success" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1"></h3>
              </header>
              <div class="modal-body text-center p-0">
                <div class="content-body"></div>
                <div class="row">
                  <div class="col-md-6 mx-auto">
                    <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="delete-confirm" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Delete User</h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"><p>Are you sure want to delete this user ?</p></div>
                <div class="row">
                  <div class="col-md-6 mb-4 mb-md-0">
                    <button class="btn button2 w-100 btn-lg" data-bs-dismiss="modal" type="button">No</button>
                  </div>
                  <div class="col-md-6">
                    <button class="btn button1 w-100 btn-lg delete-submit" type="button">Yes</button>
                  </div>
                </div>
              </div>
              <input type="hidden" class="delete-user-id" value="">
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="delete-success" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Delete User</h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"><p>User has been successfully deleted</p></div>
                <div class="row">
                  <div class="col-md-6 mb-4 mb-md-0">
                   <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="send-details-confirm" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Send Login Details</h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"><p>Are you sure want to send this user login details?</p></div>
                <div class="row">
                  <div class="col-md-6 mb-4 mb-md-0">
                    <button class="btn button2 w-100 btn-lg" data-bs-dismiss="modal" type="button">No</button>
                  </div>
                  <div class="col-md-6">
                    <button class="btn button1 w-100 btn-lg send-details-submit" type="button">Yes</button>
                  </div>
                </div>
              </div>
              <input type="hidden" class="send-details-user-id" value="">
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="send-details-success" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Send Login Details</h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"><p>Login details has been successfully send</p></div>
                <div class="row">
                  <div class="col-md-6 mb-4 mb-md-0">
                   <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="edit-user-success" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Edit USer</h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"><p>User has been successfully updated</p></div>
                <div class="row">
                  <div class="col-md-6 mb-4 mb-md-0">
                   <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="add-note-success" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">Add Note</h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"><p>Note has been successfully added</p></div>
                <div class="row">
                  <div class="col-md-6 mb-4 mb-md-0">
                   <button class="btn button1 w-100 btn-lg" data-bs-dismiss="modal" type="button">OK</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal1 modal fade" id="view-note" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            <button type="button" class="close-modal" data-bs-dismiss="modal" aria-label="Close">&times;</button>
              <header class="mb-4 pe-4">
                <h3 class="text-uppercase text-color1">View Note</h3>
              </header>
              <div class="modal-body p-0">
                <div class="content-body"></div>
              </div>
            </div>
          </div>
        </div>



<?php get_footer();?>
<script>
      $.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param * 1000000)
}, 'File size must be less than {0} MB');
       var abc = jQuery('.add-user-form').validate({
            // ignore: ".ignore",
            // onfocusout: function(element) {$(element).valid()},
            // onkeyup: false,
            ignore: [],
            rules:
            {
                first_name_en:
                {

                    required: true,  
                },
                first_name_ar:
                {

                    required: true,  
                },
                last_name_en:
                {

                    required: true, 
                },
                last_name_ar:
                {

                    required: true, 
                },
                email:
                {

                    required: true,       
                    email:true,
                    synchronousRemote: "<?php echo admin_url('admin-ajax.php');?>?action=email_action"  
                },
                 phone:
                {

                    required: true,  
        			digits: true,
        			minlength: 10,
        			maxlength: 10,
                },
                // date_of_birth:
                // {
                //     required: true, 
                // },
                // height:
                // {
                //     required: true,
                //      digits: true,
                // },
                gender:
                {
                    required: true, 
                },
                // company_en:
                // {
                //     required: true, 
                // },
                // position_en:
                // {
                //     required: true, 
                // },
                // photo:
                // {
                //     required: true, 
                //     filesize : 1.5
                // },
                // address:
                // {
                //     required: true, 
                // },
                // registration_date:    
                // {
                //     required: true, 
                // },
                // source_referral:    
                // {
                //     required: true, 
                // }
            },
            messages: {
                
                email: {
                  	
                    synchronousRemote: "<?php _e('The Email is already used','sidf');?>"
                },
                phone: {
                  minlength: "<?php _e('Please enter at least 10 numbers.','sidf');?>",
                  maxlength: "<?php _e('Please enter no more than 10 numbers.','sidf');?>"
                }
            },

        });

        jQuery(".add-user-form").submit(function(e){
            e.preventDefault();
            if(abc.form()  && abc.form())
            {
                
                 jQuery('#loader').show();
                 jQuery('.submit-btn').attr('disabled','disabled');
                
                    var fd= new FormData(jQuery('.add-user-form')[0]);
                    // process the form
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=admin_register_customer_action',
                        data: fd,
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                           location.reload();                      


                    });
            }

            return false;
        });

        jQuery(".filter-form").submit(function(e) {
    e.preventDefault();
      
        jQuery('#loader').show();
      
        var fd= new FormData(jQuery('.filter-form')[0]);
        // process the form
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>?action=admin_filter_customer_action&page=1',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            var obj = JSON.parse(result);     
            jQuery('.more-results').html(obj.html); 
             jQuery('#loader').hide();
             
          });
        
        return false;
    });

    $( document ).on( "click", "a.page-numbers", function() {
        jQuery('#loader').show();
        var href = $(this).attr('href');
        var this1 = $(this);
        var fd= new FormData(jQuery('.filter-form')[0]);
        // process the form
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>'+href+'&action=admin_filter_customer_action',
          data: fd,
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            var obj = JSON.parse(result);     
            jQuery('.more-results').html(obj.html); 
             jQuery('#loader').hide();          
          });
        
        return false;
    });

    $(document).on('change', '.disable-user', function() {
        var disable = 1;
            $('#disable-enable h3').html('Disable User');
            $('#disable-enable .content-body').html('<p>Are you sure to disable this user ?</p>');
            $('#disable-enable-success h3').html('Disable User');
            $('#disable-enable-success .content-body').html('<p>User is successfully disabled</p>');
        if(this.checked) {
        disable = 0;
        $('#disable-enable h3').html('Enable User');
        $('#disable-enable .content-body').html('<p>Are you sure to enable this user ?</p>');
        $('#disable-enable-success h3').html('Enable User');
        $('#disable-enable-success .content-body').html('<p>User is successfully enabled</p>');
            
        }
        var user_id = this.value;
        $('.disable-user-id').val(user_id);
        $('.disable-enable-id').val(disable);
        $('#disable-enable').modal('show');
         
    });

    $( document ).on( "click", ".disable-enable-close", function() {
        var user_id = $('.disable-user-id').val();
        var disable = $('.disable-enable-id').val();
        if(disable==1){
          $("input[type=checkbox][value="+user_id+"]").prop("checked",true);
        }else{
          $("input[type=checkbox][value="+user_id+"]").prop("checked",false);
        }
        $('#disable-enable').modal('hide');
    });

    $( document ).on( "click", ".disable-enable-submit", function() {
        var user_id = $('.disable-user-id').val();
        var disable = $('.disable-enable-id').val();
        $('#disable-enable').modal('hide');
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>?action=admin_enable_disable_action&user_id='+user_id+'&disable='+disable,
          data: '',
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            $('#disable-enable-success').modal('show'); 
             
          });
    });

    $( document ).on( "click", ".delete-user", function() {
        var user_id = $(this).data('index');
        $('.delete-user-id').val(user_id);
        $('#delete-confirm').modal('show'); 
    });

    $( document ).on( "click", ".delete-submit", function() {
        var user_id = $('.delete-user-id').val();
        $('#delete-confirm').modal('hide');
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>?action=delete_user_action&id='+user_id,
          data: '',
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            $('#delete-success').modal('show'); 
             
          });
    });

    jQuery('#delete-success').on('hidden.bs.modal', function () {
      location.reload();
    });

    $( document ).on( "click", ".send-details", function() {
        var user_id = $(this).data('index');
        $('.send-details-user-id').val(user_id);
        $('#send-details-confirm').modal('show'); 
    });

    
    $( document ).on( "click", ".send-details-submit", function() {
        var user_id = $('.send-details-user-id').val();
        $('#send-details-confirm').modal('hide');
        jQuery.ajax({
          type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
          url     : '<?php echo admin_url('admin-ajax.php');?>?action=send_details_user_action&id='+user_id,
          data: '',
          processData: false,
          contentType: false,
        })
        // using the done promise callback
          .done(function(result) {
            $('#send-details-success').modal('show'); 
             
          });
    });

    $( document ).on( "click", ".edit-user", function() {
        var user_id = $(this).data('index');
        var details = $(this).data('details');
        $('.edit_user_id').val(user_id);
        $('.edit_first_name_en').val(details.first_name_en);
        $('.edit_last_name_en').val(details.last_name_en);
        $('.edit_first_name_ar').val(details.first_name_ar);
        $('.edit_last_name_ar').val(details.last_name_ar);
        $('.edit_phone').val(details.phone);
        $('.edit_date_of_birth').val(details.dob);
        $('.edit_height').val(details.height);
        $('input[name=edit_gender][value="'+details.gender+'"]').prop("checked",true);
        $('.edit_company_en').val(details.company_name);
        $('.edit_company_ar').val(details.company_name_arabic);
        $('.edit_position_en').val(details.position);
        $('.edit_position_ar').val(details.position_arabic);
        $('.edit_address').val(details.address);
        $('.edit_address_ar').val(details.address_arabic);
        $('.edit_registration_date').val(details.registration_date);
        $('.edit_source_referral').val(details.source_referral);
        if(details.photo_url){
          $('.edit_image').attr('src',details.photo_url);
        }
        $('#edit-customer').modal('show');
        
    });

    var abcd = jQuery('.edit-user-form').validate({
            // ignore: ".ignore",
            // onfocusout: function(element) {$(element).valid()},
            // onkeyup: false,
            ignore: [],
            rules:
            {
                edit_first_name_en:
                {

                    required: true,  
                },
                edit_first_name_ar:
                {

                    required: true,  
                },
                edit_last_name_en:
                {

                    required: true, 
                },
                edit_last_name_ar:
                {

                    required: true, 
                },
                edit_phone:
                {

                    required: true,  
        			      digits: true,
        			      	minlength: 10,
        			maxlength: 10,
                },
                // edit_date_of_birth:
                // {
                //     required: true, 
                // },
                // edit_height:
                // {
                //     required: true, 
                //      digits: true,
                // },
                edit_gender:
                {
                    required: true, 
                },
                // edit_company_en:
                // {
                //     required: true, 
                // },
                // edit_position_en:
                // {
                //     required: true, 
                // },
                // edit_address:
                // {
                //     required: true, 
                // },
                // edit_registration_date:    
                // {
                //     required: true, 
                // },
                // edit_source_referral:    
                // {
                //     required: true, 
                // },
                // photo:
                // {
                //     filesize : 1.5
                // },
                
            },
            messages: {
                edit_phone: {
                  minlength: "<?php _e('Please enter at least 10 numbers.','sidf');?>",
                  maxlength: "<?php _e('Please enter no more than 10 numbers.','sidf');?>"
                }
            },

        });

        jQuery(".edit-user-form").submit(function(e){
            e.preventDefault();
            if(abcd.form()  && abcd.form())
            {
              $('#edit-customer').modal('hide');
                 jQuery('#loader').show();
                 jQuery('.submit-btn').attr('disabled','disabled');
                
                    var fd= new FormData(jQuery('.edit-user-form')[0]);
                    // process the form
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=admin_edit_customer_action',
                        data: fd,
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                        jQuery('#loader').hide();
                          $('#edit-user-success').modal('show');
                      


                    });
            }

            return false;
        });
        jQuery('#edit-user-success').on('hidden.bs.modal', function () {
      location.reload();
    });

    $( document ).on( "click", ".add-note", function() {
        var user_id = $(this).data('index');
        $('.note_user_id').val(user_id);
        $('#add-note').modal('show'); 
    });

    var abcde = jQuery('.add-note-form').validate({
            // ignore: ".ignore",
            // onfocusout: function(element) {$(element).valid()},
            // onkeyup: false,
            ignore: [],
            rules:
            {
                note:
                {

                    required: true,  
                }
                
            }

        });

        jQuery(".add-note-form").submit(function(e){
            e.preventDefault();
            if(abcde.form()  && abcde.form())
            {
                 $('#add-note').modal('hide');
                 jQuery('#loader').show();
                 jQuery('.submit-btn').attr('disabled','disabled');
                
                    var fd= new FormData(jQuery('.add-note-form')[0]);
                    // process the form
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=add_note_action',
                        data: fd,
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                            jQuery('#loader').hide();
                          $('#add-note-success').modal('show');
                      


                    });
            }

            return false;
        });


        jQuery(".view-note").click(function(e){
            e.preventDefault();
                
                    var user_id = $(this).data('index');
                     
                    // process the form
                    jQuery.ajax({
                        type    : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url     : '<?php echo admin_url('admin-ajax.php');?>?action=view_note_action&user_id='+user_id,
                        data: '',
                        processData: false,
                        contentType: false,
                    })
                    // using the done promise callback
                        .done(function(result) {
                            var obj = JSON.parse(result);     
                            jQuery('#view-note .content-body').html(obj.html); 
                            jQuery('#view-note').modal('show');
                      


                    });
            

            return false;
        });
</script>
