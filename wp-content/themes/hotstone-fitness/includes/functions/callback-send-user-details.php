<?php 
add_action( 'wp_ajax_send_details_user_action', 'send_details_user_callback' );
add_action( 'wp_ajax_nopriv_send_details_user_action', 'send_details_user_callback' );
function send_details_user_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $user_id = $_REQUEST['id'];   
    $password = wp_generate_password( 8, false );
    $user_id = wp_update_user( array( 'ID' => $user_id, 'user_pass' => $password ) );
    $user = get_user_by('id',$user_id);
    $email_body = '<p style="color:rgba(0,0,0,.75)">'.__('Hi','sidf').' '.$user->first_name.' '.$user->last_name.'</p>';
    $email_body .= '<p style="color:rgba(0,0,0,.75)">'.__('Below are your login details:','sidf').'</p>';
    $email_body .='<p>Username: '.$user->user_email.'<p>';
    $email_body .='<p>Password: '.$password.'<p>';
    $email_content = $email_body; 
    $headers[] = 'Content-Type: text/html; charset=UTF-8';
    wp_mail( $user->user_email, __('Hotstone Fitness: Login Details','sidf') , $email_content, $headers);

    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> 'success',
    		    );
	
	echo json_encode($result);
	exit(0);
}