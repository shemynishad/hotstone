<?php 
add_action( 'wp_ajax_view_booking_log_action', 'view_booking_log_callback' );
add_action( 'wp_ajax_nopriv_view_booking_log_action', 'view_booking_log_callback' );
function view_booking_log_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $user_id = $_REQUEST['user_id'];   
    $booking_date = get_field('booking_date',$user_id);
    $class = get_field('class',$user_id);
    $formatted_booking_date =  date("d F Y", strtotime($booking_date));
    $class_date = get_field('date',$class);
    $formatted_class_date =  date("d F Y", strtotime($class_date));
    $start_time = get_field('start_time',$class);
    $end_time = get_field('end_time',$class);
    ob_start();?>
        <p class="mb-5"><i class="fa fa-user-circle me-3" aria-hidden="true"></i>Customer Booked <?= $formatted_booking_date;?></p><hr>
        <p class="mb-5"><i class="fa fa-user-circle me-3" aria-hidden="true"></i>Class Started <?= $formatted_class_date.' '.$start_time;?></p><hr>
        <p class="mb-5"><i class="fa fa-user-circle me-3" aria-hidden="true"></i>Class Ending <?= $formatted_class_date.' '.$end_time;?></p>
   <?php $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content,
    		    );
	
	echo json_encode($result);
	exit(0);
}