<?php 
add_action( 'wp_ajax_admin_booking_customer_action', 'admin_booking_customer_callback' );
add_action( 'wp_ajax_nopriv_admin_booking_customer_action', 'admin_booking_customer_callback' );
function admin_booking_customer_callback() {
    global $wpdb;
    
    $class_id = $_REQUEST['class_id'];
    $date = $_REQUEST['date'];
    $date_formatted = date("Ymd", strtotime($date));
    $membership_id = get_field('membership',$class_id);
    ob_start();
    ?>
     <?php 
     $end_date_formatted = date("Ymd");
      $args = array(
        	'post_type' => 'subscription',
        	'posts_per_page' => -1,
        	'meta_query' => array(
        	    array(
                    'key'     => 'membership_type',
                    'compare' => '=',
                    'value'   => $membership_id,
                   ),
              array(
                    'key'     => 'start_date',
                    'compare' => '<=',
                    'value'   => $end_date_formatted,
                   ),
                array(
                    'key'     => 'end_date',
                    'compare' => '>=',
                    'value'   => $end_date_formatted,
                   ),
                array(
                    'key'     => 'options',
                    'compare' => '=',
                    'value'   => 'validity',
                   ),
                 array(
                    'key'     => 'is_disable',
                    'compare' => '!=',
                    'value'   => 1,
                   ),
        	 )
        );

        $args1 = array(
        	'post_type' => 'subscription',
        	'posts_per_page' => -1,
        	'meta_query' => array(
        	    array(
                    'key'     => 'membership_type',
                    'compare' => '=',
                    'value'   => $membership_id,
                   ),
                array(
                    'key'     => 'start_date',
                    'compare' => '<=',
                    'value'   => $end_date_formatted,
                   ),
                array(
                    'key'     => 'options',
                    'compare' => '=',
                    'value'   => 'session',
                   ),
                 array(
                    'key'     => 'is_disable',
                    'compare' => '!=',
                    'value'   => 1,
                   ),
                 array(
                    'key'     => 'remaining_sessions',
                    'compare' => '>',
                    'value'   => 0,
                   )
        	 )
        );

        $query = new WP_Query( $args );//validity option
        $query1 = new WP_Query( $args1 );//session option

        $all_customers = [];
        if($query->have_posts()):
          while($query->have_posts()): $query->the_post();
          $customer_id = get_field('customer');
          $author_obj = get_user_by('id', $customer_id);
          $customer['customer_id'] = $customer_id;
          $customer['first_name'] = $author_obj->user_firstname;
          $customer['last_name'] = $author_obj->user_lastname;

          array_push($all_customers,$customer);
        endwhile; endif;
        if($query1->have_posts()):
          while($query1->have_posts()): $query1->the_post();
          $customer_id = get_field('customer');
          $author_obj = get_user_by('id', $customer_id);
          $customer['customer_id'] = $customer_id;
          $customer['first_name'] = $author_obj->user_firstname;
          $customer['last_name'] = $author_obj->user_lastname;

          array_push($all_customers,$customer);
        endwhile; endif;

        foreach($all_customers as $k => $v) 
        {
            foreach($all_customers as $key => $value) 
            {
                if($k != $key && $v['customer_id'] == $value['customer_id'])
                {
                    unset($all_customers[$k]);
                }
            }
        }

        echo '<option value="">Choose Customer</option>';

        foreach($all_customers as $key => $val){
          $firstname = $val['first_name'];
          $lastname = $val['last_name'];
          $customer = $val['customer_id'];

          $booking = array(
            'post_type' => 'booking',
            'numberposts' => -1,
            'meta_query' => array(
               array(
                   'key'     => 'class',
                   'value'   => $class_id,
                   'compare' => '='
                  ),
                  array(
                    'key'     => 'customer',
                    'value'   => $customer,
                    'compare' => '='
                   ),
                   array(
                    'key'     => 'booking_date',
                    'value'   => $date_formatted,
                    'compare' => '='
                   ),
               
               )
           );
          $latest_booking = get_posts( $booking );
          if((count($latest_booking) == 0) && !empty($firstname)){
            echo '<option value="'.$customer.'">'.$firstname.' '.$lastname.'</option>';
          }

        }
    ?>
    
    <?php
    $content = ob_get_clean();	
	$result = Array(
		"html"		=> $content
		);
	
	echo json_encode($result);
	exit(0);
}