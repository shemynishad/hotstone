<?php 
add_action( 'wp_ajax_view_note_action', 'view_note_callback' );
add_action( 'wp_ajax_nopriv_view_note_action', 'view_note_callback' );
function view_note_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $user_id = $_REQUEST['user_id'];   
    $notes = $_REQUEST['note'];
    $notes_ar = $_REQUEST['note_ar'];
    $values = array();
    if(get_field('field_630d19ff6b820','user_'.$user_id)){
        $values = get_field('field_630d19ff6b820','user_'.$user_id);
    }
    ob_start();
    if($values):
        foreach($values as $value):?>
            <p><?= $value['note'];?></p>
            <?php if($value['note_arabic']):?>
                <p><?= $value['note_arabic'];?></p>

            <?php endif;?>
            <p>Date : <?= $value['date'];?></p>
            <hr>
        <?php endforeach;?>

    <?php else:?>
        <p>There is no any notes added</p>
    <?php endif;
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content,
    		    );
	
	echo json_encode($result);
	exit(0);
}