<?php 
add_action( 'wp_ajax_view_customer_class_action', 'view_customer_class_callback' );
add_action( 'wp_ajax_nopriv_view_customer_class_action', 'view_customer_class_callback' );
function view_customer_class_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $post_id = $_REQUEST['user_id'];
    ob_start();?>
                   <li>
                      <h5 class="title fw-medium">ID</h5>
                      <span class="text fw-normal"><?= get_field('id',$post_id);?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Date</h5>
                      <?php $date = get_field('date',$post_id);?>
                      <span class="text fw-normal"><?= date("d F Y", strtotime($date));?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Time(Start-End)</h5>
                      <span class="text fw-normal"><?= get_field('start_time',$post_id);?> - <?= get_field('end_time',$post_id);?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Duration</h5>
                       <?php $to_time = strtotime("2008-12-13 ".get_field('end_time',$post_id).":00");
                        $from_time = strtotime("2008-12-13 ".get_field('start_time',$post_id).":00");
                        $hours = round(abs($to_time - $from_time) / 3600,2);?>
                      <span class="text fw-normal"><?= $hours.' hours';?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Trainer</h5>
                      <?php $trainer_id = get_field('trainer',$post_id);
                    $author_obj = get_user_by('id', $trainer_id);?>
                    
                      <span class="text fw-normal"><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Workouts</h5>
                       <?php $workout_titles = array();
                    $workouts = explode(",",get_field('workouts',$post_id));
                    foreach($workouts as $workout):
                        $workout_titles[] = get_the_title($workout);
                    endforeach;?>
                      <span class="text fw-normal"><?= implode(",",$workout_titles);?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Class</h5>
                      <span class="text fw-normal"><?= get_field('class_title',$post_id);?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Type</h5>
                      <span class="text fw-normal"><?= get_field('name',get_field('membership',$post_id));?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Bookings</h5>
                      <?php 
                    $class_array = array(
                           'key'     => 'class',
                           'value'   => $post_id,
                           'compare' => '='
                           );
     
                  $args = array(
                    	'post_type' => 'booking',
                    	'numberposts' => -1,
                    	'meta_query' => array(
                            $class_array
                        )
                        
                    );
                    $latest_class = get_posts( $args );
                    ?>
                      <span class="text fw-normal"><?= count($latest_class);?>/<?= get_field('no_of_slots',$post_id);?></span>
                    </li>
    <?php 
    
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content,
    		    );
	
	echo json_encode($result);
	exit(0);
}