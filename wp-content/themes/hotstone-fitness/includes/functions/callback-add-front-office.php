<?php 
add_action( 'wp_ajax_admin_register_front_office_action', 'admin_register_front_office_callback' );
add_action( 'wp_ajax_nopriv_admin_register_front_office_action', 'admin_register_front_office_callback' );
function admin_register_front_office_callback() {
    global $wpdb;
    $first_name = $_REQUEST['first_name_en'];
    $last_name = $_REQUEST['last_name_en'];
    $first_name_ar = $_REQUEST['first_name_ar'];
    $last_name_ar = $_REQUEST['last_name_ar'];
    $email = $_REQUEST['email'];
    $phone = $_REQUEST['phone'];
    $password = wp_generate_password( 8, false );
         
        
        $user_id = wp_create_user( $email, $password ,$email);
     	if($user_id && $user_id != 1) {
		$user_id = wp_update_user( array( 'ID' => $user_id, 'first_name' => $first_name,'last_name' => $last_name,'display_name' => $first_name.' '.$last_name, 'role' => 'front_office_user') );
        
        update_user_meta( $user_id, 'first_name_arabic', sanitize_text_field( $first_name_ar ) );
        update_user_meta( $user_id, 'last_name_arabic', sanitize_text_field( $last_name_ar ) );
        update_user_meta( $user_id, 'phone', sanitize_text_field( $phone ) );
        update_user_meta( $user_id, 'email', sanitize_text_field( $email ) );
        update_user_meta( $user_id, 'disable', 0 );
        $next_user_id = get_field('next_front_office_id','option');
        update_user_meta( $user_id, 'user_id', 'HSU_'.sprintf("%03d", $next_user_id) );
        update_field( 'next_front_office_id', $next_user_id + 1, 'option' );

       
        
            
            $user = get_user_by('id',$user_id);
            $email_body = '<p style="color:rgba(0,0,0,.75)">'.__('Hi','sidf').' '.$user->first_name.' '.$user->last_name.'</p>';
            $email_body .= '<p style="color:rgba(0,0,0,.75)">'.__('You have successfully registered for the Hotstone Fitness with below details','sidf').'</p>';
            $email_body .='<p>Username: '.$email.'<p>';
            $email_body .='<p>Password: '.$password.'<p>';
            $email_content = $email_body; 
            $headers[] = 'Content-Type: text/html; charset=UTF-8';
            wp_mail( $user->user_email, __('Hotstone Fitness: Welcome ','sidf') , $email_content, $headers);             
            
     	} 	
        
        


    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}