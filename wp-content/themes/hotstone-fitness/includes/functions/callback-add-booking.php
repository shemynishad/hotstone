<?php 
add_action( 'wp_ajax_admin_booking_action', 'admin_booking_callback' );
add_action( 'wp_ajax_nopriv_admin_booking_action', 'admin_booking_callback' );
function admin_booking_callback() {
    global $wpdb;
    $date = $_REQUEST['date'];
    $formatted_date =  date("Ymd", strtotime($date));
    $customer = $_REQUEST['customer'];
    $class = $_REQUEST['class'];
    $current_user = wp_get_current_user();
   
        $next_group_id = get_field('next_booking_id','option');
        $my_post = array(
            'post_title'    => 'Booking-'.'HSC_'.sprintf("%03d", $next_group_id),
            'post_content'  => '',
            'post_status'   => 'publish',
            'post_author'   => 1,
            'post_type' => 'booking'
          );
           
        // Insert the post into the database
        $group_id = wp_insert_post( $my_post );
       
        update_post_meta( $group_id, 'id', 'HSC_'.sprintf("%03d", $next_group_id) );
        update_field( 'next_booking_id', $next_group_id + 1, 'option' );
        update_post_meta( $group_id, 'booking_date', sanitize_text_field( $formatted_date ) );
        update_post_meta( $group_id, 'class', sanitize_text_field( $class ) );
        update_post_meta( $group_id, 'customer', sanitize_text_field( $customer ) );
        update_post_meta( $group_id, 'created_date', date("Ymd") );
        update_post_meta( $group_id, 'created_by', $current_user->display_name );
        update_post_meta( $group_id, 'is_disable', 0 );
        

        
        


    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}