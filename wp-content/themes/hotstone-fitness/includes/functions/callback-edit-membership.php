<?php 
add_action( 'wp_ajax_admin_edit_membership_action', 'admin_edit_membership_callback' );
add_action( 'wp_ajax_nopriv_admin_edit_membership_action', 'admin_edit_membership_callback' );
function admin_edit_membership_callback() {
    global $wpdb;
    $membership_name_en = $_REQUEST['edit_membership_name_en'];
    $membership_name_ar = $_REQUEST['edit_membership_name_ar'];
    $no_of_session = $_REQUEST['edit_no_of_session'];
    $amount = $_REQUEST['edit_amount'];
    $validity = $_REQUEST['edit_validity'];
    $session_type = $_REQUEST['edit_session_type'];
    $group_id = $_REQUEST['user_id'];

    $post_data = array(
        'ID' => $group_id,
        'post_title' => sanitize_text_field( $membership_name_en ),
    );

    wp_update_post( $post_data );

         
     	update_post_meta( $group_id, 'name', sanitize_text_field( $membership_name_en ) );
        update_post_meta( $group_id, 'name_in_arabic', sanitize_text_field( $membership_name_ar ) );
        update_post_meta( $group_id, 'amount', sanitize_text_field( $amount ) );
        update_post_meta( $group_id, 'session_type', sanitize_text_field( $session_type ) );
        update_post_meta( $group_id, 'no_of_sessions', sanitize_text_field( $no_of_session ) );
        update_post_meta( $group_id, 'validity', sanitize_text_field( $validity ) );
        
        
         $image_url1=$_FILES['photo']['tmp_name'];
         if($image_url1){
             $documenten2=$_FILES['photo'];
             $upload_dir = wp_upload_dir();
             $image_data1 = file_get_contents($image_url1);
             $filename1 = $documenten2['name'];
             if(wp_mkdir_p($upload_dir['path']))
                 $file1 = $upload_dir['path'] . '/' . $filename1;
             else
                 $file1 = $upload_dir['basedir'] . '/' . $filename1;
             file_put_contents($file1, $image_data1);
             $wp_filetype = wp_check_filetype($filename1, null );
             $attachment1 = array(
                 'post_mime_type' => $wp_filetype['type'],
                 'post_title' => sanitize_file_name($filename1),
                 'post_content' => '',
                 'post_status' => 'inherit'
             );
             $attach_id1 = wp_insert_attachment( $attachment1, $file1, 0);
             $file1_url = wp_get_attachment_url( $attach_id1 );
             $path = get_attached_file( $attach_id1 );
             wp_update_attachment_metadata( $attach_id1, $metadata );
             update_post_meta( $group_id, 'photo', $attach_id1 );
         }     

    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}