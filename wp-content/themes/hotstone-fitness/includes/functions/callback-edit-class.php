<?php 
add_action( 'wp_ajax_admin_edit_class_action', 'admin_edit_class_callback' );
add_action( 'wp_ajax_nopriv_admin_edit_class_action', 'admin_edit_class_callback' );
function admin_edit_class_callback() {
    global $wpdb;
    $class_title_en = $_REQUEST['edit_class_title'];
    $class_title_ar = $_REQUEST['edit_class_title_ar'];
    $date = $_REQUEST['edit_date'];
    $formatted_date =  date("Ymd", strtotime($date));
    $start_time = $_REQUEST['edit_start_time'];
    $end_time = $_REQUEST['edit_end_time'];
    //$type = $_REQUEST['edit_membership'];
    $no_of_slots = $_REQUEST['edit_no_of_slots'];
    $trainer = $_REQUEST['edit_trainer'];
    $workouts = $_REQUEST['edit_workout'];
    $group_id = $_REQUEST['user_id'];
         
     	update_post_meta( $group_id, 'class_title', sanitize_text_field( $class_title_en ) );
        update_post_meta( $group_id, 'class_title_in_arabic', sanitize_text_field( $class_title_ar ) );
        update_post_meta( $group_id, 'date', sanitize_text_field( $formatted_date ) );
        update_post_meta( $group_id, 'start_time', sanitize_text_field( $start_time ) );
        update_post_meta( $group_id, 'end_time', sanitize_text_field( $end_time ) );
        //update_post_meta( $group_id, 'membership', sanitize_text_field( $type ) );
        update_post_meta( $group_id, 'trainer', sanitize_text_field( $trainer ) );
        update_post_meta( $group_id, 'no_of_slots', sanitize_text_field( $no_of_slots ) );
        update_post_meta( $group_id, 'workouts', implode(",",$workouts));
        

    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}