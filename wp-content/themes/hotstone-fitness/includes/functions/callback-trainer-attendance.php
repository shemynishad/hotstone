<?php 
add_action( 'wp_ajax_trainer_class_attendance_action', 'trainer_class_attendance_callback' );
add_action( 'wp_ajax_nopriv_trainer_class_attendance_action', 'trainer_class_attendance_callback' );
function trainer_class_attendance_callback() {
    global $wpdb;
    $booking_ids = $_REQUEST['booking_ids'];
    foreach($booking_ids as $booking_id):
        $customer_id = get_field('customer',$booking_id);
        update_field('attendance_taken',1,$booking_id);
                  $end_date_formatted = date("Ymd");
                  $args = array(
                    	'post_type' => 'subscription',
                    	'posts_per_page' => 1,
                    	'meta_query' => array(
                    		array(
                    			'key'     => 'customer',
                    			'value'   => $customer_id,
                    			'compare' => '=',
                    		),
                    		array(
                    			'key'     => 'is_disable',
                    			'value'   => 1,
                    			'compare' => '!=',
                    		),
                    		array(
                                'key'     => 'end_date',
                                'compare' => '>=',
                                'value'   => $end_date_formatted,
                            ),
                             array(
                                'key'     => 'remaining_sessions',
                                'compare' => '>=',
                                'value'   => 0,
                               )
                    	),
                    );
                     $query = new WP_Query( $args );
                      if($query->have_posts()):
                      while($query->have_posts()): $query->the_post();
                            $remaining_sessions = get_field('remaining_sessions');
                            update_field('remaining_sessions',intval($remaining_sessions) - 1,$query->post->ID);
                      endwhile;
                      endif;
    endforeach;
	$result = Array(
		"html"		=> ''
		);
	
	echo json_encode($result);
	exit(0);
}