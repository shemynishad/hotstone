<?php 
add_action( 'wp_ajax_edit_customer_password_action', 'edit_customer_password_callback' );
add_action( 'wp_ajax_nopriv_edit_customer_password_action', 'edit_customer_password_callback' );
function edit_customer_password_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $password = $_REQUEST['password'];
    $user_id = $current_user->ID;
         
     	if($user_id && $user_id != 1) {
		$user_id = wp_update_user( array( 'ID' => $user_id, 'user_pass' => $password) );
     	} 	
        
        

    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}