<?php 
add_action( 'wp_ajax_admin_enable_disable_action', 'admin_enable_disable_callback' );
add_action( 'wp_ajax_nopriv_admin_enable_disable_action', 'admin_enable_disable_callback' );
function admin_enable_disable_callback() {
    global $wpdb;
    $user_id = $_REQUEST['user_id'];
    $disable = $_REQUEST['disable'];
    update_user_meta( $user_id, 'disable', $disable );
    $content = ob_get_clean();	
	$result = Array(
		"html"		=> $content,
		"date"    =>  $formatted_date
		);
	
	echo json_encode($result);
	exit(0);
}