<?php 
add_action( 'wp_ajax_view_trainer_measurement_action', 'view_trainer_measurement_callback' );
add_action( 'wp_ajax_nopriv_view_trainer_measurement_action', 'view_trainer_measurement_callback' );
function view_trainer_measurement_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $post_id = $_REQUEST['user_id'];   
    $start_date = get_field('start_date',$post_id);
    $end_date = get_field('end_date',$post_id);
    $customer_id = get_field('customer',$post_id);
    
    $formatted_start_date =  date("d F Y", strtotime($start_date));
    $formatted_end_date =  date("d F Y", strtotime($end_date));
    ob_start();?>
                   <li>
                      <h5 class="title fw-medium">ID</h5>
                      <span class="text fw-normal"><?= get_field('id',$post_id);?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Customer</h5>
                      <?php $customer_id = get_field('customer',$post_id);
                    $author_obj = get_user_by('id', $customer_id);?>
                      <span class="text fw-normal"><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Fasting</h5>
                      <span class="text fw-normal"><?= get_field('fasting',$post_id);?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Height</h5>
                      <span class="text fw-normal"><?= get_field('height',$post_id);?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Weight</h5>
                      <span class="text fw-normal"><?= get_field('weight',$post_id);?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Performance Index</h5>
                      <span class="text fw-normal"><?= get_field('performance_index',$post_id);?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">SMM</h5>
                      <span class="text fw-normal"><?= get_field('smm',$post_id);?></span>
                    </li>
    <?php 
    
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content,
    		    );
	
	echo json_encode($result);
	exit(0);
}