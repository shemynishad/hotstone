<?php 
add_action( 'wp_ajax_trainer_filter_class_action', 'trainer_filter_class_callback' );
add_action( 'wp_ajax_nopriv_trainer_filter_class_action', 'trainer_filter_class_callback' );
function trainer_filter_class_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    
    $membership = '';
    if(isset($_POST['membership'])){
        $membership = $_POST['membership'];
    }

    $start_date = '';
    if(isset($_POST['start_date'])){
        $start_date = $_POST['start_date'];
    }

    $end_date = '';
    if(isset($_POST['end_date'])){
        $end_date = $_POST['end_date'];
    }

    $paged = 1;
    if(isset($_REQUEST['page'])):
    $paged = $_REQUEST['page'];
    endif;
    ob_start();?>

<?php 
$type_array = array();
if($membership){
  $type_array = array(
    'key'     => 'membership',
    'compare' => '=',
    'value'   => $membership,
   );
}


$start_date_array = array();
if($start_date){
  $start_date_formatted = date("Ymd", strtotime($start_date));
  $start_date_array = array(
    'key'     => 'date',
    'compare' => '>=',
    'value'   => $start_date_formatted,
   );
} else {
    
    $today = date('Ymd');
    $start_date_array = array(
    'key'     => 'date',
    'compare' => '>=',
    'value'   => $today,
   );
}

$end_date_array = array();
if($end_date){
  $end_date_formatted = date("Ymd", strtotime($end_date));
  $end_date_array = array(
    'key'     => 'date',
    'compare' => '<=',
    'value'   => $end_date_formatted,
   );
}


                  $today = date('Ymd');
                  $args = array(
                    	'post_type' => 'class',
                    	'posts_per_page' => -1,
                    	'meta_query' => array(
                            $type_array,
                            $start_date_array,
                            $end_date_array,
                    		array(
                    			'key'     => 'trainer',
                    			'value'   => $current_user->ID,
                    			'compare' => '=',
                    		)
                        )
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
            <ul class="list1 four-cols">
                <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
              <li>
                <div class="card1">
                  <div class="inner-wrapper">
                    <ul class="details-list mb-0">
                      <li>
                            <div class="d-flex">
                              <span class="title">ID:</span>
                              <span class="text"><?= get_field('id');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Date:</span>
                              <?php $date = get_field('date');?>
                              <span class="text"><?= date("d F Y", strtotime($date));?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Time:</span>
                              <span class="text"><?= get_field('start_time');?> - <?= get_field('end_time');?></span>
                            </div>
                          </li>
                      <li>
                            <div class="d-flex">
                              <span class="title">Workouts:</span>
                              <?php $workout_titles = array();
                    $workouts = explode(",",get_field('workouts'));
                    foreach($workouts as $workout):
                         $workout_titles[] = get_field('name',$workout);
                    endforeach;?>
                              <span class="text"><?= implode(",",$workout_titles);?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Class:</span>
                              <span class="text"><?= get_field('class_title');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Type:</span>
                              <span class="text"><?= get_field('name',get_field('membership'));?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Bookings:</span>
                              <?php 
                    $class_array = array(
                           'key'     => 'class',
                           'value'   => $query->post->ID,
                           'compare' => '='
                           );
     
                  $args = array(
                    	'post_type' => 'booking',
                    	'numberposts' => -1,
                    	'meta_query' => array(
                            $class_array
                        )
                        
                    );
                    $latest_class = get_posts( $args );
                    ?>
                              <span class="text"><?= count($latest_class);?>/<?= get_field('no_of_slots');?></span>
                            </div>
                          </li>
                    </ul>
                    <div class="row align-items-center options">
                        <?php 
                        $formatted_date =  date("m/d/Y", strtotime($date));
                         $data = array(   
                            'class_title' => get_field('class_title'),
                            'class_title_ar' => get_field('class_title_in_arabic'),
                            'date' => $formatted_date,
                            'start_time' => get_field('start_time'),
                            'end_time' => get_field('end_time'),
                            'type' => get_field('membership'),
                            'no_of_slots' => get_field('no_of_slots'),
                            'trainer' => get_field('trainer'),
                            'workouts' =>get_field('workouts')
                        );?>
                      <div class="col-auto">
                        <button type="button" class="edit-user" data-index="<?= $query->post->ID;?>" data-details='<?php echo json_encode($data);?>'><i class="circle-icon1"><img src="<?= get_template_directory_uri();?>/assets/images/edit.svg" alt="icon"></i></button>
                      </div>
                      <div class="col-auto">
                        <button type="button" class="delete-user" data-index="<?= $query->post->ID;?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri();?>/assets/images/delete.svg" alt="icon"></i></button>
                      </div>
                      <div class="col-auto">
                        <a href="<?php the_permalink();?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri();?>/assets/images/eye-icon.svg" alt="icon"></i></a>
                      </div>
                      <?php $disable = get_field('is_disable');?>
                      <div class="col text-md-end">
                        <label class="toggle1 alt">
                          <input type="checkbox" class="disable-user" <?php if(!$disable){ echo 'checked';}?> value="<?= $query->post->ID;?>">
                          <span class="slider"></span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <?php endwhile;?>
            </ul>
            <?php else:?>
                <div class="alert alert-info" role="alert"><?php _e('No class found matching the search criteria. Please try again.');?> </div>
            <?php endif;?>

    <?php
    $content = ob_get_clean();	
	$result = Array(
		"html"		=> $content,
		"date"    =>  $formatted_date
		);
	
	echo json_encode($result);
	exit(0);
}