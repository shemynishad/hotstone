<?php 
add_action( 'wp_ajax_customer_filter_class_action', 'customer_filter_class_callback' );
add_action( 'wp_ajax_nopriv_customer_filter_class_action', 'customer_filter_class_callback' );
function customer_filter_class_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    
    $membership = '';
    if(isset($_POST['membership'])){
        $membership = $_POST['membership'];
    }
    
    $keyword = '';
    if(isset($_POST['keyword'])){
        $keyword = $_POST['keyword'];
    }

    $start_date = '';
    if(isset($_POST['start_date'])){
        $start_date = $_POST['start_date'];
    }

    $end_date = '';
    if(isset($_POST['end_date'])){
        $end_date = $_POST['end_date'];
    }

    $paged = 1;
    if(isset($_REQUEST['page'])):
    $paged = $_REQUEST['page'];
    endif;
    ob_start();?>

<?php 
$type_array = array();
if($membership){
  $type_array = array(
    'key'     => 'membership',
    'compare' => '=',
    'value'   => $membership,
   );
}


$start_date_array = array();
if($start_date){
  $start_date_formatted = date("Ymd", strtotime($start_date));
  $start_date_array = array(
    'key'     => 'date',
    'compare' => '>=',
    'value'   => $start_date_formatted,
   );
} else {
    
    $today = date('Ymd');
    $start_date_array = array(
    'key'     => 'date',
    'compare' => '>=',
    'value'   => $today,
   );
}

$end_date_array = array();
if($end_date){
  $end_date_formatted = date("Ymd", strtotime($end_date));
  $end_date_array = array(
    'key'     => 'date',
    'compare' => '<=',
    'value'   => $end_date_formatted,
   );
}

$keyword_array = array();
if($keyword){
  $keyword_array = array(
      'relation' => 'OR',
        array(
        'key'     => 'first_name',
        'value'   => $keyword,
        'compare' => 'LIKE'
        ),
        array(
            'key'     => 'last_name',
            'value'   => $keyword,
            'compare' => 'LIKE'
        ),
        array(
            'key'     => 'first_name_arabic',
            'value'   => $keyword,
            'compare' => 'LIKE'
        ),
        array(
            'key'     => 'last_name_arabic',
            'value'   => $keyword,
            'compare' => 'LIKE'
        )
    );
}



                    $team_args = array( 
                      'role__in' => array( 'trainer'), 
                      'number' =>999999, 
                      
                      'meta_query' => array(
                                $keyword_array,
                            
                        )
                    );
                  
                  $team_presidents = get_users( $team_args );
                  if($team_presidents):
                      $team_presidents_array  =  array();
                      foreach($team_presidents as $team_president):
                          $team_presidents_array[]=  $team_president->ID;
                      endforeach;
                      if($team_presidents_array):
                       $team_array = array(
                           'key'     => 'trainer',
                           'value'   => $team_presidents_array,
                           'compare' => 'IN'
                           );

                $class_id_array = array();
                  $today = date('Ymd');
                  $args = array(
                    	'post_type' => 'booking',
                    	'posts_per_page' => -1,
                    	'meta_query' => array(
                             array(
                                'key'     => 'booking_date',
                                'compare' => '>=',
                                'value'   => $today,
                            ),
                            array(
                    			'key'     => 'customer',
                    			'value'   => $current_user->ID,
                    			'compare' => '=',
                    		),
                    		array(
                    			'key'     => 'is_disable',
                    			'value'   => 1,
                    			'compare' => '!=',
                    		)
                        ),
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                      while($query->have_posts()): $query->the_post();
                          $class = get_field('class');
                          $class_id_array[] = $class;
                      endwhile;
                  endif;
     
                  $args = array(
                    	'post_type' => 'class',
                    	'posts_per_page' => -1,
                    	'post__not_in' => $class_id_array,
                    	'meta_query' => array(
                            $type_array,
                            $start_date_array,
                            $end_date_array,
                            $team_array,
                    		array(
                    			'key'     => 'is_disable',
                    			'value'   => 1,
                    			'compare' => '!=',
                    		)
                        )
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
            <ul class="list1">
                <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
                  <li>
                    <div class="card1">
                      <div class="inner-wrapper">
                        <ul class="details-list mb-0">
                          <li>
                            <div class="d-flex">
                              <span class="title">ID:</span>
                              <span class="text"><?= get_field('id');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Date:</span>
                              <?php $date = get_field('date');?>
                              <span class="text"><?= date("d F Y", strtotime($date));?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Time:</span>
                              <span class="text"><?= get_field('start_time');?> - <?= get_field('end_time');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Workouts:</span>
                              <?php $workout_titles = array();
                    $workouts = explode(",",get_field('workouts'));
                    foreach($workouts as $workout):
                        $workout_titles[] = '<a href="#" class="workout-show" data-name="'.get_field('name', $workout).'" data-id="'.get_field('id', $workout).'" data-description="'.get_field('description', $workout).'" data-video="'.get_field('video_url', $workout).'">'.get_field('name',$workout).'</a>';
                    endforeach;?>
                              <span class="text"><?= implode(",",$workout_titles);?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Trainer</span>
                              <?php $trainer_id = get_field('trainer');
                    $author_obj = get_user_by('id', $trainer_id);?>
                              <span class="text"><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Class:</span>
                              <span class="text"><?= get_field('class_title');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Type:</span>
                              <span class="text"><?= get_field('type');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Bookings:</span>
                              <?php 
                    $class_array = array(
                           'key'     => 'class',
                           'value'   => $query->post->ID,
                           'compare' => '='
                           );
     
                  $args = array(
                    	'post_type' => 'booking',
                    	'numberposts' => -1,
                    	'meta_query' => array(
                            $class_array
                        )
                        
                    );
                    $latest_class = get_posts( $args );
                    ?>
                              <span class="text"><?= count($latest_class);?>/<?= get_field('no_of_slots');?></span>
                            </div>
                          </li>
                        </ul>
                        <div class="row align-items-center">
                        <div class="col-auto">
                            <button type="button" class="view-class" data-index="<?= $query->post->ID;?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri();?>/assets/images/eye-icon.svg" alt="icon"></i></button>
                          </div>
                          <div class="col-auto">
                            <button type="button" data-index="<?= $query->post->ID;?>" class="btn button5 px-md-4 py-md-3 p-2 btn-lg text-normal book-class">Join</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <?php endwhile;?>
                </ul>
            <?php else:?>
                <div class="alert alert-info" role="alert"><?php _e('No class found matching the search criteria. Please try again.');?> </div>
            <?php endif;?>
            <?php else:?>
                <div class="alert alert-info" role="alert"><?php _e('No class found matching the search criteria. Please try again.');?> </div>
            <?php endif;?>
             <?php else:?>
                <div class="alert alert-info" role="alert"><?php _e('No class found matching the search criteria. Please try again.');?> </div>
            <?php endif;?>

    <?php
    $content = ob_get_clean();	
	$result = Array(
		"html"		=> $content,
		"date"    =>  $formatted_date
		);
	
	echo json_encode($result);
	exit(0);
}