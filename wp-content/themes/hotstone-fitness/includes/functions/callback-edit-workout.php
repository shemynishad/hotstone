<?php 
add_action( 'wp_ajax_admin_edit_workout_action', 'admin_edit_workout_callback' );
add_action( 'wp_ajax_nopriv_admin_edit_workout_action', 'admin_edit_workout_callback' );
function admin_edit_workout_callback() {
    global $wpdb;
     $workout_name = $_REQUEST['edit_workout_name'];
    $workout_name_ar = $_REQUEST['edit_workout_name_ar'];
    $description = $_REQUEST['edit_description'];
    $description_ar = $_REQUEST['edit_description_ar'];
    $video_url = $_REQUEST['edit_video_url'];
    $group_id = $_REQUEST['user_id'];
         
     	update_post_meta( $group_id, 'name', sanitize_text_field( $workout_name ) );
        update_post_meta( $group_id, 'name_in_arabic', sanitize_text_field( $workout_name_ar ) );
        update_post_meta( $group_id, 'description', sanitize_textarea_field( $description ) );
        update_post_meta( $group_id, 'description_in_arabic', sanitize_textarea_field( $description_ar ) );
        update_post_meta( $group_id, 'video_url', sanitize_text_field( $video_url ) );
        
        
            

    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}