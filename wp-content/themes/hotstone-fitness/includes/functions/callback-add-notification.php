<?php 
add_action( 'wp_ajax_admin_notification_action', 'admin_notification_callback' );
add_action( 'wp_ajax_nopriv_admin_notification_action', 'admin_notification_callback' );
function admin_notification_callback() {
    global $wpdb;
    $subject = $_REQUEST['subject'];
    $message = $_REQUEST['message'];
    $message_ar = $_REQUEST['message_ar'];
    $date =  date("Ymd");
    $customer = $_REQUEST['customer'];
   
        $next_group_id = get_field('next_notification_id','option');
        $my_post = array(
            'post_title'    => 'Notification-'.'HSC_'.sprintf("%03d", $next_group_id),
            'post_content'  => '',
            'post_status'   => 'publish',
            'post_author'   => 1,
            'post_type' => 'notification'
          );
           
        // Insert the post into the database
        $group_id = wp_insert_post( $my_post );
       
        update_post_meta( $group_id, 'id', 'HSM_'.sprintf("%03d", $next_group_id) );
        update_field( 'next_notification_id', $next_group_id + 1, 'option' );
        update_post_meta( $group_id, 'date', sanitize_text_field( $date ) );
        update_post_meta( $group_id, 'subject', sanitize_text_field( $subject ) );
        update_post_meta( $group_id, 'customer', sanitize_text_field( $customer ) );
        update_post_meta( $group_id, 'message', sanitize_textarea_field( $message ) );
        update_post_meta( $group_id, 'message_in_arabic', sanitize_textarea_field( $message_ar ) );
        update_post_meta( $group_id, 'type', 'Manual');

        
        $image_url1=$_FILES['attachment']['tmp_name'];
        if($image_url1){
            $documenten2=$_FILES['attachment'];
            $upload_dir = wp_upload_dir();
            $image_data1 = file_get_contents($image_url1);
            $filename1 = $documenten2['name'];
            if(wp_mkdir_p($upload_dir['path']))
                $file1 = $upload_dir['path'] . '/' . $filename1;
            else
                $file1 = $upload_dir['basedir'] . '/' . $filename1;
            file_put_contents($file1, $image_data1);
            $wp_filetype = wp_check_filetype($filename1, null );
            $attachment1 = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => sanitize_file_name($filename1),
                'post_content' => '',
                'post_status' => 'inherit'
            );
            $attach_id1 = wp_insert_attachment( $attachment1, $file1, 0);
            $file1_url = wp_get_attachment_url( $attach_id1 );
            $path = get_attached_file( $attach_id1 );
            wp_update_attachment_metadata( $attach_id1, $metadata );
            update_post_meta( $group_id, 'attachment', $attach_id1 );
        }  
        

        
        


    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}