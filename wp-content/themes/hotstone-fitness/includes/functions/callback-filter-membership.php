<?php 
add_action( 'wp_ajax_admin_filter_membership_action', 'admin_filter_membership_callback' );
add_action( 'wp_ajax_nopriv_admin_filter_membership_action', 'admin_filter_membership_callback' );
function admin_filter_membership_callback() {
    global $wpdb;
    

    $paged = 1;
    if(isset($_REQUEST['page'])):
    $paged = $_REQUEST['page'];
    endif;
    ob_start();?>

<?php 
                  $args = array(
                    	'post_type' => 'membership_types',
                    	'posts_per_page' => 8,
                    	'paged' => $paged
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
            <div class="table1 mb-4">
              <table class="table mb-0">
                <thead>
                  <tr>
                    <td>ID</td>
                    <td>Name</td>                    
                    <td>Amount</td>                    
                    <td>Session</td>  
                    <td>No. of Session</td>  
                    <td>Validity (In Days)</td>  
                    <td></td>                    
                  </tr>
                </thead>
                <tbody>
                <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
                  <tr>
                    <td><?= get_field('id');?></td>
                    <td><?= get_field('name');?></td>
                    <td><?= get_field('amount');?> SAR</td>
                    <td><?= get_field('session_type');?></td>
                    <td><?= get_field('no_of_sessions');?></td>
                    <td><?= get_field('validity');?></td>
                    <td>
                      <ul class="user-options mb-0">
                      <?php $disable = get_field('is_disable');?>
                        <li>
                          <label class="toggle1">
                            <input type="checkbox" class="disable-user" <?php if(!$disable){ echo 'checked';}?> value="<?= $query->post->ID;?>">
                            <span class="slider"></span>
                          </label>
                        </li>
                        <?php 
                         $photo_url = '';
                         if(get_field('photo', $query->post->ID)){
                            $photo_id = get_field('photo', $query->post->ID);
                            $photo_url = wp_get_attachment_url($photo_id);
                         }
                         $data = array(   
                              'membership_name_en' => get_field('name', $query->post->ID),
                              'membership_name_ar' => get_field('name_in_arabic', $query->post->ID),
                              'no_of_session' => get_field('no_of_sessions', $query->post->ID),
                              'amount' => get_field('amount', $query->post->ID),
                              'validity' => get_field('validity', $query->post->ID),
                              'session_type' => get_field('session_type', $query->post->ID),
                              'photo_url' => $photo_url,
                              
                        );?>
                        <li>
                          <button type="button" class="option edit-user" data-index="<?= $query->post->ID;?>" data-details='<?php echo json_encode($data);?>'><img src="<?= get_template_directory_uri();?>/assets/images/pencil.svg" alt="icon"></button>
                        </li>
                        <li>
                          <button type="button" class="option delete-user" data-index="<?= $query->post->ID;?>"><img src="<?= get_template_directory_uri();?>/assets/images/bin.svg" alt="icon"></button>
                        </li>
                      </ul>
                    </td>
                  </tr>
                  <?php endwhile;?>
                </tbody>
              </table>
            </div>
            <?php
            $pages = paginate_links( array(
                'base' => '%_%',
                'format' => '?page=%#%',
                'current' => max( 1, get_query_var('paged')  ),
                'total' => $query->max_num_pages,
                'end_size' => 1,
                'type'  => 'array',
                'mid_size' => 2,
                'next_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                'prev_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ) );
            ?>            
            <?php if( is_array( $pages ) ) :?>
            <ul class="pagination justify-content-center justify-content-lg-end">
            <?php foreach ( $pages as $page ):?>
                        <li>
                            <?php echo $page;?>
                        </li>
                        <?php endforeach;?>
                    </ul>
            <?php endif;?>
            <?php else:?>
                <p>No Membership Types Found</p>
            <?php endif;?>

    <?php
    $content = ob_get_clean();	
	$result = Array(
		"html"		=> $content,
		"date"    =>  $formatted_date
		);
	
	echo json_encode($result);
	exit(0);
}