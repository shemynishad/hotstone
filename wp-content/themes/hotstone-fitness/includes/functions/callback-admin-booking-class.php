<?php 
add_action( 'wp_ajax_admin_booking_class_action', 'admin_booking_class_callback' );
add_action( 'wp_ajax_nopriv_admin_booking_class_action', 'admin_booking_class_callback' );
function admin_booking_class_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $customer_role = $current_user->roles[0];
    $date = $_REQUEST['date'];
    $date_formatted = date("Ymd", strtotime($date));

    ob_start();
    if( $customer_role == 'trainer'){
        $meta_arg =array(
            'key'     => 'trainer',
            'value'   => $current_user->ID,
            'compare' => '=',
        ); 
    }else{
       $meta_arg = '';
    }
    ?>
     <?php 
      $args = array(
        	'post_type' => 'class',
        	'posts_per_page' => -1,
        	'meta_query' => array(
        	    array(
                    'key'     => 'date',
                    'compare' => '=',
                    'value'   => $date_formatted,
                   ),
                $meta_arg
        	 )
        );


        $query = new WP_Query( $args );
        echo '<option value="">Choose Class</option>';
      if($query->have_posts()):
            while($query->have_posts()): $query->the_post();
            $id = get_field('id');
            $class_title = get_field('class_title');
            $class_array = array(
                'key'     => 'class',
                'value'   => $query->post->ID,
                'compare' => '='
                );
                $args1 = array(
                    'post_type' => 'booking',
                    'numberposts' => -1,
                    'meta_query' => array(
                        $class_array
                    )
                    
                );
            $latest_class = get_posts( $args1 );
            if(get_field('no_of_slots') > count($latest_class)):
            echo '<option value="'.$query->post->ID.'">'.$id.' ( '.$class_title.' )</option>';
        endif;
       endwhile; endif;?>
    <?php
    $content = ob_get_clean();	
	$result = Array(
		"html"		=> $content
		);
	
	echo json_encode($result);
	exit(0);
}