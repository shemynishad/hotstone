<?php 
add_action( 'wp_ajax_admin_edit_measurement_action', 'admin_edit_measurement_callback' );
add_action( 'wp_ajax_nopriv_admin_edit_measurement_action', 'admin_edit_measurement_callback' );
function admin_edit_measurement_callback() {
    global $wpdb;
    $trainer = $_REQUEST['edit_trainer'];
    $customer = $_REQUEST['edit_customer'];
    $fasting = $_REQUEST['edit_fasting'];
    $height = $_REQUEST['edit_height'];
    $weight = $_REQUEST['edit_weight'];
    $performance_index = $_REQUEST['edit_performance_index'];
    $smm = $_REQUEST['edit_smm'];
    $note = $_REQUEST['edit_note'];
    $note_ar = $_REQUEST['edit_note_ar'];
    $group_id = $_REQUEST['user_id'];
         
     	update_post_meta( $group_id, 'customer', $customer );
        update_post_meta( $group_id, 'trainer', $trainer );
        update_post_meta( $group_id, 'fasting', $fasting );
        update_post_meta( $group_id, 'height', sanitize_text_field( $height ) );
        update_post_meta( $group_id, 'weight', sanitize_text_field( $weight ) );
        update_post_meta( $group_id, 'performance_index', sanitize_text_field( $performance_index ) );
        update_post_meta( $group_id, 'smm', sanitize_text_field( $smm ) );
        update_post_meta( $group_id, 'notes', sanitize_textarea_field( $note ) );
        update_post_meta( $group_id, 'notes_in_arabic', sanitize_textarea_field( $note_ar ) );
        
        
              

    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}