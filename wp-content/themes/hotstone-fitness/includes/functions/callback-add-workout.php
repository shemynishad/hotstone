<?php 
add_action( 'wp_ajax_admin_add_workout_action', 'admin_add_workout_callback' );
add_action( 'wp_ajax_nopriv_admin_add_workout_action', 'admin_add_workout_callback' );
function admin_add_workout_callback() {
    global $wpdb;
    $workout_name = $_REQUEST['workout_name'];
    $workout_name_ar = $_REQUEST['workout_name_ar'];
    $description = $_REQUEST['description'];
    $description_ar = $_REQUEST['description_ar'];
    $video_url = $_REQUEST['video_url'];
    
    
        $my_post = array(
            'post_title'    => wp_strip_all_tags( $workout_name ),
            'post_content'  => '',
            'post_status'   => 'publish',
            'post_author'   => 1,
            'post_type' => 'workout'
          );
           
        // Insert the post into the database
        $group_id = wp_insert_post( $my_post );
        $next_group_id = get_field('next_workout_id','option');
        update_post_meta( $group_id, 'id', 'HSW_'.sprintf("%03d", $next_group_id) );
        update_field( 'next_workout_id', $next_group_id + 1, 'option' );
        update_post_meta( $group_id, 'name', sanitize_text_field( $workout_name ) );
        update_post_meta( $group_id, 'name_in_arabic', sanitize_text_field( $workout_name_ar ) );
        update_post_meta( $group_id, 'description', sanitize_textarea_field( $description ) );
        update_post_meta( $group_id, 'description_in_arabic', sanitize_textarea_field( $description_ar ) );
        update_post_meta( $group_id, 'video_url', sanitize_text_field( $video_url ) );
        update_post_meta( $group_id, 'is_disable', 0 );
        

            
        
        


    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}