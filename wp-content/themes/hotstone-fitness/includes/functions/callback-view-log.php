<?php 
add_action( 'wp_ajax_view_log_action', 'view_log_callback' );
add_action( 'wp_ajax_nopriv_view_log_action', 'view_log_callback' );
function view_log_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $post_id = $_REQUEST['user_id'];   
    $start_date = get_field('start_date',$post_id);
    $end_date = get_field('end_date',$post_id);
    $customer_id = get_field('customer',$post_id);
    
    $formatted_start_date =  date("d F Y", strtotime($start_date));
    $formatted_end_date =  date("d F Y", strtotime($end_date));
    $udata = get_userdata( $customer_id );
    $registered = $udata->user_registered;
    $formatted_registered = date("d F Y", strtotime($registered));
    ob_start();?>
        <p class="mb-5"><i class="fa fa-user-circle me-3" aria-hidden="true"></i>Customer Created <?= $formatted_registered;?></p><hr>
        <p class="mb-5"><i class="fa fa-user-circle me-3" aria-hidden="true"></i>Membership Assigned <?= $formatted_start_date;?></p><hr>
        <p class="mb-5"><i class="fa fa-user-circle me-3" aria-hidden="true"></i>Membership Ending <?= $formatted_end_date;?></p>
    <?php 
    
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content,
    		    );
	
	echo json_encode($result);
	exit(0);
}