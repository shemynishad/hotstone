<?php 
add_action( 'wp_ajax_trainer_filter_measurement_action', 'trainer_filter_measurement_callback' );
add_action( 'wp_ajax_nopriv_trainer_filter_measurement_action', 'trainer_filter_measurement_callback' );
function trainer_filter_measurement_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $trainer_keyword = '';
    if(isset($_POST['trainer_keyword'])){
        $trainer_keyword = $_POST['trainer_keyword'];
    }


    $start_date = '';
    if(isset($_POST['start_date'])){
        $start_date = $_POST['start_date'];
    }

    $end_date = '';
    if(isset($_POST['end_date'])){
        $end_date = $_POST['end_date'];
    }

    $paged = 1;
    if(isset($_REQUEST['page'])):
    $paged = $_REQUEST['page'];
    endif;
    ob_start();?>

<?php 


$start_date_array = array();
if($start_date){
  $start_date_formatted = date("Ymd", strtotime($start_date));
  $start_date_array = array(
    'key'     => 'date',
    'compare' => '>=',
    'value'   => $start_date_formatted,
   );
}

$end_date_array = array();
if($end_date){
  $end_date_formatted = date("Ymd", strtotime($end_date));
  $end_date_array = array(
    'key'     => 'date',
    'compare' => '<=',
    'value'   => $end_date_formatted,
   );
}


$trainer_keyword_array = array();
if($trainer_keyword){
  $trainer_keyword_array = array(
      'relation' => 'OR',
        array(
        'key'     => 'first_name',
        'value'   => $trainer_keyword,
        'compare' => 'LIKE'
        ),
        array(
            'key'     => 'last_name',
            'value'   => $trainer_keyword,
            'compare' => 'LIKE'
        ),
        array(
            'key'     => 'first_name_arabic',
            'value'   => $trainer_keyword,
            'compare' => 'LIKE'
        ),
        array(
            'key'     => 'last_name_arabic',
            'value'   => $trainer_keyword,
            'compare' => 'LIKE'
        )
    );
}



                    
                    $trainer_team_args = array( 
                      'role__in' => array( 'customer'), 
                      'number' =>999999, 
                      
                      'meta_query' => array(
                                $trainer_keyword_array,
                            
                        )
                    );
                  
                  $team_trainer_presidents = get_users( $trainer_team_args );
                  if($team_trainer_presidents):
                      
                      $team_trainer_presidents_array  =  array();
                      foreach($team_trainer_presidents as $team_trainer_president):
                          $team_trainer_presidents_array[]=  $team_trainer_president->ID;
                      endforeach;
                      if($team_trainer_presidents_array):
                       
                        $team_trainer_array = array(
                           'key'     => 'customer',
                           'value'   => $team_trainer_presidents_array,
                           'compare' => 'IN'
                           );


     
                  $args = array(
                    	'post_type' => 'measurement',
                    	'posts_per_page' => -1,
                    	'meta_query' => array(
                            $start_date_array,
                            $end_date_array,
                            $team_trainer_array,
                            array(
                    			'key'     => 'trainer',
                    			'value'   => $current_user->ID,
                    			'compare' => '=',
                    		),
                        )
                        
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
            <ul class="list1">
                <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
                <li>
                    <div class="card1">
                      <div class="inner-wrapper">
                        <ul class="details-list mb-0">
                          <li>
                            <div class="d-flex">
                              <span class="title">ID:</span>
                              <span class="text"><?= get_field('id');?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Date:</span>
                               <?php $date = get_field('date');?>
                              <span class="text"><?= date("d F Y", strtotime($date));?></span>
                            </div>
                          </li>
                          <li>
                            <div class="d-flex">
                              <span class="title">Customer</span>
                              <?php $trainer_id = get_field('customer');
                    $author_obj = get_user_by('id', $trainer_id);?>
                              <span class="text"><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></span>
                            </div>
                          </li>
                        </ul>
                        <div class="row align-items-center options">
                       <?php 
                         $data = array(   
                            'customer' => $customer_id,
                            'trainer' => $trainer_id,
                            'fasting' => get_field('fasting'),
                            'height'  => get_field('height'),
                            'weight'  => get_field('weight'),
                            'performance_index'  => get_field('performance_index'),
                            'smm'  => get_field('smm'),
                            'notes'  => get_field('notes'),
                            'notes_ar'  => get_field('notes_in_arabic'),
                        );?>
                      <div class="col-auto">
                        <button type="button" class="edit-user" data-index="<?= $query->post->ID;?>" data-details='<?php echo json_encode($data);?>'><i class="circle-icon1"><img src="<?= get_template_directory_uri();?>/assets/images/edit.svg" alt="icon"></i></button>
                      </div>
                      <div class="col-auto">
                        <button type="button" class="delete-user" data-index="<?= $query->post->ID;?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri();?>/assets/images/delete.svg" alt="icon"></i></button>
                      </div>
                      <div class="col-auto">
                        <button type="button" class="view-measurement" data-index="<?= $query->post->ID;?>"><i class="circle-icon1"><img src="<?= get_template_directory_uri();?>/assets/images/eye-icon.svg" alt="icon"></i></button>
                      </div>
                      <?php $disable = get_field('is_disable');?>
                      <div class="col text-md-end">
                        <label class="toggle1 alt">
                          <input type="checkbox" class="disable-user" <?php if(!$disable){ echo 'checked';}?> value="<?= $query->post->ID;?>">
                          <span class="slider"></span>
                        </label>
                      </div>
                    </div>
                      </div>
                    </div>
                  </li>
                  <?php endwhile;?>
                </ul>
            <?php else:?>
                <p>No Measurement Found</p>
            <?php endif;?>
            <?php else:?>
                <p>No Measurement Found</p>
            <?php endif;?>
             <?php else:?>
                <p>No Measurement Found</p>
            <?php endif;?>

    <?php
    $content = ob_get_clean();	
	$result = Array(
		"html"		=> $content,
		"date"    =>  $formatted_date
		);
	
	echo json_encode($result);
	exit(0);
}