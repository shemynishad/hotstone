<?php 
add_action( 'wp_ajax_view_customer_subscription_action', 'view_customer_subscription_callback' );
add_action( 'wp_ajax_nopriv_view_customer_subscription_action', 'view_customer_subscription_callback' );
function view_customer_subscription_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $post_id = $_REQUEST['user_id'];   
    $start_date = get_field('start_date',$post_id);
    $end_date = get_field('end_date',$post_id);
    $customer_id = get_field('customer',$post_id);
    
    $formatted_start_date =  date("d F Y", strtotime($start_date));
    $formatted_end_date =  date("d F Y", strtotime($end_date));
    ob_start();?>
                   <li>
                      <h5 class="title fw-medium">ID</h5>
                      <span class="text fw-normal"><?= get_field('id',$post_id);?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Membership</h5>
                      <?php $membership = get_field('membership_type',$post_id);?>
                      <span class="text fw-normal"><?= get_field('name',$membership);?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Registration Date</h5>
                      <span class="text fw-normal"><?= $formatted_start_date;?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Expiry Date</h5>
                      <span class="text fw-normal"><?= $formatted_end_date;?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Rem. Sessions</h5>
                      <span class="text fw-normal"><?= get_field('remaining_sessions',$post_id);?></span>
                    </li>
    <?php 
    
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content,
    		    );
	
	echo json_encode($result);
	exit(0);
}