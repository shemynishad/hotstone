<?php 
add_action( 'wp_ajax_view_measurement_action', 'view_measurement_callback' );
add_action( 'wp_ajax_nopriv_view_measurement_action', 'view_measurement_callback' );
function view_measurement_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $post_id = $_REQUEST['user_id'];   
    ob_start();?>
        <div class="form1">
            <div class="row">
                <div class="col-md-6 form-group">
                    <label class="form-label">Customer</label>    
                    <?php $customer_id = get_field('customer',$post_id);
                    $author_obj = get_user_by('id', $customer_id);?>
                    <span><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></span>
                </div>
                <div class="col-md-6 form-group">
                    <label class="form-label">Trainer</label>    
                    <?php $customer_id = get_field('trainer',$post_id);
                    $author_obj = get_user_by('id', $customer_id);?>
                    <span><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></span>
                </div>
                <div class="col-md-6 form-group">
                    <label class="form-label">Fasting</label>    
                    <span><?= get_field('fasting',$post_id);?></span>
                </div>
                <div class="col-md-6 form-group">
                    <label class="form-label">Height</label>    
                    <span><?= get_field('height',$post_id);?></span>
                </div>
                <div class="col-md-6 form-group">
                    <label class="form-label">Weight</label>    
                    <span><?= get_field('weight',$post_id);?></span>
                </div>
                <div class="col-md-6 form-group">
                    <label class="form-label">Performance Index</label>    
                    <span><?= get_field('performance_index',$post_id);?></span>
                </div>
                <div class="col-md-6 form-group">
                    <label class="form-label">SMM</label>    
                    <span><?= get_field('smm',$post_id);?></span>
                </div>
                <div class="col-md-6 form-group">
                    <label class="form-label">Note (EN)</label>    
                    <span><?= get_field('notes',$post_id);?></span>
                </div>
                <div class="col-md-6 form-group">
                    <label class="form-label">Note (AR)</label>    
                    <span><?= get_field('notes_in_arabic',$post_id);?></span>
                </div>
            </div>
        </div>
    <?php 
    
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content,
    		    );
	
	echo json_encode($result);
	exit(0);
}