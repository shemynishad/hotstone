<?php 
add_action( 'wp_ajax_admin_edit_booking_action', 'admin_edit_booking_callback' );
add_action( 'wp_ajax_nopriv_admin_edit_booking_action', 'admin_edit_booking_callback' );
function admin_edit_booking_callback() {
    global $wpdb;
    $date = $_REQUEST['edit_date'];
    $formatted_date =  date("Ymd", strtotime($date));
    //$customer = $_REQUEST['edit_customer'];
    //$class = $_REQUEST['edit_class'];
    $group_id = $_REQUEST['user_id'];
         
     	update_post_meta( $group_id, 'booking_date', sanitize_text_field( $formatted_date ) );
      //  update_post_meta( $group_id, 'class', sanitize_text_field( $class ) );
    //    update_post_meta( $group_id, 'customer', sanitize_text_field( $customer ) );
        

    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}