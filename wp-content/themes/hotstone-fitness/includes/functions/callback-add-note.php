<?php 
add_action( 'wp_ajax_add_note_action', 'add_note_callback' );
add_action( 'wp_ajax_nopriv_add_note_action', 'add_note_callback' );
function add_note_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $user_id = $_REQUEST['user_id'];   
    $notes = $_REQUEST['note'];
    $notes_ar = $_REQUEST['note_ar'];
    $date = date( 'Y-m-d H:i:s', current_time( 'timestamp', 0 ) );
    $value = array();
    if(get_field('field_630d19ff6b820','user_'.$user_id)){
        $value = get_field('field_630d19ff6b820','user_'.$user_id);
    }
    $value[] = array('note'=>$notes,'note_arabic'=>$notes_ar,'date'=>$date);

    update_field( 'field_630d19ff6b820', $value, 'user_'.$user_id );



    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> 'success',
    		    );
	
	echo json_encode($result);
	exit(0);
}