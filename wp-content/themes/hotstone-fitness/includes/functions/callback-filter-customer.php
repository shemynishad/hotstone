<?php 
add_action( 'wp_ajax_admin_filter_customer_action', 'admin_filter_customer_callback' );
add_action( 'wp_ajax_nopriv_admin_filter_customer_action', 'admin_filter_customer_callback' );
function admin_filter_customer_callback() {
    global $wpdb;
    
    $keyword = '';
    if(isset($_POST['keyword'])){
        $keyword = $_POST['keyword'];
    }

    $start_date = '';
    if(isset($_POST['start_date'])){
        $start_date = $_POST['start_date'];
    }

    $end_date = '';
    if(isset($_POST['end_date'])){
        $end_date = $_POST['end_date'];
    }

    $paged = 1;
    if(isset($_REQUEST['page'])):
    $paged = $_REQUEST['page'];
    endif;
    ob_start();?>

<?php  
$start_date_array = array();
if($start_date){
  $start_date_formatted = date("Ymd", strtotime($start_date));
  $start_date_array = array(
    'key'     => 'registration_date',
    'compare' => '>=',
    'value'   => $start_date_formatted,
   );
}

$end_date_array = array();
if($end_date){
  $end_date_formatted = date("Ymd", strtotime($end_date));
  $end_date_array = array(
    'key'     => 'registration_date',
    'compare' => '<=',
    'value'   => $end_date_formatted,
   );
}


$keyword_array = array();
if($keyword){
  $keyword_array = array(
      'relation' => 'OR',
        array(
        'key'     => 'first_name',
        'value'   => $keyword,
        'compare' => 'LIKE'
        ),
        array(
            'key'     => 'last_name',
            'value'   => $keyword,
            'compare' => 'LIKE'
        ),
        array(
            'key'     => 'first_name_arabic',
            'value'   => $keyword,
            'compare' => 'LIKE'
        ),
        array(
            'key'     => 'last_name_arabic',
            'value'   => $keyword,
            'compare' => 'LIKE'
        ),
        array(
            'key'     => 'phone',
            'value'   => $keyword,
            'compare' => 'LIKE'
        ),
        array(
            'key'     => 'email',
            'value'   => $keyword,
            'compare' => 'LIKE'
        )
    );
}



                  $count_args  = array(
                        'role'      => 'customer',
                        'fields'    => 'all_with_meta',
                        'number'    => 999999,
                        'meta_query' => array(
                            $keyword_array,
                            $start_date_array,
                            $end_date_array
                        )
                    );
                    $user_count_query = new WP_User_Query($count_args);
                    $user_count = $user_count_query->get_results();
                    
                    // count the number of users found in the query
                    $total_users = $user_count ? count($user_count) : 1;
                    $users_per_page = 8;
                    $page=  $paged;
                    // calculate the total number of pages.
                    $total_pages = 1;
                    $offset = $users_per_page * ($page - 1);
                    $total_pages = ceil($total_users / $users_per_page);
                    $min  =  (($page-1)*$users_per_page)  +  1;
                    $max  =  $page * $users_per_page; 
                    if($max > $total_users){
                        $max  =  $total_users;
                    }
                  $team_args = array( 
                      'role'      => 'customer',
                      'number' =>8, 
                      'paged' => $paged,
                      'meta_query' => array(
                            $keyword_array,
                            $start_date_array,
                            $end_date_array
                            )
                    );
                  
                  $team_presidents = get_users( $team_args );
                  if($team_presidents):
                  ?>
                    <div class="table1 mb-4">
                    <table class="table mb-0">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>First Name</td>       
                            <td>Last Name</td>      
                            <td>DOB</td>    
                            <td>Gender</td>               
                            <td>Email</td>                    
                            <td>Phone</td>                
                            <td></td>                    
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($team_presidents as $team_president):
                       
                       ?>
                     <tr>
                     <td><?= get_field('user_id', 'user_'.$team_president->ID);?></td>
                    <td><?= $team_president->user_firstname;?></td>
                    <td><?= $team_president->user_lastname;?></td>
                    <td><?= get_field('dob', 'user_'.$team_president->ID);?></td>
                    <td><?= ucfirst(get_field('gender', 'user_'.$team_president->ID));?></td>
                    <td><?= $team_president->user_email;?></td>
                    <td><?= get_field('phone', 'user_'.$team_president->ID);?></td>
                    <td>
                      <ul class="user-options mb-0">
                      <?php $disable = get_field('disable', 'user_'.$team_president->ID);?>
                        <li>
                          <label class="toggle1">
                            <input type="checkbox" class="disable-user" <?php if(!$disable){ echo 'checked';}?> value="<?= $team_president->ID;?>">
                            <span class="slider"></span>
                          </label>
                        </li>
                        <?php  
                        $photo_url = '';
                        if(get_field('registration_date', 'user_'.$team_president->ID)){
                           $photo_id = get_field('photo', 'user_'.$team_president->ID);
                           $photo_url = wp_get_attachment_url($photo_id);
                        }
                        $formatted_date =  date("m/d/Y", strtotime(get_field('registration_date', 'user_'.$team_president->ID)));
                        $data = array(   
                              'first_name_en' => $team_president->user_firstname,
                              'last_name_en' => $team_president->user_lastname,
                              'first_name_ar' => get_field('first_name_arabic', 'user_'.$team_president->ID),
                              'last_name_ar' => get_field('last_name_arabic', 'user_'.$team_president->ID),
                              'email' => $team_president->user_email,
                              'phone' => get_field('phone', 'user_'.$team_president->ID),
                              'dob' => get_field('dob', 'user_'.$team_president->ID),
                              'height' => get_field('height', 'user_'.$team_president->ID),
                              'gender' => get_field('gender', 'user_'.$team_president->ID),
                              'company_name' => get_field('company_name', 'user_'.$team_president->ID),
                              'company_name_arabic' => get_field('company_name_arabic', 'user_'.$team_president->ID),
                              'position' => get_field('position', 'user_'.$team_president->ID),
                              'position_arabic' => get_field('position_arabic', 'user_'.$team_president->ID),
                              'address' => get_field('address', 'user_'.$team_president->ID),
                              'address_arabic' => get_field('address_arabic', 'user_'.$team_president->ID),
                              'registration_date' => $formatted_date,
                              'photo_url' => $photo_url,
                              
                        );?>
                        <li>
                          <button type="button" class="option edit-user" data-index="<?= $team_president->ID;?>" data-details='<?php echo json_encode($data);?>'><img src="<?= get_template_directory_uri();?>/assets/images/pencil.svg" alt="icon"></button>
                        </li>
                        <li>
                          <button type="button" class="option delete-user" data-index="<?= $team_president->ID;?>"><img src="<?= get_template_directory_uri();?>/assets/images/bin.svg" alt="icon"></button>
                        </li>
                        <li>
                          <button type="button" class="option add-note" data-index="<?= $team_president->ID;?>">Add Note</button>
                        </li>
                        <li>
                          <button type="button" class="option send-details" data-index="<?= $team_president->ID;?>"><img src="<?= get_template_directory_uri();?>/assets/images/icon4.svg" alt="icon"></button>
                        </li>
                        <li>
                          <button type="button" class="option view-note" data-index="<?= $team_president->ID;?>">View Note</button>
                        </li>
                      </ul>
                    </td>
                     </tr>
                     <?php endforeach;?>
                   </tbody>
                 </table>
               </div>
                   <?php
            $pages = paginate_links( array(
                'base' => '%_%',
                'format' => '?page=%#%',
                'current' => max( 1, $paged  ),
                'total' => $total_pages,
                'end_size' => 1,
                'type'  => 'array',
                'mid_size' => 2,
                'next_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                'prev_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ) );
            ?>
            <?php if( is_array( $pages ) ) :?>
                <ul class="pagination justify-content-center justify-content-lg-end">
            <?php foreach ( $pages as $page ):?>
                        <li>
                            <?php echo $page;?>
                        </li>
                        <?php endforeach;?>
                    </ul>
            <?php endif;?>
            <?php else:?> 
                 <div class="alert alert-info" role="alert"><?php _e('No customers found matching the search criteria. Please try again.');?> </div>
                  <?php endif;?> 

    <?php
    $content = ob_get_clean();	
	$result = Array(
		"html"		=> $content,
		"date"    =>  $formatted_date
		);
	
	echo json_encode($result);
	exit(0);
}