<?php 
add_action( 'wp_ajax_admin_edit_front_office_action', 'admin_edit_front_office_callback' );
add_action( 'wp_ajax_nopriv_admin_edit_front_office_action', 'admin_edit_front_office_callback' );
function admin_edit_front_office_callback() {
    global $wpdb;
    $first_name = $_REQUEST['edit_first_name_en'];
    $last_name = $_REQUEST['edit_last_name_en'];
    $first_name_ar = $_REQUEST['edit_first_name_ar'];
    $last_name_ar = $_REQUEST['edit_last_name_ar'];
    $phone = $_REQUEST['edit_phone'];
    $user_id = $_REQUEST['user_id'];
         
     	if($user_id && $user_id != 1) {
		$user_id = wp_update_user( array( 'ID' => $user_id, 'first_name' => $first_name,'last_name' => $last_name,'display_name' => $first_name.' '.$last_name, 'role' => 'front_office_user') );
        
        update_user_meta( $user_id, 'first_name_arabic', sanitize_text_field( $first_name_ar ) );
        update_user_meta( $user_id, 'last_name_arabic', sanitize_text_field( $last_name_ar ) );
        update_user_meta( $user_id, 'phone', sanitize_text_field( $phone ) );          
            
     	} 	
        
        


    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}