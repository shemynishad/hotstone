<?php 
add_action( 'wp_ajax_admin_measurement_action', 'admin_measurement_callback' );
add_action( 'wp_ajax_nopriv_admin_measurement_action', 'admin_measurement_callback' );
function admin_measurement_callback() {
    global $wpdb;
    $trainer = $_REQUEST['trainer'];
    $customer = $_REQUEST['customer'];
    $fasting = $_REQUEST['fasting'];
    $height = $_REQUEST['height'];
    $weight = $_REQUEST['weight'];
    $performance_index = $_REQUEST['performance_index'];
    $smm = $_REQUEST['smm'];
    $note = $_REQUEST['note'];
    $note_ar = $_REQUEST['note_ar'];
   
        $my_post = array(
            'post_title'    => get_the_title($membership),
            'post_content'  => '',
            'post_status'   => 'publish',
            'post_author'   => 1,
            'post_type' => 'measurement'
          );
           
        // Insert the post into the database
        $group_id = wp_insert_post( $my_post );
        $next_group_id = get_field('next_measurement_id','option');
        update_post_meta( $group_id, 'id', 'HSC_'.sprintf("%03d", $next_group_id) );
        update_field( 'next_measurement_id', $next_group_id + 1, 'option' );
        update_post_meta( $group_id, 'customer', $customer );
        update_post_meta( $group_id, 'trainer', $trainer );
        update_post_meta( $group_id, 'date', date('Ymd') );
        update_post_meta( $group_id, 'fasting', $fasting );
        update_post_meta( $group_id, 'height', sanitize_text_field( $height ) );
        update_post_meta( $group_id, 'weight', sanitize_text_field( $weight ) );
        update_post_meta( $group_id, 'performance_index', sanitize_text_field( $performance_index ) );
        update_post_meta( $group_id, 'smm', sanitize_text_field( $smm ) );
        update_post_meta( $group_id, 'notes', sanitize_textarea_field( $note ) );
        update_post_meta( $group_id, 'notes_in_arabic', sanitize_textarea_field( $note_ar ) );
        update_post_meta( $group_id, 'is_disable', 0 );
        

        
        


    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}