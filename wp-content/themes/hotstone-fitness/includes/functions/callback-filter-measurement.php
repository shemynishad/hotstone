<?php 
add_action( 'wp_ajax_admin_filter_measurement_action', 'admin_filter_measurement_callback' );
add_action( 'wp_ajax_nopriv_admin_filter_measurement_action', 'admin_filter_measurement_callback' );
function admin_filter_measurement_callback() {
    global $wpdb;
    
    $trainer_keyword = '';
    if(isset($_POST['trainer_keyword'])){
        $trainer_keyword = $_POST['trainer_keyword'];
    }
    
    $keyword = '';
    if(isset($_POST['keyword'])){
        $keyword = $_POST['keyword'];
    }

    $start_date = '';
    if(isset($_POST['start_date'])){
        $start_date = $_POST['start_date'];
    }

    $end_date = '';
    if(isset($_POST['end_date'])){
        $end_date = $_POST['end_date'];
    }

    $paged = 1;
    if(isset($_REQUEST['page'])):
    $paged = $_REQUEST['page'];
    endif;
    ob_start();?>

<?php 


$start_date_array = array();
if($start_date){
  $start_date_formatted = date("Ymd", strtotime($start_date));
  $start_date_array = array(
    'key'     => 'date',
    'compare' => '>=',
    'value'   => $start_date_formatted,
   );
}

$end_date_array = array();
if($end_date){
  $end_date_formatted = date("Ymd", strtotime($end_date));
  $end_date_array = array(
    'key'     => 'date',
    'compare' => '<=',
    'value'   => $end_date_formatted,
   );
}

$keyword_array = array();
if($keyword){
  $keyword_array = array(
      'relation' => 'OR',
        array(
        'key'     => 'first_name',
        'value'   => $keyword,
        'compare' => 'LIKE'
        ),
        array(
            'key'     => 'last_name',
            'value'   => $keyword,
            'compare' => 'LIKE'
        ),
        array(
            'key'     => 'first_name_arabic',
            'value'   => $keyword,
            'compare' => 'LIKE'
        ),
        array(
            'key'     => 'last_name_arabic',
            'value'   => $keyword,
            'compare' => 'LIKE'
        )
    );
}

$trainer_keyword_array = array();
if($trainer_keyword){
  $trainer_keyword_array = array(
      'relation' => 'OR',
        array(
        'key'     => 'first_name',
        'value'   => $trainer_keyword,
        'compare' => 'LIKE'
        ),
        array(
            'key'     => 'last_name',
            'value'   => $trainer_keyword,
            'compare' => 'LIKE'
        ),
        array(
            'key'     => 'first_name_arabic',
            'value'   => $trainer_keyword,
            'compare' => 'LIKE'
        ),
        array(
            'key'     => 'last_name_arabic',
            'value'   => $trainer_keyword,
            'compare' => 'LIKE'
        )
    );
}



                    $team_args = array( 
                      'role__in' => array( 'customer'), 
                      'number' =>999999, 
                      
                      'meta_query' => array(
                                $keyword_array,
                            
                        )
                    );
                    
                    $trainer_team_args = array( 
                      'role__in' => array( 'trainer'), 
                      'number' =>999999, 
                      
                      'meta_query' => array(
                                $trainer_keyword_array,
                            
                        )
                    );
                  
                  $team_presidents = get_users( $team_args );
                  $team_trainer_presidents = get_users( $trainer_team_args );
                  if($team_presidents && $team_trainer_presidents):
                      $team_presidents_array  =  array();
                      foreach($team_presidents as $team_president):
                          $team_presidents_array[]=  $team_president->ID;
                      endforeach;
                      $team_trainer_presidents_array  =  array();
                      foreach($team_trainer_presidents as $team_trainer_president):
                          $team_trainer_presidents_array[]=  $team_trainer_president->ID;
                      endforeach;
                      if($team_presidents_array && $team_trainer_presidents_array):
                       $team_array = array(
                           'key'     => 'customer',
                           'value'   => $team_presidents_array,
                           'compare' => 'IN'
                           );
                        $team_trainer_array = array(
                           'key'     => 'trainer',
                           'value'   => $team_trainer_presidents_array,
                           'compare' => 'IN'
                           );


     
                  $args = array(
                    	'post_type' => 'measurement',
                    	'posts_per_page' => 8,
                    	'meta_query' => array(
                            $start_date_array,
                            $end_date_array,
                            $team_array,
                            $team_trainer_array
                        )
                        
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
            <div class="table1 mb-4">
              <table class="table mb-0">
                <thead>
                  <tr>
                    <td>ID</td>
                    <td>Customer</td>       
                    <td>Date</td>      
                    <td>Trainer</td>
                    <td></td>                    
                  </tr>
                </thead>
                <tbody>
                <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
                  <tr>
                    <td><?= get_field('id');?></td>
                    <?php $customer_id = get_field('customer');
                    $author_obj = get_user_by('id', $customer_id);?>
                    <td><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></td>
                    <?php $date = get_field('date');?>
                    <td><?= date("d F Y", strtotime($date));?></td>
                    <?php $trainer_id = get_field('trainer');
                    $author_obj = get_user_by('id', $trainer_id);?>
                    <td><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></td>
                    <td>
                      <ul class="user-options mb-0">
                      <?php $disable = get_field('is_disable');?>
                        <li>
                          <label class="toggle1">
                            <input type="checkbox" class="disable-user" <?php if(!$disable){ echo 'checked';}?> value="<?= $query->post->ID;?>">
                            <span class="slider"></span>
                          </label>
                        </li>
                        <?php 
                         $data = array(   
                            'customer' => $customer_id,
                            'trainer' => $trainer_id,
                            'fasting' => get_field('fasting'),
                            'height'  => get_field('height'),
                            'weight'  => get_field('weight'),
                            'performance_index'  => get_field('performance_index'),
                            'smm'  => get_field('smm'),
                            'notes'  => get_field('notes'),
                            'notes_ar'  => get_field('notes_in_arabic'),
                        );?>
                        <li>
                          <button type="button" class="option edit-user" data-index="<?= $query->post->ID;?>" data-details='<?php echo json_encode($data);?>'><img src="<?= get_template_directory_uri();?>/assets/images/pencil.svg" alt="icon"></button>
                        </li>
                        <li>
                          <button type="button" class="option delete-user" data-index="<?= $query->post->ID;?>"><img src="<?= get_template_directory_uri();?>/assets/images/bin.svg" alt="icon"></button>
                        </li>
                        <li>
                          <button type="button" class="option view-log" data-index="<?= $query->post->ID;?>"><i class="fa fa-eye" aria-hidden="true"></i></button>
                        </li>
                      </ul>
                    </td>
                  </tr>
                  <?php endwhile;?>
                </tbody>
              </table>
            </div>
            <?php
            $pages = paginate_links( array(
                'base' => '%_%',
                'format' => '?page=%#%',
                'current' => max( 1, get_query_var('paged')  ),
                'total' => $query->max_num_pages,
                'end_size' => 1,
                'type'  => 'array',
                'mid_size' => 2,
                'next_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                'prev_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ) );
            ?>            
            <?php if( is_array( $pages ) ) :?>
            <ul class="pagination justify-content-center justify-content-lg-end">
            <?php foreach ( $pages as $page ):?>
                        <li>
                            <?php echo $page;?>
                        </li>
                        <?php endforeach;?>
                    </ul>
            <?php endif;?>
            <?php else:?>
                <p>No Measurement Found</p>
            <?php endif;?>
            <?php else:?>
                <p>No Measurement Found</p>
            <?php endif;?>
             <?php else:?>
                <p>No Measurement Found</p>
            <?php endif;?>

    <?php
    $content = ob_get_clean();	
	$result = Array(
		"html"		=> $content,
		"date"    =>  $formatted_date
		);
	
	echo json_encode($result);
	exit(0);
}