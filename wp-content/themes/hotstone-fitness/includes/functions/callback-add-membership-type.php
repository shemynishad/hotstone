<?php 
add_action( 'wp_ajax_admin_membership_type_action', 'admin_membership_type_callback' );
add_action( 'wp_ajax_nopriv_admin_membership_type_action', 'admin_membership_type_callback' );
function admin_membership_type_callback() {
    global $wpdb;
    $membership_name_en = $_REQUEST['membership_name_en'];
    $membership_name_ar = $_REQUEST['membership_name_ar'];
    $no_of_session = $_REQUEST['no_of_session'];
    $amount = $_REQUEST['amount'];
    $validity = $_REQUEST['validity'];
    $session_type = $_REQUEST['session_type'];
    
        $my_post = array(
            'post_title'    => wp_strip_all_tags( $membership_name_en ),
            'post_content'  => '',
            'post_status'   => 'publish',
            'post_author'   => 1,
            'post_type' => 'membership_types'
          );
           
        // Insert the post into the database
        $group_id = wp_insert_post( $my_post );
        $next_group_id = get_field('next_membership_type_id','option');
        update_post_meta( $group_id, 'id', 'HSMS_'.sprintf("%03d", $next_group_id) );
        update_field( 'next_membership_type_id', $next_group_id + 1, 'option' );
        update_post_meta( $group_id, 'name', sanitize_text_field( $membership_name_en ) );
        update_post_meta( $group_id, 'name_in_arabic', sanitize_text_field( $membership_name_ar ) );
        update_post_meta( $group_id, 'amount', sanitize_text_field( $amount ) );
        update_post_meta( $group_id, 'session_type', sanitize_text_field( $session_type ) );
        update_post_meta( $group_id, 'no_of_sessions', sanitize_text_field( $no_of_session ) );
        update_post_meta( $group_id, 'validity', sanitize_text_field( $validity ) );
        update_post_meta( $group_id, 'is_disable', 0 );
        

        $image_url1=$_FILES['photo']['tmp_name'];
        if($image_url1){
            $documenten2=$_FILES['photo'];
            $upload_dir = wp_upload_dir();
            $image_data1 = file_get_contents($image_url1);
            $filename1 = $documenten2['name'];
            if(wp_mkdir_p($upload_dir['path']))
                $file1 = $upload_dir['path'] . '/' . $filename1;
            else
                $file1 = $upload_dir['basedir'] . '/' . $filename1;
            file_put_contents($file1, $image_data1);
            $wp_filetype = wp_check_filetype($filename1, null );
            $attachment1 = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => sanitize_file_name($filename1),
                'post_content' => '',
                'post_status' => 'inherit'
            );
            $attach_id1 = wp_insert_attachment( $attachment1, $file1, 0);
            $file1_url = wp_get_attachment_url( $attach_id1 );
            $path = get_attached_file( $attach_id1 );
            wp_update_attachment_metadata( $attach_id1, $metadata );
            update_post_meta( $group_id, 'photo', $attach_id1 );
        }       
        
        


    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}