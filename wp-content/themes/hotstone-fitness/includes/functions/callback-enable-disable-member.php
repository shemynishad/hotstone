<?php 
add_action( 'wp_ajax_admin_enable_disable_member_action', 'admin_enable_disable_member_callback' );
add_action( 'wp_ajax_nopriv_admin_enable_disable_member_action', 'admin_enable_disable_member_callback' );
function admin_enable_disable_member_callback() {
    global $wpdb;
    $user_id = $_REQUEST['postid'];
    $disable = $_REQUEST['disable'];
    update_post_meta( $user_id, 'is_disable', $disable );
    $content = ob_get_clean();	
	$result = Array(
		"html"		=> $content,
		"date"    =>  $formatted_date
		);
	
	echo json_encode($result);
	exit(0);
}