<?php 
add_action( 'wp_ajax_admin_filter_subscription_action', 'admin_filter_subscription_callback' );
add_action( 'wp_ajax_nopriv_admin_filter_subscription_action', 'admin_filter_subscription_callback' );
function admin_filter_subscription_callback() {
    global $wpdb;
    
    $membership = '';
    if(isset($_POST['membership'])){
        $membership = $_POST['membership'];
    }
    
    $keyword = '';
    if(isset($_POST['keyword'])){
        $keyword = $_POST['keyword'];
    }

    $start_date = '';
    if(isset($_POST['start_date'])){
        $start_date = $_POST['start_date'];
    }

    $end_date = '';
    if(isset($_POST['end_date'])){
        $end_date = $_POST['end_date'];
    }

    $paged = 1;
    if(isset($_REQUEST['page'])):
    $paged = $_REQUEST['page'];
    endif;
    ob_start();?>

<?php 
$membership_array = array();
if($membership){
  $membership_array = array(
    'key'     => 'membership_type',
    'compare' => '=',
    'value'   => $membership,
   );
}


$start_date_array = array();
if($start_date){
  $start_date_formatted = date("Ymd", strtotime($start_date));
  $start_date_array = array(
    'key'     => 'start_date',
    'compare' => '>=',
    'value'   => $start_date_formatted,
   );
}

$end_date_array = array();
if($end_date){
  $end_date_formatted = date("Ymd", strtotime($end_date));
  $end_date_array = array(
    'key'     => 'start_date',
    'compare' => '<=',
    'value'   => $end_date_formatted,
   );
}

$keyword_array = array();
if($keyword){
  $keyword_array = array(
      'relation' => 'OR',
        array(
        'key'     => 'first_name',
        'value'   => $keyword,
        'compare' => 'LIKE'
        ),
        array(
            'key'     => 'last_name',
            'value'   => $keyword,
            'compare' => 'LIKE'
        ),
        array(
            'key'     => 'first_name_arabic',
            'value'   => $keyword,
            'compare' => 'LIKE'
        ),
        array(
            'key'     => 'last_name_arabic',
            'value'   => $keyword,
            'compare' => 'LIKE'
        )
    );
}



                    $team_args = array( 
                      'role__in' => array( 'customer'), 
                      'number' =>999999, 
                      
                      'meta_query' => array(
                                $keyword_array,
                            
                        )
                    );
                  
                  $team_presidents = get_users( $team_args );
                  if($team_presidents):
                      $team_presidents_array  =  array();
                      foreach($team_presidents as $team_president):
                          $team_presidents_array[]=  $team_president->ID;
                      endforeach;
                      if($team_presidents_array):
                       $team_array = array(
                           'key'     => 'customer',
                           'value'   => $team_presidents_array,
                           'compare' => 'IN'
                           );


     
                  $args = array(
                    	'post_type' => 'subscription',
                    	'posts_per_page' => 8,
                      'paged' => $paged,
                    	'meta_query' => array(
                            $membership_array,
                            $start_date_array,
                            $end_date_array,
                            $team_array
                        )
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
            <div class="table1 mb-4">
              <table class="table mb-0">
                <thead>
                  <tr>
                    <td>ID</td>
                    <td>Customer</td>       
                    <td>Membership</td>      
                    <td>Registration date</td>    
                    <td>Expiry date</td>               
                    <td>Rem. Sessions</td>
                    <td></td>                    
                  </tr>
                </thead>
                <tbody>
                <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
                  <tr>
                    <td><?= get_field('id');?></td>
                    <?php $customer_id = get_field('customer');
                    $author_obj = get_user_by('id', $customer_id);?>
                    <td><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></td>
                    <?php $membership = get_field('membership_type');?>
                    <td><?= get_field('name',$membership);?></td>
                    <?php $start_date = get_field('start_date');?>
                    <td><?= date("d F Y", strtotime($start_date));?></td>
                    <?php $end_date = get_field('end_date');?>
                    <td><?= date("d F Y", strtotime($end_date));?></td>
                    <td><?= get_field('remaining_sessions');?></td>
                    <td>
                      <ul class="user-options mb-0">
                      <?php $disable = get_field('is_disable');?>
                        <li>
                          <label class="toggle1">
                            <input type="checkbox" class="disable-user" <?php if(!$disable){ echo 'checked';}?> value="<?= $query->post->ID;?>">
                            <span class="slider"></span>
                          </label>
                        </li>
                        <?php 
                        $attachment_name = '';
                        if(get_field('invoice')){
                           $photo_id = get_field('invoice');
                           $attachment_url = wp_get_attachment_url($photo_id);
                           $attachment_name = '<img src="'.get_template_directory_uri().'/includes/doc.png"><span class="file-name">'.basename($attachment_url).'</span>';
                           $filetype = wp_check_filetype_and_ext( $attachment_url,$attachment_url );
                           if($filetype['ext'] == 'pdf'){
                              $attachment_name = '<img src="'.get_template_directory_uri().'/includes/pdf.png"><span class="file-name">'.basename($attachment_url).'</span>'; 
                           }
                        }
                        $formatted_start_date =  date("m/d/Y", strtotime($start_date));
                        $formatted_end_date =  date("m/d/Y", strtotime($end_date));
                         $data = array(   
                            'customer' => $customer_id,
                            'membership' => $membership,
                            'start_date' => $formatted_start_date,
                            'end_date'   => $formatted_end_date,
                            'remaining_session' => get_field('remaining_sessions'),
                            'attachment_name' => '<a href="'.$attachment_url.'" target="_blank">'.$attachment_name.'</a>'
                        );?>
                        <li>
                          <button type="button" class="option edit-user" data-index="<?= $query->post->ID;?>" data-details='<?php echo json_encode($data);?>'><img src="<?= get_template_directory_uri();?>/assets/images/pencil.svg" alt="icon"></button>
                        </li>
                        <li>
                          <button type="button" class="option delete-user" data-index="<?= $query->post->ID;?>"><img src="<?= get_template_directory_uri();?>/assets/images/bin.svg" alt="icon"></button>
                        </li>
                        <li>
                          <button type="button" class="option view-log" data-index="<?= $query->post->ID;?>"><img src="<?= get_template_directory_uri();?>/assets/images/icon4.svg" alt="icon"></button>
                        </li>
                      </ul>
                    </td>
                  </tr>
                  <?php endwhile;?>
                </tbody>
              </table>
            </div>
            <?php
            $pages = paginate_links( array(
                'base' => '%_%',
                'format' => '?page=%#%',
                'current' => max( 1, $paged  ),
                'total' => $query->max_num_pages,
                'end_size' => 1,
                'type'  => 'array',
                'mid_size' => 2,
                'next_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                'prev_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ) );
            ?>            
            <?php if( is_array( $pages ) ) :?>
            <ul class="pagination justify-content-center justify-content-lg-end">
            <?php foreach ( $pages as $page ):?>
                        <li>
                            <?php echo $page;?>
                        </li>
                        <?php endforeach;?>
                    </ul>
            <?php endif;?>
            <?php else:?>
                <div class="alert alert-info" role="alert"><?php _e('No subscription found matching the search criteria. Please try again.');?> </div>
            <?php endif;?>
            <?php else:?>
                <div class="alert alert-info" role="alert"><?php _e('No subscription found matching the search criteria. Please try again.');?> </div>
            <?php endif;?>
             <?php else:?>
                <div class="alert alert-info" role="alert"><?php _e('No subscription found matching the search criteria. Please try again.');?> </div>
            <?php endif;?>

    <?php
    $content = ob_get_clean();	
	$result = Array(
		"html"		=> $content,
		"date"    =>  $formatted_date
		);
	
	echo json_encode($result);
	exit(0);
}