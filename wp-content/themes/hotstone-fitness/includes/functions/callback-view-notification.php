<?php 
add_action( 'wp_ajax_view_notification_action', 'view_notification_callback' );
add_action( 'wp_ajax_nopriv_view_notification_action', 'view_notification_callback' );
function view_notification_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $post_id = $_REQUEST['user_id'];  
    $upload_dir = wp_upload_dir();
    $attachment_id = get_field('attachment',$post_id);
    $attachment_metadata = wp_get_attachment_metadata( $attachment_id );
    $fullsize_path = get_attached_file( $attachment_id );
    ob_start();?>
        <div class="form1">
            <div class="row">
                <div class="col-md-6 form-group">
                    <label class="form-label">Subject</label>    
                    <span><?= get_field('subject',$post_id);?></span>
                </div>
                <div class="col-md-6 form-group">
                    <label class="form-label">Message (EN)</label>    
                    <span><?= get_field('message',$post_id);?></span>
                </div>
                <div class="col-md-6 form-group">
                    <label class="form-label">Message (AR)</label>    
                    <span><?= get_field('message_in_arabic',$post_id);?></span>
                </div>
                <div class="col-md-6 form-group">
                    <label class="form-label">Attachment</label>    
                    <span><a href="<?= wp_get_attachment_url($attachment_id);?>" target="_blank"><?= basename( get_attached_file( $attachment_id ) );?></a></span>
                </div>
                <div class="col-md-6 form-group">
                    <label class="form-label">Customer</label>    
                    <?php $customer_id = get_field('customer',$post_id);
                    $author_obj = get_user_by('id', $customer_id);?>
                    <span><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></span>
                </div>
            </div>
        </div>
    <?php 
    
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content,
    		    );
	
	echo json_encode($result);
	exit(0);
}