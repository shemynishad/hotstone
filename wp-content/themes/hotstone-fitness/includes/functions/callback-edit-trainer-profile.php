<?php 
add_action( 'wp_ajax_edit_trainer_action', 'edit_trainer_callback' );
add_action( 'wp_ajax_nopriv_edit_trainer_action', 'edit_trainer_callback' );
function edit_trainer_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $first_name = $_REQUEST['first_name_en'];
    $last_name = $_REQUEST['last_name_en'];
    $first_name_ar = $_REQUEST['first_name_ar'];
    $last_name_ar = $_REQUEST['last_name_ar'];
    $email = $_REQUEST['email'];
    $phone = $_REQUEST['phone'];
    $date_of_birth = $_REQUEST['date_of_birth'];
    $gender = $_REQUEST['gender'];
    $id_no = $_REQUEST['id_no'];
    $employee_no = $_REQUEST['employee_no'];
    $passport_no = $_REQUEST['passport_no'];
    $date_of_id = $_REQUEST['date_of_id'];
    $youtube_video = $_REQUEST['youtube_video'];
    
    $user_id = $current_user->ID;
         
     	if($user_id && $user_id != 1) {
		$user_id = wp_update_user( array( 'ID' => $user_id, 'first_name' => $first_name,'last_name' => $last_name,'display_name' => $first_name.' '.$last_name, 'role' => 'trainer') );
        
        update_user_meta( $user_id, 'first_name_arabic', sanitize_text_field( $first_name_ar ) );
        update_user_meta( $user_id, 'last_name_arabic', sanitize_text_field( $last_name_ar ) );
        update_user_meta( $user_id, 'phone', sanitize_text_field( $phone ) );
        update_user_meta( $user_id, 'email', sanitize_text_field( $email ) );
        update_user_meta( $user_id, 'dob', sanitize_text_field( $date_of_birth ) );
        update_user_meta( $user_id, 'gender', sanitize_text_field( $gender ) );
        update_user_meta( $user_id, 'id_no', sanitize_text_field( $id_no ) );
        update_user_meta( $user_id, 'employee_no', sanitize_text_field( $employee_no ) );
        update_user_meta( $user_id, 'passport_no', sanitize_text_field( $passport_no ) );
        update_user_meta( $user_id, 'date_of_expiry_of_id', sanitize_text_field( $date_of_id ) );
        update_user_meta( $user_id, 'youtube_url', sanitize_text_field( $youtube_video ) );
            
     	} 	
        
        
          $image_url1=$_FILES['photo']['tmp_name'];
        if($image_url1){
            $documenten2=$_FILES['photo'];
            $upload_dir = wp_upload_dir();
            $image_data1 = file_get_contents($image_url1);
            $filename1 = $documenten2['name'];
            if(wp_mkdir_p($upload_dir['path']))
                $file1 = $upload_dir['path'] . '/' . $filename1;
            else
                $file1 = $upload_dir['basedir'] . '/' . $filename1;
            file_put_contents($file1, $image_data1);
            $wp_filetype = wp_check_filetype($filename1, null );
            $attachment1 = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => sanitize_file_name($filename1),
                'post_content' => '',
                'post_status' => 'inherit'
            );
            $attach_id1 = wp_insert_attachment( $attachment1, $file1, 0);
            $file1_url = wp_get_attachment_url( $attach_id1 );
            $path = get_attached_file( $attach_id1 );
            wp_update_attachment_metadata( $attach_id1, $metadata );
            update_user_meta( $user_id, 'photo', $attach_id1 );
        }

    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}