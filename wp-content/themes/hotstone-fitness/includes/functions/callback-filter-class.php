<?php 
add_action( 'wp_ajax_admin_filter_class_action', 'admin_filter_class_callback' );
add_action( 'wp_ajax_nopriv_admin_filter_class_action', 'admin_filter_class_callback' );
function admin_filter_class_callback() {
    global $wpdb;
    
    $membership = '';
    if(isset($_POST['membership'])){
        $membership = $_POST['membership'];
    }
    
    $keyword = '';
    if(isset($_POST['keyword'])){
        $keyword = $_POST['keyword'];
    }

    $start_date = '';
    if(isset($_POST['start_date'])){
        $start_date = $_POST['start_date'];
    }

    $end_date = '';
    if(isset($_POST['end_date'])){
        $end_date = $_POST['end_date'];
    }

    $paged = 1;
    if(isset($_REQUEST['page'])):
    $paged = $_REQUEST['page'];
    endif;
    ob_start();?>

<?php 
$membership_array = array();
if($membership){
  $membership_array = array(
    'key'     => 'membership',
    'compare' => '=',
    'value'   => $membership,
   );
}


$start_date_array = array();
if($start_date){
  $start_date_formatted = date("Ymd", strtotime($start_date));
  $start_date_array = array(
    'key'     => 'date',
    'compare' => '>=',
    'value'   => $start_date_formatted,
   );
}

$end_date_array = array();
if($end_date){
  $end_date_formatted = date("Ymd", strtotime($end_date));
  $end_date_array = array(
    'key'     => 'date',
    'compare' => '<=',
    'value'   => $end_date_formatted,
   );
}

$keyword_array = array();
if($keyword){
  $keyword_array = array(
      'relation' => 'OR',
        array(
        'key'     => 'first_name',
        'value'   => $keyword,
        'compare' => 'LIKE'
        ),
        array(
            'key'     => 'last_name',
            'value'   => $keyword,
            'compare' => 'LIKE'
        ),
        array(
            'key'     => 'first_name_arabic',
            'value'   => $keyword,
            'compare' => 'LIKE'
        ),
        array(
            'key'     => 'last_name_arabic',
            'value'   => $keyword,
            'compare' => 'LIKE'
        )
    );
}



                    $team_args = array( 
                      'role__in' => array( 'trainer'), 
                      'number' =>999999, 
                      
                      'meta_query' => array(
                                $keyword_array,
                            
                        )
                    );
                  
                  $team_presidents = get_users( $team_args );
                  if($team_presidents):
                      $team_presidents_array  =  array();
                      foreach($team_presidents as $team_president):
                          $team_presidents_array[]=  $team_president->ID;
                      endforeach;
                      if($team_presidents_array):
                       $team_array = array(
                           'key'     => 'trainer',
                           'value'   => $team_presidents_array,
                           'compare' => 'IN'
                           );


     
                  $args = array(
                    	'post_type' => 'class',
                    	'posts_per_page' => 8,
					    'paged' => $paged,
                    	'meta_query' => array(
                            $membership_array,
                            $start_date_array,
                            $end_date_array,
                            $team_array
                        )
                    );
                    $query = new WP_Query( $args );
                  if($query->have_posts()):
                  ?>
            <div class="table1 mb-4">
               <table class="table mb-0">
                <thead>
                  <tr>
                    <td>ID</td>
                    <td>Date</td>     
                    <td>Time(Start-End)</td>
                    <td>Duration</td>
                    <td>Trainer</td>
                    <td>Workouts</td>
                    <td>Class</td>
                    <td>Type</td>
                    <td>Bookings</td>
                    <td></td>                    
                  </tr>
                </thead>
                <tbody>
                <?php while($query->have_posts()): $query->the_post();
                       
                    ?>
                  <tr>
                    <td><?= get_field('id');?></td>
                    <?php $date = get_field('date');?>
                    <td><?= date("d F Y", strtotime($date));?></td>
                    <td><?= get_field('start_time').'-'.get_field('end_time');?></td>
                    <?php $to_time = strtotime("2008-12-13 ".get_field('end_time').":00");
                        $from_time = strtotime("2008-12-13 ".get_field('start_time').":00");
                        $hours = round(abs($to_time - $from_time) / 3600,2);?>
                    <td><?= $hours.' hours';?></td>
                    <?php $trainer_id = get_field('trainer');
                    $author_obj = get_user_by('id', $trainer_id);?>
                    <td><?= $author_obj->user_firstname.' '.$author_obj->user_lastname;?></td>
                    <?php $workout_titles = array();
                    $workouts = explode(",",get_field('workouts'));
                    foreach($workouts as $workout):
                        $workout_titles[] = get_the_title($workout);
                    endforeach;?>
                    <td><?= implode(",",$workout_titles);?></td>
                    <td><?= get_field('class_title');?></td>
                    <td><?= get_field('name',get_field('membership'));?></td>
                     <?php 
                    $class_array = array(
                           'key'     => 'class',
                           'value'   => $query->post->ID,
                           'compare' => '='
                           );
     
                  $args = array(
                    	'post_type' => 'booking',
                    	'numberposts' => -1,
                    	'meta_query' => array(
                            $class_array
                        )
                        
                    );
                    $latest_class = get_posts( $args );
                    ?>
                    <td><?= count($latest_class);?>/<?= get_field('no_of_slots');?></td>
                    <td>
                      <ul class="user-options mb-0">
                      <?php $disable = get_field('is_disable');?>
                        <li>
                          <label class="toggle1">
                            <input type="checkbox" class="disable-user" <?php if(!$disable){ echo 'checked';}?> value="<?= $query->post->ID;?>">
                            <span class="slider"></span>
                          </label>
                        </li>
                        <?php 
                        $formatted_date =  date("m/d/Y", strtotime($date));
                         $data = array(   
                            'class_title' => get_field('class_title'),
                            'class_title_ar' => get_field('class_title_in_arabic'),
                            'date' => $formatted_date,
                            'start_time' => get_field('start_time'),
                            'end_time' => get_field('end_time'),
                            'type' => get_field('membership'),
                            'no_of_slots' => get_field('no_of_slots'),
                            'trainer' => get_field('trainer'),
                            'workouts' =>get_field('workouts')
                        );?>
                        <li>
                          <button type="button" class="option edit-user" data-index="<?= $query->post->ID;?>" data-details='<?php echo json_encode($data);?>'><img src="<?= get_template_directory_uri();?>/assets/images/pencil.svg" alt="icon"></button>
                        </li>
                        <li>
                          <button type="button" class="option delete-user" data-index="<?= $query->post->ID;?>"><img src="<?= get_template_directory_uri();?>/assets/images/bin.svg" alt="icon"></button>
                        </li>
                        <li>
                          <a href="<?php the_permalink();?>" class="option" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                        </li>
                      </ul>
                    </td>
                  </tr>
                  <?php endwhile;?>
                </tbody>
              </table>
            </div>
            <?php
            $pages = paginate_links( array(
                'base' => '%_%',
                'format' => '?page=%#%',
                'current' => max( 1, $paged  ),
                'total' => $query->max_num_pages,
                'end_size' => 1,
                'type'  => 'array',
                'mid_size' => 2,
                'next_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                'prev_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ) );
            ?>            
            <?php if( is_array( $pages ) ) :?>
            <ul class="pagination justify-content-center justify-content-lg-end">
            <?php foreach ( $pages as $page ):?>
                        <li>
                            <?php echo $page;?>
                        </li>
                        <?php endforeach;?>
                    </ul>
            <?php endif;?>
            <?php else:?>
                <div class="alert alert-info" role="alert"><?php _e('No class found matching the search criteria. Please try again.');?> </div>
            <?php endif;?>
            <?php else:?>
                <div class="alert alert-info" role="alert"><?php _e('No class found matching the search criteria. Please try again.');?> </div>
            <?php endif;?>
             <?php else:?>
                <div class="alert alert-info" role="alert"><?php _e('No class found matching the search criteria. Please try again.');?> </div>
            <?php endif;?>

    <?php
    $content = ob_get_clean();	
	$result = Array(
		"html"		=> $content,
		"date"    =>  $formatted_date
		);
	
	echo json_encode($result);
	exit(0);
}