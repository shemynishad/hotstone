<?php 
add_action( 'wp_ajax_delete_user_action', 'delete_user_callback' );
add_action( 'wp_ajax_nopriv_delete_user_action', 'delete_user_callback' );
function delete_user_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $id = $_REQUEST['id'];   

    wp_delete_user( $id ); 


    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> 'success',
    		    );
	
	echo json_encode($result);
	exit(0);
}