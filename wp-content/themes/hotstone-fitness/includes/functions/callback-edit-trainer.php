<?php 
add_action( 'wp_ajax_admin_edit_trainer_action', 'admin_edit_trainer_callback' );
add_action( 'wp_ajax_nopriv_admin_edit_trainer_action', 'admin_edit_trainer_callback' );
function admin_edit_trainer_callback() {
    global $wpdb;
    $first_name = $_REQUEST['edit_first_name_en'];
    $last_name = $_REQUEST['edit_last_name_en'];
    $first_name_ar = $_REQUEST['edit_first_name_ar'];
    $last_name_ar = $_REQUEST['edit_last_name_ar'];
    $phone = $_REQUEST['edit_phone'];
    $date_of_birth = $_REQUEST['edit_date_of_birth'];
    $date_of_hire = $_REQUEST['edit_date_of_hire'];
    $gender = $_REQUEST['edit_gender'];
    $id_no = $_REQUEST['edit_id_no'];
    $employee_no = $_REQUEST['edit_employee_no'];
    $passport_no = $_REQUEST['edit_passport_no'];
    $date_of_id = $_REQUEST['edit_date_of_id'];
    $youtube_video = $_REQUEST['edit_youtube_video'];
    $notes = $_REQUEST['edit_notes'];
    $notes_ar = $_REQUEST['edit_notes_ar'];
    $user_id = $_REQUEST['user_id'];
         
     	if($user_id && $user_id != 1) {
		$user_id = wp_update_user( array( 'ID' => $user_id, 'first_name' => $first_name,'last_name' => $last_name,'display_name' => $first_name.' '.$last_name, 'role' => 'trainer') );
        
        update_user_meta( $user_id, 'first_name_arabic', sanitize_text_field( $first_name_ar ) );
        update_user_meta( $user_id, 'last_name_arabic', sanitize_text_field( $last_name_ar ) );
        update_user_meta( $user_id, 'phone', sanitize_text_field( $phone ) );   
        update_user_meta( $user_id, 'dob', sanitize_text_field( $date_of_birth ) );
        update_user_meta( $user_id, 'gender', sanitize_text_field( $gender ) );
        update_user_meta( $user_id, 'date_of_hire', sanitize_text_field( $date_of_hire ) );
        update_user_meta( $user_id, 'id_no', sanitize_text_field( $id_no ) );
        update_user_meta( $user_id, 'employee_no', sanitize_text_field( $employee_no ) );
        update_user_meta( $user_id, 'passport_no', sanitize_text_field( $passport_no ) );
        update_user_meta( $user_id, 'date_of_expiry_of_id', sanitize_text_field( $date_of_id ) );
        update_user_meta( $user_id, 'youtube_url', sanitize_text_field( $youtube_video ) );
        update_user_meta( $user_id, 'notes', sanitize_text_field( $notes ) );
        update_user_meta( $user_id, 'notes_arabic', sanitize_text_field( $notes_ar ) );       
            
     	} 	

         $image_url1=$_FILES['photo']['tmp_name'];
        if($image_url1){
            $documenten2=$_FILES['photo'];
            $upload_dir = wp_upload_dir();
            $image_data1 = file_get_contents($image_url1);
            $filename1 = $documenten2['name'];
            if(wp_mkdir_p($upload_dir['path']))
                $file1 = $upload_dir['path'] . '/' . $filename1;
            else
                $file1 = $upload_dir['basedir'] . '/' . $filename1;
            file_put_contents($file1, $image_data1);
            $wp_filetype = wp_check_filetype($filename1, null );
            $attachment1 = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => sanitize_file_name($filename1),
                'post_content' => '',
                'post_status' => 'inherit'
            );
            $attach_id1 = wp_insert_attachment( $attachment1, $file1, 0);
            $file1_url = wp_get_attachment_url( $attach_id1 );
            $path = get_attached_file( $attach_id1 );
            wp_update_attachment_metadata( $attach_id1, $metadata );
            update_user_meta( $user_id, 'photo', $attach_id1 );
        }

        $image_url2=$_FILES['attachment']['tmp_name'];
        if($image_url2){
            $documenten2=$_FILES['attachment'];
            $upload_dir = wp_upload_dir();
            $image_data1 = file_get_contents($image_url2);
            $filename1 = $documenten2['name'];
            if(wp_mkdir_p($upload_dir['path']))
                $file1 = $upload_dir['path'] . '/' . $filename1;
            else
                $file1 = $upload_dir['basedir'] . '/' . $filename1;
            file_put_contents($file1, $image_data1);
            $wp_filetype = wp_check_filetype($filename1, null );
            $attachment1 = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => sanitize_file_name($filename1),
                'post_content' => '',
                'post_status' => 'inherit'
            );
            $attach_id1 = wp_insert_attachment( $attachment1, $file1, 0);
            $file1_url = wp_get_attachment_url( $attach_id1 );
            $path = get_attached_file( $attach_id1 );
            wp_update_attachment_metadata( $attach_id1, $metadata );
            update_user_meta( $user_id, 'attachment', $attach_id1 );
        }
        
        


    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}