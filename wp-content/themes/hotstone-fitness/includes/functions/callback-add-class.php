<?php 
add_action( 'wp_ajax_admin_class_action', 'admin_class_callback' );
add_action( 'wp_ajax_nopriv_admin_class_action', 'admin_class_callback' );
function admin_class_callback() {
    global $wpdb;
    $class_title_en = $_REQUEST['class_title'];
    $class_title_ar = $_REQUEST['class_title_ar'];
    $dates = $_REQUEST['date'];
    $start_time = $_REQUEST['start_time'];
    $end_time = $_REQUEST['end_time'];
    $membership = $_REQUEST['membership'];
    $no_of_slots = $_REQUEST['no_of_slots'];
    $trainer = $_REQUEST['trainer'];
    $workouts = $_REQUEST['workout'];
    
     foreach($dates as $key => $date):
         if($date != ''):
             $formatted_date =  date("Ymd", strtotime($date));
        $my_post = array(
            'post_title'    => wp_strip_all_tags( $class_title_en ),
            'post_content'  => '',
            'post_status'   => 'publish',
            'post_author'   => 1,
            'post_type' => 'class'
          );
           
        // Insert the post into the database
        $group_id = wp_insert_post( $my_post );
        $next_group_id = get_field('next_class_id','option');
        update_post_meta( $group_id, 'id', 'HSSL_'.sprintf("%03d", $next_group_id) );
        update_field( 'next_class_id', $next_group_id + 1, 'option' );
        update_post_meta( $group_id, 'class_title', sanitize_text_field( $class_title_en ) );
        update_post_meta( $group_id, 'class_title_in_arabic', sanitize_text_field( $class_title_ar ) );
        update_post_meta( $group_id, 'date', sanitize_text_field( $formatted_date ) );
        update_post_meta( $group_id, 'start_time', sanitize_text_field( $start_time[$key] ) );
        update_post_meta( $group_id, 'end_time', sanitize_text_field( $end_time[$key] ) );
        update_post_meta( $group_id, 'membership', sanitize_text_field( $membership ) );
        update_post_meta( $group_id, 'trainer', sanitize_text_field( $trainer ) );
        update_post_meta( $group_id, 'no_of_slots', sanitize_text_field( $no_of_slots ) );
        update_post_meta( $group_id, 'workouts', implode(",",$workouts));
        update_post_meta( $group_id, 'is_disable', 0 );
        endif;
    endforeach;

        
        


    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}