<?php 
add_action( 'wp_ajax_view_customer_notification_action', 'view_customer_notification_callback' );
add_action( 'wp_ajax_nopriv_view_customer_notification_action', 'view_customer_notification_callback' );
function view_customer_notification_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $post_id = $_REQUEST['user_id'];  
    $upload_dir = wp_upload_dir();
    $attachment_id = get_field('attachment',$post_id);
    $attachment_metadata = wp_get_attachment_metadata( $attachment_id );
    $fullsize_path = get_attached_file( $attachment_id );
    ob_start();?>
                   <li>
                      <h5 class="title fw-medium">ID</h5>
                      <span class="text fw-normal"><?= get_field('id',$post_id);?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Subject</h5>
                      <span class="text fw-normal"><?= get_field('subject',$post_id);?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Message (EN)</h5>
                      <span class="text fw-normal"><?= get_field('message',$post_id);?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Message (AR)</h5>
                      <span class="text fw-normal"><?= get_field('message_in_arabic',$post_id);?></span>
                    </li>
                    <li>
                      <h5 class="title fw-medium">Attachment</h5>
                      <span class="text fw-normal"><a href="<?= wp_get_attachment_url($attachment_id);?>" target="_blank"><?= basename( get_attached_file( $attachment_id ) );?></a></span>
                    </li>
    <?php 
    
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content,
    		    );
	
	echo json_encode($result);
	exit(0);
}