<?php 
add_action( 'wp_ajax_admin_subscription_action', 'admin_subscription_callback' );
add_action( 'wp_ajax_nopriv_admin_subscription_action', 'admin_subscription_callback' );
function admin_subscription_callback() {
    global $wpdb;
    $membership = $_REQUEST['membership'];
    $customer = $_REQUEST['customer'];
    $type = $_REQUEST['type'];
    $options = $_REQUEST['options'];
    $start_date = $_REQUEST['start_date'];
    $end_date = $_REQUEST['end_date'];
    $remaining_session = $_REQUEST['remaining_session'];
    $formatted_start_date =  date("Ymd", strtotime($start_date));
    $formatted_end_date =  date("Ymd", strtotime($end_date));

        $my_post = array(
            'post_title'    => get_the_title($membership),
            'post_content'  => '',
            'post_status'   => 'publish',
            'post_author'   => 1,
            'post_type' => 'subscription'
          );
           
        // Insert the post into the database
        $group_id = wp_insert_post( $my_post );
        $next_group_id = get_field('next_subscription_id','option');
        update_post_meta( $group_id, 'id', 'HSC_'.sprintf("%03d", $next_group_id) );
        update_field( 'next_subscription_id', $next_group_id + 1, 'option' );
        update_post_meta( $group_id, 'customer', $customer );
        update_post_meta( $group_id, 'type', $type );
        update_post_meta( $group_id, 'options', $options );
        update_post_meta( $group_id, 'membership_type', $membership );
        update_post_meta( $group_id, 'start_date', $formatted_start_date );
        if($options == 'validity'){
            update_post_meta( $group_id, 'end_date', $formatted_end_date );
            update_post_meta( $group_id, 'remaining_sessions', null );
        }else{
            update_post_meta( $group_id, 'remaining_sessions', sanitize_text_field( $remaining_session ) );
            update_post_meta( $group_id, 'end_date', null );
        }
        
        update_post_meta( $group_id, 'is_disable', 0 );
        

        $image_url1=$_FILES['attachment']['tmp_name'];
        if($image_url1){
            $documenten2=$_FILES['attachment'];
            $upload_dir = wp_upload_dir();
            $image_data1 = file_get_contents($image_url1);
            $filename1 = $documenten2['name'];
            if(wp_mkdir_p($upload_dir['path']))
                $file1 = $upload_dir['path'] . '/' . $filename1;
            else
                $file1 = $upload_dir['basedir'] . '/' . $filename1;
            file_put_contents($file1, $image_data1);
            $wp_filetype = wp_check_filetype($filename1, null );
            $attachment1 = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => sanitize_file_name($filename1),
                'post_content' => '',
                'post_status' => 'inherit'
            );
            $attach_id1 = wp_insert_attachment( $attachment1, $file1, 0);
            $file1_url = wp_get_attachment_url( $attach_id1 );
            $path = get_attached_file( $attach_id1 );
            wp_update_attachment_metadata( $attach_id1, $metadata );
            update_post_meta( $group_id, 'invoice', $attach_id1 );
        }       
        
        


    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> $content
    		    );
	
	echo json_encode($result);
	exit(0);
}