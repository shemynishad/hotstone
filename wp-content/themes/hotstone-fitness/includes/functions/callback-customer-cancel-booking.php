<?php 
add_action( 'wp_ajax_customer_cancel_booking_action', 'customer_cancel_booking_callback' );
add_action( 'wp_ajax_nopriv_customer_cancel_booking_action', 'customer_cancel_booking_callback' );
function customer_cancel_booking_callback() {
    global $wpdb;
    $current_user = wp_get_current_user();
    $id = $_REQUEST['id'];   

    wp_delete_post( $id , true); 


    ob_start();
    $content = ob_get_clean();	
	$result =   Array(
        		    "html"		=> 'success',
    		    );
	
	echo json_encode($result);
	exit(0);
}