********************************************************************************
* DUPLICATOR LITE: Install-Log
* STEP-0 START @ 07:51:24
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
PACKAGE INFO________ ORIGINAL SERVER                        |CURRENT SERVER
OS__________________: WINNT                                 |Linux
PHP VERSION_________: 7.4.27                                |7.4.30
********************************************************************************
CURRENT SERVER INFO
PHP_________________: 7.4.30 | SAPI: fpm-fcgi
PHP MEMORY__________: 4294967296 | SUHOSIN: disabled
ARCHITECTURE________: 64-bit
SERVER______________: Apache
DOC ROOT____________: "/home/hotstone/public_html"
REQUEST URL_________: "https://hotstone.ourdemo.online"
********************************************************************************
OVERWRITE PARAMS
 *** FROM PACKAGE
PARAM SET KEY[blogname]
********************************************************************************
MAINTENANCE MODE DISABLE
INSTALLER INFO

TEMPLATE____________: "base"
VALIDATE ON START___: "normal"
PATH_NEW____________: "/home/hotstone/public_html"
URL_NEW_____________: "https://hotstone.ourdemo.online"
********************************************************************************
ARCHIVE INFO

ARCHIVE NAME________: "/home/hotstone/public_html/20220830_hotstonefitness_[HASH]_20220830194521_archive.zip"
ARCHIVE SIZE________: 29.19MB
CREATED_____________: 2022-08-30 19:45:21
WP VERSION__________: 6.0.1
DUP VERSION_________: 1.5.0
LICENSE_____________: Free version
DB VERSION__________: 10.4.22
DB FILE SIZE________: 816KB
DB TABLES___________: 13
DB ROWS_____________: 389
URL HOME____________: http://localhost/hotstone/hotstone-wp
URL CORE____________: http://localhost/hotstone/hotstone-wp
URL CONTENT_________: http://localhost/hotstone/hotstone-wp/wp-content
URL UPLOAD__________: http://localhost/hotstone/hotstone-wp/wp-content/uploads
URL PLUGINS_________: http://localhost/hotstone/hotstone-wp/wp-content/plugins
URL MU PLUGINS______: http://localhost/hotstone/hotstone-wp/wp-content/mu-plugins
URL THEMES__________: http://localhost/hotstone/hotstone-wp/wp-content/themes
PATH HOME___________: D:/xampp/htdocs/hotstone/hotstone-wp
PATH ABS____________: D:/xampp/htdocs/hotstone/hotstone-wp
PATH WPCONFIG_______: D:/xampp/htdocs/hotstone/hotstone-wp
PATH WPCONTENT______: D:/xampp/htdocs/hotstone/hotstone-wp/wp-content
PATH UPLOADS________: D:/xampp/htdocs/hotstone/hotstone-wp/wp-content/uploads
PATH PLUGINS________: D:/xampp/htdocs/hotstone/hotstone-wp/wp-content/plugins
PATH MUPLUGINS______: D:/xampp/htdocs/hotstone/hotstone-wp/wp-content/mu-plugins
PATH THEMES_________: D:/xampp/htdocs/hotstone/hotstone-wp/wp-content/themes

SUBSITES
SUBSITE [ID:   1] "localhost/hotstone/hotstone-wp/"

PLUGINS
PLUGIN [SLUG:advanced-custom-fields-pro-master/acf.php         ][ON:true ]  Advanced Custom Fields PRO
PLUGIN [SLUG:akismet/akismet.php                               ][ON:false]  Akismet Anti-Spam
PLUGIN [SLUG:custom-post-type-ui/custom-post-type-ui.php       ][ON:false]  Custom Post Type UI
PLUGIN [SLUG:duplicator/duplicator.php                         ][ON:true ]  Duplicator
PLUGIN [SLUG:hello.php                                         ][ON:false]  Hello Dolly
PLUGIN [SLUG:user-role-editor/user-role-editor.php             ][ON:true ]  User Role Editor

********************************************************************************
[PHP ERR][E_NOTICE] MSG:ob_end_clean(): failed to delete buffer. No buffer to delete [CODE:8|FILE:/home/hotstone/public_html/dup-installer/main.installer.php|LINE:84]
LOG-TIME[/home/hotstone/public_html/dup-installer/ctrls/ctrl.base.php:227][DELTA:   0.10430]  MESSAGE:END RENDER PAGE
LOG-TIME[/home/hotstone/public_html/dup-installer/src/Core/Bootstrap.php:72] RESET TIME

-------------------------
AJAX ACTION [sparam_s1] START
AJAX ACTION [sparam_s1] SUCCESS
-------------------------

LOG-TIME[/home/hotstone/public_html/dup-installer/src/Core/Bootstrap.php:72] RESET TIME
STEP ACTION: "on-validate"
[PHP ERR][E_NOTICE] MSG:ob_end_clean(): failed to delete buffer. No buffer to delete [CODE:8|FILE:/home/hotstone/public_html/dup-installer/main.installer.php|LINE:84]
LOG-TIME[/home/hotstone/public_html/dup-installer/ctrls/ctrl.base.php:227][DELTA:   0.11038]  MESSAGE:END RENDER PAGE
LOG-TIME[/home/hotstone/public_html/dup-installer/src/Core/Bootstrap.php:72] RESET TIME

-------------------------
AJAX ACTION [validate] START
START TEST "Archive Check" [CLASS: DUPX_Validation_test_archive_check]
LOG-TIME[DELTA:   0.00002]  MESSAGE:TEST "Archive Check" RESULT: passed

START TEST "Duplicator importer version" [CLASS: DUPX_Validation_test_importer_version]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "Duplicator importer version" RESULT: skip

START TEST "Overwrite Install" [CLASS: DUPX_Validation_test_owrinstall]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "Overwrite Install" RESULT: skip

START TEST "Recovery Point" [CLASS: DUPX_Validation_test_recovery]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "Recovery Point" RESULT: skip

START TEST "Package is Importable" [CLASS: DUPX_Validation_test_importable]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "Package is Importable" RESULT: skip

START TEST "REST API test" [CLASS: DUPX_Validation_test_rest_api]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "REST API test" RESULT: skip

START TEST "Manual extraction detected" [CLASS: DUPX_Validation_test_manual_extraction]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "Manual extraction detected" RESULT: good

START TEST "Database Only" [CLASS: DUPX_Validation_test_dbonly_iswordpress]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "Database Only" RESULT: skip

START TEST "Package Age" [CLASS: DUPX_Validation_test_package_age]
LOG-TIME[DELTA:   0.00003]  MESSAGE:TEST "Package Age" RESULT: good

START TEST "Replace PATHs in database" [CLASS: DUPX_Validation_test_replace_paths]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "Replace PATHs in database" RESULT: skip

START TEST "Managed hosting supported" [CLASS: DUPX_Validation_test_managed_supported]
LOG-TIME[DELTA:   0.00002]  MESSAGE:TEST "Managed hosting supported" RESULT: skip

START TEST "Siteground" [CLASS: DUPX_Validation_test_siteground]
LOG-TIME[DELTA:   0.00010]  MESSAGE:TEST "Siteground" RESULT: skip

START TEST "Addon Sites" [CLASS: DUPX_Validation_test_addon_sites]
--------------------------------------
PATHS MAPPING : "/home/hotstone/public_html"
--------------------------------------
LOG-TIME[DELTA:   0.01475]  MESSAGE:TEST "Addon Sites" RESULT: good

START TEST "Wordfence" [CLASS: DUPX_Validation_test_wordfence]
LOG-TIME[DELTA:   0.00008]  MESSAGE:TEST "Wordfence" RESULT: good

START TEST "Table prefix of managed hosting" [CLASS: DUPX_Validation_test_managed_tprefix]
LOG-TIME[DELTA:   0.00002]  MESSAGE:TEST "Table prefix of managed hosting" RESULT: skip

START TEST "PHP Version Mismatch" [CLASS: DUPX_Validation_test_php_version]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "PHP Version Mismatch" RESULT: good

START TEST "PHP Open Base" [CLASS: DUPX_Validation_test_open_basedir]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "PHP Open Base" RESULT: good

START TEST "PHP Memory Limit" [CLASS: DUPX_Validation_test_memory_limit]
LOG-TIME[DELTA:   0.00002]  MESSAGE:TEST "PHP Memory Limit" RESULT: good

START TEST "PHP Extensions" [CLASS: DUPX_Validation_test_extensions]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "PHP Extensions" RESULT: good

START TEST "PHP Mysqli" [CLASS: DUPX_Validation_test_mysql_connect]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "PHP Mysqli" RESULT: passed

START TEST "PHP Tokenizer" [CLASS: DUPX_Validation_test_tokenizer]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "PHP Tokenizer" RESULT: passed

START TEST "PHP Timeout" [CLASS: DUPX_Validation_test_timeout]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "PHP Timeout" RESULT: good

START TEST "Disk Space" [CLASS: DUPX_Validation_test_disk_space]
LOG-TIME[DELTA:   0.00271]  MESSAGE:TEST "Disk Space" RESULT: good

START TEST "Permissions: General" [CLASS: DUPX_Validation_test_iswritable]
LOG-TIME[DELTA:   0.00177]  MESSAGE:TEST "Permissions: General" RESULT: passed

START TEST "Permissions: Configs Files " [CLASS: DUPX_Validation_test_iswritable_configs]
LOG-TIME[DELTA:   0.00005]  MESSAGE:TEST "Permissions: Configs Files " RESULT: passed

START TEST "Host Name" [CLASS: DUPX_Validation_test_db_host_name]
LOG-TIME[DELTA:   0.00008]  MESSAGE:TEST "Host Name" RESULT: passed

START TEST "Host Connection" [CLASS: DUPX_Validation_test_db_connection]
LOG-TIME[DELTA:   0.00170]  MESSAGE:TEST "Host Connection" RESULT: passed

START TEST "Database Version" [CLASS: DUPX_Validation_test_db_version]
LOG-TIME[DELTA:   0.00187]  MESSAGE:TEST "Database Version" RESULT: soft warning

START TEST "Create New Database" [CLASS: DUPX_Validation_test_db_create]
LOG-TIME[DELTA:   0.00002]  MESSAGE:TEST "Create New Database" RESULT: skip

START TEST "Database Engine Support" [CLASS: DUPX_Validation_test_db_supported_engine]
LOG-TIME[DELTA:   0.00011]  MESSAGE:TEST "Database Engine Support" RESULT: passed

START TEST "Database GTID Mode" [CLASS: DUPX_Validation_test_db_gtid_mode]
LOG-TIME[DELTA:   0.00006]  MESSAGE:TEST "Database GTID Mode" RESULT: passed

START TEST "Privileges: User Visibility" [CLASS: DUPX_Validation_test_db_visibility]
LOG-TIME[DELTA:   0.00005]  MESSAGE:TEST "Privileges: User Visibility" RESULT: passed

START TEST "Manual Table Check" [CLASS: DUPX_Validation_test_db_manual_tabels_count]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "Manual Table Check" RESULT: skip

START TEST "Multiple WordPress Installs" [CLASS: DUPX_Validation_test_db_multiple_wp_installs]
LOG-TIME[DELTA:   0.00021]  MESSAGE:TEST "Multiple WordPress Installs" RESULT: passed

START TEST "Privileges: User Resources" [CLASS: DUPX_Validation_test_db_user_resources]
DB QUERY [ERROR][/home/hotstone/public_html/dup-installer/classes/database/class.db.php:631] MSG: SELECT command denied to user 'hotstone_wp'@'localhost' for table 'user'
	SQL: SELECT max_questions, max_updates, max_connections FROM mysql.user WHERE user = 'hotstone_wp' AND host = 'localhost'
TRACE[1] CLASS___: DUPX_DB::mysqli_query      FILE: /home/hotstone/public_html/dup-installer/classes/validation/class.validation.database.service.php[743]
TRACE[2] CLASS___: DUPX_Validation_database_service->getUserResources FILE: /home/hotstone/public_html/dup-installer/classes/validation/database-tests/class.validation.test.db.user.resources.php[28]
TRACE[3] CLASS___: DUPX_Validation_test_db_user_resources->runTest FILE: /home/hotstone/public_html/dup-installer/classes/validation/class.validation.abstract.item.php[45]
TRACE[4] CLASS___: DUPX_Validation_abstract_item->test FILE: /home/hotstone/public_html/dup-installer/classes/validation/class.validation.manager.php[229]
TRACE[5] CLASS___: DUPX_Validation_manager->runTests FILE: /home/hotstone/public_html/dup-installer/classes/validation/class.validation.manager.php[202]
TRACE[6] CLASS___: DUPX_Validation_manager->getValidateData FILE: /home/hotstone/public_html/dup-installer/ctrls/classes/class.ctrl.ajax.php[167]
TRACE[7] CLASS___: DUPX_Ctrl_ajax::actions    FILE: /home/hotstone/public_html/dup-installer/ctrls/classes/class.ctrl.ajax.php[92]
TRACE[8] CLASS___: DUPX_Ctrl_ajax::controller FILE: /home/hotstone/public_html/dup-installer/main.installer.php[56]

LOG-TIME[DELTA:   0.00010]  MESSAGE:TEST "Privileges: User Resources" RESULT: passed

START TEST "Privileges: User Table Access" [CLASS: DUPX_Validation_test_db_user_perms]
LOG-TIME[DELTA:   0.07398]  MESSAGE:TEST "Privileges: User Table Access" RESULT: passed

START TEST "Privileges: 'Show Variables' Query" [CLASS: DUPX_Validation_test_db_custom_queries]
LOG-TIME[DELTA:   0.00096]  MESSAGE:TEST "Privileges: 'Show Variables' Query" RESULT: passed

START TEST "Source Database Triggers" [CLASS: DUPX_Validation_test_db_triggers]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "Source Database Triggers" RESULT: passed

START TEST "Character Set and Collation Support" [CLASS: DUPX_Validation_test_db_supported_default_charset]
LOG-TIME[DELTA:   0.00065]  MESSAGE:TEST "Character Set and Collation Support" RESULT: passed

START TEST "Character Set and  Collation Capability" [CLASS: DUPX_Validation_test_db_supported_charset]
LOG-TIME[DELTA:   0.00006]  MESSAGE:TEST "Character Set and  Collation Capability" RESULT: passed

START TEST "Tables Case Sensitivity" [CLASS: DUPX_Validation_test_db_case_sensitive_tables]
LOG-TIME[DELTA:   0.00074]  MESSAGE:TEST "Tables Case Sensitivity" RESULT: passed

START TEST "Tables Flagged for Removal or Backup" [CLASS: DUPX_Validation_test_db_affected_tables]
LOG-TIME[DELTA:   0.00021]  MESSAGE:TEST "Tables Flagged for Removal or Backup" RESULT: passed

START TEST "Prefix too long" [CLASS: DUPX_Validation_test_db_prefix_too_long]
LOG-TIME[DELTA:   0.00005]  MESSAGE:TEST "Prefix too long" RESULT: passed

START TEST "Database cleanup" [CLASS: DUPX_Validation_test_db_cleanup]
LOG-TIME[DELTA:   0.00001]  MESSAGE:TEST "Database cleanup" RESULT: skip


CTRL PARAMS AFTER VALIDATION
AJAX ACTION [validate] SUCCESS
-------------------------

LOG-TIME[/home/hotstone/public_html/dup-installer/src/Core/Bootstrap.php:72] RESET TIME

-------------------------
AJAX ACTION [proceed_confirm_dialog] START
AJAX ACTION [proceed_confirm_dialog] SUCCESS
-------------------------

LOG-TIME[/home/hotstone/public_html/dup-installer/src/Core/Bootstrap.php:72] RESET TIME

-------------------------
AJAX ACTION [sparam_s1] START
AJAX ACTION [sparam_s1] SUCCESS
-------------------------

LOG-TIME[/home/hotstone/public_html/dup-installer/src/Core/Bootstrap.php:72] RESET TIME

-------------------------
AJAX ACTION [extract] START
INITIALIZE FILTERS
--------------------------------------
PATHS MAPPING : "/home/hotstone/public_html"
--------------------------------------
********************************************************************************
* DUPLICATOR LITE: Install-Log
* STEP-1 START @ 07:52:10
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
USER INPUTS
INSTALL TYPE________: single site
BLOG NAME___________: "Hotstone Fitness"
HOME URL NEW________: "https://hotstone.ourdemo.online"
SITE URL NEW________: "https://hotstone.ourdemo.online/"
CONTENT URL NEW_____: "https://hotstone.ourdemo.online/wp-content"
UPLOAD URL NEW______: "https://hotstone.ourdemo.online/wp-content/uploads"
PLUGINS URL NEW_____: "https://hotstone.ourdemo.online/wp-content/plugins"
MUPLUGINS URL NEW___: "https://hotstone.ourdemo.online/wp-content/mu-plugins"
HOME PATH NEW_______: "/home/hotstone/public_html"
SITE PATH NEW_______: "/home/hotstone/public_html/"
CONTENT PATH NEW____: "/home/hotstone/public_html/wp-content"
UPLOAD PATH NEW_____: "/home/hotstone/public_html/wp-content/uploads"
PLUGINS PATH NEW____: "/home/hotstone/public_html/wp-content/plugins"
MUPLUGINS PATH NEW__: "/home/hotstone/public_html/wp-content/mu-plugins"
ARCHIVE ACTION______: "donothing"
SKIP WP FILES_______: "none"
ARCHIVE ENGINE______: "ziparchivechunking"
SET DIR PERMS_______: true
DIR PERMS VALUE_____: 0755
SET FILE PERMS______: true
FILE PERMS VALUE____: 0644
SAFE MODE___________: 0
LOGGING_____________: 1
ZIP THROTTLING______: false
WP CONFIG___________: "modify"
HTACCESS CONFIG_____: "new"
OTHER CONFIG________: "new"
FILE TIME___________: "current"
********************************************************************************

REMOVE FILTERS
	DIR : "/home/hotstone/public_html/dup-installer"
	FILE: "/home/hotstone/public_html/20220830_hotstonefitness_[HASH]_20220830194521_installer-backup.php"
	FILE: "/home/hotstone/public_html/20220830_hotstonefitness_[HASH]_20220830194521_archive.zip"
	FILE: "/home/hotstone/public_html/installer.php"
	FILE: "/home/hotstone/public_html/dup-installer-bootlog__27dfb41-30194521.txt"
EXTRACTION FILTERS
	DIR : "dup-installer"
	FILE: "20220830_hotstonefitness_[HASH]_20220830194521_installer-backup.php"
--------------------------------------


EXTRACTION: ZIP CHUNKING >>> START
MAINTENANCE MODE ENABLE
BEFORE EXTRACION ACTIONS

*** RESET CONFIG FILES IN CURRENT HOSTING >>> START
RESET CONFIG FILES: I'M GOING TO MOVE CONFIG FILE ".htaccess" IN ORIGINAL FOLDER
	CONFIG FILE HAS BEEN RESET

*** RESET CONFIG FILES IN CURRENT HOSTING >>> END
MAINTENANCE MODE ENABLE

*** CREATE FOLDER AND PERMISSION PREPARE
FOLDER PREPARE DONE
ARCHIVE OFFSET 0
FILE EXTRACTION: done processing last file in list of 5247

EXTRACTION: ZIP CHUNKING >>> DONE

EXTRACTION COMPLETE @ 07:52:10 - RUNTIME: 0.6642 sec. - Files processed: 4,648 of 4,648
AJAX ACTION [extract] SUCCESS
-------------------------

LOG-TIME[/home/hotstone/public_html/dup-installer/src/Core/Bootstrap.php:72] RESET TIME

-------------------------
AJAX ACTION [dbinstall] START
ADD PREFIX META MAP ID 0 wp_



********************************************************************************
* DUPLICATOR LITE: INSTALL-LOG
* STEP-2 START @ 07:52:11
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
USER INPUTS
DB ENGINE___________: "chunk"
VIEW MODE___________: "basic"
DB ACTION___________: "empty"
DB HOST_____________: "**OBSCURED**"
DB NAME_____________: "**OBSCURED**"
DB PASS_____________: "**OBSCURED**"
DB PORT_____________: "**OBSCURED**"
USER MODE___________: "overwrite"
TABLE PREFIX________: "wp_"
MYSQL MODE__________: "DEFAULT"
MYSQL MODE OPTS_____: ""
CHARSET_____________: "utf8mb4"
COLLATE_____________: ""
CUNKING_____________: true
VIEW CREATION_______: true
STORED PROCEDURE____: true
FUNCTIONS___________: true
REMOVE DEFINER______: false
SPLIT CREATES_______: true
--------------------------------------
TABLES
--------------------------------------
TABLE "wp_commentmeta"__________________________________[ROWS:       0] [EXTRACT|REPLACE] [INST NAME: wp_commentmeta]
TABLE "wp_comments"_____________________________________[ROWS:       1] [EXTRACT|REPLACE] [INST NAME: wp_comments]
TABLE "wp_duplicator_packages"__________________________[ROWS:       1] [EXTRACT|REPLACE] [INST NAME: wp_duplicator_packages]
TABLE "wp_links"________________________________________[ROWS:       0] [EXTRACT|REPLACE] [INST NAME: wp_links]
TABLE "wp_options"______________________________________[ROWS:     153] [EXTRACT|REPLACE] [INST NAME: wp_options]
TABLE "wp_postmeta"_____________________________________[ROWS:      29] [EXTRACT|REPLACE] [INST NAME: wp_postmeta]
TABLE "wp_posts"________________________________________[ROWS:      76] [EXTRACT|REPLACE] [INST NAME: wp_posts]
TABLE "wp_termmeta"_____________________________________[ROWS:       0] [EXTRACT|REPLACE] [INST NAME: wp_termmeta]
TABLE "wp_terms"________________________________________[ROWS:       2] [EXTRACT|REPLACE] [INST NAME: wp_terms]
TABLE "wp_term_relationships"___________________________[ROWS:       4] [EXTRACT|REPLACE] [INST NAME: wp_term_relationships]
TABLE "wp_term_taxonomy"________________________________[ROWS:       2] [EXTRACT|REPLACE] [INST NAME: wp_term_taxonomy]
TABLE "wp_usermeta"_____________________________________[ROWS:     120] [EXTRACT|REPLACE] [INST NAME: wp_usermeta]
TABLE "wp_users"________________________________________[ROWS:       4] [EXTRACT|REPLACE] [INST NAME: wp_users]
********************************************************************************

--------------------------------------
DATABASE-ENVIRONMENT
--------------------------------------
MYSQL VERSION:	This Server: 5.7.39 -- Build Server: 10.4.22
FILE SIZE:	dup-database__[HASH].sql (130.9KB)
TIMEOUT:	5000
MAXPACK:	268435456
SQLMODE-GLOBAL:	NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
SQLMODE-SESSION:NO_AUTO_VALUE_ON_ZERO
DROP ALL TABLES
--------------------------------------
DATABASE RESULTS
--------------------------------------
QUERY FIXES
GLOBAL RULES ADDED: PROC AND VIEWS

QUERY FIXES GLOBAL RULES
	SEARCH  => /^(\s*(?:\/\*!\d+\s)?\s*(?:CREATE.+)?DEFINER\s*=)([^\*\s]+)(.*)$/m
	REPLACE => $1`hotstone_wp`@`localhost`$3

	SEARCH  => /^(\s*CREATE.+(?:PROCEDURE|FUNCTION)[\s\S]*)(BEGIN)([\s\S]*)$/
	REPLACE => $1SQL SECURITY INVOKER
$2$3

--------------------------------------
** DATABASE CHUNK install start
--------------------------------------
DATABASE CHUNK SEEK POSITION: 0
Auto Commit set to false successfully
NO TABLE TO SKIP
Auto Commit set to true successfully
--------------------------------------
** DATABASE CHUNK install end
--------------------------------------
ERRORS FOUND:	0
DROPPED TABLES:	0
RENAMED TABLES:	0
QUERIES RAN:	125

TABLES ROWS IN DATABASE AFTER EXTRACTION

TABLE "wp_commentmeta"__________________________________[ROWS:     0]
TABLE "wp_comments"_____________________________________[ROWS:     1]
TABLE "wp_duplicator_packages"__________________________[ROWS:     1]
TABLE "wp_links"________________________________________[ROWS:     0]
TABLE "wp_options"______________________________________[ROWS:   153]
TABLE "wp_postmeta"_____________________________________[ROWS:    29]
TABLE "wp_posts"________________________________________[ROWS:    76]
TABLE "wp_term_relationships"___________________________[ROWS:     4]
TABLE "wp_term_taxonomy"________________________________[ROWS:     2]
TABLE "wp_termmeta"_____________________________________[ROWS:     0]
TABLE "wp_terms"________________________________________[ROWS:     2]
TABLE "wp_usermeta"_____________________________________[ROWS:   120]
TABLE "wp_users"________________________________________[ROWS:     4]

INSERT DATA RUNTIME: 4.5975 sec.
STEP-2 COMPLETE @ 07:52:15 - RUNTIME: 4.5975 sec.
AJAX ACTION [dbinstall] SUCCESS
-------------------------

LOG-TIME[/home/hotstone/public_html/dup-installer/src/Core/Bootstrap.php:72] RESET TIME

-------------------------
AJAX ACTION [webupdate] START

====================================
SET SEARCH AND REPLACE LIST INSTALL TYPE single site
====================================
SEARCH ITEM[T:path |P:12] SEARCH: D:/xampp/htdocs/hotstone/hotstone-wp REPLACE: /home/hotstone/public_html/ [SCOPE: ALL]
SEARCH ITEM[T:path |P:12] SEARCH: D:\xampp\htdocs\hotstone\hotstone-wp REPLACE: /home/hotstone/public_html/ [SCOPE: ALL]
SEARCH ITEM[T:path |P:12] SEARCH: D:/xampp/htdocs/hotstone/hotstone-wp REPLACE: /home/hotstone/public_html [SCOPE: ALL]
SEARCH ITEM[T:urlnd|P:12] SEARCH: http://localhost/hotstone/hotstone-wp REPLACE: https://hotstone.ourdemo.online/ [SCOPE: ALL]
SEARCH ITEM[T:urlnd|P:12] SEARCH: http://localhost/hotstone/hotstone-wp REPLACE: https://hotstone.ourdemo.online [SCOPE: ALL]
CHUNK LOAD DATA: IS NULL 
CHUNK ACTION: CURRENT [start][][]


********************************************************************************
DUPLICATOR LITE: INSTALL-LOG
STEP-3 START @ 07:52:16
NOTICE: Do NOT post to public sites or forums
********************************************************************************
CHARSET SERVER:	"utf8mb4"
CHARSET CLIENT:	"utf8mb4"
********************************************************************************
OPTIONS:
DISALLOW_FILE_EDIT____: [value = ], [inWpConfig = ]
DISALLOW_FILE_MODS____: [value = ], [inWpConfig = ]
AUTOSAVE_INTERVAL_____: [value = 60], [inWpConfig = ]
WP_POST_REVISIONS_____: [value = 1], [inWpConfig = ]
FORCE_SSL_ADMIN_______: [value = ], [inWpConfig = ]
WP_AUTO_UPDATE_CORE___: [value = false], [inWpConfig = ]
WP_CACHE______________: [value = ], [inWpConfig = ]
WPCACHEHOME___________: [value = /], [inWpConfig = ]
WP_DEBUG______________: [value = ], [inWpConfig = 1]
WP_DEBUG_LOG__________: [value = ], [inWpConfig = ]
WP_DEBUG_DISPLAY______: [value = 1], [inWpConfig = ]
WP_DISABLE_FATAL_ERROR_HANDLER: [value = ], [inWpConfig = ]
SCRIPT_DEBUG__________: [value = ], [inWpConfig = ]
CONCATENATE_SCRIPTS___: [value = ], [inWpConfig = ]
SAVEQUERIES___________: [value = ], [inWpConfig = ]
ALTERNATE_WP_CRON_____: [value = ], [inWpConfig = ]
DISABLE_WP_CRON_______: [value = ], [inWpConfig = ]
WP_CRON_LOCK_TIMEOUT__: [value = 60], [inWpConfig = ]
COOKIE_DOMAIN_________: [value = ], [inWpConfig = ]
WP_MEMORY_LIMIT_______: [value = 40M], [inWpConfig = ]
WP_MAX_MEMORY_LIMIT___: [value = 512M], [inWpConfig = ]
WP_TEMP_DIR___________: [value = ], [inWpConfig = ]
********************************************************************************

********************************************************************************
CHUNK PARAMS:
maxIteration__________: 0
timeOut_______________: 5000
throttling____________: 2
rowsPerPage___________: 1000
********************************************************************************

CHUNK ACTION: CURRENT [cleanup_trans][][]
CLEAN OPTIONS [wp_options]
	`option_name` LIKE "\_transient%"
	`option_name` LIKE "\_site\_transient%"
	`option_name` IN ("duplicator_ui_view_state","duplicator_package_active","duplicator_settings","duplicator_is_pro_enable_notice_dismissed")
DATABASE OPTIONS DELETED [ROWS:    16]
CHUNK ACTION: CURRENT [cleanup_extra][][]
CLEANUP EXTRA
	- SKIP DROP VIEWS
	- SKIP DROP PROCS
	- SKIP DROP FUNCS
CHUNK ACTION: CURRENT [cleanup_packages][][]
EMPTY PACKAGES TABLE
CLEAN PACKAGES
DATABASE PACKAGE DELETED [ROWS:     1]
CHUNK ACTION: CURRENT [init][][]

EVALUATE TABLE: "wp_commentmeta"__________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_comments"_____________________________________[ROWS:     1][PG:   1][SCAN:text columns]
	--- BASE STRINGS ---
	SEARCH[path ]  1:"D:/xampp/htdocs/hotstone/hotstone-wp" ============> "/home/hotstone/public_html"
	SEARCH[urlnd]  2:"http://localhost/hotstone/hotstone-wp" ===========> "https://hotstone.ourdemo.online"
	CHUNK ACTION: CURRENT [search_replace][wp_comments][0]

EVALUATE TABLE: "wp_duplicator_packages"__________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_links"________________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_options"______________________________________[ROWS:   137][PG:   1][SCAN:text columns]
	--- BASE STRINGS ---
	SEARCH[path ]  1:"D:/xampp/htdocs/hotstone/hotstone-wp" ============> "/home/hotstone/public_html"
	SEARCH[urlnd]  2:"http://localhost/hotstone/hotstone-wp" ===========> "https://hotstone.ourdemo.online"
	CHUNK ACTION: CURRENT [search_replace][wp_options][0]

EVALUATE TABLE: "wp_postmeta"_____________________________________[ROWS:    29][PG:   1][SCAN:text columns]
	--- BASE STRINGS ---
	SEARCH[path ]  1:"D:/xampp/htdocs/hotstone/hotstone-wp" ============> "/home/hotstone/public_html"
	SEARCH[urlnd]  2:"http://localhost/hotstone/hotstone-wp" ===========> "https://hotstone.ourdemo.online"
	CHUNK ACTION: CURRENT [search_replace][wp_postmeta][0]

EVALUATE TABLE: "wp_posts"________________________________________[ROWS:    76][PG:   1][SCAN:text columns]
	--- BASE STRINGS ---
	SEARCH[path ]  1:"D:/xampp/htdocs/hotstone/hotstone-wp" ============> "/home/hotstone/public_html"
	SEARCH[urlnd]  2:"http://localhost/hotstone/hotstone-wp" ===========> "https://hotstone.ourdemo.online"
	CHUNK ACTION: CURRENT [search_replace][wp_posts][0]

EVALUATE TABLE: "wp_termmeta"_____________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_terms"________________________________________[ROWS:     2][PG:   1][SCAN:text columns]
	--- BASE STRINGS ---
	SEARCH[path ]  1:"D:/xampp/htdocs/hotstone/hotstone-wp" ============> "/home/hotstone/public_html"
	SEARCH[urlnd]  2:"http://localhost/hotstone/hotstone-wp" ===========> "https://hotstone.ourdemo.online"
	CHUNK ACTION: CURRENT [search_replace][wp_terms][0]

EVALUATE TABLE: "wp_term_relationships"___________________________[ROWS:     4][PG:   1][SCAN:no columns  ]

EVALUATE TABLE: "wp_term_taxonomy"________________________________[ROWS:     2][PG:   1][SCAN:text columns]
	--- BASE STRINGS ---
	SEARCH[path ]  1:"D:/xampp/htdocs/hotstone/hotstone-wp" ============> "/home/hotstone/public_html"
	SEARCH[urlnd]  2:"http://localhost/hotstone/hotstone-wp" ===========> "https://hotstone.ourdemo.online"
	CHUNK ACTION: CURRENT [search_replace][wp_term_taxonomy][0]

EVALUATE TABLE: "wp_usermeta"_____________________________________[ROWS:   120][PG:   1][SCAN:text columns]
	--- BASE STRINGS ---
	SEARCH[path ]  1:"D:/xampp/htdocs/hotstone/hotstone-wp" ============> "/home/hotstone/public_html"
	SEARCH[urlnd]  2:"http://localhost/hotstone/hotstone-wp" ===========> "https://hotstone.ourdemo.online"
	CHUNK ACTION: CURRENT [search_replace][wp_usermeta][0]

EVALUATE TABLE: "wp_users"________________________________________[ROWS:     4][PG:   1][SCAN:text columns]
	--- BASE STRINGS ---
	SEARCH[path ]  1:"D:/xampp/htdocs/hotstone/hotstone-wp" ============> "/home/hotstone/public_html"
	SEARCH[urlnd]  2:"http://localhost/hotstone/hotstone-wp" ===========> "https://hotstone.ourdemo.online"
	CHUNK ACTION: CURRENT [search_replace][wp_users][0]
--------------------------------------
SCANNED:	Tables:13 	|	 Rows:371 	|	 Cells:2967 
UPDATED:	Tables:3 	|	 Rows:79 	|	 Cells:80 
ERRORS:		0 
RUNTIME:	2.072300 sec
CHUNK ACTION: CURRENT [rem_maintenance][][]

====================================
REMOVE MAINTENANCE MODE
====================================
MAINTENANCE MODE DISABLE
CHUNK ACTION: CURRENT [config_update][][]
SET CONFIG FILES
Retained original entry wpconfig target:/home/hotstone/public_html/wp-config.php
New htaccess file created:/home/hotstone/public_html/.htaccess

====================================
CONFIGURATION FILE UPDATES
====================================
	UPDATE ABSPATH "dirname(__FILE__) . '/'"
	UPDATE WP_SITEURL "https://hotstone.ourdemo.online/"
	UPDATE DB_NAME ""hotstone_wp""
	UPDATE DB_USER "** OBSCURED **"
	UPDATE DB_PASSWORD "** OBSCURED **"
	UPDATE DB_HOST ""localhost""
	UPDATE DB_CHARSET "utf8mb4"
	UPDATE DB_COLLATE ""
	WP CONFIG UPDATE WP_DEBUG "false"
	
*** UPDATED WP CONFIG FILE ***

====================================
HTACCESS UPDATE
====================================

WEB SERVER CONFIGURATION FILE UPDATED:
- Preparing .htaccess file with basic setup.
HTACCESS FILE - Successfully updated the .htaccess file setting.

====================================
INDEX.PHP UPDATE
====================================
INDEX.PHP updated with new blog header "dirname(__FILE__) . '/wp-blog-header.php'"

CHUNK ACTION: CURRENT [gen_update][][]

====================================
GENERAL UPDATES
====================================

====================================
MANAGE PLUGINS
====================================
Activated plugins listed here will be deactivated: Array
(
)

CHUNK ACTION: CURRENT [gen_clean][][]

====================================
GENERAL CLEANUP
====================================
RESET ALL USERS SESSION TOKENS
MIGRATION INFO SET
CHUNK ACTION: CURRENT [create_admin][][]

====================================
RESET USERS PASSWORD
====================================
CHUNK ACTION: CURRENT [notice_test][][]

====================================
CHECK FOR INDEX.HTML
====================================
NO INDEX.HTML WAS FOUND

====================================
NOTICES TEST
====================================
No General Notices Found

CHUNK ACTION: CURRENT [cleanup_tmp_files][][]

====================================
CLEANUP TMP FILES
====================================
CHUNK ACTION: CURRENT [set_files_perms][][]

====================================
SET PARAMS PERMISSION
====================================

*** SET FOLDER PERMISSION AFTER EXTRACTION
--------------------------------------
PATHS MAPPING : "/home/hotstone/public_html"
--------------------------------------
SET FOLDER PERMISSION DONE
CHUNK ACTION: CURRENT [final_report][][]

====================================
FINAL REPORT NOTICES
====================================

STEP-3 COMPLETE @ 07:52:18 - RUNTIME: 2.5148 sec. 


AJAX ACTION [webupdate] SUCCESS
-------------------------

LOG-TIME[/home/hotstone/public_html/dup-installer/src/Core/Bootstrap.php:72] RESET TIME

-------------------------
AJAX ACTION [finalpre] START
AJAX ACTION [finalpre] SUCCESS
-------------------------

LOG-TIME[/home/hotstone/public_html/dup-installer/src/Core/Bootstrap.php:72] RESET TIME

-------------------------
AJAX ACTION [finalafter] START
AJAX ACTION [finalafter] SUCCESS
-------------------------

LOG-TIME[/home/hotstone/public_html/dup-installer/src/Core/Bootstrap.php:72] RESET TIME
[PHP ERR][E_NOTICE] MSG:ob_end_clean(): failed to delete buffer. No buffer to delete [CODE:8|FILE:/home/hotstone/public_html/dup-installer/main.installer.php|LINE:84]

====================================
FINAL REPORT NOTICES LIST
====================================
====================================
LOG-TIME[/home/hotstone/public_html/dup-installer/ctrls/ctrl.base.php:227][DELTA:   0.01553]  MESSAGE:END RENDER PAGE
